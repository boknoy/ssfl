<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblInstructorInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_instructor_infos', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->unsignedBigInteger('instructor_id');
            $table->integer('contact');
            $table->string('degree_status');
            $table->string('program_graduated');
            $table->string('employee_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_instructor_infos');
    }
}
