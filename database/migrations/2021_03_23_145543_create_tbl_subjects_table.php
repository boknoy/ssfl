<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_subjects', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->unsignedBigInteger('academic_id');
            $table->string('code');
            $table->string('subject');
            $table->string('descriptive_title');
            $table->string('lec');
            $table->string('lab');
            $table->string('units');
            $table->string('pre_requisites');
            $table->integer('hours');
            $table->boolean('is_remove');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_subjects');
    }
}
