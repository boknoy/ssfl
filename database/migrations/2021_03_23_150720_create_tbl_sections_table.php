<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sections', function (Blueprint $table) {
            $table->BigIncrements('id');
            $table->unsignedBigInteger('academic)id');
            $table->string('section_name');
            $table->string('year_level');
            $table->integer('is_active');
            $table->integer('is_remove');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sections');
    }
}
