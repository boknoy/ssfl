<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepAssignedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dep_assigneds', function (Blueprint $table) {
            $table->id();
            $table->unsignedBiginteger('dep_id');
            $table->unsignedBiginteger('class_id');
            $table->unsignedBiginteger('subby');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dep_assigneds');
    }
}
