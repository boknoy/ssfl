<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\tbl_academic;
use App\tbl_curriculum;
use App\tbl_subject;
use App\tbl_schedule;
use App\tbl_section;
use App\tbl_room;
use App\temp_year_level;
use App\tbl_class;
use Illuminate\Validation\Rule;
class QuamAcademicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $academic=tbl_academic::all()->where('is_remove',false)->sortByDesc('created_at');
        $archive=tbl_academic::all()->where('is_remove',1);
        $section=tbl_section::all();
        $room=tbl_room::all();
        $subject=tbl_subject::all();
         return view('Quam.academic')
         ->with('academic',$academic)
         ->with('section',$section)
         ->with('room',$room)
         ->with('archive',$archive);
    }

    public function getCurriculum2(Request $request){

        $curriculum = tbl_curriculum::where([

                "school_year"=>$request->input('school_year'),
                "semester" =>$request->input('semester'),
                "academic_id" => $request->input('academic_id'),
                "year_level" => $request->input('year_level'),

        ])->get();
        $sub="";
            if (count($curriculum)>0) {
                foreach ($curriculum as $value) {
                     $sub=tbl_subject::all()->where('curriculum_id',$value->id);
                     foreach ($sub as $value) {
                         
                     }
                }
               
            }


            $data=[

                "curriculum" => $curriculum,
                "subject"  => $sub,

            ];
       return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function getInfo($id)
    {
        $acad=tbl_academic::find($id);

        return response()->json($acad);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getDepartment($id)
    {
        $acad=tbl_academic::find($id);
        $data=['acad'=>$acad];
        return response()->json($data);
    }

    public function dataRestore($id)
    {
     $res = tbl_academic::where('id',$id)->update(['is_remove' =>false]);
     $res=tbl_academic::find($id);

      return response()->json($res);

    }
    public function store(Request $request)
    {   

        

        $code=$request->input('code');
        $title=$request->input('title');
        $college=$request->input('college');


         $messages = [
            'code.required' => 'Code is required*',
            'title.required' => 'Title is required*',
            'code.unique' =>'This Code is already exist'
          ];
          $this->validate($request,[
             'title' =>'required',
             'code' => 'required|unique:tbl_academics,program_code',

            ],$messages);







        $academic = new tbl_academic;
        $academic->program_code=strtoupper($code);
        $academic->program_name=strtoupper($title);
        $academic->college=strtoupper($college);
        $academic->save();
        return response()->json($academic);


        
    }

    public function getCurriculum(Request $request){

            $school_year=$request->input('school_year');
             $semester=$request->input('semester');
              $curriculum_year=$request->input('curriculum_year');
               $year_level=$request->input('year_level');
                $academic_id=$request->input('academic_id');

               $curriculum=tbl_curriculum::where([
                'curriculum_year'=>$curriculum_year,
                'semester'=>$semester,
                'year_level'=>$year_level,
                'school_year' => $school_year,
                'academic_id' => $academic_id
                    ])->get();
               $s="";
               $c="";
             if (count($curriculum) > 0) {
                   foreach ($curriculum as $key ) {
                       $s=tbl_subject::all()->where('curriculum_id',$key->id);
                   }
               }else{
                 $c=new tbl_curriculum;
               $c->academic_id=$academic_id;
               $c->curriculum_year=$curriculum_year;
               $c->school_year=$school_year;
               $c->semester=$semester;
               $c->year_level=$year_level;
               $c->save();
               }
            $data=[

                'curriculum'=>$curriculum,
                'subject' =>$s,
                'curriculum2'=>$c,

            ];
            return response()->json($data);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        
       
        $curriculum =tbl_curriculum::find($id);
        $academic= tbl_academic::find($curriculum->academic_id);
       
        
        $subject = tbl_subject::all()
        ->where('academic_id',$curriculum->academic_id)
        ->where('curriculum_id',$id);
        $schedule = tbl_schedule::all()->where('curriculum_id',$id);
        $data=[
            'curriculum' =>$curriculum,
            'academic'   => $academic,
            'subject'   => $subject,
            
                   
        ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $academic=tbl_academic::find($id);
        return response()->json($academic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {  
        $code=$r->input('code');
        $title=$r->input('title');
        $college=$r->input('college');

        $messages = [
            'code.required' => ' Code is required*',
            //'prog_name.required' => 'Program Name is required*',
            //'code.unique' =>'This code is already exist'
          ];
          $this->validate($r,[
            'code' => 'unique:tbl_academics,program_code,'.$id,

           ],$messages); 

        $academic=tbl_academic::find($id);
        $academic->program_code=strtoupper($code);
        $academic->program_name=strtoupper($title);
        $academic->college=strtoupper($college);
        $academic->update();
        return response()->json($academic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $acad=tbl_academic::find($id);
        $acad->is_remove=true;
        $acad->save();

        return response()->json($acad);
    }
        
}
