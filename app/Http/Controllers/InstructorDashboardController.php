<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\assigned_instructor;
use App\tbl_schedule;
use App\tbl_class;
use App\tbl_subject;
use App\tbl_room;
use App\tbl_academic;
use App\User;
use App\tbl_faculty_load;
use App\tbl_curriculum;
use App\tbl_section;
use App\sched_dash;
use App\temp_notif;
use Carbon\Carbon;
use App\notification;
class InstructorDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Instructor.dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function getNotifi()
    {
          $t = new temp_notif;
        $notif= notification::where('type','approved')->where('type_id',Auth::user()->id)->orderByDesc('created_at')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get()
                foreach ($notif as $key) {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       if (count($temp) == 0) {
                         
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                   }
                  
         $temp = temp_notif::where('user_id',Auth::user()->id)->where('is_read',false)->orderByDesc('date_sent')->get();
         $user = User::find($t->subby);
         $msg =Carbon::parse($t->date_sent)->diffforHumans();
         $data = [ 't' => $t, 'temp' => $temp, 'user' => $user,'msg'=>$msg];

        return Response()->json($data);
    }
    
    public function dashSched()
    {
       
        $sem = sched_dash::where('user_id',Auth::user()->id)->first()->semester;
        $year =  sched_dash::where('user_id',Auth::user()->id)->first()->school_year;
        $room=tbl_room::all();
        $schedule=tbl_schedule::all();
        $subject=tbl_subject::all();
        $section = tbl_section::all();
        $ass = assigned_instructor::where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->get();

        $data=[
            'section' => $section,
              'sem'=>$sem,
              'year'=>$year,
              'room'=>$room,
              'schedule'=>$schedule,
              'subject'=>$subject,
              'ass'  =>$ass,
        ];

        return response()->json($data);
    }
    

       public function updateNotif($id)
    {
         $temp = temp_notif::find($id);
        $temp -> is_read = true;
        $temp->save();
      //  $temp2 = temp_new::where('')
            //return redirect('assign-instructor');
        $noti = notification::find($temp->notif_id);
       return response()->json($noti);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
