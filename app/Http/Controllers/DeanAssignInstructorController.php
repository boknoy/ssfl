<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\instructor_sub;
use App\subject_schedule;
use App\tbl_class;
use App\temp_year_level;
use App\tbl_subject;
use App\User;
use App\tbl_academic;
use App\tbl_schedule;
use App\assigned_instructor;
use App\tbl_section;
use DB;
use App\notification;
use App\tem_notification;
use App\temp_new;
use Illuminate\Support\Facades\Auth;
use App\tbl_room;
use App\temp_pending;
use App\dep_subject;
use App\dep_assigned;
class DeanAssignInstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::user()->id;
        $dep = tbl_academic::find(Auth::user()->department);
        $sub_sched = subject_schedule::all()->where('instructor_id',$id);
        $year =temp_year_level::all();
        $sched = subject_schedule::all()->unique('school_year');
        $class=DB::table('subject_schedules as s')
        ->leftJoin('tbl_classes as c','c.id','=','s.class_id')->get();
        $class2 =tbl_class::all();
        $section = tbl_section::all();
        $offer= subject_schedule::all();
        $teacher = User::all()
        ->whereIn('role',['Instructor','Dean']);
        
       
        return view('Dean.assign_instructor')
        ->with('sched',$sched)
        ->with('teacher',$teacher)
        ->with('year',$year)
        ->with('section',$section)
        ->with('class',$class)
        ->with('class2',$class2)
        ->with('sub_sched',$sub_sched);
    }

    public function sendSchedule($id)
    {
        $dep_ass = new dep_assigned;
        $dep_ass -> dep_id = Auth::user()->department;
        $dep_ass->class_id = $id;
        $dep_ass-> subby = Auth::user()->id;
        $dep_ass->save();

        $dep = tbl_academic::find(Auth::user()->department);
        $noti = new notification;
        $noti ->subby = Auth::user()->id;
        $noti ->type_id = $id;
        $noti ->type = "assigned";
        $noti ->msg = $dep->program_code." assigned new instructor";
        $noti->save();

        $ass = assigned_instructor::where('class_id',$id)->update(['is_send'=>true]);
        return response()->json($dep);


    }

    public function view($id)
    {
        if ($id == Auth::user()->id)
         {
            return redirect('dean-schedule');
         }else{
            return redirect('assign-instructor')->with('view',$id);
         }

        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getSched($id){
             $sub_sched = subject_schedule::all();
                  foreach ($sub_sched as $key ) 
                  {
                    $sched = tbl_schedule::all()->where('class_id',$id)->unique('subject_id');
                    $sub = tbl_subject::where('academic_id',Auth::user()->department)->get();
                    foreach ($sched as $key2)
                     {
                      foreach ($sub as $key3)
                       {
                          if ( $key3->id == $key2->subject_id)
                           {
                            $dep_sub = dep_subject::where('class_id',$key2->class_id)->where('subject_id',$key3->id)->where('dep_id',Auth::user()->department)->get();
                            if (count($dep_sub) == 0)
                             {
                              $x = new dep_subject;
                              $x->dep_id = Auth::user()->department;
                              $x->class_id = $key2->class_id;
                              $x->subject_id = $key3->id;
                              $x->save();



                            }
                          }
                      }
                    }
                  }
        $class=tbl_class::find($id);
        $temp1=temp_year_level::find($class->year_level_id);
        $sched=tbl_schedule::all()->where('class_id',$class->id);
        $academic=tbl_academic::find($temp1->academic_id);
        $subject =tbl_subject::all();
        $section=tbl_section::find($class->section_id);
        $ass = assigned_instructor::all();
        $ins = User::whereIn('role',['Dean','Instructor'])->get();
        $temp = temp_new::where('user_id',Auth::user()->id)->where('schedule_sub',$id)->update(['is_read'=>true]);
        $room = tbl_room::all();
        $dep_sub = dep_subject::where('class_id',$id)->where('dep_id',Auth::user()->department)->get();

        foreach ($dep_sub as $key) 
        {
           foreach ($subject as $key2)
            {
               if ($key2 ->id == $key->subject_id)
                {
                  if ($key2->academic_id != Auth::user()->department)
                   {
                     $dep = dep_subject::find($key->id);
                     $dep->delete();
                  }
               }
           }

        }


        $dep_sub = dep_subject::where('class_id',$id)->where('dep_id',Auth::user()->department)->get();
        $class_ass = dep_assigned::where('class_id',$id)->where('dep_id',Auth::user()->department)->get();
        $data=[
            'class' =>$class,
            'temp'  =>$temp,
            'temp1'  =>$temp1,
            'schedule' =>$sched,
            'academic' =>$academic,
            'subject'  =>$subject,
            'ass' => $ass,
            'ins' => $ins,
            'section' =>$section,
            'room' => $room,
            'dep_sub' => $dep_sub,
            'class_ass' => $class_ass, 
        ];

        return response()->json($data);

    }


    public function create()
    {
        //
    }
     public function getSy($year)
     {
        $class = DB::table('subject_schedules as s')->where('s.school_year',$year)
        ->Join('tbl_classes as c','c.id','=','s.class_id')->get();
        //->leftJoin('tbl_classes as c','c.id','=','s.class_id')->get();
        $section = tbl_section::all();
        $year = temp_year_level::all();
        $temp = temp_new::where('user_id',Auth::user()->id)->get();
        $data = ['class' => $class, 'section' => $section, 'year' => $year,'temp' => $temp];
        return response()->json($data);
     }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'instructor' => "required",
        ]);


   

      $instructor_id = $request->input('instructor');
       $schedule_id   = $request->input('schedule');
       $subject_id    = $request->input('subject');
       $class_id      = $request->input('class');
       $section      = $request->input('section');
       $room         = $request->input('room');
     
       //$submitted_by = tbl_academic::find(Auth::user()->department);
       $s = tbl_schedule::find($schedule_id);
        $f1 = assigned_instructor::where('instructor_id',$instructor_id)->where('school_year',$s->school_year)->where('semester',$s->semester)->where('time_start','<',$s->time_end)->where('time_end','>',$s->time_start)->where('day',$s->day)->get();
        $msg = "";
        $data=[];
        if(count($f1)>0)
        {
            $msg = "This Instructor is not avilable";
            $data = ['msg' => $msg];
        }
        else
        {
           
            $sched  = tbl_schedule::where('class_id',$class_id)->where('subject_id',$subject_id)->get();
            foreach ($sched as $key ) {
            $assign = new assigned_instructor;
           $assign -> submitted_by = Auth::user()->department;
           $assign -> schedule_id   = $key->id;
           $assign -> instructor_id = $instructor_id;
           $assign -> subject_id  = $subject_id;
           $assign -> class_id    = $class_id;
           $assign -> day = $key->day;
           $assign -> time_start = $key->time_start;
           $assign -> time_end = $key->time_end;
           $assign -> semester = $key->semester;
           $assign -> school_year = $key->school_year;
           $assign -> section  = $section;
           $assign ->  room    = $room;
           $assign->save();
            }
          
           $teacher = User::find($instructor_id);
           $schedules = tbl_schedule::where(['class_id' => $class_id,'subject_id' => $subject_id])->update(['is_assigned'=>true]);
           $schedule = tbl_schedule::find($schedule_id);
          /* 
           $noti = new notification;
           $noti->subby=Auth::user()->id;
           $noti->type_id=$assign->id;
           $noti->type = "assigned";
           $noti->msg = $teacher->fname." has been assigned.";
          $noti->save();


          $temp_pen= new temp_pending;
          $temp_pen->user_id=$instructor_id;
          $temp_pen->date_assigned=$assign->created_at;
          $temp_pen->save();
          */
           $assign = assigned_instructor::where('instructor_id',$instructor_id)->where('class_id',$class_id)->where('subject_id',$subject_id)->get();
           $dep_sched = dep_subject::where('class_id',$class_id)->where('subject_id',$subject_id)->where('dep_id',Auth::user()->department)->update(['is_assigned'=>true]);
           $data = ["assign" => $assign , "schedule"=>$schedule,'teacher'=>$teacher,'msg' => $msg ,'class' => $class_id];
        }
      
       
      
      return response()->json($data);
    }

    public function getAssigned($id)
    {
        $ass = assigned_instructor::where('schedule_id',$id)->get(['instructor_id']);
        $teacher = User::find($ass);
        return response()->json($teacher);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sched = tbl_schedule::all()->where('class_id',$id);
        $subject = tbl_subject::all();
        $data=[
                'schedule' =>$sched,
                'subject'=>$subject,
                ];
        return response()->json($data);
    }

    public function show2($id)
    {
        $schedule = tbl_schedule::find($id);
        $class= tbl_class::find($schedule->class_id);
        $subject = tbl_subject::find($schedule->subject_id);
        $teacher_sub = instructor_sub::all()->where('subject_id',$schedule->subject_id)->unique('instructor_id');
        $teacher = User::all()->whereIn('role',['Instructor','Dean']);
        $ass = assigned_instructor::where('schedule_id',$id)->get();
        $section=tbl_section::find($class->section_id);
        $room=tbl_room::find($schedule->room_id);

        $data = [
            'schedule' => $schedule,
            'subject'  => $subject,
            'teacher'  => $teacher,
            'teacher_sub' =>$teacher_sub,
            'ass' => $ass,
            'section' =>$section,
            'room'    =>$room,

        ];
        return response()->json($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
