<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_class;
use App\temp_year_level;
use App\tbl_schedule;
use App\tbl_subject;
use App\User;
use App\tbl_curriculum;
use App\assigned_instructor;
use App\tbl_academic;
use App\tbl_faculty_load;
use App\tbl_room;
use App\tbl_section;
class HrFacultyLoadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
    }

    public function getAssigned(){

        $ass= assigned_instructor::all()->unique('instructor_id');
        $teacher = User::all();
        $academic = tbl_academic::all();
        $pen =  assigned_instructor::all()->where('is_approved',true);
        $data = [
                    'ass' =>$ass,
                    'teacher' => $teacher,
                    'academic' => $academic,
                    'pen' =>$pen,
                ];
                return response()->json($data);
    }


    public function getPending()
    {
       // $pen = assigned_instructor::all()->where('instructor_id',$id)->where('is_approved',false)->count();
      
        return response()->json($pen);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {  
       $user=user::find($id);
       $loads = tbl_faculty_load::all()->where('instructor_id',$user->id);
       $folder = tbl_class::all()->unique('school_year');

       $data=[

             'folder' =>$folder,
             'loads'  =>$loads,
             'user'   =>$user,
    ];

       return response()->json($data);
    }
    public function semester($year)
    {
       // $class = tbl_class::all()->where('school_year',$year);
       // $loads = tbl_faculty_load::all()->where('instructor_id',$id);
        
        $year =  temp_year_level::all()->unique('semester');
      
        return response()->json($year);
    }
    public function getLoads($sem,$id)
    {
        
        $teacher = User::find($id);
        $academic = tbl_academic::find($teacher->department);
        $loads = tbl_faculty_load::all()->where('instructor_id',$id);
        $year = temp_year_level::all()->where('semester',$sem);
        $subject = tbl_subject::all();
        $schedule = tbl_schedule::all();
        $section = tbl_section::all();
        $class = tbl_class::all();
        $room = tbl_room::all();

        $data = [
            'academic' => $academic,
            'teacher' => $teacher,
            'loads'    => $loads,
            'year'     => $year,
            'subject'  => $subject,
            'schedule'  => $schedule,
            'class'   => $class,
            'section' =>$section,
            'room'     => $room,

        ];
        return response()->json($data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
