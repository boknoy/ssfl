<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\temp_year_level;
use App\tbl_section;
use App\tbl_class;
use App\tbl_academic;
use App\tbl_curriculum;
use App\curr_sub;
use App\subject_schedule;
use Illuminate\Validation\Rule;
use App\tbl_subject;
use App\class_subject;
class QuamClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {

         $messages = [
            'school_year.required' => 'School Year is required*',
            'section_id.required' => 'Section is required*',
            'year_level_id.unique' =>'This class is already exist'
          ];
          $this->validate($r,[
            'year_level_id' => Rule::unique('tbl_classes')->where(function ($query) use ($r) {
        return $query->where('section_id', $r->input('section_id'))->where('school_year', $r->input('school_year'));
        }),

            'school_year' => 'required',
            'section_id' => 'required',],$messages);
        $class = new tbl_class;
        //$class->academic_id = $r->input('academic_id');
       // $class->curriculum_id=$r->input('curriculum_id');
        $class->school_year=$r->input('school_year');
        $class->year_level_id=$r->input('year_level_id');
      //  $class->semester=$r->input('semester');
        $year = temp_year_level::find($r->input('year_level_id'));
        $curr = tbl_curriculum::find($year->curriculum_id);
        $class ->course= $curr->course_code;
        $class->semester = $year->semester;
        $class->year_level = $year->year_level;
        $class->section_id=$r->input('section_id');
        $section = tbl_section::find($r->input('section_id'));
        $class->section = $section->section_name;
        $class->save();
        
        $year = temp_year_level::find($r->input('year_level_id'));
        $curr_sub = curr_sub::all()->where('year_level_id',$r->input('year_level_id'));
        $section=tbl_section::find($r->input('section_id'));
        $subject = tbl_subject::all();
        foreach ($curr_sub as $a) {
           foreach ($subject as $b ) {
              if ($b->id == $a->subject_id) 
              {
                 $x = new class_subject;
                 $x->curr_sub_id = $a->id;
                 $x->class_id = $class->id;
                 $x->subject_id = $b->id;
                 $x->hour = $b->hours;
                 $x->time_remain = $b->hours;
                 $x->save();
              }
           }
        }
        $data=[
              
              'section' =>$section,
              'class'   =>$class,
              'year'    => $year,
              'curr_sub' => $curr_sub,


        ];

        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     
        $curriculum = tbl_curriculum::find($id);
        $academic =tbl_academic::find($curriculum->academic_id);
        $temp_year_level = temp_year_level::all()->where('curriculum_id',$id);
        $class = tbl_class::orderBy('school_year','Desc')->get();
         $section = tbl_section::all();
        $data=[
            'year' =>$temp_year_level,
            'class' => $class,
            'curriculum' => $curriculum,
            'academic' => $academic,
            'section' => $section,
        ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class=tbl_class::find($id);
        $temp=temp_year_level::find($class->year_level_id);
        $curr=tbl_curriculum::find($temp->curriculum_id);
        $section=tbl_section::all()->where('course',$curr->course_code);
        $data=[
              'class' => $class,
              'section' =>$section,
              'temp'   =>$temp,
        ];
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,$section,$sy)
    {
        $f1 = tbl_class::where('school_year',$sy)->where('section_id',$section)->whereNotIn('id',[$id])->get();
        $class=tbl_class::find($id);
        if (count($f1) == 0)
         {
             $class->school_year=$sy;
            $class->section_id = $section;
            $section = tbl_section::find($section);
            $class->section=$section->section_name;
            $class->save();
        }
        $data = ['class' => $class,'f1' => count($f1)];
      

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
