<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_class;
use App\temp_year_level;
use App\tbl_schedule;
use App\tbl_subject;
use App\User;
use App\tbl_curriculum;
use App\assigned_instructor;
use App\tbl_academic;
use App\tbl_faculty_load;
use App\tbl_room;
use App\tbl_section;
use App\tbl_instructor_info;
use App\temp_load;
use App\temp_load2;
class HrApprovedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approve = tbl_faculty_load::all()->unique('instructor_id');
        $assign= assigned_instructor::all()->where('is_approved',1);
        $teacher = User::where('role',['Instructor','Dean'])->get();
        $teacher2 = User::all();
        $dept = tbl_academic::all();
      
        return view('Hr.approved')
        ->with('dept',$dept)
        ->with('assign',$assign)
        ->with('teacher',$teacher)
        ->with('teacher2',$teacher2)
        ->with('approve',$approve);
    }
    public function getFacLoad($id,$year,$sem)
    {
         $load = tbl_faculty_load::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->where('is_over',false)->get();
        $load1 = tbl_faculty_load::all()->where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->unique('section');
        $over = tbl_faculty_load::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->where('is_over',true)->get();
        $load2 = tbl_faculty_load::all()->where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->unique('subject_id');
        $subject = tbl_subject::All();
        $teacher = User::find($id);
        $ins_info=tbl_instructor_info::where('instructor_id',$id)->first()->masteral;
        $ins_info2=tbl_instructor_info::where('instructor_id',$id)->first()->employee_type;
        $department=tbl_academic::find($teacher->department);
        $role=User::where('department',$teacher->department)->where('role','Dean')->first()->fname;
        $role2=User::where('department',$teacher->department)->where('role','Dean')->first()->mname;
        $role3=User::where('department',$teacher->department)->where('role','Dean')->first()->lname;
        
        $dean_date="";

        $x=assigned_instructor::where('semester',$sem)->where('school_year',$year)->where('instructor_id',$id)->get();

        if(count($x)>0)
        {
            $dean_date=assigned_instructor::where('semester',$sem)->where('school_year',$year)->where('instructor_id',$id)->first()->created_at;
        }  

        temp_load::truncate();
           foreach ($load1 as $key) {
            foreach ($load as $key2) {
                if ($key->section == $key2->section) {
                   $temp_load = temp_load::where('subject_id',$key2->subject_id)->where('section',$key->section)->get();
                   if (count($temp_load) == 0) {
                      $temp = new temp_load;
                      $temp->schedule_id = $key2->schedule_id;
                      $temp->subject_id = $key2->subject_id;
                      $temp->section=$key->section;
                      $temp->unique = $key2->no_students;
                      $temp->save();
                   }
                }
            }
        }
         temp_load2::truncate();
           foreach ($load1 as $key) {
            foreach ($over as $key2) {
                if ($key->section == $key2->section) {
                   $temp_load = temp_load::where('subject_id',$key2->subject_id)->where('section',$key->section)->get();
                   if (count($temp_load) == 0) {
                      $temp = new temp_load2;
                      $temp->schedule_id = $key2->schedule_id;
                      $temp->subject_id = $key2->subject_id;
                      $temp->section=$key->section;
                      $temp->unique = $key2->no_students;
                      $temp->save();
                   }
                }
            }
        }
        $temp_load = temp_load::all();
        $temp_over = temp_load2::all();
        $sched = tbl_schedule::where('semester',$sem)->where('school_year',$year)->get();
        $room = tbl_room::All();
        $data = [
                'load' => $load,
                'load1' => $load1,
                'load2' => $load2,
                'subject' => $subject,
                'teacher' =>$teacher,
                'department' =>$department,
                'ins_info'  => $ins_info,
                 'ins_info2'  => $ins_info2,
                'role'     =>$role,
                'role2'     =>$role2,
                'role3'     =>$role3,
                'temp_load' => $temp_load,
                'dean_date' =>$dean_date,
                'sched' => $sched,
                'room' => $room,
                'over' => $over,
                'temp_over' => $temp_over,
        ];

        return response()->json($data); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getFaculty($id)
    {
       $ins=User::find($id);

       return response()->json($ins);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
       $loads = new tbl_faculty_load;
       $loads->year_level_id=$request->input('year');
        $loads->instructor_id=$request->input('teacher');
         $loads->schedule_id=$request->input('schedule');
          $loads->subject_id=$request->input('subject');
           $loads->no_students=$request->input('students');
            $loads->class_id=$request->input('class');
            $loads->save();
        $ass = assigned_instructor::find($request->input('ass'));
        $ass->is_approved=true;
        $ass->save();
        return response()->json($ass);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ass = assigned_instructor::all()->where('instructor_id',$id);
        $teacher = User::find($id);
        $teacher_dep =tbl_academic::find($teacher->department);
        $submitted = User::all()->where('role','Dean');
        $academic = tbl_academic::all();
        $schedule = tbl_schedule::all();
        $subject = tbl_subject::all();
        $data = [
            'ass'=>$ass,
            'teacher'=>$teacher,
            'submitted' => $submitted,
            'academic' =>$academic,
            'teacher_dep' => $teacher_dep,
            'schedule' => $schedule,
            'subject' => $subject,

        ];
        return response()->json($data);
    }

    public function show2($id)
    {
        $ass = assigned_instructor::find($id);
        $teacher = User::find($ass->instructor_id);
        $schedule = tbl_schedule::find($ass->schedule_id);
        $subject = tbl_subject::find($schedule->subject_id);
        $room = tbl_room::find($schedule->room_id);
        $class = tbl_class::find($schedule->class_id);
        $section = tbl_section::find($class->section_id);
        $year = temp_year_level::find($class->year_level_id);
        $data = [
                    'ass'       => $ass,
                    'schedule' => $schedule,
                    'year'      => $year,
                    'room'      => $room,
                    'section'   => $section,
                    'subject'   => $subject,
                    'teacher'   => $teacher,
                    'class'     => $class,


                ];
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
