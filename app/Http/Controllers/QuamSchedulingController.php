<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_academic;
use App\tbl_room;
use App\tbl_section;
use App\tbl_curriculum;
use App\tbl_subject;
use App\tbl_schedule;
use App\tbl_class;
use App\curr_sub;
use App\temp_year_level;
use App\User;
use App\subject_schedule;
use App\tbl_faculty_load;
use App\tbl_instructor_info;
use App\assigned_instructor;
use App\notification;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use App\temp_new;
use App\class_subject;
class QuamSchedulingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function scheduleInc($id,$sem,$year)
    {
        $fac = User::find($id);
        $fac_info = tbl_instructor_info::where('instructor_id',$id)->get();
        $load = assigned_instructor::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->where('is_approved',true)->get();

        $schedule = tbl_schedule::all();
        $subject = tbl_subject::all();
        $room = tbl_room::all();
        
        $data = [

            'fac' => $fac,
            'fac_info' => $fac_info,
            'room' =>$room,
            'load' =>$load,
            'schedule' =>$schedule,    //lance buotan nga bata
            'subject' =>$subject,
 
        ];
        return response()->json($data);

    }
    public function index()
    {   
        $academic=tbl_academic::all();
        $curriculum=tbl_curriculum::orderBy('curriculum_year','desc')->get();
         $curriculum2=tbl_curriculum::all()->unique('curriculum_year');
        $subject=tbl_subject::all();
        $section=tbl_section::all();
        $room=tbl_room::all();
        $year =temp_year_level::all();
        return view('Quam.scheduling')
        ->with('year',$year)
        ->with('academic',$academic)
        ->with('curriculum',$curriculum)
        ->with('curriculum2',$curriculum2)
        ->with('section',$section)
        ->with('room',$room);
    }
    public function send($id)
    {   


        $class = tbl_class::find($id);

        $year = temp_year_level::find($class->year_level_id);
        $curr=tbl_curriculum::find($year->curriculum_id);
        $section=tbl_section::find($class->section_id);
        $ins = User::where('department',$year->academic_id)->where('role','Dean')->get();
        $user=0;
        foreach ($ins as $key => $value) {
           $user=$value->id;         
        }
       
        $send = new subject_schedule;
        $send->class_id = $class->id;
        $send->course = $curr->course_code;
        $send->school_year=$class->school_year;
        $send->semester=$year->semester;
        $send->year_level=$year->year_level;
        $send->section=$section->section_name;
        $send->save();

        $noti = new notification;
        $noti->subby = Auth::user()->id;
        $noti->type_id=$class->id;
        $noti->type = "class";
        $noti->msg = "Class schedule has been posted";
        $noti->save();
         
        return response()->json($send);
    }
    public function getCourse($id)
    {
     $class=tbl_class::find($id);
     $section=tbl_section::find($class->section_id);
     $academic=tbl_academic::all()->where('id',$section->academic_id);

     $data=[
        'academic' =>$academic,
        'section'  =>$section,
     ];
     return response()->json($data);
    }

    public function getData($id)
    {
        $curr_sub = curr_sub::find($id);
        $year = temp_year_level::find($curr_sub->year_level_id);
        $academic = tbl_academic::find($year->academic_id);
        $curriculum = tbl_curriculum::find($year->curriculum_id);
        $class =tbl_class::all()->where('year_level_id',$curr_sub->year_level_id);
        $subject=tbl_subject::find($id);
        $room=tbl_room::all();
            
        $data = [
                    'curr_sub'   => $curr_sub,
                    'year'       => $year,
                    'academic'   => $academic,
                    'curriculum' => $curriculum,
                    'class'      => $class,
                    'subject'    => $subject,
                    'room'       => $room,
                ];
                return response()->json($data);
    }


    public function getInfo($id){
      $sched = tbl_schedule::find($id);
      $sub=tbl_subject::find($sched->subject_id);

      $data=[
           'sched' =>$sched,
           'sub'   =>$sub,
      ];

      return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTime($class,$subject,$t_hr)
    {
        $time = class_subject::where('class_id',$class)->where('subject_id',$subject)->first()->time_remain;
        $total=$time-$t_hr;
        
        return response()->json($total);
    }
    public function updateTime($id,$subject,$t_hr)
    {
        $sched = tbl_schedule::Find($id);
        $total1 = 0;
        $total2 = 0;
        $total=0;
        $val = false;
        $sched2 = tbl_schedule::where('class_id',$sched->class_id)->where('subject_id',$subject)->whereNotIn('id',[$id])->get();
        $ct = class_subject::where('class_id',$sched->class_id)->where('subject_id',$subject)->first()->time_remain;
         $ct3= class_subject::where('class_id',$sched->class_id)->where('subject_id',$subject)->first()->hour;
        $ct2 = tbl_schedule::find($id);
        $remain = $ct+$ct2->t_hour;
        if (count($sched2)>0)
         {
           
               
              
               if ($t_hr >$remain)
                {
                   $val=true;
               }else
               {
                $total1 =$remain- $t_hr ;
                $total = $ct3 - $total1;
             class_subject::where('class_id',$sched->class_id)->where('subject_id',$subject)->update(['time_remain'=>$total1]);
               }
            
        }else
        {
            if ($t_hr > $remain)
             {
                $val=true;
            }else
            {
                 $total2 = $remain-$t_hr;
                  class_subject::where('class_id',$sched->class_id)->where('subject_id',$subject)->update(['time_remain'=>$total2]);
            }
           
        }

    

        return response()->json($val);
    }
    public function store(Request $r)
    { 


       

         $messages = [
            'day.required' => 'Day is required*',
            'time_start.required' => 'Time start is required*',
            'time_end.required' => 'Time end is required*',
            'room_id.required' => 'Room is required*',
            //'room_id.unique' => 'Time schedule is already exists',
            
          ];
          $this->validate($r,[
            'day' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
              'room_id' => 'required',

            'day' => 'required',
          
                   
        ],$messages);
         $class_id=$r->input('class_id');
        $curriculum_id=$r->input('curriculum_id');
        $subject_id=$r->input('subject_id');
        $school_year=$r->input('school_year');
        $year_level=$r->input('year_level');
        $semester=$r->input('semester');
        $day=$r->input('day');
        $time_start= date("H:i", strtotime($r->input('time_start')));
        $time_end=date("H:i", strtotime($r->input('time_end')));
        $room=$r->input('room_id');
        $color = $r->color;
        $t_hr = $r->t_hr;
        $day3 = null;
         if($day == 6)
         {
            $day3 = 0;
         }elseif ($day == 7 )
          {
             $day3 = 1;
         }  
         else
         {
            $day3 = $day;
         }
       
       $msg = "";
       $schedule ="";
      


         $f1= tbl_schedule::where('class_id',$class_id)->where('day',$day3)->where('time_start','<',$time_end)->where('time_end','>',$time_start)->get();
        
              $f2= tbl_schedule::where('semester',$semester)->where('school_year',$school_year)->where('room_id',$room)->where('day',$day3)->where('time_start','<',$time_end)->where('time_end','>',$time_start)->get();

              //->whereBetween('time_start',[$time_start,$time_end])  ->get();
        if (count($f1) >0) {
           // naay conflict sa same class day and time
            $msg =" This schedule has already been used in this class";
        }
         if (count($f2) >0) {
           // naay conflict sa same class day and time
            $msg =" Please choose another room";
        }

       if ($msg =="")
        {
            $time = class_subject::where('class_id',$class_id)->where('subject_id',$subject_id)->first()->time_remain;
                  $remain = $time-$t_hr;
                    $time_remain  = class_subject::where('class_id',$class_id)->where('subject_id',$subject_id)->update(['time_remain' => $remain]);
            if($day == 6)
        {
            $day2 = 0;
            for($i = 0 ; $i<2 ;$i++)
            {
                 $schedule = new tbl_schedule;
                 $schedule->class_id=$class_id;
                $schedule->curriculum_id=$curriculum_id;
                $schedule->subject_id=$subject_id;
                $schedule->school_year=$school_year;
                $schedule->year_level=$year_level;
                $schedule->semester=$semester;
                $schedule->day=$day2;
                $schedule->time_start=$time_start;
                $schedule->time_end=$time_end;
                $schedule->room_id=$room;
                $schedule->color=$color;
                $schedule->t_hour = $t_hr/2;
               // $schedule->section_id=$r->input('section');
                $schedule->is_assigned=0;
                $schedule->save();
                $day2 = 2;
            }
        }elseif($day == 7)
        {
            $day2 = 1;
            for($i = 0 ; $i<2 ;$i++)
            {
                 $schedule = new tbl_schedule;
                 $schedule->class_id=$class_id;
                $schedule->curriculum_id=$curriculum_id;
                $schedule->subject_id=$subject_id;
                $schedule->school_year=$school_year;
                $schedule->year_level=$year_level;
                $schedule->semester=$semester;
                $schedule->day=$day2;
                $schedule->time_start=$time_start;
                $schedule->time_end=$time_end;
                $schedule->room_id=$room;
                 $schedule->color=$color;
                  $schedule->t_hour = $t_hr/2;
               // $schedule->section_id=$r->input('section');
                $schedule->is_assigned=0;
                $schedule->save();
                $day2 = 3;

            }
        }else{
             $schedule = new tbl_schedule;
           $schedule->class_id=$class_id;
                $schedule->curriculum_id=$curriculum_id;
                $schedule->subject_id=$subject_id;
                $schedule->school_year=$school_year;
                $schedule->year_level=$year_level;
                $schedule->semester=$semester;
                $schedule->day=$day;
                $schedule->time_start=$time_start;
                $schedule->time_end=$time_end;
                $schedule->room_id=$room;
                 $schedule->color=$color;
                $schedule->t_hour = $t_hr;
               // $schedule->section_id=$r->input('section');
                $schedule->is_assigned=0;
                $schedule->save(); 
                 
                
            }
        }
        
     
  
         $rooms = tbl_room::all();
        $subject = tbl_subject::find($r->input('subject_id'));
         $data=[
                'class_id' => $class_id,
                'schedule' =>$schedule,
                'subject'  =>$subject,
                'f1' => $f1,
                'f2' => $f2,
                'room' =>$rooms,
                'msg' => $msg,

        ];

        return response()->json($data);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function removeSched($id)
    {
        $sched = tbl_schedule::find($id);
       
        $t_hr = class_subject::where('class_id',$sched->class_id)->where('subject_id',$sched->subject_id)->first()->time_remain;
        $hour = $sched->t_hour+$t_hr;
        $cs = class_subject::where('class_id',$sched->class_id)->where('subject_id',$sched->subject_id)->update(['time_remain'=>$hour]);
     $sched->delete();
        return response()->json($sched);
    }
    public function show($id)
    {   
       $subsched = subject_schedule::where('class_id',$id)->get();
       $class = tbl_class::find($id);
       $section = tbl_section::find($class->section_id);
       $subject = tbl_subject::all()->sortBy('created_at');
       $schedule = tbl_schedule::all()->where('class_id',$id);
      // / $schedule2 = tbl_schedule::all()->where('class_id',$id)->unique('subject_id');
       //$curr_sub=curr_sub::all()->where('year_level_id',$class->year_level_id);
       $room = tbl_room::all();
       $academic=tbl_academic::all()->where('id',$section->academic_id);
       $year_level = temp_year_level::find($class->year_level_id);

       $curr = tbl_curriculum::find($year_level->curriculum_id);
       $curr_sub = class_subject::where('class_id',$id)->get(); 
       $curr = curr_sub::where('year_level_id',$class->year_level_id)->get();
       foreach ($curr as $key ) 
       {
          $temp = class_subject::where('subject_id',$key->subject_id)->where('class_id',$id)->get();
          if (count($temp) == 0)
           {
            foreach ($subject as $key2)
             {
                if ($key2->id == $key->subject_id)
                 {
                    $x = new class_subject;
                $x->curr_sub_id = $key->id;
                 $x->class_id = $id;
                 $x->subject_id = $key2->id;
                 $x->hour = $key2->hours;
                 $x->time_remain = $key2->hours;
                 $x->save();
                }
            }
              
          }
       }
       $data=[
            'class' =>$class,
            'section' =>$section,
            'subject' =>$subject,
            'schedule' => $schedule,
            'curr_sub' =>$curr_sub,
            'room' =>$room,
            'academic'  =>$academic,
            'subsched' => $subsched,
            'curr' => $curr,
            'year_level' =>$year_level,

       ];
       return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $schedule=tbl_schedule::find($id);
        $class=tbl_class::find($schedule->class_id);
        $section=tbl_section::find($class->section_id);
        $subject=tbl_subject::all()->where('id',$schedule->subject_id);
        $room=tbl_room::all();
        $single_room= tbl_room::find($schedule->room_id);
        $single_subject = tbl_subject::find($schedule->subject_id);
        $load = tbl_faculty_load::where('schedule_id',$schedule->id)->where('subject_id',$schedule->subject_id)->get();
        $fac_info = tbl_instructor_info::all();
        $fac = User::whereIn('role' ,['Dean','Instructor'])->get();
        $data=[
            'schedule' =>$schedule,
            'subject'  =>$subject,
            'room'     =>$room,
            'single_subject' => $single_subject,
            'single_room' => $single_room,
            'load' =>$load,
            'fac_info' => $fac_info,
            'fac' => $fac,
            'class' =>$class,
            'section' =>$section,
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    { 
         $schedule = tbl_schedule::find($id);
        $time_start= date("H:i", strtotime($r->input('time_start')));
        $time_end=date("H:i", strtotime($r->input('time_end')));
        $room = $r->input('room_id');
        $day = $r->input('day');
        $class_id = $schedule->class_id;
        $semester = $schedule->semester;
        $school_year = $schedule->school_year;
        $msg = "";
        $f1= tbl_schedule::where('class_id',$class_id)->where('day',$day)->where('time_start','<',$time_end)->where('time_end','>',$time_start)->whereNotIn('id',[$id])->get();
        
              $f2= tbl_schedule::where('semester',$semester)->where('school_year',$school_year)->where('room_id',$room)->where('day',$day)->where('time_start','<',$time_end)->where('time_end','>',$time_start)->whereNotIn('id',[$id])->get();


          if (count($f1) >0) {
           // naay conflict sa same class day and time
            $msg =" This schedule has already been used in this class";
        }
         if (count($f2) >0) {
           // naay conflict sa same class day and time
            $msg =" Please choose another room";
        }

        if ($msg == "")
         {
                $schedule->day= $day;
        $schedule->time_start=$time_start;
        $schedule->time_end=$time_end;
        $schedule->room_id=$room;
        $schedule->color=$r->input('color');
        $schedule->t_hour = $r->t_hour;
         $schedule->save();
        
        }
        

       $class = tbl_class::find($schedule->class_id);
        $subject2 = tbl_subject::find($schedule->subject_id);
        $room = tbl_room::find($schedule->room_id);
        $subject = tbl_subject::all()
        ->where('curriculum_id',$class->curriculum_id)
        ->where('year_level',$class->year_level)
        ->where('semester',$class->semester);
        $schedule2= tbl_schedule::all()
        ->where('class_id',$class->id);

        $data = [
                    'schedule2' => $schedule,
                    'schedule' =>$schedule2,
                    'subject'  => $subject,
                    'subject2' => $subject2,
                    'class'   => $class,
                    'room'    => $room,
                    'msg' =>$msg,
        ];
        return response()->json($data);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
    
    public function destroy($id)
    {
        //
    }
}
