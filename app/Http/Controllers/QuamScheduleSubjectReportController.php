<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_academic;
use App\tbl_room;
use App\tbl_section;
use App\tbl_curriculum;
use App\tbl_subject;
use App\tbl_schedule;
use App\tbl_class;
use App\curr_sub;
use App\temp_year_level;
use App\tbl_instructor_info;
use App\User;
use App\subject_schedule;
class QuamScheduleSubjectReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $class = tbl_class::all();
        $section = tbl_section::all();
        $year = temp_year_level::all();
        return view('Quam.reports.schedule_subject_report')
        ->with('year',$year)
        ->with('section',$section)
        ->with('class',$class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSection($sy,$course,$sem,$yl)
    {
            $class = tbl_class::where('school_year',$sy)->where('semester',$sem)->where('year_level',$yl)->where('course',$course)->get();
            $data = ['class'=>$class,'count' => count($class)];
            return response()->json($data);
    }
    public function getClass($id)
    {
        $class = tbl_class::find($id);
        $schedule = tbl_schedule::where('class_id',$id)->get();
        $schedule2 = tbl_schedule::all()->where('class_id',$id)->unique('subject_id');
        $subject = tbl_subject::all();
        $room = tbl_room::all();
        $year_level = temp_year_level::find($class->year_level_id);
        $academic = tbl_academic::find($year_level->academic_id);
        $data = ['schedule' => $schedule,'schedule2' => $schedule2,'subject' => $subject,'room'=>$room,'year_level' => $year_level,'academic'=>$academic,'class' => $class];
        return response()->json($data);
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function show($id)
    {
       $class = tbl_class::find($id);
       $section = tbl_section::find($class->section_id);
       $subject = tbl_subject::all()->sortBy('created_at');
       $schedule = tbl_schedule::all()->where('class_id',$id);
      // / $schedule2 = tbl_schedule::all()->where('class_id',$id)->unique('subject_id');
       $curr_sub=curr_sub::all()->where('year_level_id',$class->year_level_id);
       $room = tbl_room::all();
       
       $temp = temp_year_level::find($class->year_level_id);
       $curr = tbl_curriculum::find($temp->curriculum_id);
       $academic=tbl_academic::find($temp->academic_id);
       
       $data=[
            'class' =>$class,
            'section' =>$section,
            'subject' =>$subject,
            'schedule' => $schedule,
            'curr_sub' =>$curr_sub,
            'room' =>$room,
            'academic' =>$academic,

       ];
        return view('Quam.reports.print',compact('curr','academic'))
        ->with('section',$section)
        ->with('schedule',$schedule)
        ->with('room',$room)
        ->with('subject',$subject)
        ->with('class',$class)
        ->with('id',$id);
    }

    public function show2($id)
    {
       $class = tbl_class::find($id);
       $section = tbl_section::find($class->section_id);
       $subject = tbl_subject::all()->sortBy('created_at');
       $schedule = tbl_schedule::all()->where('class_id',$id);
      $schedule2=tbl_schedule::all()->where('class_id',$id)->unique('subject_id');
       $curr_sub=curr_sub::all()->where('year_level_id',$class->year_level_id);
       $room = tbl_room::all();
       $academic=tbl_academic::all()->where('id',$section->academic_id);

      

       $data=[
            'class' =>$class,
            'section' =>$section,
            'subject' =>$subject,
            'schedule' => $schedule,
            'curr_sub' =>$curr_sub,
            'room' =>$room,
            'academic' =>$academic,

       ];
        return view('Quam.reports.print2')
        ->with('section',$section)
        ->with('schedule',$schedule)
        ->with('room',$room)
        ->with('subject',$subject)
        ->with('schedule2',$schedule2)
        ->with('class',$class)
        ->with('id',$id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
