<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_room;
use App\tbl_schedule;
use App\tbl_subject;
use App\tbl_class;
use App\tbl_section;
class QuamAddRoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms=tbl_room::all()->where('is_remove',0);
        $archive=tbl_room::all()->where('is_remove',1);
        $sy = tbl_class::all()->unique('school_year');
        return view('Quam.add_rooms',compact('sy'))
        ->with('rooms',$rooms)
        ->with('archive',$archive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

      public function getSchedule($id,$sem,$sy)
    {
          $schedule = tbl_schedule::where('room_id',$id)->where('school_year',$sy)->where('semester',$sem)->get();
          $room = tbl_room::find($id);
          $subject = tbl_subject::all()->where('is_remove',false);
          $section = tbl_section::all();
          $class = tbl_class::all();
          $data =['schedule' => $schedule,'room'=>$room,'subject' => $subject,'section' => $section,'class' => $class];
          return response()->json($data);

    }
    public function getDetails($id){

        $room=tbl_room::find($id);

        return response()->json($room);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getroomName($id)
    {
       $room=tbl_room::find($id);
       
       return response()->json($room);
    }


public function restoreRoom($id)
{   
    $room = tbl_room::where('id',$id)->update(['is_remove' =>false]);
    $room=tbl_room::find($id);
    return response()->json($room);
  
}

    public function store(Request $r)
    {
       $room_name=$r->input('room_name');
       $building=$r->input('building');
       $is_active=$r->input('is_active');
       $is_remove=$r->input('is_remove');

          $messages = [
            'room_name.required' => 'Room name is required*',
            'building.required' => 'Building is required*',
            'room_name.unique' =>'This room is already exist'
          ];
          $this->validate($r,[
            'room_name' => 'required|unique:tbl_rooms,room_description',

            'building' => 'required',],$messages);


       $rooms= new tbl_room;

       $rooms->room_description=strtoupper($room_name);
       $rooms->building=strtoupper($building);
       $rooms->is_active=$is_active;
       $rooms->is_remove=$is_remove;
       $rooms->save();
       
       return response()->json($rooms);

    }
    public function roomData($id){
        $room=tbl_room::find($id);
        
        $data=['room' =>$room];

        return response()->json($data);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response`
     */
    public function show($id)
    {
        $rooms = tbl_room::find($id);
        return response()->json($rooms);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rooms=tbl_room::find($id);

        return response()->json($rooms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
      
       $room_name=$r->input('room_name');
       $building=$r->input('building');
       $is_active=$r->input('is_active');

       $messages = [
            'room_name.required' => 'Room name is required*',
            'building.required' => 'Building is required*',
            'room_name.unique' =>'Room name is already exist'
          ];
          $this->validate($r,[
            'room_name' => 'required|unique:tbl_rooms,room_description,'.$id,

            'building' => 'required',],$messages);
    
        $rooms = tbl_room::find($id);
        $rooms->room_description=strtoupper($room_name);
        $rooms->building=strtoupper($building);
        $rooms->is_active=$is_active;
        $rooms->save();

       return response()->json($rooms);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room=tbl_room::find($id);
        $room->is_remove=true;
        $room->save();

        return response()->json($room);

    }
}
