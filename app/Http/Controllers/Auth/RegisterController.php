<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        
        return redirect($this->redirectTo)->with('message', 'Registered successfully, please login...!');
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    protected $redirectTo = '/';
    protected function redirectTo()
    {
        if(Auth::user()->role == 'Admin'){

            return '/quam_dashboard';

        }elseif(Auth::user()->role == 'HR'){

            return '/HRdashboard';

        }elseif(Auth::user()->role == 'Dean'){

            return '/Deandashboard';

        }elseif(Auth::user()->role == 'Instructor'){

            return '/Instructordashboard';

        }else{
            return '/login';
        }
    } 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:50'],
            'lname' => ['required', 'string', 'max:50'],
            'fname' => ['required', 'string', 'max:50'],
            'mname' => ['required', 'string', 'max:50'],
            'extension_name' => ['required', 'string', 'max:50'],
            'role' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:3', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'lname' => $data['lname'],
            'fname' => $data['fname'],
            'mname' => $data['mname'],
            'extension_name' => $data['extension_name'],
            'role' => $data['role'],
            'email' => $data['email'],
            'image' => 'avatar.png',
            'password' => Hash::make($data['password']),
        ]);
    }
}
