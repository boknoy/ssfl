<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\notification;
use App\temp_notif;
use App\temp_new;
use App\User;
use App\dep_subject;
use App\dep_assigned;
use App\subject_schedule;
use App\tbl_subject;
use App\tbl_schedule;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected function redirectTo()
    {
        if(Auth::user()->role == 'Admin'){


              $notif= notification::where('type','approved')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();

                foreach ($notif as $key)
                 {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       $user = User::find($key->type_id);
                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $user->fname." schedule has been approved";
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                  }

                      $notif= notification::where('type','resched')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();

                foreach ($notif as $key)
                 {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       $user = User::find($key->type_id);
                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                  }



            return '/quam_dashboard';

        }elseif(Auth::user()->role == 'HR' && Auth::user()->is_active){
            $notif= notification::where('type','assigned')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();
                foreach ($notif as $key) {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                }
            return '/hr-dashboard';

        }elseif(Auth::user()->role == 'Dean'){

                $notif= notification::where('type','class')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();
                foreach ($notif as $key)
                 {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                  }
                  $notif2 = notification::where('type','class')->get();
                  foreach ($notif2 as $k)
                   {
                      $temp = temp_new::where('schedule_sub',$k->type_id)->where('user_id',Auth::user()->id)->get();
                      if (count($temp)==0)
                       {
                        $t = new temp_new;
                        $t->user_id = Auth::user()->id;
                        $t->schedule_sub=$k->type_id;
                        $t->save();
                      }
                   }
                   $notif= notification::where('type','approved')->where('type_id',Auth::user()->id)->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();
                foreach ($notif as $key)
                 {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg = $key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                  }
                  $sub_sched = subject_schedule::all();
                  foreach ($sub_sched as $key ) 
                  {
                    $sched = tbl_schedule::all()->where('class_id',$key->class_id)->unique('subject_id');
                    $sub = tbl_subject::where('academic_id',Auth::user()->department)->get();
                    foreach ($sched as $key2)
                     {
                      foreach ($sub as $key3)
                       {
                          if ( $key3->id == $key2->subject_id)
                           {
                            $dep_sub = dep_subject::where('class_id',$key2->class_id)->where('subject_id',$key3->id)->where('dep_id',Auth::user()->department)->get();
                            if (count($dep_sub) == 0)
                             {
                              $x = new dep_subject;
                              $x->dep_id = Auth::user()->department;
                              $x->class_id = $key2->class_id;
                              $x->subject_id = $key3->id;
                              $x->save();



                            }
                          }
                      }
                    }
                  }
                

            return '/dashboard';

        }elseif(Auth::user()->role == 'Instructor'){

                     $notif= notification::where('type','approved')->where('type_id',Auth::user()->id)->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get();

                foreach ($notif as $key)
                 {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();

                       if (count($temp) == 0) {
                           $t = new temp_notif;
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg =$key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                  }
          

            return '/instructor-dashboard';

        }else{
          Auth::logout();
          session(['error'=>'Please Contact Administrtor!']);
            return '/';
        }
    } 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
