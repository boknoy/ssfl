<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_section;
use App\tbl_academic;
class QuamSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $section = tbl_section::all();
        $academic = tbl_academic::all();
        $archive=tbl_section::all()->where('is_remove',1);
        return view('Quam.section')
        ->with('academic',$academic)
        ->with('section',$section)
        ->with('archive',$archive);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
     public function getsecName($id)
    {
        $sec=tbl_section::find($id);

        return response()->json($sec);
    }


    public function restoreSection($id)
    {

    $sec = tbl_section::where('id',$id)->update(['is_remove' =>false]);
    $sec=tbl_section::find($id);
    $academic=tbl_academic::all()->where('id',$sec->academic_id);
    $data=[
        'sec'=>$sec,
        'academic' =>$academic,
    ];

    return response()->json($data);

    }


    public function secRemove($id){
        $sec=tbl_section::find($id);

        return response()->json($sec);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $course=$r->input('course');
        $section_name=$r->input('section_name');
        $year_level=$r->input('year_level');
        $is_active=$r->input('is_active');
        $is_remove=$r->input('is_remove');

         $messages = [
            'course.required' => ' Course is required*',
            'session_name.required' => 'Section name is required*',
            'year_level.required' =>'Year level is required*',
            'section_name.unique' =>'This section is already exist'
          ];
          $this->validate($r,[
            'section_name' => 'required|unique:tbl_sections,section_name',

            'year_level' => 'required',],$messages);


        $section = new tbl_section;
        $section ->course=$course;
        $section ->section_name=strtoupper($section_name);
        $section ->year_level=$year_level;
        $section ->is_active=$is_active;
        $section->is_remove=$is_remove;
        $section->save();

        $academic = tbl_academic::find($r->input('academic_id'));

        $data =[

            'section' => $section,
            'academic' => $academic,
        ];

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $section=tbl_section::find($id);
        $academic=tbl_academic::all()->where('id',$section->academic_id);
        $data=[
            'academic' =>$academic,
             'section' =>$section,
        ];
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {    
        $section_name=$r->input('section_name');
        $course=$r->input('course');
        $year_level=$r->input('year_level');
        $is_active=$r->input('is_active');

          $messages = [
  
            'section_name.unique' =>'Section name is already exist'
          ];
          $this->validate($r,[
            'section_name' => 'required|unique:tbl_sections,section_name,'.$id,

            ],$messages);


        $section=tbl_section::find($id);
        $section->course=$course;
        $section->section_name=strtoupper($section_name);
        $section->year_level=$year_level;
        $section->is_active=$is_active;
        $section->save();

        $academic=tbl_academic::find($r->input('academic_id'));

        $data=[

            'section' => $section,
            'academic' => $academic,

          ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sec=tbl_section::find($id);
        $sec->is_remove=true;
        $sec->save();

        $academic=tbl_academic::all()->where('id',$sec->academic_id);

        $data=[
            'sec' =>$sec,
            'academic' =>$academic,
        ];

        return response()->json($data);

    }
}
