<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\tbl_curriculum;
use App\temp_year_level;
use App\tbl_subject;
use App\curr_sub;
use App\tbl_academic;
class DeanCurriculum extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $curr = tbl_curriculum::all()->where('academic_id',Auth::user()->department)->unique('curriculum_year');
        $course = tbl_curriculum::all()->where('academic_id',Auth::user()->department)->unique('course_code');
        return view('Dean.curriculum',compact('curr','course'));
    }

    public function getCurr($year,$course)
    {
        $curr = tbl_curriculum::where('curriculum_year',$year)->where('course_code',$course)->get();
        $year_level = temp_year_level::all();
        $cur_sub=curr_sub::all();
        $subject=tbl_subject::all();
        $academic=tbl_academic::all();
        $data=[
              'curr' => $curr,
              'year_level' => $year_level,
              'subject'   => $subject,
              'cur_sub'   => $cur_sub,
              'academic' => $academic,
          ];

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
