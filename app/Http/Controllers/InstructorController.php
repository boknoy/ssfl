<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_instructor_info;
use App\tbl_subject;
use App\tbl_academic;
use App\User;
use App\instructor_sub;
use Hash;
use App\tbl_curriculum;
class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   

      
        $academic = tbl_academic::all();
        $instructor = tbl_instructor_info::all();
        $subject    =  tbl_subject::all()->where('is_remove',false);
        $curr = tbl_curriculum::all();

        return view('Hr.instructor')
        ->with('curr',$curr)
        ->with('instructor',$instructor)
        ->with('academic',$academic)
        ->with('subject',$subject);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


   
    public function store(Request $request)
    {
        $lname=$request->input('lname');
        $fname=$request->input('fname');
        $mname=$request->input('mname');
        $gender=$request->input('gender');
     
        $email=$request->input('email');
        $contact=$request->input('contact');
        $username=$request->input('teacher_id');
        $password=$request->input('password');

        $instructor = new tbl_instructor;
        $instructor->lname=$lname;
        $instructor->fname=$fname;
        $instructor->mname=$mname;
        $instructor->gender=$gender;
       
        $instructor->email=$email;
        $instructor->contact=$contact;
        $instructor->username=$username;
        $instructor->password=$password;

        $instructor->save();
        return response()->json($instructor);



    }
    function getData(){
        $instructor=tbl_instructor::all();
        return response()->json($instructor);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function store2(Request $request)
    {
        
        $lname = $request->input('lname');
        $fname = $request->input('fname');
        $mname = $request->input('mname');
        $ext_name = $request->input('ext_name');
        $image = $request->input('image');

        $email = $request->input('email');
        $facebook = $request->input('facebook');
        $contact = $request->input('contact');
        $address = $request->input('address');

        $program_graduated = $request->input('program_graduated');
        $department = $request->input('department');
        $employee_type = $request->input('employee_type');

        $teacher_id = $request->input('teacher_id');
        $password = $request->input('password');

        $request->validate([
        'email' => 'required|email|unique:users,email,',
       ]);

        
        $user = new user;
        $user->fname=strtoupper($fname);
        $user->mname=strtoupper($mname);
        $user->lname=strtoupper($lname);
        $user->extension_name=strtoupper($ext_name);
        $user->department=$department;
        $user->role='Instructor';
        $user->email=$email;
        if($image=="")
        {
            $user->image="avatar.png"; 
        }
        else
        {
            $user->image=$image; 
        }
        $user->password=Hash::make($password);
        $user->username=strtoupper($teacher_id);
        $user->save();

        $instructor = new tbl_instructor_info;
      
        $instructor->instructor_id=$user->id;
        $instructor->contact=$contact;
        $instructor->facebook=$facebook;
        $instructor->address=$address;
        $instructor->program_graduated=$program_graduated;
        $instructor->employee_type=$employee_type;
        $instructor->save();

        $id=$instructor->instructor_id;
        $subject = $request->input('subject');

        foreach ($subject as $key => $value) {
             $ins_sub = new instructor_sub;
             $ins_sub ->instructor_id = $id;
             $ins_sub ->subject_id =  $value;
             $ins_sub ->save();
        }

       
        $data=[
            'user'=>$user,
            'instructor'=>$instructor,
            'subject' =>$subject,
        ];
        return response()->json($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function getDetails($id)
    {
        $ins=user::find($id);
        $ins_info=tbl_instructor_info::all()->where('instructor_id',$ins->id);
        $cur=tbl_curriculum::all();
        $dep=tbl_academic::all();
        $ins_sub = instructor_sub::where('instructor_id',$id)->get();
        $subject = tbl_subject::all();
        $data=[
            'ins' =>$ins,
            'ins_info' =>$ins_info,
            'dep'     =>$dep,
            'cur'   =>$cur,
            'ins_sub' => $ins_sub,
            'subject' => $subject,
        ];

        return response()->json($data);
    }

    public function getinfo($id)
    
    {
        $user=user::find($id);
        $instructor=tbl_instructor_info::all()->where('instructor_id',$id);
        $pg = tbl_curriculum::all();
        $ins_sub = instructor_sub::all()->where('instructor_id',$id);
        $sub = tbl_subject::all();
        $data=[ 
              'user' => $user,
              'instructor' => $instructor,
              'pg' =>$pg,
              'sub' => $sub,
              'ins_sub' => $ins_sub,

        ];

        return response()->json($data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
      public function list()
    {   
        $instructor = tbl_instructor_info::all();
        $user = User::orderByDesc('created_at')->get();
        $academic=tbl_academic::all();
        $subject=tbl_subject::all();
        $curr = tbl_curriculum::all();
        return view('Hr.list_of_instructor')
        ->with('curr',$curr)
        ->with('instructor',$instructor)
        ->with('user',$user)
        ->with('academic',$academic)
        ->with('subject',$subject);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateinfo(Request $request, $id)
    {
        $lname = $request->input('lname');
        $fname = $request->input('fname');
        $mname = $request->input('mname');
        $extension_name = $request->input('ext_name');
        $image = $request->input('image');

        $email = $request->input('email');
        $facebook = $request->input('facebook');
        $contact = $request->input('contact');
        $address = $request->input('address');
        $college = $request->college;
        $program_graduated = $request->input('program_graduated');
        $department = $request->input('department');
        $employee_type = $request->input('employee_type');

        $teacher_id = $request->input('teacher_id');
        $password = $request->input('password');

        $request->validate([
        'email' => 'required|email|unique:users,email,'. $id .'id',
       ]);

       
        
        $user = User::find($id);
        $user->fname=strtoupper($fname);
        $user->mname=strtoupper($mname);
        $user->lname=strtoupper($lname);
        $user->extension_name=strtoupper($extension_name);
        $user->department=$department;
       // $user->role='Instructor';
        $user->email=$email;
        if($image=="")
        {
           $user->image="avatar.png"; 
        }
        else
        {
             $user->image=$image; 
        }
       
        $user->username=strtoupper($teacher_id);
        $user->save();
        $ins = tbl_instructor_info::where('instructor_id',$id)->first()->id;

        $instructor = tbl_instructor_info::find($ins);
        $instructor->masteral =$request->masteral; 
        $instructor->instructor_id=$user->id;
        $instructor->contact=$contact;
        $instructor->address =$address;
        $instructor->facebook = $facebook;
        $instructor->program_graduated=$program_graduated;
        $instructor->college = $college;
        $instructor->employee_type=$employee_type;
        $instructor->save();

        $id=$instructor->instructor_id;
        $subject = $request->input('subject');
        $sub =instructor_sub::where('instructor_id',$id)->delete();
           
        foreach ($subject as $key => $value) {
             $ins_sub = new instructor_sub;
             $ins_sub ->instructor_id = $id;
             $ins_sub ->subject_id =  $value;
             $ins_sub ->save();
        }

       
        $data=[
            'user'=>$user,
            'instructor'=>$instructor,
            'subject' =>$subject,
        ];
  
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {   
        $user = User::find($id);

        if($r->role == 'Dean')
        {
             $user2=User::where('role','Dean')->where('department',$user->department)->get();

             if(count($user2)>0)
             {
                $user=[];

             }else{
                
                 $role =  $r->input('role');
      
                    $user ->role = $role;
                    $user->save();
             }
        }
        else{
                 $role =  $r->input('role');
                 $user ->role = $role;
                 $user->save();
        }
       
       

        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
