<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\subject_schedule;
use App\tbl_subject;
use App\tbl_room;
use App\tbl_class;
use App\tbl_schedule;
use App\temp_year_level;
use App\tbl_academic;
use App\tbl_curriculum;
use App\tbl_section;
class DeanScheduleReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Dean.reports.schedule_report');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSchedule($year,$course,$sem,$level)
    {
        $f1 = subject_schedule::where('school_year',$year)->where('course',$course)->where('semester',$sem)->where('year_level',$level)->get();

        return Response()->json($f1);
    }
     public function getSchedule2($year,$course,$sem,$level,$sec)
    {
        $f1 = subject_schedule::where('school_year',$year)->where('course',$course)->where('semester',$sem)->where('year_level',$level)->where('section',$sec)->get();
        $sched = tbl_schedule::all();
        $subject = tbl_subject::all();
        $room = tbl_room::all();
        $data = ['room' => $room , 'subject' => $subject , 'sched' => $sched , 'f1' => $f1];
        return Response()->json($data);
    }

    public function show($class_id)
    {
        $class = tbl_class::find($class_id);
        $section = tbl_section::find($class->section_id);
        $sched = tbl_schedule::all()->where('class_id',$class_id);
        $subject = tbl_subject::all();
        $room = tbl_room::all();

        $temp = temp_year_level::find($class->year_level_id);
       $curr = tbl_curriculum::find($temp->curriculum_id);
       $academic=tbl_academic::find($temp->academic_id);

        

       return view('Dean.reports.print')
       ->with('sched',$sched)
       ->with('subject',$subject)
       ->with('room',$room)
       ->with('section',$section)
       ->with('academic',$academic)
       ->with('class',$class)
       ->with('temp',$temp);
      

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
