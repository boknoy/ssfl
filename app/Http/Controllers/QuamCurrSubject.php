<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_subject;
use App\temp_year_level;
use App\curr_sub;
use App\tbl_curriculum;
class QuamCurrSubject extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {

        $year_level_id = $r->input('year_level_id');
        $subject_id = $r->input('subject');

        $curr = temp_year_level::find($year_level_id);
            $messages = [
         
            'subject.unique' =>'This Subject is already exist in this curriculum'
          ];
          $this->validate($r,[
            'subject' => 'required|unique:curr_subs,subject_id,NULL,id,curriculum_id,'.$curr->curriculum_id,

            ],$messages);


        $currsub = new curr_sub;

        $currsub -> year_level_id = $year_level_id;
        $currsub -> subject_id    = $subject_id;
        $curr = temp_year_level::find($year_level_id);
        $currsub->curriculum_id = $curr->curriculum_id;   
        $currsub->save();

        return response()->json($currsub);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currsub = curr_sub::all()->where('year_level_id',$id);
        $subject = tbl_subject::all();

        $data = [
                    'currsub' => $currsub,
                    'subject' => $subject,
                ];
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
