<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_curriculum;
use App\temp_year_level;
use App\curr_sub;
use App\tbl_subject;
use App\tbl_academic;
use App\tbl_class;
class QuamCurriculumReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $academic = tbl_academic::all();
        $curr = tbl_curriculum::all()->unique('curriculum_year');
        $curriculum=tbl_curriculum::all()->unique('course_code');
        return view('Quam.reports.curriculum_report')
        ->with('academic',$academic)
        ->with('curr',$curr)
        ->with('curriculum',$curriculum);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function getReport($year,$course)
    {
        $year = tbl_curriculum::where('curriculum_year',$year)->where('course_code',$course)->get();
        if (count($year)>0) 
        {
            $id=0;
            foreach ($year as $key) {
              $id = $key->id;
            }
           
            $curr = tbl_curriculum::find($id);
           
            $subject = tbl_subject::all();
            $curr_sub = curr_sub::all();
            $curriculum=tbl_curriculum::find($id);
            $academic = tbl_academic::find($curriculum->academic_id);
             $year_level= temp_year_level::all()->where('curriculum_id',$id);
               $data = [

                    'curr' => $curr,
                    'academic' => $academic,
                    'subject' => $subject,
                    'curr_sub' => $curr_sub,
                    'year_level' => $year_level,
                    'curriculum' =>$curriculum,
                    'year' =>count($year) ,
                 

                 ];
                 return response()->json($data);
         
        }else
        {
            $data = ['year' => count( $year)];
            return response()->json($data);
        }
       
         

      
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
        public function getallCurr()
    {
        $curriculum=tbl_curriculum::all();

        return response()->json($curriculum);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
