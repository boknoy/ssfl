<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use Hash;
use App\User;
use App\curr_sub;
use App\temp_year_level;
use App\temp_notif;
use App\notification;
use Carbon\Carbon;
use App\tbl_schedule;
use App\tbl_subject;
use App\tbl_class;
use App\tbl_academic;
use App\reschedule;
class QuamDashboardController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
   
      return view('Quam.quam_dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function getNotifi()
    {
          $t = new temp_notif;
        $notif= notification::where('type','approved')->orderByDesc('created_at')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get()
                foreach ($notif as $key) {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       $user = User::find($key->type_id);
                       if (count($temp) == 0) {
                         
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg =$user->fname." schedule has been approved";
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                   }
        $t = new temp_notif;
        $notif= notification::where('type','resched')->orderByDesc('created_at')->get();
               // $temp = temp_notif::where('user_id',Auth::user()->id)->get()
                foreach ($notif as $key) {
                       $temp = temp_notif::where('notif_id',$key->id)->where('user_id',Auth::user()->id)->get();
                       $user = User::find($key->type_id);
                       if (count($temp) == 0) {
                         
                           $t->user_id = Auth::user()->id;
                           $t->notif_id=$key->id;
                           $t->subby = $key->subby;
                           $t->type = $key->type;
                           $t->msg =$key->msg;
                           $t->date_sent = $key->created_at;
                           $t->save();
                       }
                   }
         $temp = temp_notif::where('user_id',Auth::user()->id)->where('is_read',false)->orderByDesc('date_sent')->get();
         $user = User::find($t->subby);
         $msg =Carbon::parse($t->date_sent)->diffforHumans();
         $data = [ 't' => $t, 'temp' => $temp, 'user' => $user,'msg'=>$msg];
        return Response()->json($data);
    }

    public function Resched($id)
    {

    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function proceed($id)
    {
      
        return redirect('quam-scheduling')
        ->with('resched',$id);
    }


     public function updateNotif($id)
    {
        $temp = temp_notif::where('user_id',Auth::user()->id)->where('notif_id',$id)->update(['is_read'=>true]);
      //  $temp2 = temp_new::where('')
            //return redirect('assign-instructor');
        $noti = notification::find($id);
        $data =[ 'noti' => $noti];
        if($noti->type == "resched")
        {
          $resched=reschedule::find($noti->type_id);
        $schedule=tbl_schedule::find($resched->schedule_id);
        $class=tbl_class::find($schedule->class_id);
        $ins=User::find($noti->subby);
        $sub=tbl_subject::find($schedule->subject_id);
        $acad=tbl_academic::find($ins->department);


        $data=[
               'noti' =>$noti,
                'schedule'  =>$schedule,
                'class'   =>$class,
                'ins' =>$ins,
                'sub' =>$sub,
                'acad' =>$acad,
                'resched' =>$resched,

        ];
      }
        
       return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
