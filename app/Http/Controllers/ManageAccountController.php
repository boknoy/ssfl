<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class ManageAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $user = User::whereIn('role',['Hr','Admin'])->get();
        return view('Quam.manageaccount')
        ->with('user',$user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
     public function userData($id){
        $user=User::find($id);
        return response()->json($user);

    }

    public function updateInfo(Request $r, $id){

        $email=$r->input('email');
        $password=$r->input('password');
        $image=$r->image;

          $r->validate([
        'email' => 'required|email|unique:users,email,'. $id .'id',
         ]);
        
        $user=User::find($id);
        $user->email=$email;
        $user->password=Hash::make($password);
        $user->image=$image;
      //  $img =ltrim($image, '"');
    // $r->image->store('images','public');
        $user->save();

        return response()->json($user);

    }
       public function Block($id)
    {
        $user=user::find($id);
        $user->is_active=false;
        $user->save();

        return response()->json($user);
    }
    public function Activate($id)
    {
        $user=user::find($id);
        $user->is_active=true;
        $user->save();

        return response()->json($user);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {   
        $fname=$r->input('fname');
        $mname=$r->input('mname');
        $lname=$r->input('lname');
        $ext_name=$r->input('ext_name');
        $role=$r->input('role');
        $email=$r->input('email');
        $password=$r->input('password');

          $messages = [
            'lname.required' => 'First name is required*',
            'mname.required' => 'Middle name is required*',
            'lname.required' => 'Last name is required*',
            'role.required' => 'Role is required*',
            'email.unique' =>'This email is already exist'
          ];
          $this->validate($r,[
            'email' => 'required|unique:users,email',

            'fname' => 'required',],$messages);

        $user = new User;
        $user->fname=strtoupper($fname);
        $user->mname=strtoupper($mname);
        $user->lname=strtoupper($lname);
        $user->extension_name=strtoupper($ext_name);
        $user->role=$role;
        $user->email=$email;
        $user->password=Hash::make($password);
        $user->image="avatar.png";
        $user->save();
        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

         $data=[
           'user' =>$user,
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {   
        $password=$r->input('password');
        $user = User::find($id);
        $user->fname=$r->input('fname');
        $user->mname=$r->input('mname');
        $user->lname=$r->input('lname');
        //$user->role=$r->input('role');
        $user->email=$r->input('email');
        //$user->password=Hash::make($password);
        $user->save();

        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
