<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_faculty_load;
use App\User;
use App\tbl_instructor_info;
use App\tbl_academic;
use App\tbl_subject;
use App\temp_load;
use App\temp_load2;
use App\assigned_instructor;
use App\tbl_schedule;
use App\tbl_room;
class QuamFacultyLoadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loads = tbl_faculty_load::all()->unique('instructor_id')->sortByDesc('created_at');
        return view('Quam.faculty_load',compact('loads'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

      public function getFacultyLoad($id,$year,$sem)
    {
        $load = tbl_faculty_load::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->where('is_over',false)->get();
        $load1 = tbl_faculty_load::all()->where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->unique('section');
        $over = tbl_faculty_load::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->where('is_over',true)->get();
        $load2 = tbl_faculty_load::all()->where('instructor_id',$id)->where('semester',$sem)->where('school_year',$year)->unique('subject_id');
        $subject = tbl_subject::All();
        $teacher = User::find($id);
        $ins_info=tbl_instructor_info::where('instructor_id',$id)->first()->masteral;
        $ins_info2=tbl_instructor_info::where('instructor_id',$id)->first()->employee_type;
        $department=tbl_academic::find($teacher->department);
        $role=User::where('department',$teacher->department)->where('role','Dean')->first()->fname;
        $role2=User::where('department',$teacher->department)->where('role','Dean')->first()->mname;
        $role3=User::where('department',$teacher->department)->where('role','Dean')->first()->lname;
        
        $dean_date="";

        $x=assigned_instructor::where('semester',$sem)->where('school_year',$year)->where('instructor_id',$id)->get();

        if(count($x)>0)
        {
            $dean_date=assigned_instructor::where('semester',$sem)->where('school_year',$year)->where('instructor_id',$id)->first()->created_at;
        }  

        temp_load::truncate();
           foreach ($load1 as $key) {
            foreach ($load as $key2) {
                if ($key->section == $key2->section) {
                   $temp_load = temp_load::where('subject_id',$key2->subject_id)->where('section',$key->section)->get();
                   if (count($temp_load) == 0) {
                      $temp = new temp_load;
                      $temp->schedule_id = $key2->schedule_id;
                      $temp->subject_id = $key2->subject_id;
                      $temp->section=$key->section;
                      $temp->unique = $key2->no_students;
                      $temp->save();
                   }
                }
            }
        }
         temp_load2::truncate();
           foreach ($load1 as $key) {
            foreach ($over as $key2) {
                if ($key->section == $key2->section) {
                   $temp_load = temp_load::where('subject_id',$key2->subject_id)->where('section',$key->section)->get();
                   if (count($temp_load) == 0) {
                      $temp = new temp_load2;
                      $temp->schedule_id = $key2->schedule_id;
                      $temp->subject_id = $key2->subject_id;
                      $temp->section=$key->section;
                      $temp->unique = $key2->no_students;
                      $temp->save();
                   }
                }
            }
        }
        $temp_load = temp_load::all();
        $temp_over = temp_load2::all();
        $sched = tbl_schedule::where('semester',$sem)->where('school_year',$year)->get();
        $room = tbl_room::All();
        $data = [
                'load' => $load,
                'load1' => $load1,
                'load2' => $load2,
                'subject' => $subject,
                'teacher' =>$teacher,
                'department' =>$department,
                'ins_info'  => $ins_info,
                 'ins_info2'  => $ins_info2,
                'role'     =>$role,
                'role2'     =>$role2,
                'role3'     =>$role3,
                'temp_load' => $temp_load,
                'dean_date' =>$dean_date,
                'sched' => $sched,
                'room' => $room,
                'over' => $over,
                'temp_over' => $temp_over,
        ];

        return response()->json($data);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function view()
    { 

         return redirect('quam-faculty-load');
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
