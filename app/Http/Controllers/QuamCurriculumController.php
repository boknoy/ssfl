<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_academic;
use App\tbl_subject;
use App\temp_year_level;
use App\tbl_schedule;
use App\tbl_curriculum;
use Datatables;
use App\curr_sub;
class QuamCurriculumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {  
        $folder= tbl_curriculum::all()->unique('curriculum_year');
        $curriculum=tbl_curriculum::all();
        $academic2= tbl_academic::all();
        $academic=tbl_academic::all()->whereNotIn('college',['','-'])->unique('college');
        $subject=tbl_subject::where('is_remove',0)->orderby('code')->get();
        $year_level=temp_year_level::all();
        return view('Quam.curriculum',compact('academic2'))
        ->with('folder',$folder)
        ->with('subject',$subject)
        ->with('academic',$academic)
        ->with('curriculum',$curriculum)
        ->with('year_level',$year_level);


    }
       public function manageCurriculum($id)
    {
        $curriculum=tbl_curriculum::find($id);
        $temp=temp_year_level::all()->where('curriculum_id',$curriculum->id);
        $academic=tbl_academic::find($curriculum->academic_id);
        $course = tbl_academic::where('college',$academic->college)->get();
        $data=[
            'curriculum'=>$curriculum,
            'temp'=>$temp,
            'academic'=>$academic,
            'course' => $course,
        ];
        
        return response()->json($data);
    }

    public function getCurYear($year)
    {
        $year = tbl_curriculum::all()->where('curriculum_year',$year);
        $academic =tbl_academic::all();
        $data = ['year' => $year , 'academic'=>$academic];
        return response()->json($data);
    }
    public function removeCurrSub($id)
    {
        $remove = curr_sub::find($id);
        $sub = tbl_subject::find($remove->subject_id);

        $remove ->delete();

        $data = ['curr' => $remove , 'sub' => $sub];
        return response()->json($data);
    }
    public function saveCurr($id)
    {
        $curr = tbl_curriculum::find($id);
        $curr->is_save = true;
        $curr->save();
        return response()->json($curr);
    }
     public function editCurr($id)
    {
        $curr = tbl_curriculum::find($id);
        $curr->is_save = false;
        $curr->save();
        return response()->json($curr);
    }
    public function getAll(){
        $curriculum = tbl_curriculum::orderByDesc('id')->get();
        $academic = tbl_academic::orderByDesc('id')->get();
        $data = ['curr' => $curriculum, 'academic' => $academic ];
        return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
      $curriculum_year=$request->input('curriculum_year');
        //$year_level=$request->input('year_level');
        $academic_id=$request->input('academic_id');
        $course=$request->input('course');
        $code = $request->input('code');
       $messages = [
            'academic_id.required' => 'Department is required*',
            'curriculum_year.required' => 'Curriculum year is required*',
            'course.unique' =>'This Course is already exist',
            'code.required' => 'Course code is required',

          ];
          $this->validate($request,[
            'course' => 'unique:tbl_curriculums,course,NULL,id,curriculum_year,'.$curriculum_year,
            'code' => 'required',
            'academic_id' => 'required',],$messages);


      /*  $cur_year=$request->input('cur_year');
        $prog_code=$request->input('prog_code');
        $prog_name=$request->input('prog_name');
        $curriculum = new tbl_curriculum;
        $curriculum->cur_year=$cur_year;
        $curriculum->prog_code=$prog_code;
        $curriculum->prog_name=$prog_name;
        $curriculum->save();
        return response()->json($curriculum);*/

      //  $school_year=$request->input('school_year');
        //$semester=$request->input('semester');
     
                $c=new tbl_curriculum;
               $c->academic_id=$academic_id;
               $c->curriculum_year=$curriculum_year;
               $c->course=strtoupper($course);
               $c->course_code=strtoupper($code);
               $c-> is_save = false;
            //   $c->school_year=$curriculum_year;
             //  $c->semester=$semester;
             //  $c->year_level=$year_level;
              // 
            $data1=0;
                $curr_unique = tbl_curriculum::where('curriculum_year',$request->input('curriculum_year'))->count(); 
                $c->save();
                 if ($curr_unique == 0) {

                        $data1=$c;
                    }
        $data = [
                'curr' => $data1,
                'year'       => $c->curriculum_year,
                ];
        return response()->json($data);

       

    }
     public function getCount($year){
            $count = tbl_curriculum::where('curriculum_year',$year)->count();
            return response()->json($count);
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curriculum =tbl_curriculum::find($id);
        $academic= tbl_academic::find($curriculum->academic_id);
        $year = temp_year_level::where('curriculum_id',$id)->get();
        
        $subject = tbl_subject::all();
        $schedule = tbl_schedule::all()->where('curriculum_id',$id);
        $currsub = curr_sub ::all();
        $data=[
            'curr' =>$curriculum,
            'academic'   => $academic,
            'subject'   => $subject,
            'year'      =>$year,
            'currsub' =>$currsub,
                   
        ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curriculum=tbl_curriculum::find($id);
        $academic=tbl_academic::all()->where('id',$curriculum->academic_id);

        $data=[
           
            'curriculum' => $curriculum,
            'academic'   => $academic,
        ];

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {

        $academic_id=$r->input('academic_id');
        $curriculum_year=$r->input('curriculum_year');
        $course=$r->input('course');
        $code = $r->code;
        $a = tbl_academic::find($course);
        $curriculum=tbl_curriculum::find($id);

        $curriculum->academic_id=$academic_id;
        $curriculum->curriculum_year=$curriculum_year;
        $curriculum->course=$a->program_name;
        $curriculum->course_code=strtoupper($code);
        $curriculum->save();

        $academic=tbl_academic::find($curriculum->academic_id);

        $data=[
            'curriculum' =>$curriculum,
            'academic'   =>$academic,
        ];

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
