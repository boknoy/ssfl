<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  Illuminate\Support\Facades\Auth;
use App\User;
use App\tbl_instructor_info;
use App\assigned_instructor;
use App\tbl_subject;
use App\tbl_schedule;
use App\tbl_room;
use App\tbl_section;
use App\tbl_class;
use App\sched_dash;
use App\tbl_faculty_load;
class DeanFacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fac = User::where('department',Auth::user()->department)->whereNotIn('id',[Auth::user()->id])->get();
        $fac_info=tbl_instructor_info::all();
        return view('Dean.faculty',compact('fac','fac_info'));
     
    }
    public function mySchedule($sem,$year)
    {
        $ass = tbl_faculty_load::where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->get();
         $sched = tbl_schedule::all();
        $room = tbl_room::all();
        $subject = tbl_subject::all();
        $section = tbl_section::all();
        $class = tbl_class::all(); 
        $dash = sched_dash::where('user_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->get();
        $data=[
             'ass' =>$ass,
             'sched' =>$sched,
             'room' =>$room,
             'subject' =>$subject,
             'section' =>$section,
             'class' => $class,
             'dash' =>count($dash),
         ];
        return response()->json($data);

    }
    public function getSchedule($id,$sy,$sem)
    {
        $ass = tbl_faculty_load::where('instructor_id',$id)->where('semester',$sem)->where('school_year',$sy)->get();
        $sched = tbl_schedule::all();
        $room = tbl_room::all();
        $subject = tbl_subject::all();
        $section = tbl_section::all();
        $class = tbl_class::all();
        $data=[
             'ass' =>$ass,
             'sched' =>$sched,
             'room' =>$room,
             'subject' =>$subject,
             'section' => $section,
             'class' => $class,
         ];
        return response()->json($data);
    }

    public function postDash(Request $request)
    {
        $sem=$request->input('sem');
        $year=$request->input('year');

       $chck = sched_dash::where('user_id',Auth::user()->id)->get();
       if (count($chck)==0) 
       {
          $dash = new sched_dash;
          $dash->user_id = Auth::user()->id;
          $dash->semester = $sem;
          $dash->school_year = $year;
          $dash->save();
       }else
       {
        $dash=sched_dash::where('user_id',Auth::user()->id)->update(['semester'=>$sem,'school_year' => $year]);
       }

    }
    
    public function removeDash()
    {
         sched_dash::where('user_id',Auth::user()->id)->delete();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
