<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\assigned_instructor;
use App\tbl_schedule;
use App\tbl_class;
use App\tbl_subject;
use App\tbl_room;
use App\tbl_academic;
use App\User;
use App\tbl_faculty_load;
use App\tbl_curriculum;
use App\tbl_section;
use App\sched_dash;
use App\temp_year_level;


class InstructorMyScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
   
        $ass=assigned_instructor::all();
        $schdule=tbl_schedule::all();
        $class=tbl_class::all();
        $subject=tbl_subject::all();
        $room=tbl_room::all();
        $academic=tbl_academic::all();
        $curriculum=tbl_curriculum::all();
        
        return view('Instructor.myschedule')
        ->with('ass',$ass)
        ->with('schdule',$schdule)
        ->with('class',$class)
        ->with('subject',$subject)
        ->with('room',$room)
        ->with('academic',$academic);
        
 
    }

     public function mySched($sem,$year)
    {
        $ass = tbl_faculty_load::where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->get();
         $sched = tbl_schedule::all();
        $room = tbl_room::all();
        $subject = tbl_subject::all();
        $section = tbl_section::all();
        $class = tbl_class::all(); 
        $dash = sched_dash::where('user_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->get();
        $data=[
             'ass' =>$ass,
             'sched' =>$sched,
             'room' =>$room,
             'subject' =>$subject,
             'section' =>$section,
             'class' => $class,
             'dash' =>count($dash),
         ];
        return response()->json($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function postDash(Request $request)
    {
        $sem=$request->input('sem');
        $year=$request->input('year');

       $chck = sched_dash::where('user_id',Auth::user()->id)->get();
       if (count($chck)==0) 
       {
          $dash = new sched_dash;
          $dash->user_id = Auth::user()->id;
          $dash->semester = $sem;
          $dash->school_year = $year;
          $dash->save();
       }else
       {
        $dash=sched_dash::where('user_id',Auth::user()->id)->update(['semester'=>$sem,'school_year' => $year]);
       }

    }
    
    public function removeDash()
    {
         sched_dash::where('user_id',Auth::user()->id)->delete();
    }


    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function showdata()
    {
      $user=user::find(Auth::user()->id);
      $faculty=tbl_faculty_load::all()->where('instructor_id',$user->id);
      $schedule=tbl_schedule::all();
      $class=tbl_class::all();
      $subject=tbl_subject::all();
      $room=tbl_room::all();
      $academic=tbl_academic::all();
      $curriculum=tbl_curriculum::all();

      $data=[
           'faculty' =>$faculty,
           'schedule'  =>$schedule,
           'subject'  =>$subject,
           'class'    =>$class,
           'room'  =>$room,
           'academic'  =>$academic,
           'curriculum' =>$curriculum,
          ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     public function show($year,$sem)
    {
        $loads=tbl_faculty_load::where('year_level',$year)->where('semester',$sem)->where('instructor_id',Auth::user()->id)->get();
        $class=tbl_class::all();
        $subject = tbl_subject::all();
        $room = tbl_room::all();

        $temp = temp_year_level::all();
       $curr = tbl_curriculum::all();
       $academic=tbl_academic::all();
       $section=tbl_section::all();

        

       return view('Instructor.reports.print')
      
       ->with('subject',$subject)
       ->with('room',$room)
       ->with('section',$section)
       ->with('academic',$academic)
       ->with('class',$class)
       ->with('temp',$temp)
       ->with('loads',$loads);
      

    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
