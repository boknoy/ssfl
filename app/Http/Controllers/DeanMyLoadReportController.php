<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\tbl_schedule;
use App\assigned_instructor;
use App\tbl_subject;
use App\tbl_curriculum;
use App\tbl_faculty_load;
use App\tbl_room;
use App\tbl_section;
use App\tbl_class;
class DeanMyLoadReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Dean.reports.my_loads');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLoads($year,$sem)
    {
        $loads1 = assigned_instructor::all()->where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->unique('section');
         $loads2 = assigned_instructor::all()->where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year)->unique('subject_id');
         $loads =  assigned_instructor::all()->where('instructor_id',Auth::user()->id)->where('semester',$sem)->where('school_year',$year);
        $subject = tbl_subject::all();

        $section = tbl_section::all();
        $room = tbl_room::all();
        $class = tbl_class::all();
        $data = ['loads' => $loads,'subject' => $subject, 'room' => $room, 'section' => $section,'class' => $class,'loads2' => $loads2,'loads1' => $loads1];
        return Response()->json($data);
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
