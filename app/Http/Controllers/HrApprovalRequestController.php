<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_class;
use App\temp_year_level;
use App\tbl_schedule;
use App\tbl_subject;
use App\User;
use App\tbl_curriculum;
use App\assigned_instructor;
use App\tbl_academic;
use App\tbl_faculty_load;
use App\tbl_room;
use App\tbl_section;
use App\notification;
use Illuminate\Support\Facades\Auth;
use App\temp_load;
class HrApprovalRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
       
         $approve = assigned_instructor::all();
        $assign= assigned_instructor::all()->where('is_approved',1);
        $teacher = User::where('role',['Instructor','Dean'])->get();
        $teacher2 = User::all();
        $dept = tbl_academic::all();
        
        return view('Hr.approval_request')
        ->with('dept',$dept)
        ->with('assign',$assign)
        ->with('teacher',$teacher)
        ->with('teacher2',$teacher2)
        ->with('approve',$approve);

   
    }

    public function view($id)
    {
         return redirect('approval-request')->with('view',$id);
    }

        public function approval(){

        $ass= assigned_instructor::all()->where('is_approved',false)->where('is_send',true)->unique('instructor_id');
        $count = assigned_instructor::all()->where('is_approved',false)->where('is_send',true)->count();
        $teacher = User::all();
        $academic = tbl_academic::all();
        $pen =  assigned_instructor::where('is_approved',false)->where('is_send',true)->get();
        $data = [
                    'ass' =>$ass,
                    'teacher' => $teacher,
                    'academic' => $academic,
                    'pen' =>$pen,
                    'cout' =>$count,
                ];
                return response()->json($data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function store(Request $request)
    { 
      
      $teacher_id=$request->input('teacher_id');
      $subject_id=$request->input('subject_id');
      $schedule_id=$request->input('schedule_id');
      $class_id=$request->input('class_id');
      $no_students=$request->input('no_students');
      $id=$request->input('ass');
      $year_level=$request->input('year_level');

      $assign=assigned_instructor::find($id);

    
        $f2 =  tbl_faculty_load::all()->where('semester',$assign->semester)->where('school_year',$assign->school_year)->where('instructor_id',$teacher_id)->whereNotIn('subject_id',$subject_id)->whereNotIn('section',$assign->section)->unique('subject_id');
           $temp_load = temp_load::all();
           if (count($temp_load) >0)
            {
             temp_load::truncate();
           }
           $load = tbl_faculty_load::where('instructor_id',$teacher_id)->where('semester',$assign->semester)->where('school_year',$assign->school_year)->get();
           foreach ($load as $key ) 
           {
              $temp_load = temp_load::where('subject_id',$key->subject_id)->where('section',$key->section)->get();
              if (count($temp_load) == 0) {
                      $temp = new temp_load;
                      $temp->schedule_id = $key->schedule_id;
                      $temp->subject_id = $key->subject_id;
                      $temp->section=$key->section;
                      $temp->unique = $key->no_students;
                      $temp->save();
                   }
           }
        $sub = tbl_subject::all();
        $temp = temp_load::all();
        $t_hr = 0;
        if (count($temp) > 0)
         {
          
            foreach ($temp as $key ) 
            {
             foreach ($sub as $key2 )
              {
               if ($key2 -> id == $key -> subject_id)
                {
                 $t_hr = $t_hr + $key2 -> hours;
               }
             }
            }
        }
        $sub2 = tbl_subject::find($subject_id);
        $t_hr+= $sub2 -> hours;
      $loads = new tbl_faculty_load;
       $loads->instructor_id=$teacher_id;
       $loads->schedule_id=$schedule_id;
       $loads->subject_id=$subject_id;
       $loads->class_id=$class_id;
       $loads->school_year=$assign->school_year;
       $loads->year_level=$year_level;
       $loads->semester=$assign->semester;
       $loads->day=$assign->day;
       $loads->time_start=$assign->time_start;
       $loads->time_end=$assign->time_end;
       $loads->room=$assign->room;
       $loads->section=$assign->section;
       $loads->no_students=$no_students;
        if ($t_hr >25) 
        {
         
              $loads->is_over = true;
        }
       
      
       $loads->save();
       $subject = tbl_subject::find($subject_id);
       $noti = new notification;
       $noti->subby = Auth::user()->id;
       $noti ->type_id = $teacher_id;
       $noti->type = "approved";
       $noti->msg = $subject->subject." ".$subject->code." has been assigned to you";
       $noti->save();
       $ass = assigned_instructor::find($id);
       $ass->is_approved=true;
      $ass->save();

        return response()->json($ass);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function show($id)
    {
        $ass = assigned_instructor::all()->where('is_approved',false)->where('instructor_id',$id);
        $teacher = User::find($id);
        $teacher_dep =tbl_academic::find($teacher->department);
        $submitted = User::all()->where('role','Dean');
        $academic = tbl_academic::all();
        $schedule = tbl_schedule::all();
        $subject = tbl_subject::all();
        $data = [
            'ass'=>$ass,
            'teacher'=>$teacher,
            'submitted' => $submitted,
            'academic' =>$academic,
            'teacher_dep' => $teacher_dep,
            'schedule' => $schedule,
            'subject' => $subject,

        ];
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function show2($id)
    {
        $ass = assigned_instructor::find($id);
        $teacher = User::find($ass->instructor_id);
        $schedule = tbl_schedule::find($ass->schedule_id);
        $subject = tbl_subject::find($schedule->subject_id);
        $room = tbl_room::find($schedule->room_id);
        $class = tbl_class::find($schedule->class_id);
        $section = tbl_section::find($class->section_id);
        $year = temp_year_level::find($class->year_level_id);
        $data = [
                    'ass'       => $ass,
                    'schedule' => $schedule,
                    'year'      => $year,
                    'room'      => $room,
                    'section'   => $section,
                    'subject'   => $subject,
                    'teacher'   => $teacher,
                    'class'     => $class,


                ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
