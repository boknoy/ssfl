<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\tbl_curriculum;
use App\tbl_subject;
use App\tbl_academic;
use App\tbl_schedule;
use App\tbl_class;
use App\temp_year_level;
use App\curr_sub;
use Illuminate\Validation\Rule;

class QuamSubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        
        $subject=tbl_subject::where('is_remove',0)->orderBy('code')->get();
        $archive=tbl_subject::where('is_remove',1)->orderBy('code')->get();
        $academic=tbl_academic::all();
        return view('Quam.subject')
        ->with('subject',$subject)
        ->with('archive',$archive)
        ->with('academic',$academic);

    }
   public function restore($id)
   {
    $res = tbl_subject::where('id',$id)->update(['is_remove' =>false]);
    $res=tbl_subject::find($id);
    return response()->json($res);
   }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    public function subRemove($id){
     $sub=tbl_subject::find($id);
     $data=['sub'  =>$sub];
     return response()->json($data);
    }
    
    public function getAll()
    {
        $subject = tbl_subject::all();
        return response()->json($subject);
    }

      public function getSub($id)

    {   $cur=curr_sub::find($id);
        $subject = tbl_subject::find($cur->subject_id);
        $data=[
            'cur' =>$cur,
            'subject' =>$subject,
        ];

        return response()->json($data);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function getsubName($id){
       $sub=tbl_subject::find($id); 
       return response()->json($sub);  
    }

    public function store(Request $request)


    {

         $messages = [
            
            'academic_id.required' => ' Department is required*',
            'descriptive' =>'Descriptive title is required*',
            'units' =>'Units is required*',
            
          ];
          $this->validate($request,[

            'units' => 'required',
            'descriptive' => 'required',
            'academic_id' => 'required',
        ],$messages);
      /*  $this->validate($request,[
       'subject' => Rule::unique('tbl_subjects')->where(function ($query) use ($request) {
        return $query->where('code', $request->input('code'))
        ->where('descriptive_title', $request->input('descriptive'));
        }) ]);*/
        $academic_id=$request->input('academic_id');
        $dcode=$request->input('code');
        $scode=$request->input('subject');
        $descriptive=$request->input('descriptive');
        $lec=$request->input('lec');
        $lab=$request->input('lab');
        $units=$request->input('units');
        $pre_requisites=$request->input('pre_requisites');
        $hours=$request->input('hours');

        $subject= new tbl_subject;
        $subject->academic_id=$academic_id;
        $subject->code=strtoupper($dcode);
        $subject->subject=strtoupper($scode);
        $subject->descriptive_title=strtoupper($descriptive);
        $subject->lec=$lec;
        if($lab == "" || $lab == null ){
              $subject->lab="-";
        }else{
            $subject->lab=$lab;
        }
        
        $subject->units=$units;
        if($pre_requisites == "" || $pre_requisites == null ){
              $subject->pre_requisites="-";
        }else{
            $subject->pre_requisites=strtoupper($pre_requisites);
        }

        
        if($lec == "" || $lec == null ){
              $subject->lec="-";
        }else{
            $subject->lec=$lec;
        }
        $subject->hours=$hours;
        $subject->is_remove=0;
        $subject->save();
        return response()->json($subject);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $subject = tbl_subject::find($id);
     return response()->json($subject);
    }
    public function showSubject($id,$class)
    {
        $subject = tbl_subject::find($id);
        $is_sched = tbl_schedule::where('class_id',$class)->where('subject_id',$id)->get();

        $data = ['subject' => $subject , 'is_sched' => $is_sched,];
     return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $subject=tbl_subject::find($id);
        $academic=tbl_academic::all();
        
        $data=[
            'subject' =>$subject,
             'academic' =>$academic,
         ];
       
       return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $messages = [
            
            'academic_id.required' => ' Department is required*',
         
            
          ];
          $this->validate($request,[

            'academic_id' => 'required',
        ],$messages);

        $academic_id=$request->input('academic_id');
        $dcode=$request->input('code');
        $scode=$request->input('subject');
        $descriptive=$request->input('descriptive');
        $lec=$request->input('lec');
        $lab=$request->input('lab');
        $units=$request->input('units');
        $pre_requisites=$request->input('pre_requisites');
        $hours=$request->input('hours');
        

        $subject=tbl_subject::find($id);
        $subject->academic_id=$academic_id;
        $subject->code=strtoupper($dcode);
        $subject->subject=strtoupper($scode);
        $subject->descriptive_title=strtoupper($descriptive);
        $subject->lec=$lec;
        if($lab == "" || $lab == null ){
              $subject->lab="";
        }else{
            $subject->lab=$lab;
        }
        
        $subject->units=$units;
        if($pre_requisites == "" || $pre_requisites == null ){
              $subject->pre_requisites="";
        }else{
            $subject->pre_requisites=strtoupper($pre_requisites);
        }

        
        if($lec == "" || $lec == null ){
              $subject->lec="";
        }else{
            $subject->lec=$lec;
        }
        $subject->hours=$hours;
        $subject->save();
        return response()->json($subject);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function allremove(){

        $subject=tbl_subject::all()->where('is_remove',1);
        $count=count($subject);

        return response()->json($count);
    }
    public function destroy($id)
    {
        $subject = tbl_subject::find($id);
        $subject->is_remove=true;
        $subject->save();
        return response()->json($subject);
    }
}
