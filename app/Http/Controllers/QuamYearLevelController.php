<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\temp_year_level;
use App\tbl_section;
use App\tbl_academic;
use App\tbl_curriculum;
class QuamYearLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $year = temp_year_level::all()->where('curriculum_id',$id);
        return response()->json($year);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $temp= new temp_year_level;
        $f1 = temp_year_level::where('curriculum_id',$r->id)->where('year_level',$r->year_level)->where('semester',$r->semester)->count();
        if ($f1 == 0)
         {
             
       $temp->academic_id=$r->input('academic_id');
        $temp->curriculum_id=$r->input('id');
        $temp->year_level=$r->input('year_level');
        $temp->semester=$r->input('semester');
        $temp->save();
        }
       $data = ['f1' => $f1,'temp' => $temp];

        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
       public function semInfo($id)
    {
        $temp=temp_year_level::find($id);
        return response()->json($temp);
    }

    public function removeSem($id)
    {
        $temp=temp_year_level::find($id);
        $temp->delete();

        return response()->json($temp);
    }
    
    public function show($id)
    {
        $temp=temp_year_level::find($id);
         $curr = tbl_curriculum::find($temp->curriculum_id);
        $section=tbl_section::all()->where('course',$curr->course_code);
        $academic=tbl_academic::all()->where('id',$temp->academic_id);
       
        $data=[

            'section'  =>$section,
            'temp'    =>$temp,
            'academic' =>$academic,
            'curr' => $curr,

        ];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $year=temp_year_level::find($id);

        return response()->json($year);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
