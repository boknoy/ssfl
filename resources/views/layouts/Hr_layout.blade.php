
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>SSFL</title>
   <link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>
  <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker.css')}}">
   <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
 
<link rel="stylesheet" href="{{asset('plugins/ekko-lightbox/ekko-lightbox.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
 <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('plugins/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
<script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
<script src="{{asset('plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('plugins/toastr/toastr.min.js')}}"></script>

{{--  <link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>
 

   <link rel="stylesheet" href="{{asset('plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">

   <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
 
   <link rel="stylesheet" href="{{asset('plugins/ekko-lightbox/ekko-lightbox.css')}}">
  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}"> --}}
  
 
  
</head> 
<body class="hold-transition text-sm sidebar-mini layout-navbar-fixed layout-fixed">

    <div class="wrapper">
    @include('Hr.header')

    @include('Hr.sidebar')
    <div class="content-wrapper">
      @yield('content')
    </div>
  
        <aside class="control-sidebar control-sidebar-dark" id="aside-page">
        <div class="col-md-12">
          <form id="form-user-update">
            @csrf
              <div class="card-body box-profile">
                <div class="text-center">
                  <div class="form-group " onclick="LoadImage()">
                    <img id="hr-image"  class="" src="{{ asset('img/'.Auth::user()->image)}}"style="height: 100px; width: 100px; border: 1px solid lightgray;border-radius: 50px;border:2px solid gray"/>
                    <i class="fas fa-circle text-secondary"  style=" position: absolute;top: 21.2%;left: 58.7%;font-size: 24px"></i>
                    <button type="button" class="btn " style=" position: absolute;top: 20%;left: 55.5%;"><i class="fas fa-camera text-white "></i></button>
                  </div>

                  <input type="file" id="image" style="display: none">

                 <input type="hidden" id="img-text"  value="{{Auth::user()->image}}">
                </div>

                @php
                $fname=Auth::user()->fname; 
                $mname=Auth::user()->mname; 
                $lname=Auth::user()->lname; 


                @endphp

                <h3 class="profile-username text-center"> {{$fname}} {{substr($mname,0,1)}}. {{$lname}}</h3>

                <p class=" text-center text-bold">HR </p> 
                <div class="form-group">
                 <label>Email :</label>
                 <input type="email" name="email" class="form-control" value="{{ Auth::user()->email }}">
               </div>
                 <div class="form-group">
                 <label>Password :</label>
                 <input type="password" name="password" class="form-control" value="{{ Auth::user()->password }}">
               </div>
               <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
            
              </div>
            </form>
               <div class="col-md-12 pl-3 pr-3">
               
                    <button  class="btn btn-success btn-block " onclick="UserUpdate()"><b>Update</b></button>
               
               </div>

        </div>
      </aside>
   @include('Hr.footer')
  @include('message.messages')
</div>
<!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->



<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('plugins/filterizr/jquery.filterizr.min.js')}}"></script>
  
    <script >
       function LoadImage()
       {
        $('#image').trigger('click');
      }
      $('#image').change(function()
      {
        readURL(this)
        var file = $('#image')[0].files[0].name;
        $('#img-text').val(file);
      })
      function readURL(input) {
        if (input.files && input.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
            $('#hr-image').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        }
      }
  </script>



  <script >
  $('.select2').select2();
</script> 
<script>
  $('#tbl-approval').DataTable({
    "paging": false,
      "lengthChange": false,
      "searching": true,
      "info": false,
       "bSort": false,
       "bInfo" : true,
       "dom":"ftip",

  });

    $('#tbl-approved').DataTable({
    "paging": false,
      "lengthChange": false,
      "searching": true,
      "info": false,
       "bSort": false,
       "bInfo" : true,
       "dom":"ftip",

  });


  $('#tbl-teacher-approval').DataTable({
    "paging": false,
      "lengthChange": false,
      "searching": true,
      "info": false,
       "bSort": false,
       "bInfo" : true,
       "dom":"ftip",

  });
</script>
<script>

  $(function () {
    $("#tbl-lis-curriculum").DataTable({
      'columnDefs': [

 {
     
      "className": "text-center",
 }],
    });
  });
</script>
<script>
  $(function () {
    $("#tbl-list-academic").DataTable({
      'columnDefs': [

 {
      "targets": 3,
      "className": "text-center",
 }],
 
    });
  });
</script>

<script>
  $(function () {
    $("#tbl-list-subject").DataTable({
      'columnDefs': [

 {
      "targets": 7,
      "className": "text-center",
 }],
 
    });
    $('#subject-table').DataTable();



    $('#room-table').DataTable();
    $('#section-table').DataTable();
    $('#list-of-instrcutor-table').DataTable();

  });
</script>
<script>
   $("#subject-list").DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "info": false,
       "bSort": false,
       "bInfo" : true,
           
   });
</script>
<script >
   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  })
</script>

<script>
    function UserUpdate(){

   const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer:3000,
                    
  });

   var id = $('input[name=user_id]').val();
   var email = $('input[name=email]').val();
   var password = $('input[name=password]').val();
   var image  =$('#img-text').val();


    $.ajax({
      type:'put',
       url: 'hr-update-account/'+id,
      data:{
        "_token"      :"{{csrf_token()}}",
        "email"         :email,
        "password"      :password,
        "image"         :image
      },
      success:function(data){

        //$('#aside-page').hide();

        Toast.fire({
          icon: 'success' ,
          title: ' Successfully Updated',
        });
      },
    error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+ value,
                  });
                  
                }); 
             }

    })
  }
  
</script>

</script>

</body>
</html>
