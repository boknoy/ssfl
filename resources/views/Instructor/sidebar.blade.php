<style>
.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: maroon;
    color: white;
}
</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link text-center">
      
      <span class="brand-text font-weight text-light"><b style="font-size: 15px;"> Instructor </b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('img/'.Auth::user()->image)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
               <a href="#" data-widget="control-sidebar" data-slide="true" class="d-block">{{Auth::user()->fname}} {{substr(Auth::user()->mname,0,1)}}. {{Auth::user()->lname}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
           <li class="nav-item">
            <a href="{{url('instructor-dashboard')}}" class="nav-link " id="dash">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{url('instructor-myschedule')}}" class="nav-link " id="myschedule">
              <i class="nav-icon fas fa-clock"></i>
              <p>
                My Schedule
              </p>
            </a>
          </li>
     
           <li class="nav-item">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#logout">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <div class="modal fade" id="logout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="a-id">
     <div class="col-md-12 ">
     
        <p  class="text-muted p-0" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-circle fa-lg text-warning " > </i><label  id="academic-name" style="text-decoration: underline;"></label> Are you sure you want logout?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
          <a class="btn btn-danger btn-xs" href="{{ route('logout') }}"
   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Yes, {{ __('logout') }} </a>
 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>