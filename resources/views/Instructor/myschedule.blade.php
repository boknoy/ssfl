  @extends('layouts.Instructor_layout')
@section('content')
<style type="text/css">
  
.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
 th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-event-time{
  text-align: center;
}
 th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-v-event{
   transition: all .2s ease-in-out;
}
.fc-v-event:hover { transform: scale(1.1);cursor: pointer }


#loading {
    height: 100%;
   
    margin: auto;
    
   
    width: 100%;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>

<section class="content-header pb-0">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <label style="font-size: 20px"><i class="fas fa-chalkboard-teacher"></i> My Schedule</label>
        </div>
      </div>
    </div>
</section>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card" >
          <div class="card-body text-center  pb-1">
            <div class="row">
              <div class=" col-md-5 ">
                <div class="form-group row">
                    <label class="col-form-label col-md-4" style="font-size: 15px">School Year</label>
                  <div class="col-md-8">
                      <select id="sy" class="form-control " style="text-align-last: center;" onchange="selvalid('sy')" >
                  <option selected="" disabled="" value="0"> -- Select --</option>
                  @php
                        $class = App\subject_schedule ::all()->unique('school_year');
                      @endphp
                      @foreach($class as $c)
                        <option value="{{$c->school_year}}">{{$c->school_year}}</option>
                      @endforeach
                
                </select>
                  </div>
                </div>
              </div>
              <div class=" col-md-5 ">
                <div class="form-group row">
                  <label class="col-form-label col-md-4" style="font-size: 15px">Semester</label>
                <div class="col-md-8">
                  <select id="sem" class="form-control  " style="text-align-last: center;" onchange="selvalid('sem')">
                  <option disabled="" selected="" value="0"> --Select--</option>
                  <option value="1st">First Semester</option>
                  <option value="2nd">Second Semester</option>
                  <option value="summer">Summer</option>
                </select>
                </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group"> 
                  <button class="btn btn-outline-primary col-md-10" onclick="Search()" id="btn-search">  <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" id="load" style="display: none"></span>
                                   <i class="fas fa-search"></i> Search </button>
                </div>
              </div>

            
            </div>
          </div>
        </div>
      </div>
    </div>
      <div class="col-sm-12 d-flex justify-content-center"  >
        <div class="alert alert-info alert-dismissible col-md-12" id="no-data" style="display: none">
                
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                    <h4>No data to show</h4>
                </div>
      </div>
      <div class="col-sm-12">
        <div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
      </div>
       
         
      <div class="row" id="cal" style="display: none">

      <div class="col-md-12 mb-2">
        <input type="hidden" id="semester">
        <input type="hidden" id="year">
          <button class="btn btn-info float-right " onclick="Print()"><i class="fas fa-print"></i> Print</button>
      </div>
        <div class="col-md-12" id="reports2">
           <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.css')}}">


  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
   <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
       <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<style type="text/css">
  .fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
.fc-event-time{
  text-align: center;
}
 th.fc-timegrid-axis:after{
  content: "Time";
 
}


.fc-event {
        font-size: 16px !important;
        font-weight: bold;
    }
    .fc a{
  text-decoration: none !important;
  border: none;
  color:none;
}

</style>
        <div class="card ">
          <div class="card-header pt-2  bg-dark">
            <div class="col-md-12 text-center ">
                <div class="icheck-primary  float-right " id="check" style="position: absolute;margin-left: 81%">
                  <input type="checkbox" id="show-dash" onchange="displayDash()">
                  <label for="show-dash"> Display in dashboard</label>
                </div>
                <label class="" id="s-year" style="font-size: 19px;"></label><br>
                <label id="s-sem"></label>
  
            </div>
          </div>
          <div class="card-body " id="calendar-ins"> 
          </div>
        </div>
        </div>
      </div>
    
    
    
  </div>
</section>
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script >
  $('#myschedule').addClass('active').css( 'background-color', '#800000' );
</script>

<script >
  
function Print3()
{
      var year =$('#year').val()
      var sem=$('#semester').val()

    window.open("{{url('print-myschedule')}}/"+year+'/'+sem);
}
</script>

<script >

    const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });


  function displayDash()
  {
    if($('#show-dash').is(':checked'))
    {
      var sem = $('#sem').val();
      var year =$('#sy').val();

      $.ajax({

        type:'POST',
        url : 'instructor-post-sched-dash',
        data :{
          "_token": "{{ csrf_token() }}",
              "sem" :sem,
              "year" :year,
        },
        



      })
    }
    else
    {
      $.get('instructor-remove-dash-sched',function(data){

      })
    }
  }

 
</script>

<script type="text/javascript">

        var date = new Date(2021, 2, 22)
        var d    = date.getDate(),
            m    = date.getMonth(),
            y    = date.getFullYear()

        var Calendar = FullCalendar.Calendar;
        var calendarEl = document.getElementById('calendar-ins');
        var calendar = new Calendar(calendarEl, {
         
          initialView:'timeGridWeek',
          validRange: {
        start: '2021-03-22',
        end: '2021-03-28'
             },
          
          headerToolbar:false,
          themeSystem: 'bootstrap',
          hiddenDays: [0],
          slotMinTime: '07:00:00',
          slotMaxTime: '22:00:00',
          allDaySlot: false,
          dayHeaderFormat :{ weekday: 'short' },
          contentHeight:"auto",
          eventTimeFormat: {
          hour: "numeric",
          minute: "2-digit",
          meridiem: "short",
        },
          });
        setInterval(calendar.render(),3000)

  function selvalid(val)
  { 
      $('#'+val).css('border-color','blue')
  }

  function Search()
    {
      var sems;
      var sem = $('#sem').val();

      $('#semester').val(sem)

       switch(sem){
        case "1st":
                    sems = 'FIRST SEMESTER';
                    break;
                case '2nd':
                   sems = 'SECOND SEMESTER';
                   break;
                case 'summer':
                   sems = 'SUMMER';
                break;

       }
       $('#s-sem').text(sems)

       var sy = $('#sy').val();

        $('#year').val(sy)

       $('#s-year').text('SCHOOL YEAR '+sy)

      if (sy == "0" || sy == null)
       {
        $('#sy').css('border-color','red')

           Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });
       
        return false

       }

      if (sem == "0" || sem == null)
       {
        $('#sem').css('border-color','red')

          Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester </span>',
                  });

        return false
       }

       $('#no-data').hide()
       $('#loading').show()
      $('#load').show()
      $('#btn-search').prop('disabled',true)
      calendar.removeAllEvents()
      $('#cal').hide()
      setInterval(calendar.render(),1000)
      $.get('instructor-assigned-schedule/'+sem+'/'+sy,function(data){
        $('#load').hide()
        $('#loading').hide()
        $('#btn-search').prop('disabled',false)
        if (data.dash > 0)
         {
          $( "#show-dash" ).prop( "checked", true );
         }else
         {
          $( "#show-dash" ).prop( "checked", false );
         }
         if (data.ass.length > 0)
          {
            $('#cal').show()

          }else
          {
            $('#no-data').show()
          }
        $.each(data.ass,function(k,a){

          $.each(data.sched,function(k,value){
            if (value.id == a.schedule_id)
             {
              $('#class_id2').val(value.class_id)

        $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
                    $.each(data.class,function(k,c){
                      if(c.id == value.class_id)
                      {

                        $.each(data.section,function(k,sec){
                          if (sec.id == c.section_id)
                           {
                            var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                     event.setProp('title',event.title+' '+sec.section_name);
                           }
                        })
                        
                      }
                    })
                    $.each(data.room,function(k,r){
                      if(r.id == value.room_id)
                      {
                        var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                     event.setProp('title',event.title+' '+r.room_description);
                      }
                    })  
                           
                  }
                      
                });
              
              
             }
          })
        })

      })
    }


function Print()
  {
    $('#check').hide();

    var prtContent = document.getElementById("reports2");
    //var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
      var WinPrint = window.open("");
      //WinPrint.document.open()
      
      //WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body>'+prtContent.innerHTML+'</body></html>')
        //WinPrint.focus();
    

      WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.title="My Schedule";
    
       WinPrint.document.close();
       WinPrint.focus();

        WinPrint.print();
       WinPrint.close();
         $('#check').show();
  }
</script>

@endsection