@extends('layouts/Instructor_layout')

@section('content')

<style type="text/css">
  
.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
.fc-event-time{
  text-align: center;
}
.fc-v-event{
   transition: all .2s ease-in-out;
}
.fc-v-event:hover { transform: scale(1.1);cursor: pointer }
.fc-event {
        font-size: 16px !important; {{--Your font size--}}
    }
</style>
  @php
    $dash = App\sched_dash::where('user_id',Auth::user()->id)->get();
  @endphp

    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-12">
            <label style="font-size: 20px"><i class="fas fa-tachometer-alt"> Dashboard</i></label>
           
              <a class="float-right col-form-label" href="{{url('')}}">Home <font style="color: gray">/ Dashboard</font></a>
        
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
<div class="content">
  <div class="container-fluid">
     @if(count($dash)>0)
    <div class="row">
      <div class="col-sm-9">
        <div class="card">
          <div class="card-header bg-dark pt-2 pb-2">
            <div class="col-md-12 text-center pb-0">
              <label id="sy" style="font-size: 17px"></label>
               <div class="col-md-12 pb-0">
                  <label id="sem"></label>
               </div>
           </div>
           
          </div>
          <div class="card-body" id="calendar">
            
          </div>
        </div>
      </div>
      <div class="col-sm-3">
       
     <div class="col-lg-12 col-6">
        <div class="small-box" style="background-color:#998693 ">
          <div class="inner">
            <h3>{{App\tbl_faculty_load::where('instructor_id',Auth::user()->id)->count()}}</h3>
            <p><b> Loads </b></p>
          </div>
          <div class="icon">
            <i class="fas fa-download"></i>
          </div>
          <a href="{{url('instructor-myschedule')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
      </div>
    </div>
    @else
    <div class="row">

        <div class="col-lg-3 col-6">
        <div class="small-box" style="background-color:#998693 ">
          <div class="inner">
            <h3>{{App\tbl_faculty_load::where('instructor_id',Auth::user()->id)->count()}}</h3>
            <p><b> Loads </b></p>
          </div>
          <div class="icon">
            <i class="fas fa-download"></i>
          </div>
          <a href="{{url('instructor-myschedule')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>
    </div>
    @endif
  </div>
  
</div>

<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
@if(count($dash) > 0)
<script>
       var date = new Date(2021, 2, 22)
        var d    = date.getDate(),
            m    = date.getMonth(),
            y    = date.getFullYear()

        var Calendar = FullCalendar.Calendar;
        var calendarEl = document.getElementById('calendar');
        var calendar = new Calendar(calendarEl, {
         
          initialView:'timeGridWeek',
          validRange: {
        start: '2021-03-22',
        end: '2021-03-28'
             },
          
          headerToolbar:false,
          themeSystem: 'bootstrap',
          hiddenDays: [0],
          slotMinTime: '07:00:00',
          slotMaxTime: '23:00:00',
          allDaySlot: false,
          dayHeaderFormat :{ weekday: 'short' },
          contentHeight:"auto",
          eventTimeFormat: {
          hour: "numeric",
          minute: "2-digit",
          meridiem: "short",
        },
          });
        calendar.removeAllEvents()
        setInterval(calendar.render(),2000)
        $.get('instructor-dash-sched',function(data){
            $.each(data.ass,function(k,a){
              $('#sy').text('SCHOOL YEAR '+a.school_year);

              var sems;
              switch(a.semester){
                  case '1st':
                     sems = 'FIRST SEMESTER';
                     break;
                  case '2nd':
                     sems = 'SECOND SEMESTER';
                     break;
                  case 'summer':
                     sems = 'SUMMER';
                     break;
              }
             $('#sem').text(sems);

          $.each(data.schedule,function(k,value){
            if (value.id == a.schedule_id)
             {
                $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
                    $.each(data.class,function(k,c){
                      if(c.id == value.class_id)
                      {

                        $.each(data.section,function(k,sec){
                          if (sec.id == c.section_id)
                           {
                            var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                     event.setProp('title',event.title+' '+sec.section_name);
                           }
                        })
                        
                      }
                    })
                    $.each(data.room,function(k,r){
                      if(r.id == value.room_id)
                      {
                        var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                     event.setProp('title',event.title+' '+r.room_description);
                      }
                    })  
                           
                  }
                      
                });
              
              
             }
          })
        })
        })
</script>
@endif

<script >
  $('#dash').addClass('active');
</script>
@endsection