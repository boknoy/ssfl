	@extends('layouts.Dean_layout')
@section('content')

<style type="text/css">
	.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal !important;
}
.fc-event-time{
  text-align: center;
}
.fc-v-event{
   transition: all .2s ease-in-out;
}

tr  td  label{
  cursor: pointer;
}
th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-event {
        font-size: 14px !important; //Your font size
        font-weight: bold;
    }
   #loading {
    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
    height: 100%;
   
    margin: auto;
    
   
    width: 100%;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
<section class="content-header pb-0">
    <div class="container-fluid">
    	<div class="row">
    		<div class="col-md-12">
    			<label style="font-size: 20px"><i class="fas fa-users"></i> List of Faculty</label>
    		</div>
    	</div>
    </div>
</section>

<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body ">
					 <table id="faculty-list" class="table table-border table-striped table-hover text-sm">
					 	<thead>
					 		<tr class="bg-dark">
					 			<th>Name</th>
					 			<th>Email</th>
					 			<th>Contact</th>
					 			<th>Degree</th>
					 			<th>Employee type</th>
					 			<th></th>
					 		</tr>
					 	</thead>
					 	<tbody>
					 		@foreach($fac as $f)
					 		@foreach($fac_info as $info)
					 		@if($f->id == $info->instructor_id)
					 			<tr>
					 				<td>{{$f->fname}} {{substr($f->mname,0,1)}}. {{$f->lname}}</td>
					 				<td>{{$f->email}}</td>
					 				<td>{{$info->contact}}</td>
					 				<td>{{App\tbl_academic::where('id',Auth::user()->department)->first()->program_code}}</td>
					 				<td>{{$info->employee_type}}</td>
					 				<td><button class="btn btn-outline-success btn-xs" onclick="Schedule('{{$f->fname}}','{{substr($f->mname,0,1)}}','{{$f->lname}}',{{$f->id}})"><i class="fas fa-clock"></i> Schedule</button></td>
					 			</tr>
					 			@endif
					 			@endforeach
					 		@endforeach
					 	</tbody>
					 </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="modal-faculty-schedule">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
      <div class="modal-header bg-secondary">
        <div class="col-md-12 text-center">
          <label class="modal-title " style="font-size: 20px" id="fac-info"> </label>
          <i class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color:red;cursor: pointer;">
          </i>
         
        </div>
      </div>
      <div class="modal-body">
      	
        	<div class="row justify-content-center" id="search">
        		<div class="form-group col-sm-4 ">
        		
        			<select class="form-control form-control-sm" id="sy" style="text-align-last: center" onchange="selvalid('sy')" >
        				<option selected="" disabled="" value="">School Year </option>
        				@php
        					$class = App\subject_schedule	::all()->unique('school_year');
        				@endphp
        					@foreach($class as $c)
        						<option value="{{$c->school_year}}">{{$c->school_year}}</option>
        					@endforeach
        			</select>
        		</div>
        		<div class="form-group col-sm-4">
        		
        			<select class="form-control form-control-sm" style="text-align-last: center" id="sem" onchange="selvalid('sem')" >
        					<option selected="" disabled="" value="">Semester</option>
        					<option value="1st">First Semester</option>
        					<option value="2nd">Second Semester</option>
        					<option value="summer">Summer</option>
        			</select>
        		</div>
        		<div class="form-group  col-sm-2">
        			
        			<button class="btn btn-primary btn-sm " onclick="Search()"><i class="fa fa-search"> Search</i></button>
        		</div>
        		<input type="hidden" id="ins">
        	</div>
        	<div id="no-data" style="display: none;">
        		<div class="alert alert-info alert-dismissible" >
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                 	No data to show.
                </div>
        	</div>
        <div class="col-sm-12">
				<div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
			</div>
        	<div class="card" id="cal" style="display:none ">
        		<div class="card-body" id="calendar-inc"> 
        				
        		</div>
        	</div>
      </div>
     
    </div>
  </div>
</div>

<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script type="text/javascript">
	$('#faculty-nav').addClass('active');
	$('#faculty-href').addClass('active');
</script>



<script>
  const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });



		var date = new Date(2021, 2, 22)
		    var d    = date.getDate(),
		        m    = date.getMonth(),
		        y    = date.getFullYear()

		    var Calendar = FullCalendar.Calendar;
		    var calendarEl = document.getElementById('calendar-inc');
		    var calendar = new Calendar(calendarEl, {
		     
		      initialView:'timeGridWeek',
		      validRange: {
		    start: '2021-03-22',
		    end: '2021-03-28'
		         },
		      
		      headerToolbar:false,
		      themeSystem: 'bootstrap',
		      hiddenDays: [0],
		      slotMinTime: '07:00:00',
		      slotMaxTime: '22:00:00',
		      allDaySlot: false,
		      dayHeaderFormat :{ weekday: 'short' },
		      contentHeight:"auto",
		      });
	function Schedule(fname,mname,lname,id){
		$('#fac-info').text(fname+' '+mname+'. '+lname)
		$('#ins').val(id);
		$('#sy').val("");
		$('#sem').val("");
		calendar.removeAllEvents();
		$('#cal').hide();
		$('#loading').hide()
		$('#modal-faculty-schedule').modal('toggle');
    $('#no-data').hide()
	}

  function selvalid(val)
  { 
      $('#'+val).css('border-color','blue')
  }

	function Search()
	{
		var sy = $('#sy').val();
		var sem = $('#sem').val();
		var id = $('#ins').val();
		if (sy == null)
		 {
		  $('#sy').css('border-color','red')

       Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });

		 	return false
		 }
		 if (sem == null)
		  {
          $('#sem').css('border-color','red')

		  	 Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester</span>',
                  });

		  	return false
		  }
		  $('#loading').show()
		  $('#cal').hide()
		  $('#no-data').hide()
		$.get('dean-get-ins-schedule/'+id+'/'+sy+'/'+sem,function(data){

			
			calendar.removeAllEvents();
		   
		    if (data.ass.length == 0)
		     {
		     		$('#no-data').show().empty().append('<div class="alert alert-info alert-dismissible" ><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h5><i class="icon fas fa-info"></i> Oops!</h5>No data to show.</div>')
		     		$('#cal').hide();
		     		$('#loading').hide()

		     }else
		     {
		     	$('#cal').show();
		     	$('#no-data').empty().hide();
		     	$('#loading').hide()

		     }
		    $.each(data.ass,function(k,a){

		    	$.each(data.sched,function(k,value){
		    		if (value.id == a.schedule_id)
		    		 {
		    		 	  $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
               
                           		$.each(data.class,function(k,c){
               				if(c.id == value.class_id)
               				{

               					$.each(data.section,function(k,sec){
               						if (sec.id == c.section_id)
               						 {
               						 	var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                 		 event.setProp('title',event.title+' '+sec.section_name);
               						 }
               					})
               					
               				}
               			})
               			$.each(data.room,function(k,r){
               				if(r.id == value.room_id)
               				{
               					var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                 		 event.setProp('title',event.title+' '+r.room_description);
               				}
               			})	
		              }
		                  
		            });
              
              
		    		 }
		    	})
		    })

		})

	}
</script>




<script >
	$('#faculty-list').DataTable({
		  columnDefs: [
    {bSortable: false, targets: [2,3,5]} 
    ]
	});
</script>


@endsection