    @extends('layouts.dean_layout')
@section('content')

<style>
  .grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1);cursor: pointer }

.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal !important;
}
.fc-event-time{
  text-align: center;
}
.fc-v-event{
   transition: all .2s ease-in-out;
}
.fc-v-event:hover { transform: scale(1.1);cursor: pointer }

  .dataTables_filter{
        float: left !important;
    }
tr  td  label{
  cursor: pointer;
}
th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-event {
        font-size: 14px !important; //Your font size
        font-weight: bold;
    }
    .no-click {
    pointer-events: none;
}

.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>


@if(count($sched) == 0)
 <div class="col-md-12 d-flex justify-content-center">
        <div class="alert alert-info alert-dismissible col-md-10 text-center mt-5">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <h5 class=""><i class="icon fas fa-info-circle"></i>Oops!</h5>
                 <h6>No data show.</h6>
            </div>
      </div>
@else
<section class="content-headerpb-0">
 <div class="container-fluid">
  <div class="row">
    <div class="col-md-2 mb-2 mt-2 ml-2">
         <button id="back" class="btn btn-outline-secondary float-left" style="display: none"><i class="fas fa-angle-left"> BACK</i> </button>
     
    </div>
    <div class="col-md-8 text-center">
       <label id="assign" style="display: none;font-size: 20px"><i class="fas "> Manage Department Schedule</i></label>
    </div>
       <div class="col-md-6">
  <label id="year" style="font-size: 20px"><i class="fas fa-calendar-alt"> Manage Class Schedule</i></label>
   </div>
  
  </div>
 </div>
</section>

<section class="content">
  <div class="container-fluid">
    <div class="row">
    
      <div class="col-sm-4" id=card_sy>
        <div class="card">
          <div class="card-header p-1 m-1 bg-dark">
            <label class="card-title col-sm-12 text-center">School Year</label>
          </div>
          <div class="card-body">
            
             @foreach($sched as $c)
            
              <div class="external-event text-uppercase btn btn-outline-secondary col-md-12" id="bg-{{$c->id}}" onclick="show('{{$c->school_year}}')" >{{$c->school_year}}</div>

            @endforeach
     
            
           
            </div>
          </div>
        </div>
        <div class="col-md-8" id="card1">
          <div class="card">
            <div class="card-header p-1 m-1 bg-dark">
              <label class="card-title col-md-12 text-center">CLASS LIST</label>
            </div>
            <div class="card-body">
                <table id="sub-sched" class="table text-center table-borderless ">
        <thead>
          <tr >
            <th id="th-title" class="pt-0 pb-0" style="font-size: 15px;"></th>
          </tr>
        </thead>
        <tbody class="row mt-2 " >
          
             
        @php
        $color=array('#998693','#b3a595','#c2ac79','#a6a47f','#9ba4a9','#ada3a4','#c6b9ab','#d5c28a','#bbbc91','#b2bbc3','#d1c4be','#d8d0b9','#e8dda9','#cecbac','#c5cfd7','#95b9c7','#737ca1','#e5e4e2','#b7ceec','#e0ffff','#fbf6d9');
        @endphp
        
         
      @foreach($class as $c)
                
                    <tr class="m-0 p-0 text-center"  >
                      <td style="border-top: none" class="pt-0" style="cursor: pointer;">
                     
                      <div class=" text-center "  style="height: 130px;width: 200px" >
                          @php
                            shuffle($color) 
                          @endphp
                          
                          <div onclick="showCalendar({{$c->id}})"  class="grow card" style="font-size: 14px;background-color: {{array_shift( $color )}};cursor: pointer;" >
                            <div class=" card-body">
                              @php
                                $temp_new = App\temp_new::where('user_id',Auth::user()->id)->where('schedule_sub',$c->id)->get();
                              @endphp
                              @foreach($temp_new as $t)
                              @if(!$t->is_read)
                              <div class="ribbon-wrapper" id="ribbon{{$c->id}}" >
                              <div class="ribbon bg-success">
                                New
                              </div>
                            </div>

                            @endif
                            @endforeach
                              <label class="col-sm-12">{{App\tbl_section::where('id',$c->section_id)->first()->section_name}}</label>
                              <label class="col-sm-12">{{years(App\temp_year_level::where('id',$c->year_level_id)->first()->year_level)}}</label>
                              <label class="col-sm-12">{{semester(App\temp_year_level::where('id',$c->year_level_id)->first()->semester)}}</label>
                            </div>
                          </div>
                   
                        </div>
                       
                      </td>
                    </tr> 
           
      @endforeach 
   </tbody>
   </table>
            </div>
          </div>
        </div>
         <div class="col-sm-12">
              <div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
          </div>
        <div class="col-md-12" style="display: none" id="card2">
          <div class="row">
            <div class="col-sm-3">
                <div class="card">
                  <div class="card-header text-center bg-dark">
                    <label id="dep">BSIT SUBJECTS </label>
                   
                  </div>
                  <div class="card-body" id="subject-list">
                    
                  </div>
                 
                </div>
                <div class="alert alert-success " id="text-send" style="display: none;">
       
             <h5 class=""><i class="icon fas fa-check"></i> SUBMITTED</h5>
                  This schedule is already<br> submitted to HR.
         
                </div>
                 <input type="hidden" id="class_id2">
                 <div class="form-group" id="btn-send" style="display: none;">
                    <button class="btn btn-success btn-block" onclick="modalSend()"><i class="fa fa-paper-plane"> Send</i></button>
                  </div>
            </div>
            <div class="col-sm-9">
              <div class="card">
            <div class="card-header p-1 m-1 bg-dark">
              <div class="col-md-12 text-center">
                 <label class="" id="sec-name-year" style="font-size: 19px;"></label><br>
                 <label id="sem-semester"></label>
              </div>
            </div>
           
       
            <div class="card-body "  id="calendar" >

            </div>
          </div>
            </div>
          </div>
        
        </div>
      </div>
    </div>
  </div>
</section>

@endif

@php
  function semester($semester)
  {
    $x="";
    switch($semester)
      {
      case '1st':
        $x="FIRST SEMESTER";
        break;
      case '2nd':
        $x="SECOND SEMESTER";
        break;
      case 'summer':
        $x="SUMMER";
        break;
    }
    return $x;
   }
  function years($year)
  {
     $x="";
    switch($year)
    {
      case '1st':
        $x="FIRST YEAR";
        break;
      case '2nd':
        $x="SECOND YEAR";
        break;
      case '3rd':
        $x="THIRD YEAR";
        break;
      case '4th':
        $x="FOURTH YEAR";
        break;
      case '5th':
        $x="FIFTH YEAR";
        break;
    }
    return $x;
  }
@endphp


{{--Modal Assign Instructor--}}
<div class="modal fade" id="modal-assign-instructor">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="m-2">
        <i class=" fas fa-book pb-1 pr-1 pt-1 ml-2"> </i><label class="modal-title" id="subject-title"> </label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 pr-2 pl-3">
        <input type="hidden" id="schedule_id" name="">
        <input type="hidden" id="subject_id" name="">
        <input type="hidden" id="class_id" name="">
        <input type="hidden" id="section_name">
        <input type="hidden" id="room">
        <form id="form-assign-instructor">
        <div class="form-group col-sm-12" >
          <select class="form-control select2 " style="text-align-last: center ;width: 100%" id="instructor">
            <option value="" disabled selected>Select Instructor</option>
          </select>
        </div>
        </form>
         <div class="col-md-12 pb-2">
           <button type="button" class="btn btn-danger btn-sm float-left" onclick="modalResched()">Reschedule</button>
        <button type="button" class="btn btn-primary btn-sm float-right" onclick="saveAssign()">Assign</button>
      </div>
      </div>
    </div>


     
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

{{--Modal Assigned Instructor--}}
<div class="modal fade" id="modal-assigned-instructor">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <label class="modal-title" ><i class="fas fa-edit"></i>Change Assigned Instructor</label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <i class="fas fa-book"></i>
         <label id="subject-title2"></label>
        </div>
        <div class="form-group col-sm-12" >
         <select class="form-control select2 " style="text-align-last: center ;width: 100%" id="instructor">
          <option value="" disabled selected>Select Instructor</option>
        </select>
        </div>
        <div class="form-group col-md-12">
          <button class="btn btn-success btn-sm float-right">Update</button>
        </div>
      </div>
  

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-assign-new-instructor">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="dep-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-trash-restor fa-lg text-warning " ></i>Are you sure you want to <br>assign this subject to <br> <label  id="instructor-name" style="text-decoration: underline;"></label>?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-success btn-xs " onclick="saveAssign2()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-send-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
     
     <div class="col-md-12 ">
      <input type="hidden" id="class_id3">
     
        <p class="text-muted" style="font-size: 14px;font-weight: lighter"><i class="fa fa-paper-plane fa-lg text-info " ></i> Do you want to submit this <br>schedule to HR<label  id="course-sec-name" style="text-decoration: underline;"></label>?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-primary btn-xs " onclick="Send()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-reschedule">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header pb-2 pt-2 bg-secondary">
       <div class="col-md-12 text-center">
           <label id="subject-resched"></label>
        <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
        </i>
       </div>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <label style="color: maroon">Recent Schedule</label>
     
             <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <div class="row pl-4 pr-4">
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" name="u-day" id="mon" value="0" disabled>
                          <label for="mon">
                            Monday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-secondary d-inline">
                          <input type="radio" name="u-day" id="tue" value="1" disabled="">
                          <label for="tue">
                            Tuesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-warning d-inline">
                          <input type="radio" id="wed" name="u-day" value="2" disabled>
                          <label for="wed">
                            Wednesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-success d-inline">
                          <input type="radio" id="thu" name="u-day" value="3" disabled>
                          <label for="thu">
                            Thursday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-maroon d-inline">
                          <input type="radio" id="fri" name="u-day" value="4" disabled>
                          <label for="fri">
                            Friday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-info d-inline">
                          <input type="radio" id="sat" name="u-day" value="5" disabled>
                          <label for="sat">
                            Saturday
                          </label>
                        </div>
                      </div>
                    </div>
                  
                 
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                      <input type="text" class="form-control"  id="u-time-start" readonly/>
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                      
                        <input type="text" class="form-control"  id="u-time-end" readonly/>
                        
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
           {{-- <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                  <label style="font-size: 16px"><i class="fas fa-chalkboard-teacher"></i> Room</label>
                </div>
                <div class="col-md-12">
                  <div class="form-group pl-4 pr-4 col-md-12">
                    <select id="u-s-rooms" class="form-control  select3" style="text-align-last: center;width: 100%;">
                     
                    </select>
                  </div>
                </div>
              </div>
            </div> --}}
          
          <label style="color: maroon">Reschedule</label>
         
            <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <div class="row pl-4 pr-4">
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" name="u-day2" id="mon2" value="0">
                          <label for="mon2">
                            Monday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-secondary d-inline">
                          <input type="radio" name="u-day2" id="tue2" value="1">
                          <label for="tue2">
                            Tuesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-warning d-inline">
                          <input type="radio" id="wed2" name="u-day2" value="2">
                          <label for="wed2">
                            Wednesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-success d-inline">
                          <input type="radio" id="thu2" name="u-day2" value="3">
                          <label for="thu2">
                            Thursday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-maroon d-inline">
                          <input type="radio" id="fri2" name="u-day2" value="4">
                          <label for="fri2">
                            Friday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-info d-inline">
                          <input type="radio" id="sat2" name="u-day2" value="5">
                          <label for="sat2">
                            Saturday
                          </label>
                        </div>
                      </div>
                    </div>
                  
                 
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                      <div class="input-group date" id="timepicker5" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker5" id="n-time-start" placeholder="--:-- --"  />
                        <div class="input-group-append" data-target="#timepicker5" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                      <div class="input-group date" id="timepicker6" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker6" id="n-time-end" placeholder="--:-- --" />
                        <div class="input-group-append" data-target="#timepicker6" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


          
      </div>
         <div class="col-md-12">
               <button type="button" id="btn-res-schedule" class="btn btn-success btn-sm float-right m-3" onclick="submitResched()"><i class="fa fa-check-circle " ></i> SUBMIT</button>
            </div>
  

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
{{--CALENDAR--}}
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<script>
  $('#schedule_menu').addClass('menu-open');
  $('#schedule_href').addClass('active');
  $('#my_department').addClass('active');
</script>


<script >

    function modalResched()
  {
   $('#modal-reschedule').modal('toggle');
  }

  function submitResched()
  {
    var schedule = $('#schedule_id').val()
    var day = $("input[name='u-day2']:checked").val();
    var time_start = $('#n-time-start').val()
    var time_end = $('#n-time-end').val()
   
    if (day == null)
       {

        Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select day </span>',
                  });
       
        return false

       }

       
      if ( time_start == "")
       {

        Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please input time start</span>',
                  });
       
        return false

       }
         if ( time_end == "")
       {

        Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please input time end</span>',
                  });
       
        return false

       }

       if (time_end == time_start)
        {
           Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Time start and Time end cannot be equal</span>',
                  });
       
        return false
        }

      var now = moment(time_end,"HH:mm a"); //todays date
      var end = moment(time_start,"HH:mm a"); // another date
      var duration = moment.duration(now.diff(end));

      var t_hr=duration.asHours();  
     
      if (t_hr <= 0)
       {
       Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Time end is invalid</span>',
                  });
       
        return false

       }

       else{

    $.ajax({

      type:'post',
      url : 'save-reched',
      data:
      {
         "_token"   : "{{ csrf_token() }}",
          "day"        : day,
          "time_start" : time_start,
          "time_end"   : time_end,
          "schedule"   : schedule,

      },
      success:function(data)
      {
         Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Submitted</span>',
                  });

         $('#modal-reschedule').modal('hide');
         $('modal-assign-instructor').modal('hide');
      },
      error:function(data)
      {

      }
    })
  }

  }





  function modalSend()
  {  
    var id = $('#class_id2').val()
     
     $('#class_id3').val(id)
    
    $('#modal-send-confirm').modal('toggle');
    
  }
  function Send()
  {
    var class_id = $('#class_id3').val();

     $.get('dean-send-schedule/'+class_id,function(data){

        Toast.fire({
          icon: 'success' ,
          title: ' Successfully Send!!',
        });

          $('#modal-send-confirm').modal('hide');
          $('#btn-send').hide()
          $('#text-send').show();

     })

    
  }

</script>


<script >

 const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });



 var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },
      contentHeight:"auto",
   
      eventClick: function(info)
        {

          $.get('dean-get-schedule/'+info.event.id,function(data){

                if (data.ass.length >0)
                 {
                   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1">You already assigned instructor in this subject</span>',
                  });

                  return false
                 }
            if (data.subject.academic_id != {{Auth::user()->department}}) {
          
               Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> You can`t assign instructor in this subject</span>',
                  });

            }else{

                $('#schedule_id').val(data.schedule.id)
                $('#subject_id').val(data.subject.id);
               $('#class_id').val(data.schedule.class_id);

                switch(data.schedule.day)
                {
                  case '0' :
                        $('#mon').prop('checked',true);
                        break;
                  case '1' :
                        $('#tue').prop('checked',true);
                        break;
                  case '2' :
                        $('#wed').prop('checked',true);
                        break;
                  case '3' :
                        $('#thu').prop('checked',true);
                        break;
                  case '4' :
                        $('#fri').prop('checked',true);
                        break;
                  case '5' :
                        $('#sat').prop('checked',true);
                        break;
                }
             
                  $('#u-time-start').val(moment(data.schedule.time_start,'hh:mm').format('h:mm A'));
                  $('#u-time-end').val(moment(data.schedule.time_end,'hh:mm').format('h:mm A'));

                
              $('#subject-title').text(data.subject.descriptive_title);
              $('#subject-resched').text(data.subject.subject+' '+data.subject.code+' '+data.subject.descriptive_title);
               $('#subject-title2').text(data.subject.descriptive_title);
               $('#section_name').val(data.section.section_name);
               $('#room').val(data.room.room_description);
             $('#modal-assign-instructor').modal('toggle');
             $('#instructor').empty().append(' <option value="" disabled selected>Select Instructor</option>  ');
              $.each(data.teacher_sub,function(key,value){
                $.each(data.teacher,function(key2,value2){
                    if (value2.id==value.instructor_id) {
                      $('#instructor').append(`<option value="${value2.id}">
                                       ${value2.fname}  ${value2.lname}
                                  </option>`);
                    }
                })
              })
            }
          })
        }

   });

  function showCalendar(id){
    

    
    calendar.removeAllEvents();
    calendar.render();
    $('#card1').hide();
    $('#card2').hide();
    $('#card_sy').hide();
    $('#back').show();
    $('#year').hide();
    $('#loading').show()

    $('#ribbon'+id).hide();

    $('#class_id2').val(id)

    var c_id = id;
   
    $.get('dean-get-sched-calendar/'+c_id,function(data){
      $('#card2').show()
      $('#loading').hide()
        $('#sec-name-year').text(data.section.section_name+' | '+data.class.school_year);
        var sems="";
        switch(data.temp1.semester)
        {
                case '1st':
                   sems = 'FIRST SEMESTER';
                   break;
                case '2nd':
                   sems = 'SECOND SEMESTER';
                   break;
                case 'summer':
                   sems = 'SUMMER';
                   break;
        }
        $('#sem-semester').text(sems);
        //$('#sem').text(sems)
        $('#subject-list').empty();
        var x=0
        var b = 0;
        $.each(data.dep_sub,function(k,sub){
          $.each(data.subject,function(k,s){
            if (s.id == sub.subject_id)
             {
              if (sub.is_assigned)
               {
                 $('#subject-list').append('<button id="sub'+s.id+'"   class="external-event text-uppercase btn btn-outline-secondary col-md-12"  disabled>'+s.subject+ ' '+s.code+' | Assigned <span class="text-lowercase">   </span></button>')
                 b++
               }else
               {
                $('#subject-list').append('<button id="sub'+s.id+'"   class="external-event text-uppercase btn btn-outline-secondary col-md-12" onclick="" style="pointer-events:none" >'+s.subject+ ' '+s.code+' <span class="text-lowercase">   </span></button>')
                x=x+1;
                b++
               }
             
             }
          })
        })
        if (x==0 && data.class_ass.length == 0)
         {
          $('#btn-send').show()
         }else
         {
           $('#btn-send').hide()
         }
         if (data.class_ass.length > 0)
          {
            $('#text-send').show()
          }else
          {
            $('#text-send').hide()
          }
          if (b == 0)
          {
            $('#btn-send').hide()
            $('#text-send').hide()
          }
        $.each(data.schedule, function(key,value) {

              
            $.each(data.subject, function(key2,value2) {
              
           
              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  time_end_hr=parseInt(res_hr);
                  time_end_min=parseInt(res_min);
                  var color="";
                  if(value2.academic_id != {{Auth::user()->department}})
                  {
                    color = "#D3D3D3";
                  }else
                  {
                    color = value.color;
                  }
                  
                          var events={
                                    id             : value.id,    
                                    title          : value2.subject+' '+ value2.code,
                                    
                                    start          : new Date(y, m, d +parseInt(value.day) , time_start_hr, time_start_min),
                                    end            : new Date(y, m, d+ parseInt(value.day), time_end_hr, time_end_min),
                                    allDay         : false,
                                 
                                    backgroundColor: color, //Success (green)
                                    

                                  }

                                    calendar.addEvent(  events );
                                    calendar.render();
                                 $.each(data.ass,function(k,a){

                                  if (a.schedule_id == value.id)
                                   {
                                    $.each(data.ins,function(k,i){
                                      if (i.id == a.instructor_id)
                                       {
                                        var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                        event.setProp('title',event.title+' '+i.fname);
                                       }
                                    })
                                     
                 
                
                                    //event.setProp('backgroundColor','#00a65a');
                                    
                                   }
                                 })
                                 $.each(data.room,function(k,r){
                                  if (value.room_id == r.id)
                                   {
                                    var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                        event.setProp('title',event.title+'\n'+r.room_description);
                                   }
                                 })
                  
                }

      })

    })
    })
  


 

   calendar.render(); 
}
</script>

<script>
  function saveAssign2()
  {
     var instructor = $('#instructor').val();
    var schedule   = $('#schedule_id').val();
    var subject    = $('#subject_id').val();
    var class_id    = $('#class_id').val();
    var section    = $('#section_name').val();
    var room      =$('#room').val();
    
            $.ajax({
 
            type : 'post',
            url  : 'dean-save-assign',
            data :
            {
              "_token"          : "{{ csrf_token() }}",
              "instructor"      : instructor,
              "schedule"        : schedule,
              "subject"         :subject,
              "class"           : class_id,
              "section"         :section,
              'room'            :room,
            },
            success:function(data)
            {
              if(data.msg != "")
              {
                Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"></span>'+data.msg,
                  });
              }
              else
              {
                 //var event = calendar.getEventSourceById(  data.schedule )
             //event.backgroundColor = 'red';
               Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1">Successfully Assigned</span>',
                  });

                  $('#modal-assign-new-instructor').modal('hide');
             
                 
                
                
                  var event = calendar.getEventById(data.schedule.id);
                 
                
                //event.setProp('backgroundColor','#00a65a');
                 event.setProp('title',event.title+' '+data.teacher.fname);
               
                
                
         
              
                
                $('#modal-assign-instructor').modal('hide');
              }
            
            },
            error:function(data)
            {
              $.each(data.responseJSON.errors,function(key,value)
              {
                  Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> </span>'+value,
                  });
              })
            },
    })
        
   /* */
  }
  function saveAssign()
  {
    var instructor = $('#instructor').val();
    var schedule   = $('#schedule_id').val();
    var subject    = $('#subject_id').val();
    var class_id    = $('#class_id').val();
    var section    = $('#section_name').val();
    var room      =$('#room').val();
     $.get('dean-ass-ins/'+instructor+'/'+subject+'/'+class_id,function(data){
          if(data == 1)
          {
             $.get('dean-ins-name/'+instructor,function(data){

                $('#instructor-name').text(data.fname+' '+data.mname.charAt(0)+'. '+data.lname);
             })

            $('#modal-assign-new-instructor').modal('show');

          }else

          {
            $.ajax({

            type : 'post',
            url  : 'dean-save-assign',
            data :
            {
              "_token"          : "{{ csrf_token() }}",
              "instructor"      : instructor,
              "schedule"        : schedule,
              "subject"         :subject,
              "class"           : class_id,
              "section"         :section,
              'room'            :room,
            },
            success:function(data)
            {
              if(data.msg != "")
              {
               
                Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"></span>'+data.msg,
                  });
              }
              else
              {
                 //var event = calendar.getEventSourceById(  data.schedule )
             //event.backgroundColor = 'red';
                Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1">Successfully Assigned</span>',
                  });
             
                
                
                $('#modal-assign-instructor').modal('hide');
                 
                  showCalendar(parseInt(data.class))
                
                //event.setProp('backgroundColor','#00a65a');
                $.each(data.assign,function(k,ass){
                     var event = calendar.getEventById(ass.schedule_id);
                    event.setProp('title',event.title+' '+data.teacher.fname);
                })

               
                
                
         
              
                
                
              }
            
            },
            error:function(data)
            {
              $.each(data.responseJSON.errors,function(key,value)
              {
                  Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"></span>'+value,
                  });
              })
            },
    })
          }
     })
   /* */
  }
</script>

  <script >

 $('#back').click(function(){
  $('#back').hide();
  $('#card_sy').show();
   $('#card2').hide();
   $('#card1').show();
   $('#side').show();
   $('#year').show();
   $('#assign').hide();
 })  

                
$('#tbl-class').DataTable({
  "paging":   false,
        "ordering": false,
        "info":     false,

});
 $('#sub-sched').DataTable({
    "paging":   false,
        "ordering": false,
        "info":     false,
         "dom":"ftip",
  });

 function show(year)
 {
  $('#card2').hide();
  $('#card1').show();
  $('#th-title').text(year)
  @foreach($sched as $c )
        if("{{$c->school_year}}"== year)
        {
          
          $('#bg-{{$c->id}}').removeClass('btn-outline-secondary');
          $('#bg-{{$c->id}}').addClass('btn-primary no-click');

        }else
        {
          $('#bg-{{$c->id}}').addClass('btn-outline-secondary');
          $('#bg-{{$c->id}}').removeClass('btn-primary no-click');
        }
    @endforeach
    var color = ['#998693','#b3a595','#c2ac79','#a6a47f','#9ba4a9','#c6b9ab','#d5c28a','#bbbc91'];
      
    var i=0;
     var t=  $("#sub-sched").DataTable();
              t.clear();
      $.get('dean-get-sy/'+year,function(data){
          $.each(data.class,function(key,c){
            $.each(data.section,function(key,s){
              if (c.section_id == s.id) 
              {
                $.each(data.year,function(key,y){
                   if (y.id == c.year_level_id)
                   {
                    $.each(data.temp,function(k,temp){
                      if (temp.is_read == true && temp.schedule_sub == c.id)
                       {

                        t.row.add([
                            '<div onclick="showCalendar('+c.id+')" class=" text-center "  style="font-size: 14px;height: 130px;width:200px;"><div  class="grow card "style="background-color:'+color[i++]+'"><div class=" card-body" > <label class="col-sm-12">'+s.section_name+'</label><label class="col-sm-12">'+years(y.year_level)+'</label><label class="col-sm-12">'+semester(y.semester)+'</label></div></div></div>'
                            ]);
                       }
                       if (temp.is_read == false && temp.schedule_sub == c.id)
                        {
                          t.row.add([
                          '<div onclick="showCalendar('+c.id+')" class=" text-center "  style="font-size: 14px;height: 130px;width:200px;"><div  class="grow card "style="background-color:'+color[i++]+'"><div class=" card-body" ><div class="ribbon-wrapper" id="ribbon'+c.id+'" ><div class="ribbon bg-success">New</div></div> <label class="col-sm-12">'+s.section_name+'</label><label class="col-sm-12">'+years(y.year_level)+'</label><label class="col-sm-12">'+semester(y.semester)+'</label></div></div></div>'
                          ]);
                        }
                    })
                     
                
                t.draw();
                   }
                })
            
               
             
              }
            
            })
             

          })
      })
    
 }
 function semester(semester)
  {
    var x="";
    switch(semester)
    {
      case '1st':
        x="FIRST SEMESTER"
        break;
      case '2nd':
        x="SECOND SEMESTER"
        break;
      case 'summer':
        x="SUMMER"
        break;
    }
    return x;
  }
  function years(year)
  {
    var x="";
    switch(year)
    {
      case '1st':
        x="FIRST YEAR"
        break;
      case '2nd':
        x="SECOND YEAR"
        break;
      case '3rd':
        x="THIRD YEAR"
        break;
      case '4th':
        x="FOURTH YEAR"
        break;
      case '5th':
        x="FIFTH YEAR"
        break;
    }
    return x;
  }
//$("#calendar").width(1000).css({'margin-left': 'auto','margin-right': 'auto'});


</script>

<script>


$('#timepicker5').datetimepicker({
      format: 'LT',
      stepping: 30,
     
      disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
      
    })
$('#n-time-start').val("");
$('#timepicker6').datetimepicker({
      format: 'LT',
      stepping: 30,
       disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
    })
$('#n-time-end').val("");

</script>

@if(session('view'))
  <script>
    showCalendar({{session('view')}})
  </script>
@endif
@endsection