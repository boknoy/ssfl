@extends('layouts.Dean_layout')

@section('content')
<style type="text/css">
.noBorder {
    border: 0px;
    padding:0; 
    margin:0;
    border-collapse: collapse;
}

</style>
<section class="content-header pb-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<label style="font-size: 20px"><i class="fas fa-file-download"></i> My Schedule</label>
			</div>
		</div>
	</div>
</section>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body pb-1">
						<div class="row justify-content-center">
							<div class="col-md-5">
								<div class="form-group row">
									<label class="col-md-4 col-form-label" style="font-size: 15px">School Year</label>
							   <div class="col-md-8">
							   		<select class="form-control " id="sy">
									<option selected="" disabled="" value="0"> --Select--</option>
									@php
			        					$class = App\subject_schedule	::all()->unique('school_year');
			        				@endphp
		        					@foreach($class as $c)
		        						<option value="{{$c->school_year}}">{{$c->school_year}}</option>
		        					@endforeach
 
								</select>
							   </div>
							</div>
							</div>
						    <div class="col-md-5">
						    		<div class="form-group row">
								<label class="col-form-label col-md-4 text-center" style="font-size: 15px">Semester</label>
								<div class="col-md-8">
									<select class="form-control " id="sem">
									<option selected="" disabled="" value="0"> --Select--</option>
								<option value="1st">First Semester</option>
									<option value="2nd">Second Semester</option>
									<option value="summer">Summer</option>
                            
								</select>
								</div>
							</div>
						    </div>
							<div class="form-group col-md-2">
							     <button class="btn btn-outline-primary col-md-10" onclick="search()"><i class="fas fa-search"></i> Search</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="display: none" id="loads">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<table class="table table-bordered text-sm table-sm">
						  	 <thead>
						  	  <tr>
						  	  	<th class="text-center" colspan="4">BASIC LOADS</th>
						  	  	<th class="text-center" colspan="6">TEACHING LOADS</th>
						  	  </tr>
						  	  <tr style="font-size: 12px">
						  	  
						  	  	<th>Subject Code</th>
						  	  	<th>Descriptive Title</th>
						  	  	<th>Course & Year</th>
						  	  	<th>Day</th>
						  	  	<th>Time</th>
						  	  	<th>Total No. of Students</th>
							  	  	<th>Room</th>
							  	  	<th>Units</th>
							  	  	<th>Total No. of Hours</th>
						  	  </tr>
						  	 </thead>
						  	 <tbody id="tbl-load" >
						  	  
						  	 
						  	 </tbody>
						  	 
						    </table>
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="{{asset('plugins/moment/moment.min.js')}}"></script>


<script>

 const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });


	function search()
	{
		var sem = $('#sem').val()

		var sy = $('#sy').val()
		
		 if (sy == null)
		  {
		  	$('#sy').css('border-color','red')

		     Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });

		  	return false
		  }
		 	if (sem == null)
		 {
		 	$('#sem').css('border-color','red')

		 	   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester</span>',
                  });

		 	return false
		 }
		 $('#loads').show();
		 $.get('get-my-loads/'+sy+'/'+sem,function(data){
		 		$.each(data.loads1,function(k,l1){
		 			
		 			$.each(data.loads2,function(k1,l2){
		 				if (l2.section == l1.section)
		 				 {
		 				 	
		 				 	$.each(data.subject,function(k,sub){

		 				 		if (sub.id == l2.subject_id)
		 				 		 { 
		 				 		 	var i=1
		 				 	        $.each(data.loads,function(k,l){

		 				 		 		if (l.subject_id == sub.id)
		 				 		 		 {
		 				 		 		 	i++
		 				 		 		 	//$('#tbl-load').append('<tr><td>'+days(l.day)+'</td><td></td></tr>')
		 				 		 		 }
		 				 		 	 })
		 				 		 	$('#tbl-load').append('<tr id="tr'+l2.id+'" class="p-0 m-0"><td >'+sub.subject+' '+sub.code+'</td><td >'+sub.descriptive_title+'</td><td>'+l1.section+'</td><td class="p-0 mb-0"><table id="tbl'+l2.id+'" class="table noBorder" ></table></td><td class="p-0"><table id="tbl2'+l2.id+'" class="table noBorder"></table></td><td>a</td><td  class="p-0"><table id="tbl3'+l2.id+'" class="table noBorder"></table></td><td >'+sub.units+'</td><td class="" id="td_hr'+l2.id+'"></td>/tr>')

		 				 		var t_hr = 0
						 		   $.each(data.loads,function(k,l){

		 				 		 		if (l.subject_id == sub.id)
		 				 		 		 {
		 				 		 		 	       
													    var now = moment(l.time_end,"HH:mm:ss"); //todays date
														var end = moment(l.time_start,"HH:mm:ss"); // another date
														var duration = moment.duration(now.diff(end));
														t_hr+=duration.asHours()
											$('#tbl3'+l2.id).append('<tr ><td class="p-0" style="border-top:none">'+l.room+'</td></tr>')
		 				 		 		 	$('#tbl'+l2.id).append('<tr  style="width:100%;height:100%;" ><td class="p-0" style="border-top:none">'+days(l.day.toString())+'</td></tr>')
		 				 		 		 	$('#tbl2'+l2.id).append('<tr><td class="p-0" style="border-top:none;border-left:none;border">'+moment(l.time_start,['HH:mm ']).format(" hh:mm ")+'-'+moment(l.time_end,['HH:mm A']).format(" hh:mm A")+'</td></tr>')
		 				 		 		 }
		 				 		 	 })
						 				
		 				 		 		
		 								$('#td_hr'+l2.id).text(t_hr)
						 		 		 
						 			}
						 		})
		 				 	
		 				 }
		 				
		 			})
		 			
		 		})
		 })
	}



	function days(val)
	{
		day ="";
		switch(val)
			{

				
				case '0':
					day ='MON';
					break;
				case '1':
					day ='TUE';
					break;
				case '2':
					day ='WED';
					break;
				case '3':
					day ='THU';
					break;
				case '4':
					day ='FRI';
					break;
				case '5':
					day ='SAT';
					break;
				
			}
			return day;
	}
</script>








@endsection