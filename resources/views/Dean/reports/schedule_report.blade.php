@extends('layouts.Dean_layout')
@section('content')
<style type="text/css">
	
.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
.fc-event-time{
  text-align: center;
}


.fc-event {
        font-size: 15px !important;
        font-weight: bold;
    }

#loading {
    background: #f4f4f2 url("img/page-bg.png") repeat scroll 0 0;
    height: 100%;
   
    margin: auto;
    
   
    width: 100%;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
<section class="content-header">
  <div class="content-fluid">
  	<div class="col-md-12">
  	  <div class="row">
  	     <div class="col-md-12">
  	     	<label style="font-size: 19px"><i class=" fas fa-file"></i> Schedule Report</label>
  	     </div>
  	  </div>	
  	</div>
  </div>
</section>


<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body row d-flex justify-content-center pb-1">
					  <div class="form-group col-md-2">
					    <select id="school-year" class="form-control form-control-sm" style="text-align-last: center" onchange="filter()">
					    	<option selected="" disabled="" value="0"> -- School Year --</option>
					    	@php
					    		$sub = App\subject_schedule::all()->unique('school_year');
					    	@endphp
					    	@foreach($sub as $s)
					    		<option value="{{$s->school_year}}">{{$s->school_year}}</option>

					    	@endforeach
					    </select>
					  </div>
					    <div class="form-group col-md-2">
					    <select id="course" class="form-control form-control-sm" style="text-align-last: center" onchange="filter()">
					    	<option selected="" disabled="" value="0">-- Course --</option>
					    	@php

					    		$curr = App\tbl_curriculum::all()->where('academic_id',Auth::user()->department)->unique('course_code');
					    	@endphp

					    	@foreach($curr as $c)
					    		<option value="{{$c->course_code}}">{{$c->course_code}}</option>
					    	@endforeach
					    </select>
					  </div>
					    <div class="form-group col-md-2 ">
					    <select id="sem" class="form-control form-control-sm" style="text-align-last: center" onchange="filter()">
					    	<option disabled="" selected="" value="0">-- Semester --</option>
					    	<option value="1st">First Semester</option>
					    	<option value="2nd">Second Semester</option>
					    	<option value="summner">Summer</option>
					    </select>
					  </div>
					    <div class="form-group col-md-2">
					    <select id="year-level" class="form-control form-control-sm" style="text-align-last: center"  onchange="filter()" >
					    	<option selected="" disabled="" value="0">-- Year Level --</option>
					    	<option value="1st">First Year</option>
					    	<option value="2nd">Second Year</option>
					    	<option value="3rd">Third Year</option>
					    	<option value="4th">Fourth Year</option>
					    	<option value="5th">Fith Year</option>


					    </select>
					  </div>
					   <div class="form-group col-md-2">
					    <select id="sec" class="form-control form-control-sm" style="text-align-last: center;" disabled="">
					    	<option selected="" disabled="" value="0">Section</option>
					    	

					    </select>
					  </div>
					 
					   <div class="form-group col-md-2">
					     <button class="btn btn-success btn-sm btn-sm col-md-9" id="search" onclick="Search()"><i class="fas fa-search"></i> Search</button>
					  </div>
				    </div>
				</div>
			</div>
		</div>
       
           		<div class="col-md-12 justify-content-center"  >
				<div class="alert alert-info alert-dismissible" id="no-data" style="display: none">
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                    <h4>No data to show</h4>
                </div>
		
           </div>
			

			<div class="col-sm-12">
				<div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
			</div>
		<div class="row" id="show1" style="display: none;">
			<div class="col-md-12 mb-2">
				<input type="hidden" id="class_id2">
				<button class="btn btn-info float-right " onclick="Print()"><i class="fas fa-print"></i> Print</button>
			</div>
			<div class="col-md-12" id="reports2">
				
				   <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.css')}}">


  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
   <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
       <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
       <script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<style type="text/css">
	.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
.fc-event-time{
  text-align: center;
}


.fc-event {
        font-size: 15px !important;
        font-weight: bold;
    }
    .fc a{
  text-decoration: none !important;
  border: none;
  color:none;
}

</style>
				<div class="card" >
					<div class="card-header bg-dark pt-1 pb-1">
						<div class="col-md-12 text-center">
							<label id="s_y" style="font-size: 19px"></label>
							<div class="col-md-12">
								<label id="s-sem"></label>
							</div>
						</div>
					</div>
					<div class="card-body" id="calendar">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>







<script>
$('#report_menu').addClass('menu-open');
$('#report_href').addClass('active');
$('#schedule_report').addClass('active');
</script>

<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>


<script >
	
function Print()
{
      var class_id =$('#class_id2').val()
	  window.open("{{url('print-schedule')}}/"+class_id);
}
</script>



<script>

	  const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });






	       var date = new Date(2021, 2, 22)
		    var d    = date.getDate(),
		        m    = date.getMonth(),
		        y    = date.getFullYear()

		    var Calendar = FullCalendar.Calendar;
		    var calendarEl = document.getElementById('calendar');
		    var calendar = new Calendar(calendarEl, {
		     
		      initialView:'timeGridWeek',
		      validRange: {
		    start: '2021-03-22',
		    end: '2021-03-28'
		         },
		      
		      headerToolbar:false,
		      themeSystem: 'bootstrap',
		      hiddenDays: [0],
		      slotMinTime: '07:00:00',
		      slotMaxTime: '23:00:00',
		      allDaySlot: false,
		      dayHeaderFormat :{ weekday: 'short' },
		      contentHeight:"auto",
		      eventTimeFormat: {
				  hour: "numeric",
				  minute: "2-digit",
				  meridiem: "short",
				},
		      });
		    setInterval(calendar.render(),1000)

		    function Search()
		    {
		    	var year = $('#school-year').val()
		    	           $('#s_y').text('SY - '+year);
		    	var course = $('#course').val()
		    	var level = $('#year-level').val()

		    	 var yl="";
		    	    switch(level)
		    	    {
		    	    	case '1st':
		    	    	     yl = 'FIRST YEAR';
		    	    	     break;

		    	    	case '2nd':
		    	    	     yl = 'SECOND YEAR';
		    	    	     break;

		    	    	case '3rd':
		    	    	     yl = 'THIRD YEAR';
		    	    	     break;
		    	    	case '4th':
		    	    	     yl = 'FOUR YEAR';
		    	    	     break;
		    	    	case '5th':
		    	    	     yl = 'FIFTH YEAR';
		    	    	     break;
		    	    }
		    	    
		    	   

		    	var sem = $('#sem').val()

                var sems="";
		    	    switch(sem)
		    	    {
		    	    	case '1st':
		    	    	     sems = 'FIRST SEMESTER';
		    	    	     break;

		    	    	case '2nd':
		    	    	     sems = 'SECOND SEMESTER';
		    	    	     break;

		    	    	case 'summer':
		    	    	     sems = 'SUMMER';
		    	    	     break;
		    	    }

		    	          $('#s-sem').text(yl+' | '+sems);

		    	var sec = $('#sec').val()


		    	if (year == "" || year == null)
		    	 {
		    	 	
			 	   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });

		    	 	return false
		    	 }
		    	 else if (course == "" || course == null)
		    	 {
		    	 	
			 	   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select course </span>',
                  });

		    	 	return false
		    	 }
		    	   else if(sem == "" || sem == null)
		    	  {
		    	  	
			 	   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester</span>',
                  });

		    	  	return false
		    	  }

		    	  else if(level == "" || level == null)
		    	  {
		    	  	
			 	   Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select year level</span>',
                  });

		    	  	return false
		    	  }
		    	  else if (sec == null)
		    	  {
		    	  	 Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select section</span>',

                  });
		    	  	 return false
		    	  }
		    	
		    	  calendar.removeAllEvents()
		    	  $('#show1').hide()
		    	  $('#loading').show()
		    	  $.get('get-class-schedule/'+year+'/'+course+'/'+sem+'/'+level+'/'+sec,function(data){
		    	  		$('#show1').show()
		    	  		$('#loading').hide()

		    	  		$.each(data.f1,function(k,f){
		    	  			$.each(data.sched,function(k,value){
		    	  				if(f.class_id == value.class_id)
		    	  				{ 
		    	  					$('#class_id2').val(value.class_id)
		    	  					   $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                        id:value.id,    
                        title     : value2.subject+' '+value2.code,
                        start     : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end       : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay    : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
               			$.each(data.class,function(k,c){
               				if(c.id == value.class_id)
               				{

               					$.each(data.section,function(k,sec){
               						if (sec.id == c.section_id)
               						 {
               						 	var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                 		 event.setProp('title',event.title+' '+sec.section_name);
               						 }
               					})
               					
               				}
               			})
               			$.each(data.room,function(k,r){
               				if(r.id == value.room_id)
               				{
               					var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                 		 event.setProp('title',event.title+' '+r.room_description);
               				}
               			})	
                           
		              }
		                  
		            });
		    	  				}
		    	  			})
		    	  		})
		    	  })
		    }


		    function filter()
		    {
		    	var sem = $('#sem').val()
		    	var year = $('#school-year').val()
		    	var course = $('#course').val()
		    	var level = $('#year-level').val()
		    	$('#show1').hide()
		    	if (year == null)
		    	 {
		    	 	
		    	 	return false
		    	 }
		    	 if (sem == null)
		    	 {
		    	 	
		    	 	return false
		    	 }
		    	 if (course == null)
		    	 {
		    	 	
		    	 	return false
		    	 }
		    	  if (level == null)
		    	 {
		    	 	
		    	 	return false
		    	 }
		    	  $('#loading').show()
		    	  $('#no-data').hide()
		    	 $.get('get-class-schedule/'+year+'/'+course+'/'+sem+'/'+level,function(data){
		    	 	$('#loading').hide()
		    	 		if (data.length >0) 
		    	 		{
		    	 			$('#sec').prop('disabled',false)
		    	 			$('#sec').empty()
		    	 			$.each(data,function(k,sec){
		    	 				$('#sec').append('<option value="'+sec.section+'">'+sec.section+'</option>')
		    	 			})
		    	 			$('#no-data').hide()
		    	 		}
		    	 		else{
		    	 			$('#no-data').show()
		    	 			$('#sec').empty().append('<option value="0" selected disabled>Section</option>')
		    	 			$('#sec').prop('disabled',true)
		    	 		}
		    	 })	
		    }
		    
		    function Print2()
	{
		var prtContent = document.getElementById("reports2");
		//var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    	var WinPrint = window.open("");
    	//WinPrint.document.open()
    	
    	WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body>'+prtContent.innerHTML+'</body></html>')
    	  //WinPrint.focus();
    

    	//WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.title= $('#course').val();
    
       WinPrint.document.close();
       WinPrint.focus();

        WinPrint.print();
       WinPrint.close();
	}
</script>

@endsection