<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{$section->section_name}}-{{$class->school_year}}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
   
 <link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>

   <link rel="stylesheet" href="{{asset('plugins/fullcalendar/main.css')}}">


  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  
  <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
       <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>




 <style type="text/css">
  .fc-event-title 
  {
    text-align: center;
    padding: 0 1px;
    white-space: normal;
    font-weight: bold;
    font-size: 20px;
   }
.fc-event-time
{
  font-size: 1.5em !important;
  text-align: center;
  font-weight: bold;
}
        @media print { 
            .visible-print {
                display: inherit !important;
            }

            .hidden-print {
                display: none !important;
            }
              body, html, #wrapper {
     width: 100%;
    }
 
      
   

        }
        
.fc a{
  text-decoration: none !important;
  border: none;
  color:none;
}


th.fc-timegrid-axis:after{
  content: "Time";
 }
 

    </style>

</head>
<body>

  <!-- Main content -->


       <div class="col-md-12 text-center">
              <div class="col-md-12">
          <img src="{{asset('/img/spc.png') }}" width="100px" height="90px" style="position: absolute;margin-left: -175px">
        
       <strong style="font-size: 20px;font-family: Italic;"> Southern Philippines College</strong><br>
        <span style="font-size: 16px;font-family: Italic;font-weight: bold">Julio Pacana Street, Licuan, Cagayan de Oro City</span><br>
      
        <label class="pt-2 pb-1" style="font-size: 16px">{{$academic->college}}</label><br>
      
      
        
     
      </div>
                 <label id="program-name"  style="font-size: 16px">{{$academic->program_name}}</label><br>
                 <label id="class-title" style="font-size: 15px">{{$section->section_name}} | {{$class->school_year}}</label><br>
                  <label  style="font-size: 15px"> {{year($temp->year_level)}} | {{sem($temp->semester)}}</label>
             </div>
    <!-- title row -->

 <div id="calendar" style="width: 1000px;border-style :none"></div>
   

<script src="{{asset('plugins/moment/moment.min.js')}}"></script>

<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
@php
function sem($val)
{
    $sems="";
              switch($val)
              {
                case '1st':
                     $sems = 'FIRST SEMESTER';
                     break;

                case '2nd':
                     $sems = 'SECOND SEMESTER';
                     break;

                case 'summer':
                     $sems = 'SUMMER';
                     break;
              }
              return $sems;

}

function year($val){
  $yl="";
              switch($val)
              {
                case '1st':
                     $yl = 'FIRST YEAR';
                     break;

                case '2nd':
                     $yl = 'SECOND YEAR';
                     break;

                case '3rd':
                     $yl = 'THIRD YEAR';
                     break;
                case '4th':
                     $yl = 'FOUR YEAR';
                     break;
                case '5th':
                     $yl = 'FIFTH YEAR';
                     break;
              }
              return $yl;
              
             

         

              
}

@endphp

<script>

   var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },
       contentHeight:"auto",

   });
   // calendar.removeAllEvents(); 
    var room="";
     var room_name;
     var bldg="";

              var x=0;
            @foreach($sched as $s)
              
                   @foreach($room as $r)
                      @if($s->room_id == $r->id)
                      
                        room = "{{$r->room_description}}";
                        bldg = "{{$r->building}}";
                      @endif
                   @endforeach
            @foreach($subject as $sub)

              @if ($s->subject_id == $sub->id)

                 var str_hr = "{{$s->time_start}}"; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min ="{{$s->time_start}}"; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = "{{$s->time_end}}"; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = "{{$s->time_end}}"; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( "{{$s->day}}");
                 
                  var events={
                           id:{{$s->id}},    
                        title          : "{{$sub->subject}} {{$sub->code}}",
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: "{{$s->color}}", //Success (green)
                        

                        };
                         calendar.addEvent(  events );
                        calendar.render();  
               
                 // alert("{{$s->day}}")
              
              @endif
            @endforeach
              calendar.render();  
               @endforeach
 //$("#calendar").hieghth(100%);
    
</script>
<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
