@extends('layouts.Hr_layout')
@section('content')

<style>

.dataTables_filter > label{
	width: 100%;
	text-align:center;
}
.grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1);cursor: pointer }
/*p {
  margin-bottom: 1em;
  color: #fff;
  font-size: 15px;
}*/
	#print {
display:block;
}
#loading {
    
    height: 100%;
   
    margin: auto;
    
   
    width: 100%;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
	<section class="content-header pb-0">
		<div class="container-fluid">
			 <div class="col-md-12 ">
			 	<div class="row">
			 		<div class="col-md-12">
			 			<label id="title-request" style="font-size: 20px;"><i class="fas ">Approval Schedule</i></label>
			 		</div>
			 		<div class="col-md-2 pb-2">
			 			<button class="btn btn-outline-secondary" style="display: none;"  id="goback" onclick="goback()"> <i class="fa fa-angle-left"> BACK</i> </button>
			 		</div>
			 		<div class="col-md-8 text-center ">
			 			<label  style="display: none;font-size: 18px;" id="approve">Schedule's Pre-Assessment </label>
			 		</div>
						
					</div>
					
			
			
		
		</div>
		</div>
	</section>

	<div class="content  ">
			<div class="container-fluid">
				<div class="col-sm-12 ">
				<div id="loading" class="mt-5" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
			</div>
				<div class="col-sm-12 " id="card1" style="display: none">
					<div class="row ">
						<div class="col-sm-12  ">
							<div class="card card-dark">
							   
								<div id="over1" class="card-body table-responsive" >
									<table id="tbl-approval" class="table table-hover table-striped table-border " >
										<thead>
										<tr class="bg-dark">
											<th width="220px">Name</th>
											<th width="200px">Department</th>
											<th width="200px">Status</th>
											<th >Action</th>
										</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								
								
					          </div> 
							</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12" id="card2" style="display: none">
							<div class="card"  >
								<div class="card-header text-center p-1 bg-dark">
									<label id="teacher-name" ></label>
								</div>
								<div class="card-body table-responsive" id="card-approval">
									<table id="tbl-teacher-approval" class="table table-striped table-hover text-sm">
											<thead>
												<th >Assigned By</th>
												<th >Subject Code</th>
												<th>Room</th>
												<th>Schedule</th>
												
												<th >Action</th>
											</thead>
											<tbody >
												
											</tbody>
									</table>
									
								</div>
							</div>{{--PENDING--}}

							
							</div>
			</div>
		</div>


<div class="modal fade" id="modal-approval">
  <div class="modal-dialog modal-lg">
  	<div class="modal-content" >
  	  <div class="modal-header  bg-secondary pb-2 pt-3">
  	  	<div class="col-md-12 text-center">
  	  		<label style="font-size: 20px">Subject for Approval</label>
  	  		 <i class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
               
              </i>
  	  	</div>
  	  </div>
          
            <div class="modal-body">
            	<form id="form-approval">
					<input type="hidden" id="teacher_id">
					<input type="hidden" id="schedule_id">
					<input type="hidden" id="subject_id">
					<input type="hidden" id="ass">
					<input type="hidden" id="year_level">
					<input type="hidden" id="class_id">
				    <div class="col-md-12">
				    	<div class="row">
				    		<div class=" col-md-6 form-group">
				    			<label class="col-form-label">INSTRUCTOR: <span class="text-secondary" id="approval-name" style="font-weight: 600"></span></label>
				    		</div>
				    		<div class="col-md-6">
				    			<div class="row">
				    				<div class="col-sm-9 text-right">
				    					<label class=" col-form-label">NO. OF STUDENTS: </label>
				    				</div>
				    				<div class="col-sm-3">
				    					<input type="number" id="no_students" min="1" class="form-control form-control-sm ">
				    				</div>
				    			</div>
				    		</div>
				    	</div>
				    	<div class="card col-md-12">
				    		<div class="card-body row">
				    			<div class=" col-md-4 pb-0">
				    				<label >SCHOOL YEAR: </label>
				    				<label class="text-secondary"  id="tyear" style="font-weight: 600"></label>
				    			</div>
				    			<div class=" col-md-4">
				    				<label>COURSE & YEAR: </label>
				    				<label class="text-secondary"  id="tcourse_year" style="font-weight: 600"></label>
				    			</div>
				    			<div class=" col-md-4 ">
				    				<label >SEMESTER: </label>
				    				<label class="text-secondary"  id="tsemester" style="font-weight: 600"></label>
				    			</div>
				    		</div>
				    	</div>
				    	<div class="card col-md-12">
				    		<div class="card-body row">
				    			<div class=" col-md-4">
				    				<label>SUBJECT CODE: </label>
				    				<label class="text-secondary" id="tsubject" style="font-weight: 600"></label>
				    			</div>
				    			<div class=" col-md-8 ">
				    				<label>DESCRIPTIVE TITLE :</label>
				    				<label class="text-secondary  " id="tdes_title" style="font-weight: 600"></label>
				    			</div>
				    		</div>
				    	</div>
				    	<div class="card col-md-12">
				    		<div class="card-body row">
				    			<div class=" col-md-4">
				    				<label>UNITS :</label>
				    				<label class="text-secondary" id="tunits" style="font-weight: 600"></label>
				    			</div>
				    			<div class="col-md-4">
				    				<label>DAY/TIME: </label>
				    				<label class="text-secondary" id="ttime" style="font-weight: 600"></label>
				    			</div>
				    			<div class=" col-md-4">
				    				<label>ROOM RO.: </label>
				    				<label class="text-secondary" id="troom" style="font-weight: 600"></label>
				    			</div>
				    		</div>
				    	</div>
				    </div>
				</form>
				<div class="form-group col-md-12 ">
					<button type="button" class="btn btn-success float-right" onclick="saveLoads()"><i class="fas fa-check"></i> Approve</button>
				</div>
			</div>
          
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    

<div class="modal fade" id="modal-reject-schedule">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="a-id">
     <div class="col-md-12 ">
     
        <p  class="text-muted p-0" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " >&nbsp; </i><label  id="academic-name" style="text-decoration: underline;"></label>  will be rejected
        	<br>
          Do you want to proceed?
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeAcademic()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>
  

<script src="{{asset('plugins/moment/moment.min.js')}}"></script>

  <script type="text/javascript">
   $('#approval_nav').addClass('active');
    $('#approval').addClass('active');
  </script>



 <script>

function tConv24(time24) {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h = (H % 12) || 12;
  h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
};




var cout=0;

$('#loading').show()
	$('#card1').hide()
	var x=0
 function update()
{	


	$.get('hr-approval',function(data){
		$('#loading').hide()
		            if (x==0)
					 {
					 	$('#card1').show()
					 	x=1
					 }
		$.each(data.ass,function(key,value){

				var i=0;
				var str;
				var dep;
				var fname;
				var mname;
				var lname;
				var t_id;

			$.each(data.teacher,function(key,t){
                
               
				
				
				if(t.id == value.instructor_id)
				{
					 t_id=t.id;
					 fname=t.fname;
					 mname=t.mname;
					 lname=t.lname;
					$.each(data.academic,function(key,a){
					   
						if(a.id == t.department)
						{
							dep=a.program_code;
						
					      $.each(data.pen,function(key,pen)
						   {
						   	
							   if(pen.instructor_id == t.id)
							   {
								   i+=1;
							
							   

								  
							
							
										
							      }
							})
							 
					
						}
					})
				}
			})


				if(cout <=0){
					var tt=  $("#tbl-approval").DataTable();
							
											

										var tt=$('#tbl-approval').DataTable();
											tt.row.add([
												fname+' '+mname.charAt(0)+'. '+lname,
												dep,
												'<span class="badge badge-warning" ><span  id="pen'+t_id+'">'+i+'</span> pending</span>',
										'<div class="btn-group pt-0 pb-0">'+
											
											'<a href="#" class="btn btn-outline-primary btn-xs" onclick="show('+t_id+')"><i class="fa fa-tasks fa"></i> Manage</a>',
										]).node().id = 'tr'+t_id;
								tt.draw();
							cout -=1;
				}
				if(cout != data.cout && cout >0){

					
						var tt=  $("#tbl-approval").DataTable();
							
											tt.row('#tr'+t_id).remove().draw();

										var tt=$('#tbl-approval').DataTable();
											tt.row.add([
												fname+' '+mname.charAt(0)+'. '+lname,
												dep,
												'<span class="badge badge-warning"><span  id="pen'+t_id+'">'+i+'</span> pending</span>',
										'<div class="btn-group pt-0 pb-0">'+
											
											'<a href="#" class="btn btn-outline-primary btn-xs" onclick="show('+t_id+')"><i class="fa fa-tasks fa"> Manage</i></a>',
										]).node().id = 'tr'+t_id;
								tt.draw();

				}
				
				
					
								  


		})
		cout = data.cout;
		
	})
			//clearInterval(myInterval)
 }	 

 var myInterval=	setInterval("update()", 1000);

//update();


 function show(id)
{
	clearInterval(myInterval);
	/*@foreach($teacher as $t)

	if (id == {{$t->id}}) 
	{
		$('#teacher{{$t->id}}').addClass('bg-success');
		
	}else{
		$('#teacher{{$t->id}}').removeClass();
	}

	@endforeach*/
	var table = $('#tbl-teacher-approval').DataTable();

//clear datatable
   table.clear().draw();
	$('#card1').hide();
	$('#approval-title').hide();
	$('#card2').show();
	$('#card3').hide();
	$('#goback').show();
	$('#title-request').hide();
	$('#approve').show();
    var day;
    var status;
    var t_units;
    var t_hours;

	
	$.get('hr-assign-instructor/'+id,function(data){
			$('#teacher-name').text(data.teacher.fname+' '+data.teacher.mname.charAt(0)+'. '+data.teacher.lname);
			$.each(data.ass,function(key,value){

				if(value.is_approved==false)
				{
				  $.each(data.schedule,function(key,s){
					if (s.id == value.schedule_id) 
					{

						
					
						$.each(data.academic,function(key,a){
							if (a.id == value.submitted_by) 
							{

								$.each(data.subject,function(key,sub){
									if (s.subject_id == sub.id)
									 {
									 	
									 	var t=  $("#tbl-teacher-approval").DataTable();
													t.row.add([
									                a.program_code,
									                sub.subject +' '+sub.code+' ',
									                value.room,
									                days(value.day.toString())+' | '+tConv24(s.time_start)+'-'+tConv24(s.time_end),
									                
									                '<div class="btn-group">'+
									                '<button class="btn btn-outline-success btn-xs" onclick="approved('+value.id+')"><i class="fas fa-check-circle"></i> Approve</button>'+
									               // '<button class="btn btn-outline-danger btn-xs" onclick="modalReject('+value.id+')"><i class="fa fa-trash"></i> Reject </button>'+
									                '</div>',
									                ]).node().id = 'tr'+value.id;
									                t.draw();
									              
									 }
								})
							}
						})
					}
				  })
				}
				
			
			})
	})


setTimeout(function(){ myInterval = setInterval(  "update()", 2000 ); }, 2000);
//clearInterval(inter1);
 
}

function days(val)
{
	var day =""
	switch(val)
			{

				
				case '0':
					day ='MON';
					break;
				case '1':
					day ='TUE';
					break;
				case '2':
					day ='WED';
					break;
				case '3':
					day ='THU';
					break;
				case '4':
					day ='FRI';
					break;
				case '5':
					day ='SAT';
					break;
				
			}
			return day
}

function modalReject(id){
	$('#modal-reject-schedule').modal('show');
}

function approved(id)
{

		$.get('hr-get-approval-subject/'+id,function(data){
			$('#approval-name').text(data.teacher.fname +' '+ data.teacher.lname)
			$('#tcourse_year').text(data.section.section_name)
			$('#tsubject').text(data.subject.subject+' '+data.subject.code)
			$('#tunits').text(data.subject.units)
			$('#tdes_title').text(data.subject.descriptive_title)
			$('#year_level').val(data.year.year_level)

			
			var sem="";
			switch(data.year.semester)
			{
				case '1st':
				   sem = 'FIRST SEMESTER';
				   break;
				case '2nd':
				   sem = 'SECOND SEMESTER';
				   break;
				case 'summer':
				   sem = 'SUMMER';
				   break;
			}
			$('#tsemester').text(sem)

			$('#tyear').text(data.class.school_year)
			var day;
			switch(data.schedule.day)
			{

				
				case '0':
					day ='MON';
					break;
				case '1':
					day ='TUE';
					break;
				case '2':
					day ='WED';
					break;
				case '3':
					day ='THU';
					break;
				case '4':
					day ='FRI';
					break;
				case '5':
					day ='SAT';
					break;
				
			}
			$('#tday').text(day)
			$('#ttime').text(day+' | '+moment(data.schedule.time_start,['HH:mm']).format("hh:mm")+'-'+ tConv24(data.schedule.time_end))
			$('#troom').text(data.room.room_description)
			$('#year').val(data.year.id)
			$('#teacher_id').val(data.teacher.id)
			$('#schedule_id').val(data.schedule.id)
			$('#subject_id').val(data.subject.id)
			$('#ass').val(data.ass.id)
			$('#class_id').val(data.class.id)

             			

		})




	$('#modal-approval').modal('toggle')
}


function saveLoads()
{
	 const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    }); 

	clearInterval(myInterval);

		var teacher_id  = $('#teacher_id').val()
		var schedule_id = $('#schedule_id').val()
		var subject_id  = $('#subject_id').val()
		var ass         = $('#ass').val()
		var class_id    = $('#class_id').val()
		var no_students = $('#no_students').val()
		var year_level  = $('#year_level').val()

			if (no_students == null || no_students == "")
			 {
			 	Toast.fire({
                    icon: 'error' ,
                    title: 'Number of students is required*',
                  });
			 	return false

			 }
		$.ajax({
			type : 'post',
			url : 'hr-save-approved-subject-loads',
			data : 
			{
				"_token": "{{ csrf_token() }}",
				'teacher_id' : teacher_id,
				'schedule_id' : schedule_id,
				'subject_id'  : subject_id,
				'ass'		:ass,
				'no_students' :no_students,
				'class_id'	:class_id,
				'year_level' :year_level,
			},
		success:function(data)
			{
				 $('#tr'+data.id).remove();
				 Toast.fire({
                    icon: 'success' ,
                    title: 'Approved Successfully!!',
                  });

				$("#tbl-approval").DataTable().clear();
				$('#form-approval')[0].reset();
				
				$('#modal-approval').modal('hide');

				
            

			},
			error:function(data)
			{

			}
		})

		setTimeout(function(){ myInterval = setInterval(  "update()", 1000 ); }, 1000);
}


function goback(){

	$('#card1').show();
	$('#approval-title').show();
	$('#card2').hide();
	$('#goback').hide();
	$('#approve').hide();
	$('#title-request').show();
	location.reload()
}

  </script>

@endsection












































