 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-dark navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link"></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>

       <li class="nav-item d-none d-sm-inline-block">
        <a href="http://southphilcollege.com" class="nav-link text-bold" style="font-family: Arial, Helvetica, sans-serif;"> <img src="{{asset('/img/spc.png') }}" alt="AdminLTE Logo" style="height: 35px;width: 37px;position: absolute;margin-left: -39px;margin-top: -6px"> SOUTHERN PHILIPPINES COLLEGE</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    

    <!-- Right navbar links -->
   <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
           <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown" id="notif-parent">
        <a class="nav-link" data-toggle="dropdown" href="#" >
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge" id="notif">{{App\temp_notif::where('user_id',Auth::user()->id)->where('is_read',false)->count()}}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="notif-container" style="max-height: 400px ;overflow-y: auto">
            @php 
            $noti = App\notification::where('type','assigned')->orderByDesc('created_at')->get();
          @endphp
           @foreach($noti as $n)
         <a href="javascript:void(0);" class="dropdown-item" onclick="notiClick({{$n->id}})">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('img/'.App\User::find($n->subby)->first()->image)}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
              
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  {{App\User::where('id',$n->subby)->first()->role}} {{App\User::where('id',$n->subby)->first()->fname}}
                   @php  
                    $temp = App\temp_notif::where('user_id',Auth::user()->id)->where('notif_id',$n->id)->get();
                @endphp
                  <span class="float-right text-sm
                   @foreach($temp as $t) 
                   
                    @if($t->is_read)
                    text-secondary
                     @else 
                     text-primary 
                     @endif
                   
                       @endforeach" id="seen{{$n->id}}"><i class="fas fa-circle"></i></span>
                </h3>
                <p class="text-sm">{{ $n->msg }}</p>

                <p class="text-sm text-primary"><i class="far fa-clock mr-1 text-warning"> </i>  {{$n->created_at->diffforHumans()}}</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          <div class="dropdown-divider"></div>
          @endforeach
          @if(count($noti) == 0)
          <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer text-warning">You have 0 notification</a>
          @endif
        </div>
      </li>
          <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
     
     
    </ul>
  </nav>



<script>
    var noti = setInterval(function(){

      $.get('get-notif-hr',function(data){
        $('#notif').text(data.temp.length)
          if (data.user !=null)
           {
            //$('#notif-container').empty(); 
          
          $('#notif-container').prepend('<button class="dropdown-item" onclick="notiClick('+data.t.notif_id+')"><div class="media"><img src="{{asset('img/')}}/'+data.user.image+'" alt="User Avatar" class="img-size-50 mr-3 img-circle"><div class="media-body"><h3 class="dropdown-item-title">'+data.user.role+'<span class="float-right text-sm text-primary" id="seen'+data.t.notif_id+'"><i class="fas fa-circle"></i></span></h3><p class="text-sm">'+data.t.msg+'</p><p class="text-sm text-primary"><i class="far fa-clock mr-1 text-warning"> </i>'+data.msg+'</p></div></div></button')
        }
      })

  },3000)
  setInterval(function(){

     $("#notif-parent").load(window.location.href + " #notif-parent")
  },60000)

    function notiClick(id)
  {

    $('#seen'+id).removeClass('text-primary');
     $('#seen'+id).addClass('text-secondary');
      $.get('update-notif-hr/'+id,function(data){
          
   
          location.assign("{{url('approval-request-view')}}/"+data.type_id);

      })
  }
</script>