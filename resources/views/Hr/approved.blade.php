@extends('layouts.Hr_layout')

@section('content')

<style>

.dataTables_filter > label{
	width: 100%;
	text-align:center;
}
.grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1);cursor: pointer }
p {
  margin-bottom: 1em;
 
  font-size: 15px;
}
	#print {
display:block;
}
#loading {
    height: 100%;
   
    margin: auto;
    
   
    width: 100%;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}

</style>
	<section class="content-header pb-0">
		<div class="container-fluid">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 text-center ">
						<button class="btn btn-outline-secondary float-left" style="display: none;"  id="back"> <i class="fa fa-angle-left"> BACK</i> </button>
						<label id="year" style="display: none;font-size:20px;margin-left: -80px"><i class="fas">School Year</i></label>
					</div>
					<div class="col-md-12">
						<label class="" style="font-size: 20px"  id="approved-title"><i class="fas fa"> Faculty Loads</i> </label>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="content">
			<div class="container-fluid">
				<div class="col-sm-12 " id="card1">
					<div class="row justify-content-center ">
						<div class="col-sm-12 ">
							<div class="card ">
							
								<div id="over1" class="card-body table-responsive" >
									<table id="tbl-approved" class="table table-hover" >
										<thead>
										<tr class="bg-dark">
										
											<th>Name</th>
											<th>Department</th>
											<th>Email</th>
											<th>Contact</th>
											<th>Employee Status</th>
											<th>Action</th>
										</tr>
										
										</thead>
										<tbody>
											@foreach($approve as $ap)
											@php
											$user = App\User::find($ap->instructor_id);
											$dep = App\tbl_academic::find($user->department);
											$infos = App\tbl_instructor_info::where('instructor_id',$user->id)->first()->id;
											$info = App\tbl_instructor_info::find($infos);
										@endphp
												<tr>
													<td>
														{{$user->fname}}
														{{$user->lname}}
													</td>
													<td>{{$dep->program_code}}</td>
													<td>{{$user->email}}</td>
													<td>{{$info->contact}}</td>
													<td>{{$info->employee_type}}</td>
													<td><button class="btn btn-xs btn-outline-success" onclick="showLoads({{$user->id}})"> <i class="fa fa-download"> loads</i></button></td>
												</tr>
											@endforeach
										</tbody>
									</table>
								
								
					          </div> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	

{{--MODAL--}}

      <div class="modal  fade" id="modal-loads">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header  pt-3 pb-2 bg-secondary">
               <div class="col-md-12 text-center">
               	 <label id="loads-teacher-name" style="font-size: 20px"></label>
              <i class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 15px;color: red;cursor: pointer;">
              </i>
               </div>
            </div>
            <div class="modal-body">
          	<div class="card">
          		<div class="card-body pb-1">
          					<div class="row">
							<div class=" col-md-5 ">
							  <div class="form-group row">
							  	  <label class="col-form-label col-md-4 text-center" style="font-size: 15px">School Year</label>
							    <div class="col-md-8">
							    		<select id="sy" class="form-control form-control-sm" style="text-align-last: center;" onchange="selvalid('sy')">
									<option selected="" disabled="" value="0"> -- Select --</option>
									@php
			        					$class = App\subject_schedule	::all()->unique('school_year');
			        				@endphp
		        					@foreach($class as $c)
		        						<option value="{{$c->school_year}}">{{$c->school_year}}</option>
		        					@endforeach
								
								</select>
							    </div>
							  </div>
							</div>
							<div class=" col-md-5 ">
								<div class="form-group row">
									<label class="col-form-label col-md-4 text-center" style="font-size: 15px">Semester</label>
								<div class="col-md-8">
									<select id="sem" class="form-control form-control-sm " style="text-align-last: center;" onchange="selvalid('sem')">
									<option disabled="" selected="" value="0"> --Select--</option>
									<option value="1st">First Semester</option>
									<option value="2nd">Second Semester</option>
									<option value="summer">Summer</option>
								</select>
								</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group"> 
									<button class="btn btn-outline-primary btn-sm col-md-9" onclick="Search()" id="btn-search">  <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true" id="load" style="display: none"></span>
                                   <i class="fas fa-search"></i> Search </button>
								</div>
							</div>

						
						</div>
          		</div>
          	</div>
          	 <div class="col-sm-12 d-flex justify-content-center"  >
				<div class="alert alert-info alert-dismissible col-md-12" id="no-data" style="display: none">
         
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                    <h4>No data to show</h4>
                </div>
			</div>
			<div class="col-sm-12">
				<div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
	            </div>
	        </div>
            <div class="col-sm-12" id="card-load" style="display: none">
             		<table  class="table table-bordered text-sm table-sm" >
             				<thead>
             				   <tr style="font-size:13px">
								<th style="width: 110px">Subject Code</th>
								<th>Descriptive Title</th>
								<th>Cours & Year</th>
								<th class="text-center">Day</th>
								<th class="text-center" style="width: 150px">Time</th>
								<th class="text-center">No. of Students</th>
								<th class="text-center" style="width: 120px">Room</th>
								<th>Units</th>
								<th>Total No. of Hours</th>
							   </tr>
             				</thead>
             				<tbody id="tbl-load" style="font-size:12px">
             					
             				</tbody>
             				 <tr style="border: 0" id="tbl-print-tr">
  	                         </tr>
             			</table>
             		   	<table class="table table-bordered text-sm table-sm mt-5">
				  	 <thead>
				  	  <tr>
				  	  	<th colspan="10">OVERLOAD/EXTRA LOAD</th>
				  	  </tr>
				  	   <tr style="font-size:13px">
								<th style="width: 110px">Subject Code</th>
								<th>Descriptive Title</th>
								<th>Cours & Year</th>
								<th class="text-center">Day</th>
								<th class="text-center" style="width: 150px">Time</th>
								<th class="text-center">No. of Students</th>
								<th class="text-center" style="width: 120px">Room</th>
								<th>Units</th>
								<th>Total No. of Hours</th>
							   </tr>
				  	 </thead>
				  	 <tbody id="tbl-over" style="font-size:12px">
				  	 
				  	 {{-- <tr style="border: 0">
				  	  	<th colspan="8" class="text-right">Total Number of units and hours</th>
				  	  	<td>3</td>
				  	  	<td>5</td>
				  	  </tr>--}}
				  	 </tbody>
				  	  <tr style="border: 0" id="tbl-print-tro">
  	                   </tr>
  	                   <tr  {{--style="border-bottom:solid white !important;border-left:solid white !important;border-right:solid white !important;"--}} id="tbl-print-tr4o">
				  	 </tr>
				    </table>
         
             </div>
            </div>
            <input type="hidden" id="ins_id">
            <div class="modal-footer justify-content-between">
             
              <div class="col-md-12">
              	<button type="button" class="btn btn-primary float-right" onclick="printReport()"><i class="fas fa-print"></i> Print</button>
              </div>
             
            <div id="reports" style="display:none;">

				<link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
				<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
				 
				 <div class="container-fluid col-sm-12">
				  <div class="row justify-content-center" >
				  	<div class="row col-md-12 pb-3 text-center">
				  		<div class="col-md-3 text-right">
				  		   <img src="{{asset('img/spc.png')}}" width="110px" height="100px">
				  		</div>
				  		<div class="col-md-6">
				  			 	<h4 class="text-uppercase text-bold">Southern de Oro Philippines College</h4>
				  	<label class="text-center mb-4">J.Pacana St., Licuan, Cagayan de Oro City</label>
				  	<h5 class="text-bold">INDIVIDUAL FACULTY LOAD SHEET</h5>
				  	<label id="p-year"></label>
				  		</div>
				  		<div class="col-md-3"></div>
				 
				  </div>
				  <div class="col-md-7 mt-3">
				  	<label>Surname: <u id="p-lname"></u></label><br>
				  	<label>College: <u id="p-dep"></u></label><br>
				  	<label>Degree Finish: <u id="degree-finished"></u></label>
				  </div>
				  <div class="col-md-3 ml-0 ">
				  	<label class="">Given Name: <u id="p-fname"></u></label><br>
				  	<label>Department: <u id="p-dep3"></u></label><br>
				  	<label>MA/MS/Ph.D/Ed.D/:<u id="p-masteral"></u> </label>
				  </div>
				  
				  	<label>MI:<u id="p-mi">  </u></label>

				  <div class="col-md-10 mt-3">
				  	<label>Employment Status: <u id="employ-status"></u> </label>
				  </div>

				 

				  </div>
				  <div class="row pt-3">
				   <div class="col-md-12 ">
				  	<table class="table table-bordered text-sm table-sm">
				  	 <thead>
				  	  <tr>
				  	  	<th class="text-center" colspan="4">BASIC LOADS</th>
				  	  	<th class="text-center" colspan="6">TEACHING LOADS</th>
				  	  </tr>
				  	  <tr style="font-size:13px">
								<th style="width: 110px">Subject Code</th>
								<th>Descriptive Title</th>
								<th>Cours & Year</th>
								<th class="text-center">Day</th>
								<th class="text-center" style="width: 150px">Time</th>
								<th class="text-center">No. of Students</th>
								<th class="text-center" style="width: 120px">Room</th>
								<th>Units</th>
								<th>Total No. of Hours</th>
							   </tr>
				  	 </thead>
				  	 <tbody id="tbl-load2" style="font-size:12px">
				  	  
				  	 
				  	 </tbody>
				  	 <tr style="border: 0" id="tbl-print-tr2">
				  	 </tr>
				    </table>
				   </div>
				  </div>
				  
				   	<table class="table table-bordered text-sm table-sm mt-3">
				  	 <thead>
				  	  <tr>
				  	  	<th colspan="10">OVERLOAD/EXTRA LOAD</th>
				  	  </tr>
				  	   <tr style="font-size:13px">
								<th style="width: 110px">Subject Code</th>
								<th>Descriptive Title</th>
								<th>Cours & Year</th>
								<th class="text-center">Day</th>
								<th class="text-center" style="width: 150px">Time</th>
								<th class="text-center">No. of Students</th>
								<th class="text-center" style="width: 120px">Room</th>
								<th>Units</th>
								<th>Total No. of Hours</th>
							   </tr>
				  	 </thead>
				  	 <tbody id="tbl-over2" style="font-size:12px">
				  	 
				  	 {{-- <tr style="border: 0">
				  	  	<th colspan="8" class="text-right">Total Number of units and hours</th>
				  	  	<td>3</td>
				  	  	<td>5</td>
				  	  </tr>--}}
				  	 </tbody>
				 
				  	 	 <tr style="border: 0" id="tbl-print-tr2o">
				  	 </tr>
				  	 <tr  {{--style="border-bottom:solid white !important;border-left:solid white !important;border-right:solid white !important;"--}} id="tbl-print-tr3o">
				  	 </tr>
				
				  	
				    </table>
				 
				  <div class="row p-3">
				   <div class="col-md-4 text-center">
				   	<label>PREPARED BY:</label><br>
				   	<label><u id="d-dean"></u></label><br>
				   	<label class="mt-0">College Dean</label><br>
				   	<label>Date:<u id="date_dean"></u></label>
				   </div>
				   <div class="col-md-2"></div>
				   <div class="col-md-2"></div>
				   <div class="col-md-4 text-center">
				   	<label>REVIEWED BY:</label><br>
				   	<label><u class="text-uppercase">{{Auth::user()->fname}} {{substr(Auth::user()->fname,0,1)}}. {{Auth::user()->lname}}</u></label><br>
				   	<label>HRMO/Personnel Office</label><br>
				   	<label>Date: <u id="created_at"></u></label>
				   </div>
				   <div class="col-md-4 text-center pt-5">
				   	<label>APPROVED BY:</label><br>
				   	<label><u>DR. FE S. TOLIBAS, PH.D.</u></label><br>
				   	<label>Director of Academics Affairs</label><br>
				   	<label>Date:_______________________</label>
				   </div>
				   <div class="col-md-2"></div>
				   <div class="col-md-2"></div>
				   <div class="col-md-4 text-center pt-5">
				   	<label>CONFORME</label><br>
				   	<label ><u id="fullname"></u></label><br>
				   	<label>College Instructor</label><br>
				   	<label>Date:<u id="ins_date"></u></label>

				   </div>
				  </div>
				  <div class="row pt-3">
				  	<div class="col-md-10">
				  	 ©Copyright Southern de Oro Philippines College | Quality Forms and Records                                                                  						
				    </div>
				    {{--<div class="col-2 text-right">
				    	I3R1 ǀ 4/23/14	
				    </div>
				    <div class="col-10">
				     <label>SPC-QFR-ACA-009| Individual Faculty Load Sheet
				     </label>
				    </div>--}}
				  </div>
				 </div>
				</div>
				
			</div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <script src="{{asset('plugins/moment/moment.min.js')}}"></script>



<script>

 const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer:3000,
                  
    });

    function selvalid(val)
	{	
			$('#'+val).css('border-color','#007bff')
	}

	function showLoads(id)
	{
		$('#sy').val("0");
		$('#sem').val("0");
        $('#card-load').hide();
		$('#loading').hide();
		$('no-data').hide();

		$('#ins_id').val(id)
		$.get('hr-get-faculty-details/'+id,function(data){
			$('#loads-teacher-name').text(data.fname+' '+data.mname.charAt(0)+'. '+data.lname)
   
		})
		$('#modal-loads').modal('show')
	
	}
	function Search()
	{
		var ins_id = $('#ins_id').val()
		var sy = $('#sy').val()
		var sem = $('#sem').val()

		var sems="";
		    	    switch(sem)
		    	    {
		    	    	case '1st':
		    	    	     sems = 'FIRST';
		    	    	     break;

		    	    	case '2nd':
		    	    	     sems = 'SECOND';
		    	    	     break;

		    	    	case 'summer':
		    	    	     sems = 'SUMMER';
		    	    	     break;
		    	    }

		$('#p-year').text(sems+' Semester, Academic Year '+sy);

		if (sy == null)
		 {
		 		$('#sy').css('border-color','red') 

		 	 Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });

		 	return false
		 }
		 if (sem == null)
		 {      
		 	    $('#sem').css('border-color','red')

		 		  Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester</span>',
                  });

		 		
		 	return false
		 }
		 
		 $('#loading').show()
		 $('#no-data').hide()
		 $('#card-load').hide()


		$('#tbl-load').empty();
		$('#tbl-load2').empty();
		$('#tbl-over').empty();
		$('#tbl-over2').empty();
		$('#tbl-print-tr').empty();
		$('#tbl-print-tr2').empty();
		$('#tbl-print-tro').empty();
		$('#tbl-print-tr2o').empty();
		$('#tbl-print-tr3o').empty();
		$('#tbl-print-tr4o').empty();
		 $.get('get-fac-load/'+ins_id+'/'+sy+'/'+sem,function(data){

			 	    var t_units=0;
			 	    var t_hours=0;
			 	     var t_units2=0;
			 	    var t_hours2=0;
			 	    var p_fname=data.teacher.fname;
					var p_lname=data.teacher.lname;
					var p_mi=data.teacher.mname;
					var p_dep=data.department.college;
					var degree=data.department.program_code;
					var p_masteral=data.ins_info;
					var d_fname=data.role;
					var d_mname=data.role2;
					var d_lname=data.role3;
					var emp_status=data.ins_info2;
					var date =  moment(data.dean_date).format('MM/DD/YYYY');
					$('#date_dean').text(date)
					$('#ins_date').text(date)

					$('#fullname').text(p_fname+' '+p_mi.charAt(0)+'. ' +p_lname);
					$('#p-mi').text(p_mi.charAt(0));
					$('#p-fname').text(p_fname);
					$('#p-lname').text(p_lname);
					$('#p-masteral').text(p_masteral);
					$('#p-dep').text(p_dep);
					$('#degree-finished').text(degree);
					$('#employ-status').text(emp_status);
					$('#d-dean').text(d_fname+' '+d_mname.charAt(0)+'. '+d_lname);

					//$('#p-dep2').text(p_dep);
					//$('#p-dep3').text(p_dep);

		 			$('#tbl-load').empty()
		 			$('#tbl-load2').empty()
		 			$('#tbl-print-tr').empty()
		 			$('#tbl-print-tr2').empty()

		 			$('#loading').hide()
	
		 			if (data.load.length > 0)
		 			 {
		 			 	$('#card-load').show()
		 			 	$('#no-data').hide()
		 			 }else
		 			 {
		 			 	$('#no-data').show()
		 			 	$('#card-load').hide()
		 			 }

	 			
		 			$.each(data.temp_load,function(k1,l2){

		 				 	
		 				 	$.each(data.subject,function(k,sub){

		 				 		if (sub.id == l2.subject_id )
		 				 		 {
		 				 		 	
		 				 		 
		 				 	         t_units +=parseInt(sub.units);

		 				 		 	$('#tbl-load').append('<tr id="tr'+l2.id+'" class="p-0 m-0"><td class="text-center p-0" style="vertical-align: middle;">'+sub.subject+' '+sub.code+'</td><td style="vertical-align: middle;" class="text-center p-0">'+sub.descriptive_title+'</td><td style="vertical-align: middle;" class="text-center p-1">'+l2.section+'</td><td class="text-center p-0" style="vertical-align: middle;"><table id="tbl'+l2.id+'" class="table p-0 m-0" style="font-size:12px"></table></td><td class="text-center p-0"style="vertical-align: middle;"><table id="tbl2'+l2.id+'" class="table p-0 m-0 " style="font-size:12px"></table></td><td class="text-center p-0" style="vertical-align: middle;">'+l2.unique+'</td><td  class="p-0" style="vertical-align: middle;"><table  class="table p-0 m-0" style="font-size:12px"><tbody id="tbl3'+l2.id+'"></tbody></table></td><td class="text-center p-0" style="vertical-align: middle;">'+sub.units+'</td><td class="text-center p-0" id="td_hr'+l2.id+'" style="vertical-align: middle;"></td>/tr>')

		 				 		 		$('#tbl-load2').append('<tr id="tr'+l2.id+'" class="p-0 m-0"><td class="text-center p-0" style="vertical-align: middle;">'+sub.subject+' '+sub.code+'</td><td style="vertical-align: middle;" class="text-center p-0">'+sub.descriptive_title+'</td><td style="vertical-align: middle;" class="text-center p-0">'+l2.section+'</td><td class="text-center p-0" style="vertical-align: middle;"><table id="tbl1'+l2.id+'" class="table p-0 m-0 " style="font-size:12px !important" > </table></td><td class="text-center p-0"style="vertical-align: middle;"><table id="tbl22'+l2.id+'" class="table p-0 m-0" style="font-size:12px"></table></td><td class="text-center p-0" style="vertical-align: middle;">'+l2.unique+'</td><td  class="p-0" style="vertical-align: middle;"><table  class="table p-0 m-0" style="font-size:12px"><tbody id="tbl33'+l2.id+'" style="font-size:12px"></tbody></table></td><td class="text-center p-0" style="vertical-align: middle;">'+sub.units+'</td><td class="text-center p-0" id="td_hr2'+l2.id+'" style="vertical-align: middle;"></td>/tr>')

		 				 		   var t_hr = 0
						 		   $.each(data.load,function(k,l){

		 				 		 		if (l.subject_id == sub.id && l2.section  == l.section)
		 				 		 		 {
		 				 		 		 	var r = ""
		 				 		 		 	$.each(data.sched ,function(k,s){
		 				 		 		 			if (s.id == l.schedule_id)
		 				 		 		 			 {
		 				 		 		 			 	$.each(data.room,function(k,ro){
		 				 		 		 			 		if(s.room_id == ro.id)
		 				 		 		 			 		{
		 				 		 		 			 			r = ro.room_description
		 				 		 		 			 		}
		 				 		 		 			 	})
		 				 		 		 			 	
		 				 		 		 			 }
		 				 		 		 	})
		 				 		 		 	       
													    var now = moment(l.time_end,"HH:mm:ss"); //todays date
														var end = moment(l.time_start,"HH:mm:ss"); // another date
														var duration = moment.duration(now.diff(end));
														t_hr+=duration.asHours()

													 var date2 = moment(l.created_at).format('MM/DD/YYYY');	

													 $('#created_at').text(date2)



											$('#tbl3'+l2.id).append('<tr class="p-0"><td class="text-center" style=" border: 0px solid">'+r+'</td></tr>')
		 				 		 		 	$('#tbl'+l2.id).append('<tr class="p-0"><td class="text-center" style=" border: 1px solid #ffffff">'+days(l.day.toString())+'</td></tr>')
		 				 		 		 	$('#tbl2'+l2.id).append('<tr class="p-0"><td class="text-center" style=" border: 1px solid #ffffff">'+moment(l.time_start,['HH:mm ']).format(" hh:mm ")+'-'+moment(l.time_end,['HH:mm A']).format(" hh:mm A")+'</td></tr>')

		 				 		 		 		$('#tbl33'+l2.id).append('<tr class="p-0"><td class="text-center" style="border:solid white !important;font-size:10px;">'+r+'</td></tr>')
		 				 		 		 	$('#tbl1'+l2.id).append('<tr   class="p-0"><td class="text-center" style="border:solid white !important;font-size:10px;">'+days(l.day.toString())+'</td></tr>')
		 				 		 		 	$('#tbl22'+l2.id).append('<tr class="p-0"><td class="text-center" style="border:solid white !important;font-size:10px;">'+moment(l.time_start,['HH:mm ']).format(" hh:mm ")+'-'+moment(l.time_end,['HH:mm A']).format(" hh:mm A")+'</td></tr>')
		 				 		 		 }
		 				 		 	 })
						 				
		 				 		 		
		 								$('#td_hr'+l2.id).text(t_hr)
		 								$('#td_hr2'+l2.id).text(t_hr)
		 							    t_hours += parseInt(t_hr);
						 		 		 
						 			}
						 		})
		 				 	
		 				 
		 				
		 			})
		 			$.each(data.temp_over,function(k1,l2){

		 				 	
		 				 	$.each(data.subject,function(k,sub){

		 				 		if (sub.id == l2.subject_id )
		 				 		 {
		 				 		 	
		 				 		 
		 				 	         t_units2 +=parseInt(sub.units);

		 				 		 	$('#tbl-over').append('<tr id="tro'+l2.id+'" class="p-0 m-0"><td class="text-center p-1" style="vertical-align: middle;">'+sub.subject+' '+sub.code+'</td><td style="vertical-align: middle;" class="text-center p-1">'+sub.descriptive_title+'</td><td style="vertical-align: middle;" class="text-center p-1">'+l2.section+'</td><td class="text-center p-1" style="vertical-align: middle;"><table id="tblo'+l2.id+'" class="table p-0 m-0"></table></td><td class="text-center p-1"style="vertical-align: middle;"><table id="tbl2o'+l2.id+'" class="table p-0 m-0 "></table></td><td class="text-center p-1" style="vertical-align: middle;">'+l2.unique+'</td><td  class="p-1" style="vertical-align: middle;"><table  class="table p-0 m-0" ><tbody id="tbl3o'+l2.id+'"></tbody></table></td><td class="text-center p-1" style="vertical-align: middle;">'+sub.units+'</td><td class="text-center p-1" id="td_hro'+l2.id+'" style="vertical-align: middle;"></td>/tr>')

		 				 		 		$('#tbl-over2').append('<tr id="tro'+l2.id+'" class="p-0 m-0"><td class="text-center p-1" style="vertical-align: middle;">'+sub.subject+' '+sub.code+'</td><td style="vertical-align: middle;" class="text-center p-1">'+sub.descriptive_title+'</td><td style="vertical-align: middle;" class="text-center p-1">'+l2.section+'</td><td class="text-center p-1" style="vertical-align: middle;"><table id="tbl1o'+l2.id+'" class="table p-0 m-0 "></table></td><td class="text-center p-1"style="vertical-align: middle;"><table id="tbl22o'+l2.id+'" class="table p-0 m-0"></table></td><td class="text-center p-1" style="vertical-align: middle;font-size:12px">'+l2.unique+'</td><td  class="p-1" style="vertical-align: middle;"><table  class="table p-0 m-0" ><tbody id="tbl33o'+l2.id+'" style="font-size:12px"></tbody></table></td><td class="text-center p-1" style="vertical-align: middle;">'+sub.units+'</td><td class="text-center p-1" id="td_hr2o'+l2.id+'" style="vertical-align: middle;"></td>/tr>')

		 				 		   var t_hr2 = 0
						 		   $.each(data.over,function(k,l){

		 				 		 		if (l.subject_id == sub.id && l2.section  == l.section)
		 				 		 		 {
		 				 		 		 	var r = ""
		 				 		 		 	$.each(data.sched ,function(k,s){
		 				 		 		 			if (s.id == l.schedule_id)
		 				 		 		 			 {
		 				 		 		 			 	$.each(data.room,function(k,ro){
		 				 		 		 			 		if(s.room_id == ro.id)
		 				 		 		 			 		{
		 				 		 		 			 			r = ro.room_description
		 				 		 		 			 		}
		 				 		 		 			 	})
		 				 		 		 			 	
		 				 		 		 			 }
		 				 		 		 	})
		 				 		 		 	       
													    var now = moment(l.time_end,"HH:mm:ss"); //todays date
														var end = moment(l.time_start,"HH:mm:ss"); // another date
														var duration = moment.duration(now.diff(end));
														t_hr2+=duration.asHours()

													 var date2 = moment(l.created_at).format('MM/DD/YYYY');	

													 $('#hr_date').text(date2)



											$('#tbl3o'+l2.id).append('<tr class="p-0"><td class="text-center" style=" border: 1px  solid #ffffff">'+r+'</td></tr>')
		 				 		 		 	$('#tblo'+l2.id).append('<tr class="p-0"  ><td class="text-center" style=" border: 1px solid #ffffff">'+days(l.day.toString())+'</td></tr>')
		 				 		 		 	$('#tbl2o'+l2.id).append('<tr class="p-0"><td class="text-center" style=" border: 1px solid #ffffff">'+moment(l.time_start,['HH:mm ']).format(" hh:mm ")+'-'+moment(l.time_end,['HH:mm A']).format(" hh:mm A")+'</td></tr>')

		 				 		 		 		$('#tbl33o'+l2.id).append('<tr class="p-0"><td class="text-center" style="border:solid white !important;font-size:10px;">'+r+'</td></tr>')
		 				 		 		 	$('#tbl1o'+l2.id).append('<tr  class="p-0" ><td class="text-center" style="border:solid white !important;font-size:10px;">'+days(l.day.toString())+'</td></tr>')
		 				 		 		 	$('#tbl22o'+l2.id).append('<tr class="p-0"> <td class="text-center" style="border:solid white !important;font-size:10px;">'+moment(l.time_start,['HH:mm ']).format(" hh:mm ")+'-'+moment(l.time_end,['HH:mm A']).format(" hh:mm A")+'</td></tr>')
		 				 		 		 }
		 				 		 	 })
						 				
		 				 		 		
		 								$('#td_hro'+l2.id).text(t_hr2)
		 								$('#td_hr2o'+l2.id).text(t_hr2)
		 							    t_hours2 += parseInt(t_hr2);
						 		 		 
						 			}
						 		})
		 				 	
		 				 
		 				
		 			})
		 			
		 		
				var t_units3 = t_units + t_units2
				var t_hours3 = t_hours + t_hours2
		 		$('#tbl-print-tr').prepend('<th colspan="7" class="text-right">Total number of units and hours</th>'+
				'<td class="text-center">'+t_units+'</td>'+
				'<td class="text-center">'+t_hours+'</td>');
				$('#tbl-print-tr2').prepend('<th colspan="7" class="text-right">Total number of units and hours</th>'+
				'<td class="text-center">'+t_units+'</td>'+
				'<td class="text-center">'+t_hours+'</td>');
				$('#tbl-print-tro').prepend('<th colspan="7" class="text-right">  Number of units and hours</th>'+
				'<td class="text-center">'+t_units2+'</td>'+
				'<td class="text-center">'+t_hours2+'</td>');
				$('#tbl-print-tr2o').prepend('<th colspan="7" class="text-right"> Number of units and hours</th>'+
				'<td class="text-center">'+t_units2+'</td>'+
				'<td class="text-center">'+t_hours2+'</td>');
				$('#tbl-print-tr3o').prepend('<th colspan="7" class="text-right" >Total number of units and hours</th>'+
				'<td class="text-center text-bold" ><u>'+t_units3+'</u></td>'+
				'<td class="text-center text-bold" ><u>'+t_hours3+'</u></td>');
				$('#tbl-print-tr4o').prepend('<th colspan="7" class="text-right" >Total number of units and hours</th>'+
				'<td class="text-center text-bold" ><u>'+t_units3+'</u></td>'+
				'<td class="text-center text-bold" ><u>'+t_hours3+'</u></td>');
		

		      

		})

	}
</script>
  <script type="text/javascript">
   $('#schedule_nav').addClass('active');
    $('#approved').addClass('active').css('background-color','#800000');
  </script>


      <script type="text/javascript">
      		
 	$('#back').click(function(){
 		$('#card1').show();
 		$('#approved-title').show();
 		$('#card2').hide();
 		$('#card3').hide();
 		$('#back').hide();
 		$('#year').hide();
 	})
 
 					
      </script>

<script>
function time_convert(num)
 { 
  var hours = Math.floor(num / 60);  
  var minutes = num % 60;
  return hours + ":" + minutes;         
}

function printReport()
    {
    	var name = $('#p-fname').text()+ ' '+$('#p-mi').text()+'. '+$('#p-lname').text()
    	var sem = $('#p-year').text()
    	var prtContent = document.getElementById("reports");
		//var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    	var WinPrint = window.open("");
    	//WinPrint.document.open()
    	
    	//WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body>'+prtContent.innerHTML+'</body></html>')
    	  //WinPrint.focus();
    

    	WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.title=name+'-'+sem;
    
       WinPrint.document.close();
       WinPrint.focus();

        WinPrint.print();
       WinPrint.close();

       
    }
</script>
{{--
<style type="text/css">

</style>

<div id="print" style="display:none;">

<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
 
 <div class="container-fluid col-sm-12">
  <div class="row justify-content-center" >
  	<div class="row col-md-12 pb-3 text-center">
  		<div class="col-md-3 text-right">
  		   <img src="{{asset('img/spc.png')}}" width="110px" height="100px">
  		</div>
  		<div class="col-md-6">
  			 	<h4 class="text-uppercase text-bold">Southern de Oro Philippines College</h4>
  	<label class="text-center mb-4">J.Pacana St., Licuan, Cagayan de Oro City</label>
  	<h5 class="text-bold">INDIVIDUAL FACULTY LOAD SHEET</h5>
  	<label id="p-year"></label>
  		</div>
  		<div class="col-md-3"></div>
 
  </div>
  <div class="col-md-7">
  	<label>Surname: <u id="p-lname"></u></label><br>
  	<label>College: <u id="p-dep"></u></label><br>
  	<label>Degree Finish: <u id="p-dep2"></u></label>
  </div>
  <div class="col-md-3 ml-0 ">
  	<label class="">Given Name: <u id="p-fname"></u></label><br>
  	<label>Department: <u id="p-dep3"></u></label><br>
  	<label>MA/MS/Ph.D/Ed.D/:<u>MIT</u> </label>
  </div>
  
  	<label>MI:<u id="p-mi">  </u></label>

  <div class="col-md-10 mt-3">
  	<label>Employment Status: </label>
  </div>

 

  </div>
  <div class="row pt-3">
   <div class="col-md-12 ">
  	<table class="table table-bordered text-sm table-sm">
  	 <thead>
  	  <tr>
  	  	<th class="text-center" colspan="4">BASIC LOADS</th>
  	  	<th class="text-center" colspan="6">TEACHING LOADS</th>
  	  </tr>
  	  <tr style="font-size: 12px">
  	  
  	  	<th>Subject Code</th>
  	  	<th>Descriptive Title</th>
  	  	<th>Course & Year</th>
  	  	<th>Day</th>
  	  	<th>Time</th>
  	  	<th>Total No. of Students</th>
  	  	<th>Room</th>
  	  	<th>Units</th>
  	  	<th>Total No. of Hours</th>
  	  </tr>
  	 </thead>
  	 <tbody id="tbl-print" >
  	  
  	 
  	 </tbody>
  	 <tr style="border: 0" id="tbl-print-tr">
  	 </tr>
    </table>
   </div>
  </div>
  <div class="row ">
   <div class="col-md-12 table-responsive">
   	<table class="table table-bordered text-sm">
  	 <thead>
  	  <tr>
  	  	<th colspan="10">OVERLOAD/EXTRA LOAD</th>
  	  </tr>
  	  <tr style="font-size: 12px">
  	  	<th>Decode</th>
  	  	<th>Subject Code</th>
  	  	<th>Descriptive Title</th>
  	  	<th>Course & Year</th>
  	  	<th>Day </th>
  	  	<th>Time</th>
  	  	<th>Total No. of Students</th>
  	  	<th>Room</th>
  	  	<th>Units</th>
  	  	<th>Total No. of Hours</th>
  	  </tr>
  	 </thead>
  	 <tbody id="">
  	 
  	 <tr style="border: 0">
  	  	<th colspan="8" class="text-right">Total Number of units and hours</th>
  	  	<td>3</td>
  	  	<td>5</td>
  	  </tr>
  	 </tbody>
    </table>
   </div>
  </div>
  <div class="row p-3">
   <div class="col-md-4 text-center">
   	<label>PREPARED BY:</label><br>
   	<label><u>VICENT JANU RAZALO</u></label><br>
   	<label class="mt-0">College Dean</label><br>
   	<label>Date:_______________________</label>
   </div>
   <div class="col-md-2"></div>
   <div class="col-md-2"></div>
   <div class="col-md-4 text-center">
   	<label>REVIEWED BY:</label><br>
   	<label><u>MARIA GRACIA B. JURIAL, PH.D.</u></label><br>
   	<label>HRMO/Personnel Office</label><br>
   	<label>Date:_______________________</label>
   </div>
   <div class="col-md-4 text-center pt-5">
   	<label>APPROVED BY:</label><br>
   	<label><u>DR. FE S. TOLIBAS, PH.D.</u></label><br>
   	<label>Director of Academics Affairs</label><br>
   	<label>Date:_______________________</label>
   </div>
   <div class="col-md-2"></div>
   <div class="col-md-2"></div>
   <div class="col-md-4 text-center pt-5">
   	<label>CONFORME</label><br>
   	<label ><u id="fullname"></u></label><br>
   	<label>College Instructor</label><br>
   	<label>Date:_______________________</label>

   </div>
  </div>
  <div class="row pt-3">
  	<div class="col-md-10">
  	 ©Copyright Southern de Oro Philippines College | Quality Forms and Records                                                                  						
    </div>
    <div class="col-2 text-right">
    	I3R1 ǀ 4/23/14	
    </div>
    <div class="col-10">
     <label>SPC-QFR-ACA-009| Individual Faculty Load Sheet
     </label>
    </div>
  </div>
 </div>
</div>--}}


<script type="text/javascript">
	function days(val)
	{
		day ="";
		switch(val)
			{

				
				case '0':
					day ='MON';
					break;
				case '1':
					day ='TUE';
					break;
				case '2':
					day ='WED';
					break;
				case '3':
					day ='THU';
					break;
				case '4':
					day ='FRI';
					break;
				case '5':
					day ='SAT';
					break;
				
			}
			return day;
	}
</script>
@endsection		