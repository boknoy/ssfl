@extends('layouts.Hr_layout')
@section('content')
@include('Hr.css')
<section class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        
      </div>
      <div class="col-md-6">
        <ol class="breadcrumb float-sm-right">
        
        </ol>
      </div>
    </div>
  </div>
</section>

<section class="wizard-section" id="alldiv">
    <div class="row no-gutters">
        <div class="col-lg-12 col-md-12">
            <div class="form-wizard">
                <form id="form-add-instructor" >
                    <div class="form-wizard-header" style="margin-top: -40px;">
                        <ul class="list-unstyled form-wizard-steps clearfix">
                            <li id="icon-user" class="active"><span><i class="fas fa-users" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-contact"><span><i class="fa fa-address-book" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-other"><span><i class="fa fa-info-circle" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-account"><span><i class="fa fa-user" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                        </ul>
                    </div>
                    <fieldset class="wizard-fieldset show" id="field-personal">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Personal Information</h5>
                                <div class="col-sm-12 justify-content-center ">
                                    <div class="row">
                                        <div class="col-sm-8 ">
                                            <div class="form-group">
                                                <input type="text" class="form-control text-uppercase" id="fname">
                                                <label for="fname" class="wizard-form-text-label">First Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control text-uppercase" id="mname">
                                                <label for="mname" class="wizard-form-text-label">Middle Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control text-uppercase" id="lname">
                                                <label for="lname" class="wizard-form-text-label">Last Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control text-uppercase" id="ext_name">
                                                <label for="exname" class="wizard-form-text-label">Extension Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 justify-content-right">
                                            <div class="form-group">
                                            <img id="blah" src="" width="100" style="height: 200px; width: 320px; border: 1px solid lightgray;"/>
                                            </div>
                                            <div class="form-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input"  id="image" onchange="loadFile(event)" style="display: none;">
                                                <label class="custom-file-label" for="image">Choose file</label>
                                            </div>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-next-btn btn-success float-right">Next <i class="fa fa-arrow-right"></i></a>
                                </div>
                            <div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-contact">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Contact Information</h5>
                                <div class="row">
                                    <div class="col-sm-6 ">
                                        <div class="form-group">
                                            <input type="email" class="form-control " id="email">
                                            <label for="Email" class="wizard-form-text-label">Email</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group">
                                            <input type="text" class="form-control " id="facebook" >
                                            <label for="Facebook" class="wizard-form-text-label">Facebook</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group">
                                            <input type="number" class="form-control " id="phone_number ">
                                            <label for="phone_number" class="wizard-form-text-label">Phone Number</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group">
                                            <input type="text" class="form-control text-uppercase" id="address">
                                            <label for="Address" class="wizard-form-text-label">Address</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"> <i class="fa fa-arrow-left"></i> Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn btn-success float-right"> Next <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-other">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Other Information</h5>
                                <div class="form-group">
                                    <label >Program Graduated</label>
                                    <select class="form-control wizard-required" id="program_graduated">
                                      <option selected disabled>--Select--</option>
                                        @foreach($academic as $a)
                                            <option value="{{$a->id}}">{{$a->program_code}}</option>
                                        @endforeach
                                      
                                    </select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group">
                                    <label >Department</label>
                                    <select class="form-control wizard-required" id="department">
                                        <option selected disabled>--Select--</option>
                                       @foreach($academic as $a)
                                            <option value="{{$a->id}}">{{$a->program_code}}</option>
                                        @endforeach
                                    </select>
                                    
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group">
                                    <label >Employee Type</label>
                                    <select class="form-control" id="employee_type">
                                        <option selected disabled>--Select--</option>
                                        <option value="full">Full Time</option>
                                        <option value="part">Part Time</option>
                                    </select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group">
                                    <label>Choose Subject Specialization : <i class="fa fa-arrow-down"></i></label>
                                    <select class="form-control select2" multiple="multiple" id="subject" name="subject[]" style="width: 100%;">
                                        
                                        @foreach($subject as $row)
                                            <option value="{{$row->id}}">{{$row->descriptive_title}}</option>
                                        @endforeach 
                                    </select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"><i class="fa fa-arrow-left"></i> Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn btn-success float-right">Next <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-account">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Account</h5>
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <input type="text" class="form-control text-uppercase text-uppercase" id="teacher_id">
                                        <label for="teacher_id" class="wizard-form-text-label">Teacher's (ID)</label>
                                        <div class="wizard-form-error"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <input type="password" class="form-control " id="password">
                                        <label for="password" class="wizard-form-text-label">Password</label>
                                        <div class="wizard-form-error"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group">
                                        <input type="password" class="form-control " id="retype-password">
                                        <label for="password" class="wizard-form-text-label">Retype Password</label>
                                        <div class="wizard-form-error"></div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"> <i class="fa fa-arrow-left"></i> Previous</a>
                                    <button type="button" class="form-wizard-submit btn-success float-right" onclick="AddInstructor()"> Save <i class="fa fa-check fa-lg"></i> </button>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                </form>
            </div>
        </div>
    </div>
</section>


@include('Hr.script')



<script>

  function AddInstructor(){

    var fname     = $('#fname').val();
    var mname     = $('#mname').val();
    var lname     = $('#lname').val();
    var ext_name  = $('#ext_name').val();
    var image       = $('#image').val();

    var facebook          = $('#facebook').val();
    var address           = $('#address').val();
    var contact           = $('#phone_number').val();
    var program_graduated = $('#program_graduated').val();
    var department        = $('#department').val();
    var employee_type     = $('#employee_type').val();
    var subject           = $("select[name='subject[]']").val();
    var email             = $('#email').val();
    var password          = $('#password').val();
    var teacher_id        = $('#teacher_id').val();
    alert(password)
    
     $.ajax({

      type: 'POST',
       url: 'hr-save-instructor',
      data:{
        "_token"  :"{{ csrf_token() }}",
        "fname"             :fname,
        "mname"             :mname,
        "lname"             :lname,
        "ext_name"          :ext_name,
        "teacher_id"        :teacher_id,
        //"image"           :image,
        "email"             :email,
        "facebook"          :facebook,
        "address"           :address,
        "contact"           :contact,
        "program_graduated" :program_graduated,
        "department"        :department,
        "employee_type"     :employee_type,
        "subject[]"         :subject,
        "password"          :password,
        

    },
      success:function(value){
       $('#form-add-instructor')[0].reset();
       $('#subject').val('').select2();
       $('#field-personal').addClass('show');
       $('#field-account').removeClass('show');
       $('#icon-contact').removeClass('activated');
       $('#icon-other').removeClass('activated');
       $('#icon-account').removeClass('active');


        alert('success save')
      },
       error:function(data){

      }
     })

  }
</script>

@endsection