@extends('layouts.Hr_layout')

@section('content')
<style type="text/css">
.noBorder {
    border: 0px;
    padding:0; 
    margin:0;
    border-collapse: collapse;
}

</style>
<section class="content-header pb-0">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<label style="font-size: 20px"><i class="fas fa-file-download"></i> List of Instructor by Department</label>
			</div>
		</div>
	</div>
</section>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body pb-1">
						<div class="row justify-content-center">
							<div class="col-md-5">
								<div class="form-group row">
									<label class="col-md-4 col-form-label" style="font-size: 15px">School Year</label>
							   <div class="col-md-8">
							   		<select class="form-control " id="sy">
									<option selected="" disabled="" value="0"> --Select--</option>
									@php
			        					$class = App\subject_schedule	::all()->unique('school_year');
			        				@endphp
		        					@foreach($class as $c)
		        						<option value="{{$c->school_year}}">{{$c->school_year}}</option>
		        					@endforeach
 
								</select>
							   </div>
							</div>
							</div>
						    <div class="col-md-5">
						    		<div class="form-group row">
								<label class="col-form-label col-md-4 text-center" style="font-size: 15px">Department</label>
								<div class="col-md-8">
									<select class="form-control " id="sem">
									<option selected="" disabled="" value="0"> --Select--</option>
							        
                            
								</select>
								</div>
							</div>
						    </div>
							<div class="form-group col-md-2">
							     <button class="btn btn-outline-primary col-md-10" onclick="search()"><i class="fas fa-search"></i> Search</button>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="display: none" id="loads">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<table class="table table-bordered text-sm table-sm">
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script >
	$('#sched_menu').addClass('menu-open');
	$('#sched_nav').addClass('active');
	$('#sched').addClass('active');
</script>

@endsection