@extends('layouts.Hr_layout')
@section('content')
@include('Hr.css')
<section class="content-header pb-0">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 ">
      <button onclick="GoBack()" class="btn btn-outline-secondary btn-sm" id="btn-return" style="display: none"><i class="fas fa-angle-left"> BACK</i> </button>
      <label style="font-size: 20px" id="text-title"><i class="fas fa-chalkboard-teacher"> Manage Instructor</i></label>

      <button type="button" class="btn btn-outline-primary btn-sm float-right" onclick="NewInstructor()" id="btn-add"><i class="fas fa-plus"></i> Instructor</button>
      </div>
    </div>
  </div>
</section>

<section class="content"  id="div1-list" >
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          @php
          $i=1;
          @endphp
          <div class="card-body table-responsive" >
            <table id="list-of-instructor-table" class="table table-hover table-bordered text-sm">
              <thead>
                <tr>
                  <th width="10"><b>#</b></th>
                  <th>Name</th>
                  <th>Role</th>
                  <th>Email</th>
                  <th>Department</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody >
               
                 @foreach($user as $u)
                 @foreach($instructor as $ins)
                 @if($ins->instructor_id == $u->id)
                <tr id="tr{{$u->id}}">
                  <td>{{$i}}</td>
                  <td>{{$u->fname}} {{substr($u->mname, 0,1)}}. {{$u->lname}}</td>
                  <td>{{$u->role}}</td>
                  <td>{{$u->email}}</td>
                  
                  <td>
                      @php
                            $dep = App\tbl_academic::find($u->department); 

                      @endphp
                      @if($dep !=null)    
                      {{$dep->program_code}}
                      @endif
                        
                </td>
                  <td><div class="btn-group p-0 m-0 ">
                    <button class="btn btn-outline-info btn-sm" onclick="View({{$u->id}})"><i class="fas fa-eye"></i> View</button>
                    <button type="button" class="btn btn-outline-success btn-xs" onclick="Update({{$u->id}})"><i class="fas fa-edit"></i> Edit</button>
                    <button type="button" class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown">
                      
                      <div class="dropdown-menu" role="menu">
                        <a class="dropdown-item" href="#" onclick="Rule({{$u->id}},'Dean');">DEAN</a>
                        <a class="dropdown-item" href="#" onclick="Rule({{$u->id}},'Instructor');">INSTRUCTOR</a>
                      </div>
                    </button>
                    
                  </div></td>
                </tr>
                @php
                $i++;
                @endphp
                @endif
                @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          
        </div>
      </div>
    </div>
</section>

{{--Add Instructor--}}
<section class="wizard-section" id="div-add-form" style="display: none">
    <div class="row no-gutters">
        <div class="col-lg-12 col-md-12">
            <div class="form-wizard">
                <form id="form-add-instructor" >
                    <div class="form-wizard-header" style="margin-top: -40px;">
                        <ul class="list-unstyled form-wizard-steps clearfix">
                            <li id="icon-user" class="active"><span><i class="fas fa-users" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-contact"><span><i class="fa fa-address-book" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-other"><span><i class="fa fa-info-circle" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                            <li id="icon-account"><span><i class="fa fa-user" style="margin-top: 10px; font-size: 20px;"></i></span></li>
                        </ul>
                    </div>
                    <fieldset class="wizard-fieldset show" id="field-personal">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Personal Information</h5>
                                <div class="col-sm-12 justify-content-center ">
                                    <div class="row">
                                        <div class="col-sm-9 ">
                                            <div class="form-group " id="u-fname">
                                                <input type="text" class="form-control text-uppercase wizard-required" id="fname">
                                                <label for="fname" class="wizard-form-text-label">First Name</label>
                                                <div class="wizard-form-error text-danger">First name is required*</div>
                                            </div>
                                            <div class="form-group" id="u-mname">
                                                <input type="text" class="form-control  text-uppercase" id="mname">
                                                <label for="mname" class="wizard-form-text-label">Middle Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                            <div class="form-group" id="u-lname">
                                                <input type="text" class="form-control wizard-required text-uppercase" id="lname">
                                                <label for="lname" class="wizard-form-text-label">Last Name</label>
                                                <div class="wizard-form-error text-danger"> Last name is required*  </div>
                                            </div>
                                            <div class="form-group " id="u-ext_name">
                                                <input type="text" class="form-control text-uppercase" id="ext_name">
                                                <label for="exname" class="wizard-form-text-label">Extension Name</label>
                                                <div class="wizard-form-error"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3  text-right">
                                            <div class="form-group " onclick="LoadImage()">

                                            <img id="teacher-image" src="{{asset('/img/avatar.png')}}" style="height: 200px; width: 200px; border: 1px solid lightgray;"/>
                                            <button type="button" class="btn " style=" position: absolute;top: -2%;left: 86%;"><i class="fa fa-edit text-info"></i></button>
                                            </div>
                                            <input type="file" id="image" style="display: none">

                                            <div class="form-group">
                                            
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" id="img-text" value="avatar.png">
                                <script >
                                       
                                    function LoadImage()
                                    {
                                        $('#image').trigger('click');

                                    }
                                    $('#image').change(function(){
                                            readURL(this)
                                           
                                              var file = $('#image')[0].files[0].name;
                                              $('#img-text').val(file);


                                    })
                                     function readURL(input) {
                                          if (input.files && input.files[0]) {
                                              var reader = new FileReader();
                                              
                                              reader.onload = function (e) {
                                                  $('#teacher-image').attr('src', e.target.result);
                                              }
                                              reader.readAsDataURL(input.files[0]);
                                          }
                                      }
                                </script>
                                <div class="form-group clearfix">
                                    <button type="button" class="form-wizard-next-btn float-right" style="border:none">Next <i class="fa fa-arrow-right"></i></button>
                                </div>
                            <div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-contact">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Contact Information</h5>
                                <div class="row">
                                    <div class="col-sm-6 ">
                                        <div class="form-group " id="u-email">
                                            <input type="email" class="form-control wizard-required " id="email" >
                                            <label for="Email" class="wizard-form-text-label">Email</label>
                                            <div class="wizard-form-error text-danger">Email is required*</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group " id="u-facebook">
                                            <input type="text" class="form-control " id="facebook" >
                                            <label for="Facebook" class="wizard-form-text-label">Facebook</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group " id="u-phone_number">
                                            <input type="number" class="form-control wizard-required" id="phone_number">
                                            <label for="phone_number" class="wizard-form-text-label">Phone Number</label>
                                            <div class="wizard-form-error text-danger">Phone no. is required*</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 ">
                                        <div class="form-group " id="u-address">
                                            <input type="text" class="form-control text-uppercase" id="address">
                                            <label for="Address" class="wizard-form-text-label">Address</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"> <i class="fa fa-arrow-left"></i> Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn btn-success float-right"> Next <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-other">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Other Information</h5>
                                 <div class="row p-0">
                                      <div class=" col-md-6">
                                    <label >Degree Finished</label>
                                    <select class="form-control wizard-required" id="program_graduated">
                                      <option selected disabled value=""> --Select--</option>
                                        @foreach($curr as $c)
                                            <option value="{{$c->id}}">{{$c->course}}</option>
                                        @endforeach
                                      
                                    </select>
                                    <div id="pg-error" class=" text-danger" style="display: none">Please select Program Graduated*</div>
                                </div>
                                 <div class=" col-md-3">
                                    <label >MIT/MA/MS/Ph.D/Ed.D/:</label>
                                    <select class="form-control wizard-required" id="master" onchange="Masteral(this.value)">
                                      <option selected disabled>--Select--</option>
                                      <option value="MIT">MIT</option>
                                      <option value="MA">MA</option>
                                      <option value="MS">MS</option>
                                      <option value="Ph.D">Ph.D</option>
                                      <option value="Ed.D">Ed.D</option>
                                      <option value="Others">Others</option>

                                       
                                        
                                      
                                    </select>
                                  
                                </div>
                                <div class="form-group col-sm-3" style="display: none" id="div-masteral">
                                    <input type="text" id="masteral" class="form-control">
                                </div>
                                 </div>
                                 <div class="row">
                                     <div class="form-group col-sm-4 ">
                                    <label >Department</label>
                                    <select class="form-control wizard-required" id="department" onchange="Dep(this.value)">
                                        <option selected disabled value="">--Select--</option>
                                       @foreach($academic as $a)
                                            
                                            <option value="{{$a->id}}">{{$a->program_code}}</option>

                                        @endforeach
                                    </select>
                                    
                                    <div id="dep-error" class=" text-danger" style="display: none"> Please select Department*</div>
                                </div>
                                <div class="form-group col-sm-4 ">
                                    <label >College</label>
                                    <input type="text" readonly="" id="college" class="form-control ">
                                    
                                  
                                </div>
                                 <div class="form-group col-sm-4 ">
                                    <label >Employee Type</label>
                                    <select class="form-control wizard-required" id="employee_type">
                                        <option selected disabled value="">--Select--</option>
                                        <option value="Full Time">Full Time</option>
                                        <option value="Part Time">Part Time</option>
                                    </select>
                                    <div id="et-error" class=" text-danger" style="display: none">Please select Employee type*</div>
                                </div> 
                                 </div>
                                
                                <div class="row">
                                        
                                <div class="form-group m col-sm-6">
                                    <label>Choose Subject Specialization : <i class="fa fa-arrow-down"></i></label>
                                    <select class="form-control select2 wizard-required" style="width: 100%" onchange="addSpec(this);" id="spec">
                                        <option selected="" disabled="" value="">--Specialization--</option>
                                        @foreach($subject as $s)

                                            <option value="{{$s->id}}">{{$s->subject }} {{ $s->code}} {{$s->descriptive_title}}</option>
                                        @endforeach
                                    </select>
                                      
                                    <div id="spec-error" class=" text-danger" style="display: none">Specialization is required*</div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <br>
                                     <ul class="todo-list mt-2" data-widget="todo-list" id="sub-list">
                                        
                                      </ul>
                                </div>
                                </div>
                                <script >
                                    function Dep(val)
                                    {
                                        @foreach($academic as $d)

                                            if (val == {{$d->id}})
                                             {
                                                 $('#college').val("{{$d->college}}")
                                             }
                                        @endforeach
                                       
                                    }
                                </script>
                                
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"><i class="fa fa-arrow-left"></i> Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn btn-success float-right">Next <i class="fa fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                    <fieldset class="wizard-fieldset" id="field-account">
                        <div class="card">
                            <div class="card-header">
                                <h5 style="color: maroon">Account</h5>
                                <div class="col-sm-12 ">
                                    <div class="form-group " id="u-teacher-id">
                                        <input type="text" class="form-control text-uppercase text-uppercase" id="teacher_id">
                                        <label for="teacher_id" class="wizard-form-text-label">Teacher's (ID)</label>
                                        <div class="wizard-form-error"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group" id="u-password">
                                        <input type="password" class="form-control wizard-required" id="password">
                                        <label for="password" class="wizard-form-text-label">Password</label>
                                        <div class="wizard-form-error text-danger">Password is required*</div>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-12 ">
                                    <div class="form-group " id="u-retype-password">
                                        <input type="password" class="form-control wizard-password" id="retype-password">
                                        <label for="retype-password" class="wizard-form-text-label">Retype Password</label>
                                        <div class="wizard-form-error text-danger"> Retype password is required*</div>
                                        <div class="text-danger" id="pass-error" style="display: none">Password not match</div>
                                    </div>
                                </div>
                                <input type="hidden" id="user-id">
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn btn-info float-left"> <i class="fa fa-arrow-left"></i> Previous</a>
                                    <button type="button" class="form-wizard-submit btn-success float-right" onclick="AddInstructor()"> Save <i class="fa fa-check fa-lg text-success"></i> </button>
                                      <button type="button" id="btn-update" class="form-wizard-btn btn-success float-right" onclick="SaveUpdate()" style="background-color: maroon; color: #ffffff; display: inline-block; min-width: 100px; min-width: 120px; padding: 10px; text-align: center;"> Update <i class="fa fa-check fa-lg text-success"></i> </button>
                                </div>
                            </div>
                        </div>
                    </fieldset> 
                </form>
            </div>
        </div>
    </div>
</section>

{{--Update Instructor--}}
{{--<div class="col-sm-12 mt-5" id="div-add-form">
  <div class="card">
    <div class="card-body">
        <div class="row">
        <div class="col-sm-3">
            <img src="{{asset('img/avatar.png')}}" alt="profile pic" height="100px" width="100px">
         </div>
         <div class="col-sm-9">
           <div class="row">
             <div class="form-group col-sm-6">
               <label>First Name</label>
               <input type="text" id="fname" class="form-control form-control-sm">
             </div>
           </div>
         </div>
       </div>
    </div>
  </div>

</div>--}}

<div class="modal fade" id="modal-spec">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
     <input type="hidden" id="sub">
     <div class="col-md-12 ">
      <input type="hidden" id="s-class-id2">
     
        <p class="text-muted" style="font-size: 14px;font-weight: lighter"><i class="fas fa-file-signature fa-lg text-info " ></i> Do you want to assign this subject <br> to his/her specialization<label  id="course-sec-name" style="text-decoration: underline;"></label>?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-primary btn-xs " id="add-subject" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-role">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
     <input type="hidden" id="sub">
     <div class="col-md-12 ">
      <input type="hidden" id="t-role">
      <input type="hidden" id="t-id">
     
        <p class="text-muted" style="font-size: 14px;font-weight: lighter"><i class="fas fa-user-edit fa-lg text-info " ></i> Do you want to update his/her <br> role as a <label  id="t-title" style="text-decoration: underline;"></label>?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-primary btn-xs " onclick="saveRole()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-view-instructor">
  <div class="modal-dialog modal-xl">
    <div class="modal-content" >
    <div class="modal-header bg-dark">
        <div class="col-md-12 text-center">
          <label id="" style="font-size: 19px">Instructor Details</label>
          <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 19px;color: red;cursor: pointer;">
          </i>
        </div>
     
    </div>
    <div class="modal-body">
       <label class="text-maroon">Personal Information</label>
      <div class="card col-sm-12">
        <div class="card-body row">

          <div class="col-sm-3">
            <label>First Name: <span class="text-secondary" id="v-fname"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Middle Name: <span class="text-secondary"  id="v-mname"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Last Name: <span class="text-secondary"  id="v-lname"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Ext. Name: <span class="text-secondary"  id="v-ext-name"></span></label>
          </div>
        </div>
      </div>
         <label class="text-maroon">Contact Information</label>
      <div class="card col-sm-12">
        <div class="card-body row">

          <div class="col-sm-3">
            <label>Email: <span class="text-secondary"  id="v-email"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Facebook: <span class="text-secondary"  id="v-facebook"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Contact #: <span class="text-secondary"  id="v-contact"></span></label>
          </div>
           <div class="col-sm-3">
            <label>Address: <span class="text-secondary"  id="v-address"></span></label>
          </div>
        </div>
      </div>
         <label class="text-maroon">Other Information</label>
      <div class="card col-sm-12">
        <div class="card-body row">

          <div class="col-sm-6">
            <label>Degree Finished: <span class="text-secondary"  id="v-degree-finished"></span></label>
          </div>
           <div class="col-sm-6">
            <label>MIT/MA/MS/Ph.D/Ed.D/: <span class="text-primary"  id="v-masteral"></span></label>
          </div>
           <div class="col-sm-6">
            <label>Department: <span class="text-secondary"  id="v-department"></span></label>
          </div>
           <div class="col-sm-6">
            <label>College: <span class="text-secondary"  id="v-college"></span></label>
          </div>
           <div class="col-sm-6">
            <label>Employee Status: <span class="text-secondary"  id="v-employee_type"></span></label>
          </div>
           <div class="col-sm-6">
            <label>Specialization: <span class="text-secondary"  ></span></label>
            <ul id="special"></ul>
          </div>
        </div>
      </div>
     
    </div>
  </div>
  </div>
</div>


<script>
     const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });
    $('#add-subject').click(function(){
         var sub= $('#sub').val();

         var subject = document.getElementsByName('subject[]');
        var len = document.getElementsByName('subject[]').length;

         if (len>0) {

            for(var i=0;i<len;i++)
            {
               
                if(subject.item(i).value == sub)
                {
                    Toast.fire({
                icon: 'error' ,
                title: ' Specialization already exist',
                  });
                    return false;
                }
            }
         }
         
         @foreach($subject as $s)
                if({{$s->id}} == sub)
                {
                       $('#sub-list').append('<li id="sub{{$s->id}}"><span class="text"  ><input type="hidden" name="subject[]" value="{{$s->id}}" >{{$s->subject}} {{$s->code}} {{$s->descriptive_title}}</span><small class="badge badge-secondary"></i></small><div class="tools" onclick="removeSub({{$s->id}})"><i class="fas fa-trash"></i></div></li>')
                }



         @endforeach   

        $('#modal-spec').modal('hide');
    })

    function removeSub(id)
    {
            $('#sub'+id).remove();

    }
    function addSpec(val)
    {
        $('#modal-spec').modal('toggle');
        $('#sub').val(val.value);
    }
</script>

<script>
    function NewInstructor(){
        $('#btn-update').hide();
        $('#div1-list').hide();
        $('#btn-add').hide();
        $('#div-update-form').hide();
        $('#text-title').hide();
        $('#div-add-form').show();
        $('#btn-return').show();
        $('#form-add-instructor')[0].reset();
        $('#subject').val('').select2();
         $('#field-personal').addClass('show');
         $('#field-account').removeClass('show');
         $('#icon-contact').removeClass('active');
         $('#icon-other').removeClass('active');
         $('#icon-contact').removeClass('activated');
         $('#icon-other').removeClass('activated');
         $('#icon-account').removeClass('activated');
         $('#icon-account').removeClass('active');
       // $('#teacher-image').attr('src','');

        $('#u-fname').removeClass('focus-input focus-select ');
        $('#u-mname').removeClass('focus-input focus-select ');
        $('#u-lname').removeClass('focus-input focus-select ');
        $('#u-ext_name').removeClass('focus-input focus-select ');
        $('#u-email').removeClass('focus-input focus-select ');
        $('#u-facebook').removeClass('focus-input focus-select ');
        $('#u-phone_number').removeClass('focus-input focus-select ');
        $('#u-address').removeClass('focus-input focus-select ');
        $('#u-teacher-id').removeClass('focus-input focus-select ');
        $('#u-password').removeClass('focus-input focus-select ');
        $('#u-retype-password').removeClass('focus-input focus-select ');


    }
</script>

<script>
    function GoBack(){
        $('#div1-list').show();
        $('#btn-add').show();
        $('#text-title').show();
        $('#div-add-form').hide();
        $('#div-update-form').hide();
        $('#btn-return').hide();
    }
</script>



<script>
      function Rule(id,rule)
  {
     $('#t-role').val(rule);
     $('#t-id').val(id);
     $('#t-title').text(rule);

     $('#modal-role').modal('toggle');
     
    
  }

   function saveRole(){

    var id =$('#t-id').val();
    var role =$('#t-role').val();


     $.ajax({

      type:'PUT',
      url : 'hr-update-instructor/'+id,
      data :
       {
          "_token"  :"{{ csrf_token() }}",
          'role' :role,
       

       },
       success:function(data)
       {
         if(data.length==0)
         {
            Toast.fire({
                icon: 'error' ,
                title: 'Cannot assign duplicate dean in same department',
                  });
         }
         else{

            $('#modal-role').modal('hide');

          Toast.fire({
                icon: 'success' ,
                title: 'Successfully Updated',
                  });
    

          $('#tr' + data.id + ' td:nth-child(3)' ).text(data.role);
         }
      
       },
       error:function(data)
       {

       }
     })

   }



</script>






<script>

    function View(id)
    {  
      $.get('hr-get-instructor-details/'+id,function(data){

             $('#v-fname').text(data.ins.fname)
             $('#v-mname').text(data.ins.mname.charAt(0))
             $('#v-lname').text(data.ins.lname)
             $('#v-ext-name').text(data.ins.extension_name)
             $('#v-email').text(data.ins.email)

              $.each(data.ins_info,function(key,ins){
                if(data.ins.id==ins.instructor_id){
                 
                 $('#v-facebook').text(ins.facebook)
                 $('#v-contact').text(ins.contact)
                 $('#v-address').text(ins.address)
                 $('#v-masteral').text(ins.masteral)
                 $('#v-college').text(ins.college)
                 $('#v-employee_type').text(ins.employee_type)
                }
              })
              $.each(data.cur,function(key,c){
                if(data.ins.department == c.academic_id){
                   $('#v-degree-finished').text(c.course)
                  
                }
               
              }) 
              $.each(data.dep,function(key,d){
               if(data.ins.department == d.id){
                $('#v-department').text(d.program_code)
                $('#v-college').text(d.college)
               }
              })
                $('#special').empty()
              $.each(data.ins_sub,function(k,sub){
                  $.each(data.subject,function(k,s){
                    if (s.id == sub.subject_id) 
                    {
                      $('#special').append('<ul>'+s.subject+' '+s.code+' '+s.descriptive_title+'</ul>')
                    }
                  })

              })
             

      })
      $('#modal-view-instructor').modal('toggle');
    }




    function AddInstructor(){

                       if($('#password').val() == "")
                       {
                        return false;
                       } 
                    if($('#password').val() !="" && $('#retype-password').val() !="")

                    {
                        if ($('#password').val() != $('#retype-password').val()) 
                        {
                            $('#pass-error').show();
                            return false;
                        }else
                        {
                            $('#pass-error').hide();
                        }
                    }
  
      var fname             = $('#fname').val();
      var mname             = $('#mname').val();
      var lname             = $('#lname').val();
      var ext_name          = $('#ext_name').val();
      var image             = $('#img-text').val();
      var college           = $('#college').val();
      var facebook          = $('#facebook').val();
      var address           = $('#address').val();
      var contact           = $('#phone_number').val();
      var program_graduated = $('#program_graduated').val();
      var department        = $('#department').val();
      var employee_type     = $('#employee_type').val();
      var subject           = $("input[name='subject[]']").map(function(){return $(this).val();}).get();
      var email             = $('#email').val();
      var password          = $('#password').val();
      var teacher_id        = $('#teacher_id').val();
      
       $.ajax({
  
        type: 'POST',
         url: 'hr-save-instructor',
        data:{
          "_token"  :"{{ csrf_token() }}",
          "fname"             :fname,
          "mname"             :mname,
          "lname"             :lname,
          "ext_name"          :ext_name,
          "teacher_id"        :teacher_id,
          "image"           :image,
          "email"             :email,
          "facebook"          :facebook,
          "address"           :address,
          "contact"           :contact,
          "program_graduated" :program_graduated,
          "department"        :department,
          "employee_type"     :employee_type,
          "subject"         :subject,
          "password"          :password,
          'college' :college,
          
  
      },
        success:function(value){
         $('#form-add-instructor')[0].reset();
         $('#spec').val('').select2();
         $('#field-personal').addClass('show');
         $('#field-account').removeClass('show');
         $('#icon-contact').removeClass('activated');
         $('#icon-other').removeClass('activated');
         $('#icon-account').removeClass('active');
            $('#teacher-image').attr('src', '');
  
          Toast.fire({
                icon: 'success' ,
                title: ' Successfully Created',
                  });
          location.reload();
        },
         error:function(data){

                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class=""></span>'+ value,
                  });
                  
                }); 
  
        }
       })
  
    }
  </script>


<script>

 function Update(id){
$('#field-personal').addClass('show');
         $('#field-account').removeClass('show');
         $('#icon-contact').removeClass('active');
         $('#icon-other').removeClass('active');
         $('#icon-contact').removeClass('activated');
         $('#icon-other').removeClass('activated');
         $('#icon-account').removeClass('activated');
         $('#icon-account').removeClass('active');
    $('#sub-list').empty();
     
      $('#user-id').val(id);

      
      $('.form-wizard-submit').hide();
      $('#btn-update').show();
       $.get('hr-edit-instructor/' +id,function(data){
       
        $('#fname').val(data.user.fname);
        $('#mname').val(data.user.mname);
        $('#lname').val(data.user.lname);
        $('#ext_name').val(data.user.extension_name);
        $('#teacher-image').attr('src',"{{asset('img/')}}/"+data.user.image);
        $('#img-text').val(data.user.image)
        $('#email').val(data.user.email);
        $('#phone_number').val(data.user.contact);
        $('#teacher_id').val(data.user.username);
        


        $('#u-fname').addClass('focus-input focus-select ');
        $('#u-mname').addClass('focus-input focus-select ');
        $('#u-lname').addClass('focus-input focus-select ');
        $('#u-ext_name').addClass('focus-input focus-select ');
        $('#u-email').addClass('focus-input focus-select ');
        $('#u-facebook').addClass('focus-input focus-select ');
        $('#u-phone_number').addClass('focus-input focus-select ');
        $('#u-address').addClass('focus-input focus-select ');
        $('#u-teacher-id').addClass('focus-input focus-select ');
        $('#u-password').addClass('focus-input focus-select ');
        $('#u-retype-password').addClass('focus-input focus-select ');

        $.each(data.instructor,function(key,value){
            $('#address').val(value.address);
            $('#facebook').val(value.facebook);
            $('#contact').val(value.contact);
           
            
            $.each(data.pg,function(key,value2){
                if(value2.id == value.program_graduated){
                
                    $('#program_graduated').append('<option selected value="'+value2.id+'">'+value2.course+'</option>')
                }
            })
          $('#college').val(value.college);
           $('#masteral').val(value.masteral);
           $('#master').val(value.masteral);
           $('#employee_type').val(value.employee_type);
           $('#phone_number').val(value.contact)
        })

        $.each(data.ins_sub,function(key,ins){
            $.each(data.sub,function(key,sub){
                    if (ins.subject_id == sub.id)
                     {
                        $('#sub-list').append('<li id="sub'+sub.id+'"><span class="text"  ><input type="hidden" name="subject[]" value="'+sub.id+'" >'+sub.subject+ ' '+sub.code+' '+sub.descriptive_title+'</span><small class="badge badge-secondary"></i></small><div class="tools" onclick="removeSub('+sub.id+')"><i class="fas fa-trash"></i></div></li>')
                     }

            })

        })
        $('#department').val(data.user.department);
         $('#u-password').hide();
         $('#u-retype-password').hide();
        
       
        $('#div1-list').hide();
        $('#btn-add').hide();
        $('#text-title').hide();
        $('#div-add-form').show();
        
        $('#btn-return').show();

       })


    
 }

function SaveUpdate(){

      var fname             = $('#fname').val();
      var mname             = $('#mname').val();
      var lname             = $('#lname').val();
      var ext_name          = $('#ext_name').val();
      var image             = $('#img-text').val();
      
      var email             = $('#email').val();
      var facebook          = $('#facebook').val();
      var address           = $('#address').val();
      var contact           = $('#phone_number').val();

     
      var program_graduated = $('#program_graduated').val();
      var masteral          = $('#masteral').val();
      var department        = $('#department').val();
      var college           = $('#college').val();
      var employee_type     = $('#employee_type').val();
      var subject           = $("input[name='subject[]']").map(function(){return $(this).val();}).get();
      
      var password          = $('#password').val();
      var teacher_id        = $('#teacher_id').val();
      var id                =$('#user-id').val();
     
  
        $.ajax({
            type: 'put',
             url: 'hr-update-instructor-info/'+id,
            data: {
                "_token": "{{ csrf_token() }}",
                  "fname"             :fname,
                  "mname"             :mname,
                  "lname"             :lname,
                  "ext_name"          :ext_name,
                  "image"             :image,
                  "email"             :email,
                  "facebook"          :facebook,
                  "address"           :address,
                  "contact"           :contact,
                  "program_graduated" :program_graduated,
                  "masteral"          :masteral,
                  "department"        :department,
                  "college"           :college,
                  "employee_type"     :employee_type,
                  "subject"           :subject,
                  "password"          :password,
                  "teacher_id"        :teacher_id,
                
            },
            success:function(response){
                Toast.fire({
                icon: 'success' ,
                title: ' Successfully Updated',
                  });

                location.reload();
           $('#tr' + response.user.id + ' td:nth-child(2)' ).text(response.user.fname+''+response.user.mname+''+response.user.lname);
           $('#tr' + response.user.id + ' td:nth-child(3)' ).text(response.user.email);
           $('#tr' + response.user.id + ' td:nth-child(4)' ).text(response.user.role);
           $('#tr' + response.user.id + ' td:nth-child(5)' ).text(response.department);

            },
           error: function (data)
            {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+ value,
                  });
                  
                }); 
            }
        })
     

    }
</script>
<script>
 
    setInterval(function () {
        $('#list').load('#list');
    }, 3000);

</script>




<script>
    jQuery(document).ready(function() {
        // click on next button
        jQuery('.form-wizard-next-btn').click(function() {

            var parentFieldset = jQuery(this).parents('.wizard-fieldset');
            var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
            var next = jQuery(this);
            var nextWizardStep = true;
            parentFieldset.find('.wizard-required').each(function(){
                var thisValue = jQuery(this).val();
    
                if( thisValue == "" ) {
                    jQuery(this).siblings(".wizard-form-error").slideDown();
                    nextWizardStep = false;
                }
                else {
                   
                }
                if($('#icon-other').hasClass('active'))
                {
                    
                   if($('#program_graduated').val() == null){
                    
                    $('#pg-error').show();
                    nextWizardStep = false;
                   }
                   else {
                    $('#pg-error').hide();
                }
                   if($('#department').val() == null){
                    
                   $('#dep-error').show();
                    nextWizardStep = false;
                   }
                   else {
                    $('#dep-error').hide();
                }
                   if($('#spec').val() ==null){
                    
                    $('#spec-error').show();

                    nextWizardStep = false;
                   }
                   else {
                    $('#spec-error').hide();
                }
                   if($('#employee_type').val()==null){
                    
                    $('#et-error').show();
                    nextWizardStep = false;
                   }
                   else {
                    $('#et-error').hide();
                }
                var len =document.getElementsByName('subject[]').length;
                 if(len > 0){
                    $('#spec-error').hide();
                    nextWizardStep =true;
                }else{
                    $('#spec-error').show();
                    nextWizardStep = false;
                    $('#spec').val(null)
                }
                }
                
            });
            
            if( nextWizardStep) {
                next.parents('.wizard-fieldset').removeClass("show","400");
                currentActiveStep.removeClass('active').addClass('activated').next().addClass('active',"400");
                next.parents('.wizard-fieldset').next('.wizard-fieldset').addClass("show","400");
                jQuery(document).find('.wizard-fieldset').each(function(){
                    if(jQuery(this).hasClass('show')){
                        var formAtrr = jQuery(this).attr('data-tab-content');
                        jQuery(document).find('.form-wizard-steps .form-wizard-step-item').each(function(){
                            if(jQuery(this).attr('data-attr') == formAtrr){
                                jQuery(this).addClass('active');
                                var innerWidth = jQuery(this).innerWidth();
                                var position = jQuery(this).position();
                                jQuery(document).find('.form-wizard-step-move').css({"left": position.left, "width": innerWidth});
                            }else{
                                jQuery(this).removeClass('active');
                            }
                        });
                    }
                });
            }
        });
        //click on previous button
        jQuery('.form-wizard-previous-btn').click(function() {
            var counter = parseInt(jQuery(".wizard-counter").text());;
            var prev =jQuery(this);
            var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
            prev.parents('.wizard-fieldset').removeClass("show","400");
            prev.parents('.wizard-fieldset').prev('.wizard-fieldset').addClass("show","400");
            currentActiveStep.removeClass('active').prev().removeClass('activated').addClass('active',"400");
            jQuery(document).find('.wizard-fieldset').each(function(){
                if(jQuery(this).hasClass('show')){
                    var formAtrr = jQuery(this).attr('data-tab-content');
                    jQuery(document).find('.form-wizard-steps .form-wizard-step-item').each(function(){
                        if(jQuery(this).attr('data-attr') == formAtrr){
                            jQuery(this).addClass('active');
                            var innerWidth = jQuery(this).innerWidth();
                            var position = jQuery(this).position();
                            jQuery(document).find('.form-wizard-step-move').css({"left": position.left, "width": innerWidth});
                        }else{
                            jQuery(this).removeClass('active');
                        }
                    });
                }
            });
        });
        //click on form submit button
        jQuery(document).on("click",".form-wizard .form-wizard-submit" , function(){
            var parentFieldset = jQuery(this).parents('.wizard-fieldset');
            var currentActiveStep = jQuery(this).parents('.form-wizard').find('.form-wizard-steps .active');
            parentFieldset.find('.wizard-required').each(function() {
                var thisValue = jQuery(this).val();
                if( thisValue == "" ) {
                    jQuery(this).siblings(".wizard-form-error").slideDown();
                }
                else {
                    jQuery(this).siblings(".wizard-form-error").slideUp();
                }
            });
        });
        // focus on input field check empty or not
        jQuery(".form-control").on('focus', function(){
            var tmpThis = jQuery(this).val();
            if(tmpThis == '' ) {
                jQuery(this).parent().addClass("focus-input");
            }
            else if(tmpThis =='' ){
                jQuery(this).parent().addClass("focus-select");
            }
            else if(tmpThis !='' ){
                jQuery(this).parent().addClass("focus-input");
            }
        }).on('blur', function(){
            var tmpThis = jQuery(this).val();
            if(tmpThis == '' ) {
                jQuery(this).parent().removeClass("focus-input");
                jQuery(this).siblings('.wizard-form-error').slideDown("3000");
            }
            else if(tmpThis !='' ){
                jQuery(this).parent().addClass("focus-select");
                jQuery(this).siblings('.wizard-form-error').slideUp("3000");
            }
            else if(tmpThis !='' ){
                jQuery(this).parent().addClass("focus-input");
                jQuery(this).siblings('.wizard-form-error').slideUp("3000");
            }
        });
    });
    
</script>

<script>
$('#list-of-instructor-table').DataTable({
         columnDefs: [
    {bSortable: false, targets: [3,4,5]} 
    ]
    });

function Masteral(val)
{
  if (val == 'Others')
   {
    $('#div-masteral').show();
    $('#masteral').val("")
   }else
   {
     $('#div-masteral').hide();
      $('#masteral').val(val)
   }
  
}
</script>
@endsection