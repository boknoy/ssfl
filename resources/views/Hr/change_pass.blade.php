@extends('layouts.Hr_layout')
@section('content')


<div class="content-header">
  <div class="container-fluid">
  	<div class="row">
  		<div class="col-md-6">
  			
  		</div>
  	</div>
  </div>
</div>
<div class="content">
	<div class="container-fluid">
		<div class="col-md-12">
			<div class="card">
				<div class="card-body">
					<div class="form-group mb-5">
						<h4>Change Password</h4>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<label class="col-form-label">Current Password <strong style="color: red">*</strong></label>
						</div>
						<div class="input-group col-md-5">
				          <input type="password" class="form-control" id="current_password" >
				          <div class="input-group-append">
				            <div class="input-group-text">
				              <span class="fas fa-unlock"></span>
				            </div>
				          </div>
				        </div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<label class="col-form-label">New Password  <strong style="color: red">*</strong></label>
						</div>
						<div class="input-group col-md-5">
				          <input type="password" class="form-control" id="new_password" >
				          <div class="input-group-append">
				            <div class="input-group-text">
				              <span class="fas fa-lock"></span>
				            </div>
				          </div>
				        </div>
					</div>
					<div class="form-group row">
						<div class="col-md-3">
							<label class="col-form-label">Confirm New Password  <strong style="color: red">*</strong></label>
						</div>
						<div class="input-group col-md-5">
				          <input type="password" class="form-control" id="confirm_password" >
				          <div class="input-group-append">
				            <div class="input-group-text">
				              <span class="fas fa-lock"></span>
				            </div>
				          </div>
				        </div>
					</div>
					<div class="form-group row">
						<div class="col-md-8">
							<button class="btn btn-outline-success col-md-3.5 float-right">Update Password</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection