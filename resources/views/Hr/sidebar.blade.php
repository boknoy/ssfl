
<style>
.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: maroon;
    color: white;
}
</style>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('img/spc.png')}}"  class="brand-image img-circle elevation-2">
      <span class="brand-text font-weight text-light"><b style="font-size: 15px;"> Human Resources </b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('img/'.Auth::user()->image)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
               <a href="#" data-widget="control-sidebar" data-slide="true" class="d-block">{{Auth::user()->fname}} {{substr(Auth::user()->mname,0,1)}}. {{Auth::user()->lname}}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{url('hr-dashboard')}}" class="nav-link @if(Request::is('hr-dashboard')) active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>

          {{--  <li class="nav-item" id="schedule_menu" >
              <a href="#" class="nav-link " id="schedule_href">
                <i class="nav-icon far fa-calendar-alt "></i>
                <p>Manage Schedule
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item" >
                  <a class="nav-link "  href="{{url('approval-request')}}" id="approval">
                    <i class="nav-icon far fa-circle" ></i>
                    <p>Approval Request</p>
                    <span class="badge badge-warning right"></span>
                  </a>
                </li>
              
                 <li class="nav-item" >
                  <a  class="nav-link "  href="{{url('hr-approved')}}" id="approved" >
                    <i class="nav-icon far fa-circle " ></i>
                    <p>Approved Schedule</p>
                    <span class="badge badge-warning right"></span>
                  </a>
                </li>
              </ul>
          </li>
          --}}

            <li class="nav-item" id="approval_nav" >
                  <a class="nav-link "  href="{{url('approval-request')}}" id="approval">
                    <i class="nav-icon fas fa-calendar-alt" ></i>
                    <p>Manage Schedule</p>
                    <span class="badge badge-warning right"></span>
                  </a>
                </li>

            <li class="nav-item" id="approved_nav" >
                  <a  class="nav-link "  href="{{url('hr-approved')}}" id="approved" >
                    <i class="nav-icon fas fa-download " ></i>
                    <p>Faculty Loads</p>
                    <span class="badge badge-warning right"></span>
                  </a>
                </li>

         <li class="nav-item ">
            <a href="{{url('hr-instructor-list')}}" class="nav-link @if(Request::is('hr-instructor-list')) active @endif">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Manage Instructor
              </p>
            </a>
          <!--  -->
          </li>  
          {{-- <li class="nav-item">
                <a href="{{url('hr-change-pass')}}" class="nav-link @if(Request::is('hr-change-pass')) active @endif">
                  <i class="nav-icon fas fa-lock"></i>
                  <p>Change Password</p>
                </a>
              </li>--}} 
              
           {{-- <li class="nav-item  " id="sched_menu">
              <a href="#" class="nav-link " id="sched_nav">
                <i class="nav-icon fas fa-file-signature"></i>
                <p>
                  Reports
                </p>
                <i class="fas fa-angle-left right"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('hr-schedule-report')}}" class="nav-link " id="sched">
                    <i class="far fa-copy nav-icon ml-3 "></i>
                    <p>Approval Schedules</p>
                  </a>
                </li>
                 <li class="nav-item">
                  <a href="{{url('hr-faculty-load-report')}}" class="nav-link " id="faculty">
                    <i class="far fa-copy nav-icon ml-3 "></i>
                    <p>Faculty Loads</p>
                  </a>
                </li>
                 <li class="nav-item">
                  <a href="{{url('hr-faculty-department-report')}}" class="nav-link " id="instructor">
                    <i class="far fa-copy nav-icon ml-3 "></i>
                    <p>Instructor</p>
                  </a>
                </li>
              
              
              </ul>
            </li>  --}}
            <li class="nav-item">
                  <a href="#" class="nav-link" data-toggle="modal" data-target="#logout">
                    <i class="fas fa-sign-out-alt nav-icon  "></i>
                    <p>Logout</p>
                  </a>
                </li>
          

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<div class="modal fade" id="logout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="a-id">
     <div class="col-md-12 ">
     
        <p  class="text-muted p-0" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-circle fa-lg text-warning " > </i><label  id="academic-name" style="text-decoration: underline;"></label> Are you sure you want logout?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
          <a class="btn btn-danger btn-xs" href="{{ route('logout') }}"
   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Yes, {{ __('logout') }} </a>
 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>