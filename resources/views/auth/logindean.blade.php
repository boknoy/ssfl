<link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>
<title>Subject Scheduling & Faculty Loading</title>
<link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<body class="hold-transition login-page" style="background: url('DashboardDesign/spclogo.png');  position: relative;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<div class="login-box">
  <div class="login-logo">
  <img src="{{asset('Dashboarddesign/spc.png')}}" alt="AdminLTE Logo" class="img-circle" style="width: 150px; height: auto;"></center><br>
    
  </div>
  <!-- /.login-logo -->
  <div class="card" style=" box-shadow: 20px 20px lightgray;">
    <div class="card-body login-card-body"><center>
            <b>Login Dean to start your session</b></center><br>
      <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="input-group mb-3">
            <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary" style="margin-left: 20px;">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember" >
                    Remember me
                </label>
            </div>
          </div>
      
        </div>
        <div class="social-auth-links text-center mb-3">
            <button type="submit" class="btn btn-secondary btn-block" style="background: maroon; color: white;">Sign In</button>
           
        </div>
        <!-- /.social-auth-links -->

        <p class="mb-1">
            @if (Route::has('password.request'))
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    I forgot my password
                </a>
            @endif
        </p>
      </form>

      
     
    </div>
    <!-- /.login-card-body -->
  </div>
</div>

</body>