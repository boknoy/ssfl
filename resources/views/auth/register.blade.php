<link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>
<title>Subject Scheduling & Faculty Loading</title>
<link rel="stylesheet" href="{{asset('plugins/jqvmap/jqvmap.min.css')}}">
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<body class="hold-transition register-page" style="background: url('DashboardDesign/spclogo.png');  position: relative;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;">
<div class="register-box">
  <div class="register-logo">
    <img src="{{asset('Dashboarddesign/spc.png')}}" alt="AdminLTE Logo" class="img-circle" style="width: 150px; height: auto;"></center><br>
  </div>

  <div class="card" style="box-shadow: 20px 20px lightgray;">
    <div class="card-body register-card-body"><center>
      <p class="login-box-msg"><b>Register a new membership</b></p>
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <input type="hidden" name="role" value="Admin">
        <input type="hidden" name="username" value="lance12">
        <input type="hidden" name="lname" value="Sumaylo">
        <input type="hidden" name="fname" value="lance">
        <input type="hidden" name="mname" value="mname">
        <input type="hidden" name="extension_name" value="lan">




        <div class="input-group mb-3">
            <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
            <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
      
        <div class="social-auth-links text-center">
            <button type="submit" class="btn btn-block" style="background: maroon; color: white;">Register</button>
           
        </div>
      </form>

     

      <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
    </div>
  </div>
</div>

</body>
