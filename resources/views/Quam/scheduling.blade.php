@extends('layouts.quam_layout')
@section('content')
@php
$todaydate = \Carbon\Carbon::now('Asia/Manila')->format('M');

@endphp
<style>
  .grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1);cursor: pointer }
.fc-v-event .fc-event-title {
  text-align: center;
    top: 0;
    bottom: 0;
    max-height: 100%;
    overflow: hidden;
}
.fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}

.fc-event-time{
  text-align: center;
}

#hover,#ahover{
  

 color: #323337;

}
#ahover:hover{
  color:white;
  color:#fefcff;
}
#hover:hover{
  color:white;
  border:1px solid #566d7e;
}
#curriculum-list td {
  border-top: none;
}
.fc-sticky{
  word-wrap: break-word;
}
 .dataTables_filter >label {

  width:100%;
  text-align:center;
  
}
 th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-col-header{
  background-color:#36414d;
  color:white;
}

h4  a  i {
  cursor: pointer;
}
.fc-timegrid-event{
  cursor: pointer;
}

.select2-selection__rendered {
    line-height: 35px !important;
}
.select2-container .select2-selection--single {
    height: 40px !important;
}
.select2-selection__arrow {
    height: 38px !important;
}

.#acord,#hover,#d-hover,#h-hover,#ahover{
  cursor: pointer;
}

.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

.modal-backdrop {
  z-index: -1;
}

</style>


<section class="content-header pb-1">
 
   <div class="container-fluid">
    <input type="hidden" id="todaydate" value="{{$todaydate}}">
 
      <button onclick="Goback()" class="btn btn-sm btn-outline-secondary mb-3 " style="display: none"  id="return-btn"><i class="fa fa-angle-left fa-md" > BACK</i> </button>
       <button onclick="Goback3()" class="btn btn-sm btn-outline-secondary mb-3 " style="display: none"  id="return-btn2"><i class="fa fa-angle-left fa-md" > BACK</i> </button>
       <div id="d1" class="col-md-12" style="display: none">
           <div  class="row" >
        <div class="col-md-2 pl-0">
          <button onclick="Goback2()" class="btn btn-sm btn-outline-secondary float-left" style="display: none" id="return-class"><i class="fa fa-angle-left fa-md"> CLASS</i> </button>
        </div>
        <div class="col-md-10 text-center">
           <label class="m-2 mt-2"><i class="fa fa-calendar-alt" style="font-size: 20px"> Manage Schedule Details</i></label>
        </div>
        </div>
       </div>
      <div class="col-md-12">
            <label id="t-name" style="font-size: 20px"><i class="fas fa-calendar-alt "  style="font-size: 23px"></i> Manage Scheduling</label>
      </div>
      <div class="col-md-12 text-center"  id="sched-title" style="display: none">
        <h5 id="academic-title2"></h5>
        <label id="curriculum-year"></label>
      </div>
   </div>
  
</section>

<div class="content" id="card1">
  <div class="container-fluid">
   <div class="col-md-12">
      <div class="row ">
        <div class="col-sm-4">
          <div class="card">
            <div class="card-header p-1 m-1 bg-dark">
              <label class="card-title col-12 text-center" >
                Curriculum Year
              </label>
            </div>
            <div class="card-body ">
              <div class="row text-center " id="folder-parent">
                @foreach($curriculum2 as $c)
                  <div  class="col-sm-4 row text-bold   grow ml-1" onclick="curYear('{{$c->curriculum_year}}')">
                    <i class="fa fa-graduation-cap fa-3x  text-warning" id="bg-{{$c->id}}"><sup class="ml-2 grow"><span class="badge badge-success navbar-badge">{{App\tbl_curriculum::where('curriculum_year',$c->curriculum_year)->count()}}</span></sup> </i>

                 
                      <label  class="pl-2"> {{$c->curriculum_year}}</label>
                 
                  </div>
                @endforeach
              </div>
             
            </div>
          </div>
        </div>
        <div class="col-sm-8">
          <div class="card">
            <div class="card-header m-1 p-1 bg-dark text-center ">
              <label class="card-title col-sm-12 " id="cur-year"> List of Curriculum</label>
            </div>
            <div class="card-body">
                <table id="curriculum-list" class="table pt-0 mt-0  pb-0 text-center">
        <thead>
          <tr style="display: none">
            <th class="text-center justify-content-center"></th>
          </tr>
        </thead>
        <tbody class="row">
          
             
       @php
        $color=array('#998693','#b3a595','#c2ac79','#a6a47f','#9ba4a9','#ada3a4','#c6b9ab','#d5c28a','#bbbc91','#b2bbc3','#d1c4be','#d8d0b9','#e8dda9','#cecbac','#c5cfd7','#95b9c7','#737ca1','#e5e4e2','#b7ceec','#e0ffff','#fbf6d9');
        @endphp
            
        @foreach($curriculum as $c)
        <tr class="  text-center" >
          <td style="border-top: none;"  >
         
        <div class=" text-center "  style="height: 100px;width: 142px" >
              @php
                shuffle($color) 
              @endphp
            
              <div onclick="ShowScheduling({{$c->id}},{{$c->academic_id}})" class="grow card " style="background-color: {{array_shift( $color )}}">
                <div class=" card-body" >
                    <h5>{{$c->curriculum_year}}</h5>
                   
                        <label>{{$c->course_code}}</label>
                     
                   
                   
                </div>
              </div>
        </div>
          
          </td> 
        </tr>
        @endforeach
       
        
      
           
        </tbody>
      </table>
            </div>
          </div>
      
        </div>
      </div>
   </div>
  
  </div>
</div>
      
<script type="text/javascript">

  function curYear(year)
  {
    
    @foreach($curriculum2 as $c )
        if("{{$c->curriculum_year}}"== year)
        {
          
          $('#bg-{{$c->id}}').removeClass('text-warning');
          $('#bg-{{$c->id}}').addClass('text-info');

        }else
        {
          $('#bg-{{$c->id}}').addClass('text-warning');
          $('#bg-{{$c->id}}').removeClass('text-info');
        }
    @endforeach
    var color = ['#998693','#b3a595','#c2ac79','#a6a47f','#9ba4a9','#c6b9ab','#d5c28a','#bbbc91'];
      
    var i=0;
     var t=  $("#curriculum-list").DataTable();
              t.clear();
      $.get('quam-get-curyear/'+year,function(data){
          $.each(data.year,function(key,value){
            $.each(data.academic,function(key,a){
              if (a.id == value.academic_id) 
              {
            
               
              t.row.add([
                  '<div class=" text-center "  style="height: 100px;width:142px;"><div onclick="ShowScheduling('+value.id+','+value.academic_id+')" class="grow card "style="background-color:'+color[i++]+'"><div class=" card-body" ><h5>'+value.curriculum_year+'</h5> <label>'+value.course_code+'</label></div></div></div>'
                  ]);
                
                t.draw();
              }
            
            })
             

          })
      })
    
                
                  
  }

</script>

<div class="col-sm-12">
  <div id="loading" style="display: none">
    <ul class="bokeh">
      <li></li>
      <li></li>
      <li></li>
    </ul>
  </div>
</div>

<div class="content pt-2" id="card2" style="display: none">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3 text-center">
        <div class="card">
          <div class="card-header" style="background-color:#c5cfd7">
            <h3 class="card-title text-bold col-sm-12 text-center" id="class-title">
            </h3>
          </div>
          <div class="card-body text-center">
            <div id="subject-list" class=" " style="">
            </div>
          </div>
        </div>
        
        <div class="alert alert-success " id="text-send" style="display: none;">
       
             <h5 class=""><i class="icon fas fa-check"></i> SUBMITTED</h5>
                  This schedule is already<br> submitted to all departments.
         
                </div>
        <button class="pb-2 mt-2 mb-2 btn btn-outline-success btn-sm btn-block" onclick="modalsendConfirm()" id="btn-send" style="display: none"><i class="fa fa-paper-plane"> Submit</i> </button>
      </div>
      <div class="col-md-9">
        <div class="card card-dark card-outline card-tabs">
          <div class="card-header p-0 pt-1 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="true">Calendar Schedule</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-tabs-two-profile-tab" data-toggle="pill" href="#custom-tabs-two-profile" role="tab" aria-controls="custom-tabs-two-profile" aria-selected="false">Tabulated Data</a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-two-tabContent">
              <div class="tab-pane fade show active" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                <div class="card" id="cal">
                  <div class="card-body p-0">
                    <div id="calendar"></div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="custom-tabs-two-profile" role="tabpanel" aria-labelledby="custom-tabs-two-profile-tab">
                <table id="schedule-table" class="table table-hover table-striped table-border text-sm">
                  <thead class="bg-dark">
                    <tr style="font-size: 13px">
                      <th >#</th>
                      <th>Subject Code</th>
                      <th>Room</th>
                      <th>Day</th>
                      <th>Time Start</th>
                      <th>Time End</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="schedule-list" style="font-size: 13px">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="content" id="card3" style="display: none">
  <div class="container-fluid">
    <div class="col-md-12">
      <div id="accordion" id="acord">
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-add-schedule">
  <div class="modal-dialog modal-lg">

     <div class="modal-content" >
      <div class="modal-header pt-2 pb-2" style="background-color: #343a40" >
        <label class="modal-title text-primary" id="s-subject-code" style="font-size: 16px;padding-right: 4px;"></label>
        <label class=" text-bold modal-title text-white" id="s-descriptive-title" style="font-size: 16px;"></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-white">&times;</span>
      </button>
    
      </div>
      <div class="modal-body">
        <form id="form-add-schedule">
          <input type="hidden" id="s-academic-id">
          <input type="hidden" id="s-class-id">
          <input type="hidden"  id="s-curriculum_id">
          <input type="hidden" id="s-id">
          <input type="hidden" id="s-school_year">
          <input type="hidden" id="s-year_level">
          <input type="hidden" id="s-semester">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <div class="row pl-4 pr-4">
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" name="day" id="mon" value="0">
                          <label for="mon">
                            Monday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-secondary d-inline">
                          <input type="radio" name="day" id="tue" value="1">
                          <label for="tue">
                            Tuesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-warning d-inline">
                          <input type="radio" id="wed" name="day" value="2">
                          <label for="wed">
                            Wednesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-success d-inline">
                          <input type="radio" id="thu" name="day" value="3">
                          <label for="thu">
                            Thursday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-maroon d-inline">
                          <input type="radio" id="fri" name="day" value="4">
                          <label for="fri">
                            Friday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-info d-inline">
                          <input type="radio" id="sat" name="day" value="5">
                          <label for="sat">
                            Saturday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-orange d-inline" >
                          <input type="radio" id="mw" name="day" value="6">
                          <label for="mw">
                            MW
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-dark d-inline">
                          <input type="radio" id="tth" name="day" value="7">
                          <label for="tth">
                            TTH
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                      <div class="input-group date" id="timepicker" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker" id="time-start" placeholder="--:-- --" />
                        <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                      <div class="input-group date" id="timepicker2" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker2" id="time-end" placeholder="--:-- --" />
                        <div class="input-group-append" data-target="#timepicker2" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                  <label style="font-size: 16px"><i class="fas fa-chalkboard-teacher"></i> Room</label>
                </div>
                <div class="col-md-12">
                  <div class="form-group pl-4 pr-4  col-md-12">
                  <select id="s-room" class="form-control select2" style="text-align-last: center;width: 100%;">
                    <option selected disabled>--Select--</option>
                  </select>
                </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                  <label style="font-size: 16px"><i class="fas fa-eye-dropper"></i> Schedule Color</label>
                </div>
                <div class="col-md-12 ">
                   <div class="form-group pl-4 pr-4">
              
                  <div class="input-group my-colorpicker2" >
                    <input type="text" class="form-control" id="color">

                    <div class="input-group-append">
                      <span class="input-group-text"><i class="fas fa-square"></i></span>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                </div>
              </div>
            </div>
          </div>

      
      </form>
      <div class="col-md-12">
               <button type="button" id="btn-save-schedule" class="btn btn-success float-right "><i class="fas fa-save"></i> SAVE</button>
            </div>
      </div>
    </div>
  </div>
</div>



<!--Modal Create Class-->
<div class="modal fade" id="modal-create-class">
  <div class="modal-dialog">
     <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">CLASS</font></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-create-class">
        @csrf
      
            @php
            $years =  range('2021', date('Y',strtotime('+20 year')));
            @endphp
            <div class="col-md-12 form-group">
              <label class="form-text text-white">SCHOOL YEAR:</label>
                <select  class="form-control form-control-sm" id="school_year" style="text-align-left: center;border-color:  #c3b091;color: " >
                  <option selected disabled >--Select--</option>
                  @foreach($years as &$year)
                  <option value="{{$year . '-' . ($year + 1)}}" >{{$year . '-' . ($year + 1)}}</option>
                  @endforeach
                </select>
            </div>
             <div class="col-md-12 form-group">
            
              <div class="row ">
                 <div class="col-md-12">
                    <label class="form-text text-white">SECTION:</label>
                 </div>
                <div class="col-md-10 text-left">
                     <select id="s-sections" class="form-control form-control-sm " style="text-align-left: center;border-color:  #c3b091;color: " >
                 <option selected disabled>--Select--</option>
                </select>
                </div>
             
             
              <div class="col-md-2" >
                   
                 <button type="button" class="btn btn-primary btn-sm" id="add-section"><i class="fas fa-plus"></i>  Add</button>
     
              </div>
             
              </div>
            </div>
          
            <input type="hidden" id="year-level-id">
             </form>
          </div>
       

         <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d">
              <div class="col-md-12 pl-3">
                 <button type="button"  class="btn btn-success btn-sm btn-block" onclick="saveClass()"><i class="fas fa-save"></i> Save </button>
              </div>
            </div>
    
   </div>
  </div>
</div>

<!-- Modal Update Class -->
<div class="modal fade" id="modal-update-class">
  <div class="modal-dialog ">
     <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">CLASS DETAILS</font></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-update-class">
        @csrf
      
            @php
            $years =  range('2021', date('Y',strtotime('+20 year')));
            @endphp
            <div class=" form-group">
              <label class="form-text text-white">SCHOOL YEAR:</label>
                <select  class="form-control form-control-sm" id="u-school-year" style="text-align-last: center;border-color:  #c3b091;color: " >
                  <option selected disabled >--Select--</option>
                  @foreach($years as &$year)
                  <option value="{{$year . '-' . ($year + 1)}}" >{{$year . '-' . ($year + 1)}}</option>
                  @endforeach
                </select>
            </div>
             <div class=" form-group">
              <label class="form-text text-white">SECTION:</label>
                <select id="u-s-sections" class="form-control form-control-sm" style="text-align-last: center;border-color:  #c3b091;color: " >
                  
                </select>
            </div>
          
            <input type="hidden" id="u-year-level-id">
            <input type="hidden" id="u-class-id">
             </form>
          </div>
       

         <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d">
               <button type="button"  class="btn btn-success btn-sm" onclick="saveUpdateClass()"><i class="fas fa"></i> UPDATE </button>
            </div>
    
   </div>
  </div>
</div>

<div class="modal fade" id="modal-alert-message">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-circle fa-lg text-warning " ></i> <label  id="s-subject-name" style="text-decoration: underline;"></label></u> is already assigned.<br>
         Do you want to proceed?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-primary btn-xs " onclick="AddSched()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-schedule-delete">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="sc-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 14px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to remove <label  id="s-sub-name" style="text-decoration: underline;"></label>?<br>
         This action cannot be undone.</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeSched()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-send-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
     
     <div class="col-md-12 ">
      <input type="hidden" id="s-class-id2">
     
        <p class="text-muted" style="font-size: 14px;font-weight: lighter"><i class="fa fa-paper-plane fa-lg text-info " ></i> Do you want to submit this <br>schedule to <label  id="course-sec-name" style="text-decoration: underline;"></label>?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-primary btn-xs " onclick="Send()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>


<div class="modal fade" id="modal-update-schedule">
  <div class="modal-dialog modal-lg">

     <div class="modal-content" >
      <div class="modal-header pt-2 pb-2" style="background-color: #343a40" >
        <label class="modal-title text-success" id="sub-title" style="font-size: 16px;padding-right: 4px;"></label>
        <label class=" text-bold modal-title text-white" id="sub-description" style="font-size: 16px;"></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-white">&times;</span>
      </button>
    
      </div>
      <div class="modal-body">
        <form id="form-update-schedule">
          <input type="hidden" id="u-id">
          <input type="hidden" id="u-subject">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <div class="row pl-4 pr-4">
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" name="u-day" id="mon2" value="0">
                          <label for="mon2">
                            Monday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-secondary d-inline">
                          <input type="radio" name="u-day" id="tue2" value="1">
                          <label for="tue2">
                            Tuesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-warning d-inline">
                          <input type="radio" id="wed2" name="u-day" value="2">
                          <label for="wed2">
                            Wednesday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-success d-inline">
                          <input type="radio" id="thu2" name="u-day" value="3">
                          <label for="thu2">
                            Thursday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-maroon d-inline">
                          <input type="radio" id="fri2" name="u-day" value="4">
                          <label for="fri2">
                            Friday
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-2 ">
                      <div class="form-group clearfix">
                        <div class="icheck-info d-inline">
                          <input type="radio" id="sat2" name="u-day" value="5">
                          <label for="sat2">
                            Saturday
                          </label>
                        </div>
                      </div>
                    </div>
                  
                 
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                      <div class="input-group date" id="timepicker3" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker3" id="u-time-start" placeholder="--:-- --" />
                        <div class="input-group-append" data-target="#timepicker3" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                      <div class="input-group date" id="timepicker4" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker4" id="u-time-end" placeholder="--:-- --" />
                        <div class="input-group-append" data-target="#timepicker4" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="far fa-clock"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                  <label style="font-size: 16px"><i class="fas fa-chalkboard-teacher"></i> Room</label>
                </div>
                <div class="col-md-12">
                  <div class="form-group pl-4 pr-4 col-md-12">
                    <select id="u-s-rooms" class="form-control  select3" style="text-align-last: center;width: 100%;">
                     
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body  pt-1 pb-1">
                <div class="col-md-12">
                    <label  style="font-size: 16px"><i class="fas fa-eye-dropper"></i> Schedule Color</label>
                </div>
                <div class="col-md-12">
                   <div class="form-group pl-4 pr-4">
                

                  <div class="input-group my-colorpicker2" >
                    <input type="text" class="form-control" id="u-color">

                    <div class="input-group-append">
                      <span class="input-group-text"><i class="fas fa-square " id="i-color"></i></span>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                </div>
              </div>
            </div>
          </div>

      
      </form>
      <div class="col-md-12">
               <button type="button" id="btn-update-schedule" class="btn btn-success btn-sm float-right "><i class="fas "></i> UPDATE</button>
            </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-view-class">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
    <div class="modal-header pt-0 pb-0"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
   
    </div>
    <div class="modal-body text-center">
       <div class="col-md-12 text-center">
       <label style="font-size: 16px">BACHELOR OF SCIENCE IN INFORMATION TECHNOLOGY </label>
     </div>
     <div class="col-md-12">
       <div class="card">
         <div class="card-body">
           <div id="calendar"></div>
         </div>
       </div>
     </div>
   
    

    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-add-section">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
       <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">SECTION</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
       </div>
      <div class="modal-body">
         <form id="form-add-section">
          @csrf
        
        <div class="col-md-12 pr-3 pl-3">
            <div class="form-group">
            <label class="form-label text-white" >COURSE:</label>
            <input type="text" id="course-name" class="form-control form-control-sm" readonly="">
            <input type="hidden" id="academic-id" >
          </div>
          <div class="form-group">
            <label class="form-label text-white">YEAR LEVEL :</label>
            <input type="text" id="year-title" class="form-control form-control-sm" readonly="">
          </div>
          <div class="form-group">
            <label class="form-label text-white">SECTION NAME :</label>
            <input type="text"  id="section-name" class="form-control form-control-sm text-uppercase" >

          </div>
             <div class="form-group">
                <label class="form-label text-white">Status :</label>
                <input type="text" id="u-is_active" value="Available" readonly="" class="form-control"> 
                  
            </div>
        </div>
        </form>
          </div>
       
      
  
        <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d">
          <div class="col-md-12 pr-3 pl-3">
            <button type="button" class="btn btn-success btn-sm btn-block" onclick="addsections()">
             <i class="fas fa-save"></i> Save</button>
          </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-schedule-success">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-dark"></span>
        </button>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12 ">
          <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="far fa-check-circle text-success " style="font-size: 20px"></i> Schedule Successfully Created!
          </p>
        </div>
        <div class="col-md-12 ">
          <button type="button" class="btn btn-primary btn-xs  col-md-2" >Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-schedule-update">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-dark"></span>
        </button>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12 ">
          <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="far fa-check-circle text-success " style="font-size: 20px"></i> Schedule Successfully Created!
          </p>
        </div>
        <div class="col-md-12 ">
          <button type="button" class="btn btn-primary btn-xs  col-md-2" >Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-schedule-error">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-dark"></span>
        </button>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12 ">
          <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="far fa-times-circle text-success " style="font-size: 20px"></i>Error 
          </p>
        </div>
        <div class="col-md-12 ">
          <button type="button" class="btn btn-primary btn-xs  col-md-2" >Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-schedule-error2">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-dark"></span>
        </button>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12 ">
          <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="far fa-times-circle text-success " style="font-size: 20px"></i>Error2 
          </p>
        </div>
        <div class="col-md-12 ">
          <button type="button" class="btn btn-primary btn-xs  col-md-2" >Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-schedule-3">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-dark"></span>
        </button>
      </div>
      <div class="modal-body text-center">
        <div class="col-md-12 ">
          <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="far fa-times-circle text-success " style="font-size: 20px"></i>Error3
          </p>
        </div>
        <div class="col-md-12 ">
          <button type="button" class="btn btn-primary btn-xs  col-md-2" >Ok</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-assigned-instructor">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
      <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">
      <input type="text" id="sc-id">

     
      </div>
    </div>
  </div>
</div>


      <div class="modal fade" id="modal-event">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" >
            <div class="modal-header" style="background-color: #83878a">
              <div class="col-md-12 text-center">
                <label class="modal-title text-white ml-4" style="font-size: 23px" id="sec-name"></label>
                <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color:red;cursor: pointer;">
                </i>
                <div class="col-sm-12">
                  <label class="modal-title text-white" id="s-sem"></label>
                </div>
              </div>
            </div>
            <div class="modal-body pt-4">
              <div class="col-md-12">
                <div class="card col-md-12">
                  <div class="card-body row">
                      <div class="col-sm-4">
                  <label class="">Subject Code: </label>
                  <label class="text-secondary" id="sub-subject"></label>
                </div>

                <div class="col-sm-8 ">
                  <label>Descriptive Title: </label>
                  <label class="text-secondary" id="sub-descriptive-title"></label>
                </div>
                  </div>
                </div>
                <div class="card col-md-12">
                  <div class="card-body row">
                    <div class="col-md-4">
                    <label >Units: </label>
                    <label class="text-secondary" id="sub-units"></label>
                  </div>
                    <div class="col-sm-4">
                    <label>Day/Time: </label>
                    <label class="text-secondary" id="sub-day-time"></label>
                  </div>
                  <div class="col-sm-4">
                    <label>Room No.: </label>
                    <label class="text-secondary" style="cursor: pointer;" onclick="viewRoom()"><u><i id="sub-room"> </i></u></label>
                  </div>
                
                  </div>
                </div>
                <input type="hidden" id="v-room-id">
                 <input type="hidden" id="v-school-year">
                 <input type="hidden" id="v-semester">
                 <input type="hidden" id="v-fac-id">
                <div class="card col-sm-12">
                  <div class="card-body col-md-12" id="instructor-info">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="modal fade" id="modal-room-schedule">
        <div class="modal-dialog modal-lg ">
          <div class="modal-content" >
            <div class="card-header bg-secondary">
              <div class="col-sm-12 text-center">
                <label  class="modal-title ml-3" id="v-room-title" style="font-size: 23px"></label>
                <i  class="close fa fa-times " data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
                </i>
                <div class="col-sm-12">
                  <label id="v-year-level" class="modal-title"></label>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body " id="calendar-room">

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-inc-schedule">
        <div class="modal-dialog modal-lg ">
          <div class="modal-content" >
            <div class="card-header bg-secondary">
              <div class="col-sm-12 text-center">
                <label  class="modal-title ml-3" id="v-inc-title" style="font-size: 23px"></label>
                <i  class="close fa fa-times " data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
                </i>
                <div class="col-sm-12 text-center">
                  <label class="modal-title" id="v-inc-year-level" ></label>
                </div>
              </div>
            </div>
             <div class="card ">
               
                   <div class="card-body " id="calendar-inc">
                     
                   </div>
              </div>
            </div>
          </div>
        </div>

<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script>
   const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });
  function scheduleIns()
  {
    $('#modal-inc-schedule').modal('show')

    var semester = $('#v-semester').val()
    var school_year = $('#v-school-year').val()
    var inc = $('#v-fac-id').val()
  
        var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar-inc');
    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      contentHeight:"auto",
      dayHeaderFormat :{ weekday: 'short' },
     });
    setTimeout(function(){ calendar.render() }, 1000);
  $.get('get-inc-schedule/'+inc+'/'+semester+'/'+school_year,function(data){
          $('#v-inc-title').text(data.fac.fname+' '+data.fac.lname)
          $('#v-inc-year-level').text(school_year+' '+sem(semester))
          $.each(data.load,function(k,l){

            $.each(data.schedule, function(key,value) {
             if (l.schedule_id == value.id) 
             {
               $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject + ' '+value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );
                         calendar.render();
                    $.each(data.room,function(k,r){
                      if(r.id == value.room_id)
                      {
                        var event = calendar.getEventById(value.id);
                                     //   event.setProp('title','');
                                        event.setProp('title',event.title+' '+r.room_description);
                      }
                    })
                          
               
                           
              }
                  
            });
             }
           
              
                });
          })
    })
  }
  function sem(val)
  {
    
    var x="";
    switch(val)
    {
      case '1st':
              x = 'FIRST SEMESTER';
              break;
      case '2nd':
              x = 'SECOND SEMESTER';
              break
      case  'summer':
              x ='SUMMER';
              break;

    
      }
      return x
    }
</script>
<script>

  function viewRoom()
  {
    $('#modal-room-schedule').modal('show')
    var room = $('#v-room-id').val()
    var semester = $('#v-semester').val()
    var school_year = $('#v-school-year').val()
     
        var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar-room');
    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
       contentHeight:"auto",
      dayHeaderFormat :{ weekday: 'short' },
     });

      setTimeout(function(){ calendar.render() }, 1000);
      calendar.removeAllEvents()
    $.get('get-room-schedule/'+room+'/'+semester+'/'+school_year,function(data){
              $('#v-room-title').text(data.room.room_description+' '+data.room.building)
              $('#v-year-level').text(school_year+' '+sem(semester))
             $.each(data.schedule, function(key,value) {
                 var section="";
                 $.each(data.class,function(k,c){
                  if(c.id == value.class_id)
                  {
                    $.each(data.section,function(k,sec){
                     if(sec.id == c.section_id)
                     {
                      section = sec.section_name
                     }
                  })
                  }
                  
                 
                 })  
            $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : section,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
               
                           
              }
                  
            });
              
                });

    })
    
    setTimeout(function(){ calendar.render() }, 1000);


  }
</script>
<Script>

function tConv24(time24) {
  var ts = time24;
  var H = +ts.substr(0, 2);
  var h = (H % 12) || 12;
  h = (h < 10)?("0"+h):h;  // leading 0 at the left for 1 digit hours
  var ampm = H < 12 ? " AM" : " PM";
  ts = h + ts.substr(2, 3) + ampm;
  return ts;
};

  function addsections()
  {
    
    var course = $('#course-name').val();
    var year = $('#year-title').val();
    var section =$('#section-name').val();
    var is_active = 1;

    $.ajax({
       type: 'POST',
      url : 'quam-add-section',
      data:{
           "_token"   : "{{ csrf_token() }}",
           "section_name" :section,
           "year_level" :year,
           "course" :course,
           "is_active" :is_active,
      },
      success:function(data)
      {

        $('#modal-add-section').hide();
         Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Added!!</span>',
                  });

        $('#s-sections').append('<option selected value="'+data.section.id+'">'+data.section.section_name+'</option>')
      },
      error:function(data)
      {
        $.each(data.responseJSON.errors, function(key,value) {
              Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+value,
                  });
                  
                }); 
      }
    })
  }
</script>

<script>
      $('.select2').select2()
       $('.select3').select2()
</script>

<script>
  $('#add-section').click(function()
  {
    $('#modal-add-section').modal('toggle');
  })


  function modalsendConfirm()
  {

    var id =$('#s-class-id2').val();

    

    $.get('quam-get-course-data/'+id,function(data){

       $('#course-sec-name').text(data.section.section_name);
       })

    $('#modal-send-confirm').modal('toggle');
   

  }

  function Send(){
    var class_id = $('#s-class-id2').val();

   $.get('quam-send-schedule/'+class_id,function(data){


         Toast.fire({
          icon: 'success' ,
          title: ' Successfully Send!!',
        });
         $('#text-send').show()
         $('#btn-send').hide()
         $('#modal-send-confirm').modal('hide');

   })
  }
</script>
<script>

  function CreateClass(id){
    var year;

    $('#s-sections')
    .empty()
    .append('<option selected disabled>--Select--</option>');

    $.get('quam-get-temp-data/'+id,function(data){

      
     $('#year-level-id').val(data.temp.id);
     
     $('#year-title').val(data.temp.year_level);

      year=data.temp.year_level;
      
      data
      $('#course-name').val(data.curr.course_code.toString());
         $('#academic-id').val(data.curr.academic_id);
         $('#section-name').val(data.curr.course_code+''+year.charAt(0)+'-');

     
     $.each(data.section, function(key,value) {
      
      if(data.temp.year_level == value.year_level){

        if(value.is_active == true)
        {
          $('#s-sections').append('<option value="'+value.id+'">'+value.section_name+'</option>');
        }
        
      } 
    }); 

    $('#modal-create-class').modal('toggle');
    })
  }
function saveClass(){

  
  var school_year=$('#school_year').val();
  var section_id=$('#s-sections').val();
  var year_level_id=$('#year-level-id').val();
  
  $.ajax({
    type:'POST',
     url:'quam-save-class',
    data:{

      "_token"        :"{{csrf_token()}}",
      //"academic_id"   :academic_id,
      //"curriculum_id" :curriculum_id,
      "school_year"   :school_year,
      "section_id"   :section_id,
      "year_level_id"   :year_level_id,
     // "semester"     :c_semester,

    },
    success:function(data){
     Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Added!!</span>',
                  });

      $('#form-create-class')[0].reset();
      $('#modal-create-class').modal('hide');
      
     
                          $('#tbl-class'+data.year.year_level+data.year.semester).append('<tr class="text-center">'+
                          '<td>'+data.class.school_year+'</td>'+
                          '<td>'+data.section.section_name+'</td>'+
                          '<td class="text-center "><div class="btn-group p-0 m-0 ">'+
                            ''+
                            '<button class="btn btn-outline-info btn-xs" onclick="EditClass('+data.class.id+')" ><i class="fa fa-edit"></i> Edit</button>'+
                            '<button class="btn btn-outline-dark btn-xs"  onclick="Schedule('+data.class.id+')"><i class="fa fa-clock"></i> Schedule</button>'+
                          '</div</td>'+
                          '</tr>');
                        },
      error:function(data){
          
         $.each(data.responseJSON.errors, function(key,value) {
              Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+value,
                  });
                  
                }); 


        
      }
  })

}


  function ViewClass(id){

    $('#modal-view-class').modal('toggle');

  }


  function EditClass(val){
    $('#u-s-sections').empty();
  
   $.get('quam-edit-class/'+val,function(data){

    
    $('#u-school-year').val(data.class.school_year);
    $('#u-class-id').val(val)


 
     $.each(data.section,function(key,sec){
      if(data.temp.year_level == sec.year_level){
           
           if(data.class.section_id == sec.id)
           {
            $('#u-s-sections').append('<option value="'+sec.id+'" selected>'+sec.section_name+'</option>');
           }else
           {
              if(sec.is_active==true){
              $('#u-s-sections').append('<option value="'+sec.id+'" >'+sec.section_name+'</option>');
              }
         
           }
         

      } 

     })
   
    
 

      $('#modal-update-class').modal('toggle');


   })




  
}

function saveUpdateClass(){

  var sy =$('#u-school-year').val()
  var section=$('#u-s-sections').val()
  var id =$('#u-class-id').val()

  $.get('quam-update-class/'+id+'/'+section+'/'+sy,function(data){
      if (data.f1 == 0) 
      {
          Toast.fire({
          icon: 'success' ,
          title: ' Successfully Updated!!',
        });
          $('#tr' + data.class.id + ' td:nth-child(1)' ).text(data.class.school_year);
          $('#tr' + data.class.id + ' td:nth-child(2)' ).text(data.class.section);
          $('#modal-update-class').modal('hide');
        }else
        {
          Toast.fire({
          icon: 'error' ,
          title: ' This class is already exists',
        });
        }
    

      
  })

}

</script>


<script>	

 
    
function ShowScheduling(val,val2) {
  calendar.removeAllEvents(); 
     $('#card1').hide();
     $('#t-name').hide();
     $('#sched-title').show();
     $('#card3').show();
     $('#return-btn').show();
     $('#accordion').empty();
     var todaydate = $('#todaydate').val();

     $.get('quam-class-get-data/'+val,function(data){

      $('#academic-title').text(data.academic.program_code)
       $('#academic-title2').text(data.curriculum.course)
        $('#curriculum-sem').text(data.curriculum.semester+" Semester")
         $('#curriculum-year_level').text(data.curriculum.year_level+" Year")
        $('#curriculum-year').text("Curriculum Year: "+data.curriculum.curriculum_year)
        $('#academic-year').text(data.curriculum.school_year)

          $.each(data.year,function(key2,value2){
            var year_name;
            var semester_name;
             

            switch(value2.year_level)
            {
              case '1st':
                  year_name = 'FIRST';
                  break;
              case '2nd':
                  year_name = 'SECOND';
                  break;
              case '3rd':
                  year_name = 'THIRD';
                  break;
              case '4th':
                  year_name = 'FOURTH';
                  break;
              case '5th':
                  year_name = 'FIFTH';
                  break;
            }
            switch(value2.semester)
            {
              case '1st':
                  semester_name = 'FIRST';
                  break;
              case '2nd':
                  semester_name = 'SECOND';
                  break;
              case 'summer':
                  semester_name = 'SUMMER';
                  break;
             
            }

              if(todaydate == "Aug" || todaydate == "Sep" || todaydate == "Oct" || todaydate == "Nov" || todaydate == "Dec"){
              fs = "FIRST";
                if(semester_name == fs)
                 {
            $('#accordion').append('<div class="card  mb-0" id="hover">'+
                '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                  '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                    '<a id="ahover" class="d-block w-100 collapsed" data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="false" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                    '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                  '</h4>'+
                '</div>'+
                '<div id="collapse'+value2.id+'" class="collapse" data-parent="#accordion">'+
                   '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                  '<table class="table table-bordered table-sm text-dark">'+
                        '<thead>'+
                            '<th class="text-center">School Year</th>'+
                            '<th class="text-center">Section </th>'+
                            '<th class="text-center" style="width:250px">Action</th>'+
                        '</thead>'+
                        '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                        '</tbody>'+
                          
                      '</table>'+
                '</div>'+
              '</div>');

                }else{
                  $('#accordion').append('<div class="card  mb-0" id="hover">'+
                      '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                        '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                          '<a id="ahover" class="d-block w-100 collapsed" data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="false" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                          '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                        '</h4>'+
                      '</div>'+
                    
                      '<div id="collapse'+value2.id+'" class="collapse" data-parent="#accordion">'+
                        '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                        '<table class="table table-bordered table-sm text-dark">'+
                              '<thead>'+
                                
                                  '<th class="text-center">School Year</th>'+
                                  '<th class="text-center">Section </th>'+
                                  '<th class="text-center" style="width:250px">Action</th>'+
                              '</thead>'+
                              '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                              '</tbody>'+
                                
                            '</table>'+
                      '</div>'+
                    '</div>');
                } }else if(todaydate == "Jan" || todaydate == "Feb" || todaydate =="Mar" || todaydate =="Apr" || todaydate == "May"){

              ss = "SECOND";
                if(semester_name == ss) {
                  $('#accordion').append('<div class="card  mb-0" id="hover">'+
                      '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                        '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                          '<a id="ahover" class="d-block w-100 " data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="true" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                          '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                        '</h4>'+
                      '</div>'+
                    
                      '<div id="collapse'+value2.id+'" class="collapse-open" data-parent="#accordion">'+
                        '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                        '<table class="table table-bordered table-sm text-dark">'+
                              '<thead>'+
                                
                                  '<th class="text-center">School Year</th>'+
                                  '<th class="text-center">Section </th>'+
                                  '<th class="text-center" style="width:250px">Action</th>'+
                              '</thead>'+
                              '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                              '</tbody>'+
                                
                            '</table>'+
                      '</div>'+
                    '</div>');
                }else{

                  $('#accordion').append('<div class="card  mb-0" id="hover">'+
                      '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                        '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                          '<a id="ahover" class="d-block w-100 collapsed" data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="false" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                          '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                        '</h4>'+
                      '</div>'+
                    
                      '<div id="collapse'+value2.id+'" class="collapse" data-parent="#accordion">'+
                        '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                        '<table class="table table-bordered table-sm text-dark">'+
                              '<thead>'+
                                
                                  '<th class="text-center">School Year</th>'+
                                  '<th class="text-center">Section </th>'+
                                  '<th class="text-center" style="width:250px">Action</th>'+
                              '</thead>'+
                              '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                              '</tbody>'+
                                
                            '</table>'+
                      '</div>'+
                    '</div>');
                }
            }else if(todaydate == "Jun" || todaydate == "Jul"){
              var s = "SUMMER";

                if(semester_name === s) {

                  $('#accordion').append('<div class="card  mb-0" id="hover">'+
                      '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                        '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                          '<a id="ahover" class="d-block w-100 " data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="false" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                          '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                        '</h4>'+
                      '</div>'+
                    
                      '<div id="collapse'+value2.id+'" class="collapse-open" data-parent="#accordion">'+
                        '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                        '<table class="table table-bordered table-sm text-dark">'+
                              '<thead>'+
                                
                                  '<th class="text-center">School Year</th>'+
                                  '<th class="text-center">Section </th>'+
                                  '<th class="text-center" style="width:250px">Action</th>'+
                              '</thead>'+
                              '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                              '</tbody>'+
                                
                            '</table>'+
                      '</div>'+
                    '</div>');
                }else{
                  $('#accordion').append('<div class="card  mb-0" id="hover">'+
                      '<div class="card-header" style="background-color: #b2bbc3;" id="d-hover">'+
                        '<h4 class="card-title w-100 text-bold  text-center" id="h-hover">'+
                          '<a id="ahover" class="d-block w-100 collapsed" data-toggle="collapse" href="#collapse'+value2.id+'" aria-expanded="false" >'+year_name+' YEAR '+semester_name+' SEMESTER '+ 
                          '<i class="fas fa-angle-double-down text-center"></i>'+'</a>'+
                        '</h4>'+
                      '</div>'+
                    
                      '<div id="collapse'+value2.id+'" class="collapse" data-parent="#accordion">'+
                        '<button onclick="CreateClass('+ value2.id+')" class="btn btn-outline-primary float-right btn-xs  m-2"><i class=" fa fa-plus"> Create Class</i></button>'+
                        '<table class="table table-bordered table-sm text-dark">'+
                              '<thead>'+
                                
                                  '<th class="text-center">School Year</th>'+
                                  '<th class="text-center">Section </th>'+
                                  '<th class="text-center" style="width:250px">Action</th>'+
                              '</thead>'+
                              '<tbody id="tbl-class'+value2.year_level+value2.semester+'">'+
                              '</tbody>'+
                                
                            '</table>'+
                      '</div>'+
                    '</div>');
                }
            }

              $.each(data.class,function(key,value){

            $.each(data.section,function(key3,value3){
              if(value3.id == value.section_id && value2.id == value.year_level_id){


              $('#tbl-class'+value2.year_level+value2.semester).append('<tr id="tr'+value.id+'" class="text-center">'+
                          '<td>'+value.school_year+'</td>'+
                          '<td>'+value3.section_name+'</td>'+
                          '<td class="text-center "><div class="btn-group p-0 m-0 ">'+
                            ''+
                            '<button class="btn btn-outline-info btn-xs" onclick="EditClass('+value.id+')"><i class="fa fa-edit"> </i> Edit</button>'+
                            '<button class="btn btn-outline-dark btn-xs"  onclick="Schedule('+value.id+')"><i class="fa fa-clock"></i> Schedule</button>'+
                          '</div</td>'+
                          '</tr>');
              }
            })
            
          })
          


          })//each year level
          
                  


      });//get
     //  calendar.render();      
      
  }//funtion ShowScheduling    
</script>




<script>


     var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },
      contentHeight:"auto",
     
        eventClick: function(info)
        {
          var id = info.event.id
    
         $.get('quam-get-schedule/'+id,function(data){
                 var day = parseInt( data.schedule.day);
                $('#sub-subject').text(data.single_subject.subject+' '+data.single_subject.code);
                $('#sub-descriptive-title').text(data.single_subject.descriptive_title);
                $('#sub-units').text(data.single_subject.units);
                $('#sub-day-time').text(days(day)+'|'+moment(data.schedule.time_start,['HH:mm']).format("hh:mm")+'-'+tConv24(data.schedule.time_end));
                $('#v-room-id').val(data.single_room.id);
                $('#v-school-year').val(data.schedule.school_year);
                $('#v-semester').val(data.schedule.semester);
                $('#sub-room').text(data.single_room.room_description);
                
                $('#sec-name').text(data.section.section_name+' | '+data.class.school_year);

              
                var sem=data.schedule.semester;

                $('#s-sem').text(sems(sem)+' SEMESTER');

                if(data.load.length > 0){

                  $.each(data.load,function(key,lo){
                  if(data.schedule.class_id == lo.class_id && data.schedule.subject_id == lo.subject_id){

                     $.each(data.fac,function(key,fac){
                      if(lo.instructor_id == fac.id){
                          
                          $.each(data.fac_info,function(key,fac2){
                            if(fac.id == fac2.instructor_id){
                              $('#v-fac-id').val(fac.id)


                              $('#instructor-info').append('<div class="row"><div class="col-md-12 bg-dark"><label class="text-left pt-1">Instructor</label><label class="float-right"><i class="fa fa-info-circle text-info float-right pt-2" style="cursor:pointer" onclick="scheduleIns()"> Schedule</i></label></div><div class="col-md-4 "><label >Name: <span class="text-secondary">'+fac.fname+' '+fac.mname.charAt(0)+'. '+fac.lname+'</span></label></div><div class="col-md-4"><label >Contact #: <span class="text-secondary">'+fac2.contact+'</span></label></div><div class="col-md-4 "><label >Email : <span class="text-secondary">'+fac.email+'</span></label></div><div class="col-md-5"></div></div>');
                            }
                          })


                         }
                     })
                }
                   
                })

                }
                else{
       
                   $('#instructor-info').append('<div class="text-danger">No instructor assign yet.</div>');
                }
               
            
          })
            
          $('#modal-event').modal('show');
          $('#instructor-info').empty();
        }

   });

    $('#custom-tabs-two-home-tab').click(function(){
   
    var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '22:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },
      contentHeight:"auto",
       eventClick: function(info)
        {
          var id = info.event.id
      
         $.get('quam-get-schedule/'+id,function(data){
                 var day = parseInt( data.schedule.day);
                $('#sub-subject').text(data.single_subject.subject+' '+data.single_subject.code);
                $('#sub-descriptive-title').text(data.single_subject.descriptive_title);
                $('#sub-units').text(data.single_subject.units);
                $('#sub-day-time').text(days(day)+' |'+moment(data.schedule.time_start,['HH:mm']).format(" hh:mm")+'-'+tConv24(data.schedule.time_end));
                $('#sub-room').text(data.single_room.room_description);
                
                if(data.load.length > 0){

                  $.each(data.load,function(key,lo){
                  if(data.schedule.class_id == lo.class_id && data.schedule.subject_id == lo.subject_id){

                     $.each(data.fac,function(key,fac){
                      if(lo.instructor_id == fac.id){
                          
                          $.each(data.fac_info,function(key,fac2){
                            if(fac.id == fac2.instructor_id){
                              $('#v-fac-id').val(fac.id)

                               $('#instructor-info').append('<div class="row"><div class="col-md-12 bg-dark"><label class="text-left pt-1">Instructor</label><label class="float-right"><i class="fa fa-info-circle text-info float-right pt-2" style="cursor:pointer" onclick="scheduleIns()"> Schedule</i></label></div><div class="col-md-4 "><label >Name: <span class="text-secondary">'+fac.fname+' '+fac.mname.charAt(0)+'. '+fac.lname+'</span></label></div><div class="col-md-4"><label >Contact #: <span class="text-secondary">'+fac2.contact+'</span></label></div><div class="col-md-4 "><label >Email : <span class="text-secondary">'+fac.email+'</span></label></div><div class="col-md-5"></div></div>');
                            }
                          })


                         }
                     })
                }
                   
                })

                }
                else{
       
                   $('#instructor-info').append('<div class="text-danger">No instructor assign yet.</div>');
                }
               
            
          })
            
          $('#modal-event').modal('show');
          $('#instructor-info').empty();
        }

      
      
   });


     
      calendar.removeAllEvents();
     var id= $('#s-class-id').val();
      
      $.get('quam-get-schedule-list/'+id,function(data){
           
      var i =0;
        if(data.schedule.length==0)
        {
        calendar.removeAllEvents();
         calendar.render();//4  
         var events =[];
        }
        $.each(data.schedule, function(key,value) {

             
            $.each(data.subject, function(key,value2) {
            
             
                
               
              if (value.subject_id == value2.id) {


                

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;

                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
                  var day =parseInt(value.day);

                      
                      var events={
                         id:value.id,    
                      title          : value2.subject+' '+ value2.code,
                      start  : new Date(y, m, d + day, time_start_hr, time_start_min),
                      end            : new Date(y, m, d+ day, time_end_hr, time_end_min),
                      allDay         : false,
                        //url            :'https://www.facebook.com',
                      backgroundColor: value.color, //Success (green)
                    
                   
                      };
                      
                      
                     calendar.addEvent(events);
                       calendar.render();  //5
                       
                 
              
                  
                 
               
              }
                  
            });
              
        });

      })
   

    })
   
    function sems(sem)
    {
      var sems="";
         switch(sem)
                {
                  case "1st":
                        sems = 'FIRST';
                        break;
                  case "2nd":
                        sems = 'SECOND';
                        break;
                  case "summer":
                        sems = 'SUMMER';
                        break;
                }
                  return sems;
                 
    }

     function days(day)
  {

     var days="";
                switch(day)
                {
                  case 0:
                        days = 'MON';
                        break;
                  case 1:
                        days = 'TUE';
                        break;
                  case 2:
                        days = 'WED';
                        break;
                  case 3:
                        days = 'THU';
                        break;
                  case 4:
                        days = 'FRI';
                        break;
                  case 5:
                        days = 'SAT';
                        break;
                  case 6:
                        days = 'MW';
                        break;
                  case 7:
                        days = 'TTH';
                        break;


                }
                return days;
  }

     
  function Schedule(id){
    calendar.removeAllEvents();
    $('#subject-list ').empty();
     $('#card2').hide();
     $('#loading').show();
     $('#sched-title').hide();
     $('#card3').hide();
     $('#return-btn').hide();
     $('#d1').show();
     $('#t-name').hide();
     $('#return-class').show();
     $("#schedule-table").DataTable().clear().draw();
     $('#custom-tabs-two-home').addClass('show active');
     $('#custom-tabs-two-home-tab').addClass('active');
     $('#custom-tabs-two-profile-tab').removeClass('active');
     $('#custom-tabs-two-profile').removeClass('show active');
     var room;
     var room_name;
     var bldg;
      $.get('quam-get-schedule-list/'+id,function(data){
        $('#card2').show()
        $('#loading').hide()
        calendar.render()
        if(data.subsched.length > 0)
        {
          $('#btn-send').hide();
        $('#text-send').show();
        }
        else{
          $('#btn-send').show();
          $('#text-send').hide();
        }
          $('#s-class-id').val(data.class.id);
          $('#s-class-id2').val(data.class.id);
          $('#class-title').text(data.section.section_name +' | '+data.class.school_year)
           var total=0;
            $.each(data.subject, function(key,value) {
          
          //Append Subject
         
              $.each(data.curr_sub,function(key2,value2){
                if(value2.subject_id == value.id)
                {
                  
                  if (value2.time_remain==0.00)
                   {
                    $('#subject-list ').append('<div  class="external-event text-uppercase btn btn-outline-danger col-md-12" onclick="getSubject('+value2.curr_sub_id+','+id+')" style="pointer-events: none;">'+value.subject+ ' '+value.code+' | <span class="text-lowercase"> '+value2.time_remain+' hours remaining</span></div>');
                    total=total+value2.time_remain;
                   }else
                   {
                     $('#subject-list ').append('<div class="external-event text-uppercase btn btn-outline-secondary col-md-12" onclick="getSubject('+value2.curr_sub_id+','+id+')" style="cursor:pointer;">'+value.subject+ ' '+value.code+' | <span class="text-lowercase"> '+value2.time_remain+' hours remaining</span></div>');
                      total=total+value2.time_remain;
                   }
                  
                  
                
                }
              })
            })
             if (total == 0 && data.subsched.length == 0)
                    {
                      $('#btn-send').show()
                      $('#text-send').hide();
                    }
                    else
                    {
                       $('#btn-send').hide()

                    }
             $.each(data.schedule, function(key,value) {
                    $.each(data.room,function(key,r){
                      if(r.id == value.room_id)
                      {
                        room =r.room_description;
                        bldg = r.building;
                      }
                    })
            $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {
                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+ value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                        

                        };
                         calendar.addEvent(  events );
                        calendar.render();  //6
               
                 
              var t=  $("#schedule-table").DataTable();
              t.search('').draw();
              t.row.add([
        '',
                value2.subject+' '+value2.code,
                room+'-'+bldg,
                days(day),
                moment(value.time_start,['HH:mm A']).format(" h:mm A"),
                moment(value.time_end,['HH:mm A']).format(" h:mm A") ,
                '<div class="row btn-group" >'+
                    '<button class="btn btn-outline-info btn-xs  " onclick="editschedule('+value.id+')" > <i class="fa fa-edit"></i> </button>'+
                    '<button class="btn btn-outline-danger btn-xs  " onclick="modalschedRemove('+value.id+')"> <i class="fa fa-trash"></i> </button>'+
                  '</div>']).node().id = 'tr'+value.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
               
              }
                  
            });
              
                });

      })
      calendar.render();//1
  }


  function modalschedRemove(id){

     $('#sc-id').val(id);
   $.get('quam-get-sched-data/'+id,function(data){
     $('#s-sub-name').text(data.sub.subject+' '+data.sub.code);
   
   })
     $('#modal-schedule-delete').modal('toggle');
  
  }
  function removeSched() {
       
       var id =$('#sc-id').val();
      
      $.get('quam-remove-schedule/'+id,function(data){
           
           Toast.fire({
          icon: 'success' ,
          title: ' Successfully Removed!!',
        });

         $('#modal-schedule-delete').modal('hide');

       // $('#tr'+id).remove();
       Schedule(data.class_id);

      })
  }
  function days(day)
  {

     var days="";
                switch(day)
                {
                  case 0:
                        days = 'MON';
                        break;
                  case 1:
                        days = 'TUE';
                        break;
                  case 2:
                        days = 'WED';
                        break;
                  case 3:
                        days = 'THU';
                        break;
                  case 4:
                        days = 'FRI';
                        break;
                  case 5:
                        days = 'SAT';
                        break;
                  case 6:
                        days = 'MW';
                        break;
                  case 7:
                        days = 'TTH';
                        break;


                }
                return days;
  }


function editschedule(id){
     $.get('quam-get-schedule/'+id,function(data){

      $.each(data.room,function(key,ro){
        if(data.schedule.room_id == ro.id){

        
         $('#u-s-rooms').append('<option value="'+ro.id+'" selected>'+ro.room_description+' - '+ro.building+'</option>');
           }else
           {
            if(ro.is_active==true){

               $('#u-s-rooms').append('<option value="'+ro.id+'" >'+ro.room_description+' - '+ro.building+'</option>');

             }
           
          }

        
      })

      $.each(data.subject,function(key,value){
        if(data.schedule.subject_id == value.id){

        $('#sub-title').text(value.subject+'-'+value.code);
        $('#sub-description').text(''+value.descriptive_title);
        $('#u-subject').val(value.id);
        }
      
        })

      $('#u-id').val(id);

      switch(data.schedule.day)
      {
        case '0' :
              $('#mon2').prop('checked',true);
              break;
        case '1' :
              $('#tue2').prop('checked',true);
              break;
        case '2' :
              $('#wed2').prop('checked',true);
              break;
        case '3' :
              $('#thu2').prop('checked',true);
              break;
        case '4' :
              $('#fri2').prop('checked',true);
              break;
        case '5' :
              $('#sat2').prop('checked',true);
              break;
      }
      $('#i-color').css('color','');
      var color =data.schedule.color;

      //$('input[name="u-day"]:checked').val(data.schedule.day);

      $('#u-time-start').val(moment(data.schedule.time_start,'hh:mm').format('h:mm A'));
      $('#u-time-end').val(moment(data.schedule.time_end,'hh:mm').format('h:mm A'));

      $('#u-color').val(data.schedule.color);
      $('#i-color').css('color',color);

       $('#modal-update-schedule').modal('toggle');

     })
   
  }
  $('#btn-update-schedule').click(function(){
    var id =$('#u-id').val();
    var day =$("input[name='u-day']:checked").val();
    var time_start=$('#u-time-start').val();
    var time_end=$('#u-time-end').val();
    var room_id=$('#u-s-rooms').val();
    var color=$('#u-color').val();
    var subject = $('#u-subject').val();

      var now = moment(time_end,"HH:mm a"); //todays date
      var end = moment(time_start,"HH:mm a"); // another date
      var duration = moment.duration(now.diff(end));

      var t_hr=duration.asHours();
      if (day > 5)
       {
        t_hr = t_hr * 2
       }
     
      if (t_hr == 0)
       {
          Toast.fire({
                    icon: 'error' ,
                    title: ' Time start must be not equal to time end',
                  });
            return false
       }
      if (t_hr < 0 )
       {
       Toast.fire({
                    icon: 'error' ,
                    title: ' Time end is invalid',
                  });
            return false
       }
   $.get('update-time/'+id+'/'+subject+'/'+t_hr,function(data){

            if (data)
             {
              Toast.fire({
                    icon: 'error' ,
                    title: ' Updated time is invalid',
                  });
              return false
             }else
             {
              $.ajax({                                 
      type:'put',
       url: 'quam-update-schedule/'+id,
      data:{
        "_token"      :"{{csrf_token()}}",
        "day"         :day,
        "time_start"  :time_start,
        "time_end"    :time_end,
        "room_id"     :room_id,
        "color"       :color,
        't_hour'      :t_hr,
      },
      success:function(data){

       
        //$('#modal-schedule-update').modal('toggle');

        if (data.msg !="")
         {
          Toast.fire({
                    icon: 'error' ,
                    title: ''+data.msg,
                  });
          return false
         }
        $('#modal-update-schedule').modal('hide');
        $('#form-update-schedule')[0].reset();


        Toast.fire({
          icon: 'success' ,
          title: ' Updated Successfully',
        });


       
            Schedule(data.class.id)
           

                  
            

      
      
    
      },
      error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {

                  //$('#modal-schedule-error').modal('toggle');
                    Toast.fire({
                    icon: 'error' ,
                    title: ' '+value,
                  });
                  
                }); 
             }

    })
             }
   })

    /**/
      
  })


 $('#btn-save-schedule').click(function(){
    
    var class_id = $('#s-class-id').val();
    var academic_id=$('#s-academic-id').val();
    var subject_id=$('#s-id').val();
    var curriculum_id=$('#s-curriculum_id').val();
    var school_year =$('#s-school_year').val();
    var year_level=$('#s-year_level').val();
    var semester =$('#s-semester').val();
    var time_start=$('#time-start').val();
    var time_end =$('#time-end').val();
    var room =$('#s-room').val();
    var day=$("input[name='day']:checked").val();
    var color = $('#color').val();
      var now = moment(time_end,"HH:mm a"); //todays date
      var end = moment(time_start,"HH:mm a"); // another date
      var duration = moment.duration(now.diff(end));

      if (time_start == "") 
      {
       
         Toast.fire({
                    icon: 'error' ,
                    title: ' Time start is required*',
                  });
            return false
      }
      if (time_end == "") 
      {
       
         Toast.fire({
                    icon: 'error' ,
                    title: ' Time end is required*',
                  });
            return false
      }
      var t_hr=duration.asHours();
      if (day > 5)
       {
        t_hr = t_hr * 2
       }
     
      if (t_hr == 0)
       {
            Toast.fire({
                    icon: 'error' ,
                    title: ' Time start must be not equal to time end',
                  });
            return false
       }
      if (t_hr < 0 )
       {
       Toast.fire({
                    icon: 'error' ,
                    title: ' Time end is invalid',
                  });
            return false
       }
       
   $.get('get-time/'+class_id+'/'+subject_id+'/'+t_hr,function(data){
          if(data < 0)
          {
           
            Toast.fire({
                    icon: 'error' ,
                    title: ' Time is not valid for this subject',
                  });
            return false
          }else{
            $.ajax({

      type:'POST',
      url : 'quam-add-scheduling',
      data : {

        "_token": "{{ csrf_token() }}",
        'academic_id' : academic_id,
        'curriculum_id'  : curriculum_id,
        'subject_id'  : subject_id,
        'school_year' :school_year,
        'year_level'  :year_level,
        'semester'    :semester,
        'time_start'  : time_start,
        'time_end'    : time_end,
        'room_id'   : room,
        'day'   : day,
        'class_id' : class_id,
        'color' :color,
        't_hr' : t_hr,
       // 'section' :section,

      },
      success:function(data){
        var events =[];
         


        if (data.msg!="") {

          toastr.error('<span class="mr-1"> </span>'+data.msg,)
          {{-- Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="mr-1"> </span>'+data.msg,
                  }); --}}
           calendar.render();//2
          return false;
         }else
         {
          Schedule(data.class_id);

           Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="mr-1"> Successfully Added</span>',
                  });

         }
              
                  
          $('#modal-add-schedule').modal('hide');
           $('#form-add-schedule')[0].reset(); 
      },
       error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
                  });
                  
                }); 
             }
    })
          }

   })

  //  var section= $('#s-section').val();

   /**/

    
  })//on click save function
 calendar.render();//3
</script>

<script >
 
</script>

<script>
  function Goback(){
  $('#card1').show();
  $('#d1').hide();
  $('#t-name').show();
   $('#card2').hide();
   $('#return-btn').hide();
   $('#sched-title').hide();
   $('#card3').hide();
    var t= $('#subject-list').empty();

}

function Goback2(){
  $('#d1').hide();
   $('#card1').hide();
  $('#card2').hide();
  $('#card3').show();
   $('#return-btn').show();
   $('#return-class').hide();
   $('#sched-title').show();

}

function getSubject(id,class_id){

  $('#s-room')
    .empty()
    .append('<option selected disabled>--Select--</option>');
    ;

    var sub_id;
   $.get('quam-get-scheduling-data/'+id,function(data){
        $('#s-id').val(data.curr_sub.subject_id);
        $('#s-academic-id').val(data.academic.id);
        
        $('#s-curriculum_id').val(data.curriculum.id);

        $('#s-semester').val(data.year.semester);
        $('#s-year_level').val(data.year.year_level);

        $('#s-year_level').val(data.year.year_level);

        sub_id = data.curr_sub.subject_id;

        $.each(data.room,function(key,ro){
            
            if(ro.is_active==true){



               $('#s-room').append('<option value="'+ro.id+'">'+ro.room_description+' '+ro.building+'</option>');
            }
        })


       
       $.each(data.class,function(key,cl){

        if(data.curr_sub.year_level_id == cl.year_level_id){

          $('#s-school_year').val(cl.school_year);
        }

       })

     
      

        $.each(data.section, function(key,value) {
                
                    $('#s-section').append('<option value="'+value.id+'">'+value.section_name+'</option>');
                }); 

  
  
    $.get('quam-get-subject-schedule/'+sub_id+'/'+class_id,function(data){

         //$('#s-academic-id').val(data.academic_id);
        
        //$('#s-curriculum_id').val(data.curriculum_id);
        $('#s-subject-code').text(data.subject.subject+' '+data.subject.code);
        $('#s-descriptive-title').text('   '+data.subject.descriptive_title);

        $('#s-subject-name').text(data.subject.subject+' '+data.subject.code);
          if(data.is_sched.length > 0)
          {  
            //confirm('This subject is already assign proceed anyway?');
            $('#modal-alert-message').modal('show');
          }else{

            $('#modal-add-schedule').modal('show');

          }
          //
       // $('#modal-add-schedule').modal('show');
    })
  })
}

function AddSched(){
  $('#modal-alert-message').modal('hide');
  $('#modal-add-schedule').modal('show');
}
</script>

<script>
  //             add subject
 
</script>

<script>

  function RoomSelect_update(val){
    
      
      alert(val($('#u-room-id option:selected').text()+'-'));

        
  }
</script>

<script>


$('#timepicker').datetimepicker({
      format: 'LT',
      stepping: 30,
      disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
    })
$('#timepicker2').datetimepicker({
      format: 'LT',
      stepping: 30,
       disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
    })

$('#timepicker3').datetimepicker({
      format: 'LT',
      stepping: 30,
     
      disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
      
    })
$('#timepicker4').datetimepicker({
      format: 'LT',
      stepping: 30,
       disabledHours: [0, 1, 2, 3, 4, 5, 6,22.5,23,24]
    })


</script>

<script type="text/javascript">

    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })
    $("#calendar").width(750).css({'margin-left': 'auto','margin-right': 'auto'});
    function Goback3()
    {
      location.reload();
    }
</script>

@if(session('resched'))
  <script type="text/javascript">
     $('#card1').hide();
     $('#t-name').hide();
     $('#sched-title').show();
     $('#card3').show();
     $('#return-btn').hide();
     $('#accordion').empty();
     
     $('#return-btn2').show()
  Schedule({{session('resched')}})
    $('#return-class').hide();

  </script>

@endif

@endsection