@extends('layouts.Quam_layout')
@section('content')
<style type="text/css">
	.fc-event-title 
  {
    text-align: center;
    padding: 0 1px;
    white-space: normal;
    font-weight: bold;
    font-size: 20px;
   }
   .fc-event-time
{
  font-size: 1em !important;
  text-align: center;
}
th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-col-header{
  background-color: #343a40;
  color:white;
}
.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
<section class="content-header">
	<div class="container-fluid">
		<div class="col-md-12 ">
				<div class="row">
				
					<div class="col-md-12">
						<label 
						style="font-size: 23px"><i class="far fa-calendar-alt"></i> Class Report</label>
					</div>
				 </div>
			</div>
		
	</div>
</section>

<div class="content">
  <div class="container-fluid">
  
      <div class="card">
        <div class="card-body">
            <div class="col-sm-12 d-flex justify-content-center">
          <div class="form-group col-sm-2">
            
            <select class="form-control " style="text-align-last: center;" id="sy" onchange="filter(this.value)">
              <option value="" selected="" disabled="">SCHOOL YEAR</option>
              @php
                $sy = App\tbl_class::all()->unique('school_year');
              @endphp
              @foreach($sy as $y)

                  <option value="{{$y->school_year}}">{{$y->school_year}}</option>
              @endforeach
            </select>
          </div>
            <div class="form-group col-sm-2">
             @php 
                    $curr = App\tbl_curriculum ::all()->unique('course_code');
                  @endphp
            <select class="form-control " style="text-align-last: center;" id="course" onchange="filter(this.value)">
                  <option selected="" disabled="">--COURSE--</option>
              @foreach($curr as $c)
                  <option value="{{$c->course_code}}">{{$c->course_code}}</option>
 
              @endforeach
            </select>
           

          </div>
          <div class="form-group col-sm-3">
            
            <select class="form-control " style="text-align-last: center;" id="sem" onchange="filter(this.value)">
               <option value="" selected="" disabled="">SEMESTER</option>
               <option value="1st">First Semester</option>
               <option value="2nd">Second Semester</option>
               <option value="summer">Summer</option>
            </select>
           

          </div>
          <div class="form-group col-sm-2">
            
            <select class="form-control " style="text-align-last: center;" id="yl" onchange="filter(this.value)">
               <option value="" selected="" disabled="">YEAR LEVEL</option>
               <option value="1st">First Year</option>
               <option value="2nd">Second Year</option>
               <option value="3rd">Third Year</option>
               <option value="4th">Fourth Year</option>
               <option value="5th">Fifth Year</option>
            </select>
           

          </div>
          <div class="form-group col-sm-2">
            <select class="form-control" id="sec" style="text-align-last: center;" disabled="">
                <option value="" disabled="" selected=""> SECTION</option>
            </select>
          </div>
          <div class="form-group" >
            <button class="btn btn-success " onclick="Search()"> <i class="fa fa-search"> Search</i></button>
          </div>
        </div>
      </div>
    </div>
  	 <div class="col-sm-12">
          <div class="form-group m-2 ">
  
          <button class="btn btn-info float-right mb-2" onclick="Print2()" id="print" style="display: none"><i class="fa fa-print"> Print</i></button>
         
        </div>
        <div id="no-data" style="display: none;">
            <div class="alert alert-info alert-dismissible" >
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                  No data to show.
                </div>
          </div>
        <div class="col-sm-12">
        <div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
      </div> 
      <div class="card col-sm-12">
       
        <div class="card-body" id="toPrint" style="display: none">
          
             <div class="col-md-12 text-center">
                <div class="col-md-12">
                  <img src="{{asset('/img/spc.png') }}" width="100px" height="90px" style="position: absolute;margin-left: -175px">
                  <strong style="font-size: 20px;font-family: Italic;"> Southern Philippines College</strong><br>
                  <span style="font-size: 16px;font-family: Italic;font-weight: bold">Julio Pacana Street, Licuan, Cagayan de Oro City</span><br>
                  <label class="pt-2 pb-1" id="college-name" style="font-size: 16px"></label><br>
                </div>
                <label id="program-name" style="font-size: 16px"></label><br>
                <label id="class-title" style="font-size: 15px"></label>
                <input type="hidden" id="class_id">
             </div>
              <div id="calendar">
                  
                </div>
        </div>

      </div> 

       <div class="col-md-12  text-right">
      
        <button class="btn btn-info mb-2 ml-2" onclick="Print3()" id="print2" style="display: none"><i class="fa fa-print"> Print</i></button>
 
       </div>
         
      <div class="col-md-12 ">
         <div class="card">
        <div class="card-body" id="SchedPrint" style="display: none;" >

  <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
          <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
          <style type="text/css">
            @page {
    size: landscape;
    
  }
          </style>
           <div class="col-md-12 text-center">
                <div class="col-md-12">
                  <img src="{{asset('/img/spc.png') }}" width="100px" height="90px" style="position: absolute;margin-left: -175px">
                  <strong style="font-size: 20px;font-family: Italic;"> Southern Philippines College</strong><br>
                  <span style="font-size: 16px;font-family: Italic;font-weight: bold">Julio Pacana Street, Licuan, Cagayan de Oro City</span><br>
                  <label class="pt-2 pb-1" id="college-name1" style="font-size: 16px"></label><br>
                </div>
                <label id="program-name1" style="font-size: 16px"></label><br>
                <label id="class-title1" style="font-size: 15px"></label>
                <input type="hidden" id="class_id">
             </div>
          <table class="table  table-bordered text-sm">
            <thead>
              <th>SCODE</th>
              <th>DESCRIPTIVE TITLE</th>
              <th class="text-center">UNITS</th>
              <th>DAY</th>
              <th class="text-center">TIME</th>
              <th class="text-center"> ROOM</th>
            </thead>
            <tbody id="tbl-schedule">
              
            </tbody>
          </table>
        </div> 
      </div>
      </div>
     </div>
	  </div>
	</div>
	</div>
</div>

</div>
  
<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<script>
	$('#report_menu').addClass('menu-open');
	$('#report_href').addClass('active');
	$('#subject_report').addClass('active');
   const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });
    
   function Print2()
   {
      var class_id = $('#class_id').val()
      window.open("{{url('print')}}/"+class_id);
   }
   
   function Print3()
   {
     var class_id = $('#class_id').val()
      window.open("{{url('print2')}}/"+class_id);

   }
  function filter(val)
  {
     var sy = $('#sy').val()
     var sem = $('#sem').val()
     var yl = $('#yl').val()
     var course = $('#course').val()
     if (sy == null)
      {
        return false
      }
      if (course == null)
       {
        return false
       }
      if(sem == null)
      {
        return false
      }
      if (yl == null)
       {
        return false
       }


       $('#no-data').hide()
       $('#loading').show()
       $('#toPrint').hide()
       $('#SchedPrint').hide()
       $('#print').hide()
        $('#print2').hide()
      $.get('quam-get-section/'+sy+'/'+course+'/'+sem+'/'+yl,function(data)
      {
        $('#loading').hide()
          if(data.count >0)
          {
            $('#sec').empty()
            $.each(data.class,function(k,c){
               $('#sec').append('<option value="'+c.id+'">'+c.section+'</option>')
            })
            $('#sec').attr('disabled',false)
          }else
          {
             $('#sec').attr('disabled',true).empty().append('<option selected disabled>SECTION</option>')
             $('#no-data').show()
             $('#print').hide()
             $('#print2').hide()
          }
      })
  }

</script>
<script>
	 var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');

    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },

   });


  function Search()
  {
    var sy = $('#sy').val()
    var sem = $('#sem').val()
    var sec = $('#sec').val()
    var yl = $('#yl').val()
    var course = $('#course').val()
    if (sy == null)
     {
         Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"> School Year is required* </span>',
                  });
         return false
     }
     if (course == null)
     {
         Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"> Course is required* </span>',
                  });
         return false
     }
     if (sem == null)
      {
        Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"> Semester is required* </span>',
                  });
         return false
      }
      if (yl == null) 
      {
        Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"> Year Level is required* </span>',
                  });
        return false

      }

      if (sec==null)
       {
        Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"> Section is required* </span>',
                  });
         return false
       }
       $('#loading').show()
       $('#no-data').hide()
       $('#print').hide()
       $('#print2').hide()
       $('#toPrint').hide()
       $('#SchedPrint').hide()
       $.get('class-report/'+sec,function(data){
        $('#toPrint').show()
        $('#SchedPrint').show()
        $('#print').show()
        $('#print2').show()
        $('#loading').hide()
        $('#class_id').val(data.class.id)

          $('#class-title').text(data.class.section +' | '+data.class.school_year)
            
            $('#program-name').text(data.academic.program_name);
            $('#college-name').text(data.academic.college);
            
             $('#class-title1').text(data.class.section +' | '+data.class.school_year)
            
            $('#program-name1').text(data.academic.program_name);
            $('#college-name1').text(data.academic.college);
            
              calendar.removeAllEvents()
             $.each(data.schedule, function(key,value) {
                    $.each(data.room,function(key,r){
                      if(r.id == value.room_id)
                      {
                        room =r.room_description;
                        bldg = r.building;
                      }
                    })
            $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+ value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                        

                        };
                         calendar.addEvent(  events );
                        calendar.render();  
               
                 
              
              }
                  
            });
              
                });
calendar.render();
          $('#tbl-schedule').empty()
            $.each(data.schedule2,function(k,s){
              $.each(data.subject,function(k,sub){
                if (sub.id == s.subject_id)
                 {
                  var room;
                  $.each(data.room,function(k,r){
                    if (r.id == s.room_id) 
                    {
                      room = r.room_description;
                    }
                  })
                  $('#tbl-schedule').append('<tr><td class="text-center">'+sub.subject+' '+sub.code+'</td><td>'+sub.descriptive_title+'</td><td class="text-center">'+sub.units+'</td><td id="td-day'+s.id+'" ></td><td>'+moment(s.time_start,['HH:mm ']).format(" h:mm ") +' -'+moment(s.time_end,['HH:mm A']).format(" h:mm A")+'</td><td class="text-center">'+room+'</td></tr>')
                 
                
                 }
              })
               var str="";
                $.each(data.schedule,function(k,s1){
                    if (s.subject_id == s1.subject_id)
                     {
                      str = str+ STR(s1.day)
                      $('#td-day'+s.id).text(str)
                      str = str+'-'
                     }
                  })
            })
       })

  }

function STR(day)
{
  var x="WLA"
  switch(day)
  {
    case '0':
        x = 'M'
        break
    case '1':
        x = 'T'
        break
    case '2':
        x = 'W'
        break
    case '3':
        x = 'TH'
        break

    case '4':
        x = 'FRI'
        break
    case '5':
        x = 'SAT'
        break


  }
  return x
}

    function show(id)
    {
    	$('#modal-schedule').modal('toggle');
    	schedule(id);
    	 
    }
    function Print()
    {
    	//calendar.removeAllEvents(); 
    	//schedule(id);

		var prtContent = document.getElementById("toPrint");
		//var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    	var WinPrint = window.open();
    	//WinPrint.document.open()
    	
    	WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body class="hold-transition ">'+prtContent.innerHTML+'</body></html>')
    	  //WinPrint.focus();
    

    	//WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.title='Class Schedule';
    
      //  WinPrint.document.close();
      //  WinPrint.focus();

        WinPrint.print();
       WinPrint.close();
       calendar.render();

      //  $("#calendar").width(750).css({'margin-left': 'auto','margin-right': 'auto'});
    }
    function schedule(id)
    {
    	 calendar.removeAllEvents(); 
    var room;
     var room_name;
     var bldg;
      $.get('quam-get-schedule-list/'+id,function(data){
          //$('#s-class-id').val(data.class.id);
          $('#class-title').text(data.section.section_name +' | '+data.class.school_year)
            
            $('#program-name').text(data.curr.course);
             $.each(data.academic,function(key,acad){
              if(data.section.academic_id == acad.id){
               
               $('#college-name').text(acad.college);
              }
             })
         
              
             $.each(data.schedule, function(key,value) {
                    $.each(data.room,function(key,r){
                      if(r.id == value.room_id)
                      {
                        room =r.room_description;
                        bldg = r.building;
                      }
                    })
            $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : value2.subject+' '+ value2.code,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                        

                        };
                         calendar.addEvent(  events );
                        calendar.render();  
               
                 
              
              }
                  
            });
              
                });
calendar.render();
      })
      
     
  
    
}
</script>
<script type="text/javascript">
	@foreach($class as $c )
	  	  			@foreach($year as $y)
	  	  				@if($y->id == $c->year_level_id)
	  	  					@foreach($section as $s)
	  	  						@if($c->section_id == $s->id)
	  	  							$('#td{{$c->id}}').text(years("{{$y->year_level}}"))
	  	  							$('#td2{{$c->id}}').text(semester("{{$y->semester}}"))

	  	  						@endif

	  	  					@endforeach
	  	  				@endif
	  	  			@endforeach
	  	  		@endforeach
	function semester(semester)
	{
		var x="";
		switch(semester)
		{
			case '1st':
				x="FIRST SEMESTER"
				break;
			case '2nd':
				x="SECOND SEMESTER"
				break;
			case 'summer':
				x="SUMMER"
				break;
		}
		return x;
	}
	function years(year)
	{
		var x="";
		switch(year)
		{
			case '1st':
				x="FIRST YEAR"
				break;
			case '2nd':
				x="SECOND YEAR"
				break;
			case '3rd':
				x="THIRD YEAR"
				break;
			case '4th':
				x="FOURTH YEAR"
				break;
			case '5th':
				x="FIFTH YEAR"
				break;
		}
		return x;
	}
</script>

<script type="text/javascript">
  function Print3()
  {
      var title =  $('#class-title1').text()
      var prtContent = document.getElementById("SchedPrint");
    //var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
      var WinPrint = window.open("");
      //WinPrint.document.open()
      
      //WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body>'+prtContent.innerHTML+'</body></html>')
        //WinPrint.focus();
     
      WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.title=title+'-B';
    
       WinPrint.document.close();
       WinPrint.focus();

        WinPrint.print();
       WinPrint.close();

  }
</script>


@endsection