<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SSFL</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
   
 <link rel="icon" href="{{asset('DashboardDesign/spc.png')}}" type="image/logo.ico"/>



  <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
  
  <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
       <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>




 <style type="text/css">
  .fc-event-title 
  {
    text-align: center;
    padding: 0 1px;
    white-space: normal;
    font-weight: bold;
    font-size: 20px;
   }
.fc-event-time
{
  font-size: 1em !important;
  text-align: center;
}
        @media print { 
            .visible-print {
                display: inherit !important;
            }

            .hidden-print {
                display: none !important;
            }
              body, html, #wrapper {
     width: 100%;
    }
 
      
   



    </style>

</head>
<body>

  <!-- Main content -->


       <div class="col-md-12 text-center">
              <div class="col-md-12">
          <img src="{{asset('/img/spc.png') }}" width="100px" height="90px" style="position: absolute;margin-left: -175px">
        
       <strong style="font-size: 20px;font-family: Italic;"> Southern Philippines College</strong><br>
        <span style="font-size: 16px;font-family: Italic;font-weight: bold">Julio Pacana Street, Licuan, Cagayan de Oro City</span><br>
      
        <label class="pt-2 pb-1" style="font-size: 16px">{{App\tbl_academic::where('id',$section->academic_id)->first()->college}}</label><br>
         </div>
                 <label id="program-name"  style="font-size: 16px">{{App\tbl_academic::where('id',$section->academic_id)->first()->program_name}}</label><br>
                 <label id="class-title" style="font-size: 15px">{{$section->section_name}} | {{$class->school_year}}</label>
             </div>
     

       <div class="col-md-12">
        <input type="hidden" id="me" value="{{$id}}" name="me">
         <table class="table table-bordered">
           <thead>
             <tr>
               <th>Subject Code</th>
               <th>Descriptive Title</th>
               <th>Unit</th>
                <th>Day</th>
               <th>Time</th>
               <th>Room</th>
             </tr>
           </thead>
           <tbody id="tbl-schedule">
            
        
           </tbody>
         </table>
       </div>
     
    
 
 <script src="{{asset('plugins/moment/moment.min.js')}}"></script>  
  

<script >
  



</script>



<script>
  window.addEventListener("load", window.print());
</script>
</body>
</html>
