@extends('layouts.Quam_layout')

@section('content')
<style type="text/css">
	.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
<section class="content-header">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12 "  style="font-size: 20px"><label><i class=" fas fa-graduation-cap fa-2x"></i>Curriculum Report</label></div>
		
		</div>
	</div>
</section>

<div class="content">
  <div class="container-fluid">
   <div class="col-md-12">
	  <div class="card "  id="card1">
	  	<div class="card-body text-center col-sm-12">
		  <div class="row pb-0 pt-0 mb-0 mt-0 d-flex justify-content-center col-sm-12" >
			<div class="col-md-5 ">
			  <div class="form-group row ">
			    <label class="col-form-label col-md-4">Curriculum Year:</label>
				<div class="col-md-8">
				  <select class="form-control text-bold" style="text-align-last: center"  id="year">
				  	<option value="" disabled="" selected="">--Select--</option>
				  	@foreach($curr as $c)
				  			<option value="{{$c->curriculum_year}}">{{$c->curriculum_year}}</option>
				  	@endforeach
				  </select>
				</div>
			  </div>
			</div>
			<div class="col-md-5 ">
		      <div class="form-group row">
		      	<label class="col-form-label col-md-3">Course:</label>
		      	<div class="col-md-8">
		      	  <select class="form-control text-bold" style="text-align-last: center" id="curr_id" > 
		      	  	<option value="" disabled="" selected="">--Select--</option>
		      	  	@foreach($curriculum as $a)
		      	  			<option value="{{$a->course_code}}">{{$a->course_code}}</option>
		      	  	@endforeach
		      	  </select>
		      	</div>
		      </div>
		    </div>
		 		<div class="col-md-2">
		 			<div class="form-group">
		 				<button class="btn  btn-outline-success float-left pb-2" onclick="findReport()" style=""><i class="fa fa-search"> Search</i></button>
		 			</div>
		 		</div>
		    </div>
		   
		  </div> 
		</div>
	  </div>
	  	<div class="col-sm-12 d-flex justify-content-center"  >
				<div class="alert alert-info alert-dismissible" id="no-data" style="display: none">
                 
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                    <h4>No data to show</h4>
                </div>
			</div>
			<div class="col-sm-12">
				<div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>	
	    <div class="col-sm-12" id="reports" >
	    </div>
	 </div>
	</div>

{{----}}

<script>
	$('#report_menu').addClass('menu-open');
	$('#report_href').addClass('active');
	$('#curriculum_report').addClass('active');
</script>

<script>

	const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });
</script>

<script>
	function findReport()
	{

		var course = $('#curr_id').val();
		var year = $('#year').val();
		$('#reports').empty();
		if( course == null || year == null)
		{
			 
           Toast.fire({
          icon: 'error' ,
          title: '<span class="ml-2 text-red">Please Select Curriculum year and Course</span> ',
        });
			return false;
		}
		$('#loading').show()
		$('#no-data').hide()
		$.get('quam-curr-report/'+year+'/'+course,function(data){

           $('#loading').hide()
               
           if (data.year >0)
            {
            	$('#no-data').hide()
            	$('#reports').append('<div class="row"><div class="form-group col-sm-12"><button class="btn btn-outline-info float-right mr-2" onclick="Print()"><i class="fas fa-print"> PRINT</i></button></div></div><div class="card pt-4" id="reports2"><div class="" style="width: 100%;text-align:center"><img src="{{asset('img/spc.png') }}" width="95px" height="85px" style="position: absolute;margin-left: -160px"><strong style="font-size: 20px;font-family: Italic;"> Southern Philippines College</strong><br><span style="font-size: 15px;font-family: Italic;font-weight: bold">Julio Pacana Street, Licuan, Cagayan de Oro City</span><br><span>Tel Nos.: 856-2609, 856-2610, 855-5357</span><br><label style="font-size: 16px;padding-top:11px;padding-bottom:11px">'+data.academic.college+'</label><br><label style="font-size: 16px;font-family: Italic">'+data.curriculum.course+'</label><br><span style="font-weight:bold">Curriculum Reference: '+year+'</span></div><div class="card-body" ></div></div>')
	     


			$.each(data.year_level,function(key,year){
				
					$('#reports2').append('<table class="table table-sm text-sm table-bordered text-center " style="margin-top:10px" CELLSPACING=0 ><thead style="background-color:#b2bbc3;" ><th colspan="7" style="font-size:.875rem!important;border:1px solid ">'+years(year.year_level)+ '<span style="margin-left : 20px"></span>' + semester(year.semester)+'</th></thead><thead class="text-xs " style="background-color:#b2bbc3;"><th style="width: 50px;font-size:.75rem!important;border:1px solid">SUBJECT</th><th style="width: 50px;font-size:.75rem!important;border:1px solid ">CODE</th><th style="width: 350px;font-size:.75rem!important;border:1px solid ">DESCRIPTIVE TITLE</th><th style="width: 50px;font-size:.75rem!important;border:1px solid ">LEC</th><th style="width: 50px;font-size:.75rem!important;border:1px solid ">LAB</th><th style="width: 50px;font-size:.75rem!important;border:1px solid ">UNITS</th><th style="width: 100px;font-size:.75rem!important;border:1px solid ">Pre-requisites</th></thead><tbody id="tbody'+year.id+'"></tbody><thead style="background-color:#b2bbc3;"><th colspan="3" class="text-right" style="text-align: right;font-size:.75rem!important;border:1px solid ">TOTAL:</th><th id="lec'+year.id+'" style="font-size:.75rem!important;border:1px solid "></th><th id="lab'+year.id+'" style="font-size:.75rem!important;border:1px solid "></th><th id="units'+year.id+'" style="font-size:.75rem!important;border:1px solid "></th><th style="border:1px solid "></th></thead></table>')
					var t_units=0;
					var t_lab=0;
					var t_lec=0;
					$.each(data.curr_sub,function(key,curr){
						if(curr.year_level_id == year.id)
						{
							$.each(data.subject,function(key,sub){
								if(sub.id == curr.subject_id)
								{
									t_units+=parseInt(sub.units);
									if(sub.lab != '-' || !sub.lab  )
									{

										t_lab+=parseInt(sub.lab);
									}
									if(sub.lec != '-')
									{
										t_lec+=parseInt(sub.lec);
									}
									
									
									$('#tbody'+year.id).append('<tr class="text-center"><td style="width:50px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.subject+'</td><td style="width:50px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.code+'</td><td class="text-left" style="font-size:.75rem!important;width:350px;border:1px solid ">'+sub.descriptive_title+'</td><td style="width:50px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.lec+'</td><td style="width:50px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.lab+'</td><td style="width:50px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.units+'</td><td style="width:100px;font-size:.75rem!important;text-align:center;border:1px solid ">'+sub.pre_requisites+'</td></tr>')
								}
							})
						}
					})
				$('#units'+year.id).text(t_units);
				$('#lab'+year.id).text(t_lab);	
				$('#lec'+year.id).text(t_lec);
			})
		}else
		{
			$('#no-data').show()
		}

			
		})
	}
</script>
<script>
	function semester(semester)
	{
		var x="";
		switch(semester)
		{
			case '1st':
				x="FIRST SEMESTER"
				break;
			case '2nd':
				x="SECOND SEMESTER"
				break;
			case 'summer':
				x="SUMMER"
				break;
		}
		return x;
	}
	function years(year)
	{
		var x="";
		switch(year)
		{
			case '1st':
				x="FIRST YEAR"
				break;
			case '2nd':
				x="SECOND YEAR"
				break;
			case '3rd':
				x="THIRD YEAR"
				break;
			case '4th':
				x="FOURTH YEAR"
				break;
			case '5th':
				x="FIFTH YEAR"
				break;
		}
		return x;
	}
</script>
<script type="text/javascript">
	function Print()
	{
		var prtContent = document.getElementById("reports2");
		//var winPrint = window.open('', '', 'left=0,top=0,width=800,height=600,toolbar=0,scrollbars=0,status=0');
    	var WinPrint = window.open("");
    	//WinPrint.document.open()
    	
    	WinPrint.document.write('<!html lang="en"><head> <meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><title>AdminLTE 3 | Calendar</title><head></head><body class="hold-transition ">'+prtContent.innerHTML+'</body></html>')
    	  //WinPrint.focus();
    

    	//WinPrint.document.write(prtContent.innerHTML);
		WinPrint.document.title='Curriculum';
    
      //  WinPrint.document.close();
      //  WinPrint.focus();

        WinPrint.print();
       WinPrint.close();
	}
	
</script>


@endsection