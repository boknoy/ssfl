 <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark " id="fixed-header">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link"></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
        <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>
       <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"></a>
      </li>

       <li class="nav-item d-none d-sm-inline-block">
        <a href="http://southphilcollege.com" class="nav-link text-bold" style="font-family: Arial, Helvetica, sans-serif;"> <img src="{{asset('/img/spc.png') }}" alt="AdminLTE Logo" style="height: 35px;width: 37px;position: absolute;margin-left: -39px;margin-top: -6px"> SOUTHERN PHILIPPINES COLLEGE</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
    

    
  <li class="nav-item dropdown" id="notif-parent">
        <a class="nav-link" data-toggle="dropdown" href="#" >
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge" id="notif">{{App\temp_notif::where('user_id',Auth::user()->id)->where('is_read',false)->count()}}</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="notif-container" style="max-height: 400px ;overflow-y: auto">
            @php 
            $noti = App\temp_notif::whereIn('type',['approved','resched'])->where('user_id',Auth::user()->id)->orderByDesc('created_at')->get();
          @endphp
           @foreach($noti as $n)

           @if($n->type=="resched")
             <a href="javascript:void(0);" class="dropdown-item" onclick="notiClick2({{$n->notif_id}})">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('img/'.App\User::where('id',$n->subby)->first()->image)}}" alt="User Avatar" class="mr-3" style="height: 50px; width: 50px; border: 1px solid lightgray;border-radius: 50px;"/>
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  {{App\User::where('id',$n->subby)->first()->role}}
                  
                  <span class="float-right text-sm
                 
                   
                    @if($n->is_read)
                    text-secondary
                     @else 
                     text-primary 
                     @endif
                   
                     " id="seen{{$n->id}}"><i class="fas fa-circle"></i></span>
                </h3>
                <p class="text-sm">{{$n->msg}}</p>

                <p class="text-sm text-primary"><i class="far fa-clock mr-1 text-warning"> </i>  {{$n->created_at->diffforHumans()}}</p>
              </div>
            </div>
            <!-- Message End -->
          </a>

           @else

         <a href="javascript:void(0);" class="dropdown-item" onclick="notiClick({{$n->notif_id}})">
            <!-- Message Start -->
            <div class="media">
              <img src="{{asset('img/'.App\User::where('id',$n->subby)->first()->image)}}" alt="User Avatar" class="mr-3" style="height: 50px; width: 50px; border: 1px solid lightgray;border-radius: 50px;"/>
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  {{App\User::where('id',$n->subby)->first()->role}}
                  
                  <span class="float-right text-sm
                 
                   
                    @if($n->is_read)
                    text-secondary
                     @else 
                     text-primary 
                     @endif
                   
                     " id="seen{{$n->id}}"><i class="fas fa-circle"></i></span>
                </h3>
                <p class="text-sm">{{$n->msg}}</p>

                <p class="text-sm text-primary"><i class="far fa-clock mr-1 text-warning"> </i>  {{$n->created_at->diffforHumans()}}</p>
              </div>
            </div>
            <!-- Message End -->
          </a>
          @endif
          <div class="dropdown-divider"></div>
          @endforeach
          @if(count($noti) == 0)
          <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer text-warning">You have 0 notification</a>
          @endif
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li>
    </ul>
  </nav>
  <div class="modal fade" id="modal-reschedule">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header pb-2 pt-2 bg-secondary">
       <div class="col-md-12 text-center">
           <label id="subject-name"></label>
        <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
        </i>
       </div>
      </div>
      <div class="modal-body">
      <div class="col-md-12">
        <label style="color: maroon">Recent Schedule</label>
     
             <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <h4 id="re-day"></h4>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                      <input type="text" class="form-control"  id="re-time-start" readonly/>
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                      
                        <input type="text" class="form-control"  id="re-time-end" readonly/>
                        
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          
    
        
       
            
         
          <label style="color: maroon">Reschedule</label>
         
            <div class="card ">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12 ">
                  <label style="font-size: 16px"><i class="fas fa-calendar-day"></i> Day</label>
                </div>
                <div class="col-md-12">
                  <h4 id="n-day"></h4>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-body pt-1 pb-1">
                <div class="col-md-12">
                    <label style="font-size: 16px;font-family: "><i class="fas fa-clock"></i> Time</label>
                </div>
                <div class="col-md-12 ">
                  <div class="row pl-4 pr-4">
                    <div class="form-group col-sm-6">
                      <label class="">Start:</label>
                   
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker5" id="n-time-start" readonly="" />
                      
                     
                    </div>
                    <div class=" form-group col-md-6">
                      <label class="">End:</label>
                     
                        <input type="text" class="form-control datetimepicker-input" data-target="#timepicker6" id="n-time-end" readonly="" />
                       
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <input type="hidden" id="re-class" >
       <div class="col-md-12">
               <button type="button" id="btn-res-schedule" class="btn btn-success btn-sm float-right" onclick="proceed()">Proceed <i class="fa fa-angle-right " ></i> </button>
            </div>

          
      </div>
        
  

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>




<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script>
  var noti = setInterval(function(){

      $.get('get-notif-quam',function(data){
        $('#notif').text(data.temp.length)
          if (data.user !=null)
           {
            //$('#notif-container').empty(); 
          
          $('#notif-container').prepend('<button class="dropdown-item" onclick="notiClick('+data.t.notif_id+')"><div class="media"><img src="{{asset('img/')}}/'+data.user.image+'" alt="User Avatar" class="img-size-50 mr-3 img-circle"><div class="media-body"><h3 class="dropdown-item-title">'+data.user.role+'<span class="float-right text-sm text-primary" id="seen'+data.t.notif_id+'"><i class="fas fa-circle"></i></span></h3><p class="text-sm">'+data.t.msg+'</p><p class="text-sm text-primary"><i class="far fa-clock mr-1 text-warning"> </i>'+data.msg+'</p></div></div></button')
        }
      })

  },3000)
  setInterval(function(){

     $("#notif-parent").load(window.location.href + " #notif-parent")
  },60000)
  
  function notiClick(id)
  {

    $('#seen'+id).removeClass('text-primary');
     $('#seen'+id).addClass('text-secondary');
      $.get('update-notif-quam/'+id,function(data){
          
   
          location.assign("{{url('quam-faculty-load')}}");

      })
  }

   function notiClick2(id)
  {

    $('#seen'+id).removeClass('text-primary');
     $('#seen'+id).addClass('text-secondary');
      $.get('update-notif-quam/'+id,function(data){

        $('#re-class').val(data.class.id)
       
        $('#subject-name').text(data.sub.subject+' '+data.sub.code+' '+data.sub.descriptive_title)  
              
                   $('#re-day').text(days2(data.schedule.day))

                   $('#n-day').text(days2(data.resched.day.toString()))
       
                  $('#re-time-start').val(moment(data.schedule.time_start,'hh:mm').format('h:mm A'));
                  $('#re-time-end').val(moment(data.schedule.time_end,'hh:mm').format('h:mm A'));

                    $('#n-time-start').val(moment(data.schedule.time_start,'hh:mm').format('h:mm A'));
                  $('#n-time-end').val(moment(data.schedule.time_end,'hh:mm').format('h:mm A'));
       
       
      $('#modal-reschedule').modal('show');
        

      })
  }


  function days2(val)
  {
      var days="";
      switch(val)
                {
                  case '0' :
                        days="Monday";
                        break;
                  case '1' :
                          days="Tuesday";
                        break;
                  case '2' :
                         days="Wednesday";
                        break;
                  case '3' :
                          days="Thursday";
                        break;
                  case '4' :
                           days="Friday";
                        break;
                  case '5' :
                           days="Saturday";
                        break;
                }

                return days;
             
  }

  function proceed(){
    var class_id =$('#re-class').val();
    
    location.assign("{{url('quam-proceed')}}/"+class_id)

  }

</script>


