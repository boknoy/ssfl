@extends('layouts.quam_layout')

@section('content')

<style type="text/css">
  .centered {
  display: flex;
  align-items: center;
  justify-content: center;
}
</style>

<section class="content-header pb-1">
  <div class="container-fluid">
  	<div class="col-md-12">
  	  <div class="row">
  	  	<div class="col-md-6">
  	  		

          <label style="font-size: 20px"><i class="fas fa-book "></i>  Manage Subject</label>
        
      </div>
      <div class="col-md-6">
      		<div id="arch-sub">
      			<button class="btn btn-outline-secondary p-1 ml-2 float-sm-right btn-sm " onclick="Archive();"><i class="fas fa-archive "></i><span class="badge badge-warning" style="position: absolute;margin-left: 45px;margin-top:-8px;font-weight: lighter;font-size: 10px" >{{App\tbl_subject::where('is_remove',true)->count()}}</span> Archive</button>
      		</div>

  	  			  <button class="btn btn-outline-primary btn-sm float-sm-right" onclick="ModalSubject()"><i class="fas fa-plus"></i> Subject</button>
      </div>
    </div>
   </div>
  </div>
</section>
<section class="content" >
	<div class="container-fluid">
	 <div class="col-md-12">
	
	 	<div class="card">
	 		
	 		<div class="card-body table-responsive">
	 			<div class=" p-0" >
	                <table class="table table-bordered table-hover text-sm text-center table-sm" id="tbl-list-subject">
	                	<thead class="text-center bg-dark">
	                		<tr style="font-size: 13px;font-weight: bold">
		                		<th class="pt-2 pb-2" style="width: 100px">SUBJECT</th>
		                		<th class="pt-2 pb-2" style="width: 50px">CODE</th>
		                		<th class="pt-2 pb-2" style="width: 500px">DESCRIPTIVE TITLE</th>
		                		<th class="pt-2 pb-2" style="width: 50px">LEC</th>
		                		<th class="pt-2 pb-2" style="width: 50px">LAB</th>
		                		<th class="pt-2 pb-2" style="width: 50px">UNITS</th>
		                		<th class="pt-2 pb-2" style="width: 120px">PRE-REQUISITES</th>
		                		<th class="pt-2 pb-2" style="width: 50px">TOTAL HOURS</th>
		                		<th class="pt-2 pb-2" style="width: 80px">ACTION</th>
		                	</tr>
	                	</thead>
	                	<tbody>
	                	@foreach($subject as $key => $s)
	                			<tr id="tr{{$s->id}}">
	                				<td>{{$s->subject}}</td>

	                				@if($s->code =="" || $s->code==null)
	                				<td>-</td>
	                				@else
	                				<td>{{$s->code}}</td>
	                				@endif

	                				<td>{{$s->descriptive_title}}</td>

	                				@if($s->lec =="" || $s->lec==null)
	                				<td>-</td>
	                				@else
	                				<td>{{$s->lec}}</td>

	                				@endif
	                				@if($s->lab=="" || $s->lab==null)
	                				<td>-</td>
	                				@else
	                				<td>{{$s->lab}}</td>
	                				@endif
	                				
	                				<td>{{$s->units}}</td>
	                				@if($s->pre_requisites=="" || $s->pre_requisites==null)
	                				<td>-</td>
	                				@else
	                				<td>{{$s->pre_requisites}}</td>
	                				@endif
	                				<td>{{$s->hours}}</td>
	                				<td align="center"><div class="row btn-group">
	                					<button class="btn btn-outline-info btn-xs" onclick="editSubject({{$s->id}})"><i class="fas fa-pencil-alt"> </i></button>
	                					<button class="btn btn-outline-danger btn-xs" onclick="modalRemove({{$s->id}})"><i class="fa fa-trash" > </i></button>
	                				</div></td>
	                			</tr>
	                	@endforeach
	                	</tbody>
	                </table>
	            </div>
	 		</div>
	 	</div>
	 </div>
	</div>
</section>
<!--onclick="removeSubject({{$s->id}})"-->
<!--MODAL-->
<div class="modal fade" id="modal-archive">
	<div class="modal-dialog modal-xl">
		<div class="modal-content" >
			<div class="modal-header bg-secondary" >
				<div class="col-md-12 text-center">
					<label class="modal-title " style="font-size: 19px">LIST OF REMOVED SUBJECTS</label>
					<i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
					</i>
				</div>
			</div>
           <div class="modal-body table-responsive">
           	<div class=" p-0" >
	                <table id="tbl-list-subject2" class="table table-borderless text-sm  table-sm" >
	                	<thead class="bg-dark" >
	                		<th class="pt-2 pb-2" style="width: 100px">SUBJECT</th>
	                		<th class="pt-2 pb-2" style="width: 60px">CODE</th>
	                		<th class="pt-2 pb-2" style="width: 500px">DESCRIPTIVE TITLE</th>
	                		<th class="pt-2 pb-2" style="width: 60px">LEC</th>
	                		<th class="pt-2 pb-2" style="width: 60px">LAB</th>
	                		<th class="pt-2 pb-2" style="width: 60px">UNITS</th>
	                		<th class="pt-2 pb-2" style="width: 130px">PRE-REQUISITES</th>
	                		<th class="pt-2 pb-2" style="width: 50px">TOTAL HOURS</th>
	                		<th class="pt-2 pb-2 text-center" style="width: 100px" >ACTION</th>
	                		
	                	</thead>
	                	<tbody>
	                	@foreach($archive as $key => $s)
	                
	                			<tr id="trt{{$s->id}}">
	                				<td class="pt-2 pb-2">{{$s->subject}}</td>
	                				<td class="pt-2 pb-2">{{$s->code}}</td>
	                				<td class="text-left">{{$s->descriptive_title}}</td>
	                				<td class="pt-2 pb-2">{{$s->lec}}</td>
	                				<td class="pt-2 pb-2">{{$s->lab}}</td>
	                				<td class="pt-2 pb-2">{{$s->units}}</td>
	                				<td class="pt-2 pb-2">{{$s->pre_requisites}}</td>
	                				<td class="pt-2 pb-2">{{$s->hours}}</td>
	                				<td class="pt-2 pb-2" align="center">
	                					<button class="btn btn-outline-info btn-xs" onclick="modalConfirm({{$s->id}})"><i class="fas fa-trash-restore"></i></button>
	                				
	                				</td>
	                			</tr>
	                	@endforeach
	                	</tbody>
	                </table>
	            </div>
	 		</div>
           
            
          
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->

<div class="modal fade" id="modal-add-subject">
  <div class="modal-dialog modal-md">
	<div class="modal-content"  style="background-color: #343a40">
	  <div class="modal-header">
	    <div class="col-md-12 text-center">
	    	<label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">SUBJECT</font></label>
	    	<button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
	  		<span aria-hidden="true" class="text-white">&times;</span>
	  	</button>
	    </div>
	  </div>
	  <div class="modal-body">
	  	<form id="form-new-subject" class="text-uppercase">
	  	  @csrf
	  	  <div class="row pr-3 pl-3">
	  	  	<div class="form-group col-sm-12">
	  	  	  <label class="form-text text-white">DEPARTMENT:</label>
	  	  	  <select id="academic_id" class="form-control form-control-sm">
	  	  	  	<option disabled="" selected="">--Select--</option>
	  	  	  	@foreach($academic as $a)
	  	  	  	<option value="{{$a->id}}">{{$a->program_code}}</option>
	  	  	  	@endforeach
	  	  	  </select>
	  	  	</div>
	  	  	<div class="form-group col-sm-6">
	  	  	  <label class="form-text text-white">SUBJECT:</label>
	  	  	  <input type="text" id="subject" class="form-control form-control-sm  text-uppercase" >
	  	  	</div>
	  	  	<div class="form-group col-sm-6">
	  	  	  <label class="form-text text-white">CODE:</label>
	  	  	  <input type="text" id="code" class="form-control form-control-sm  text-uppercase" >
	  	  	</div>
	  	  	<div class="form-group col-sm-12">
	  	  	  <label class="form-text text-white">DESCRIPTIVE TITLE:</label>
	  	  	  <input type="text" id="descriptive" class="form-control form-control-sm text-uppercase" >
	  	  	</div>
	  	  	<div class="form-group col-sm-4">
	  	  	  <label class="form-text text-white">LEC:</label>
	  	  	  <input type="number" id="lec" class="form-control form-control-sm text-uppercase">
	  	  	</div>
	  	  	<div class="form-group col-sm-4">
	  	  	  <label class="form-text text-white">LAB:</label>
	  	  	  <input type="number" id="lab" class="form-control form-control-sm text-uppercase">
	  	  	</div>
	  	  	<div class="form-group col-sm-4">
	  	  	  <label class="form-text text-white">UNITS:</label>
	  	  	  <input type="number" id="units" class="form-control form-control-sm text-uppercase" >
	  	  	</div>
	  	  	<div class="form-group col-sm-8">
	  	  	  <label class="form-text text-white">PRE-REQUISITES:</label>
	  	  	  <input type="text" id="pre-requisites" class="form-control form-control-sm text-uppercase" >
	  	  	</div>
	  	  	<div class="form-group col-sm-4">
	  	  	  <label class="form-text text-white">TOTAL HOURS:</label>
	  	  	  <input type="number" id="hours" class="form-control form-control-sm " min="1">
	  	  	</div>
	  	  </div>
	  	</form>
	  </div>
	  <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d">
	  	<div class="col-md-12 pl-3 pr-3">
	  		<button type="button" class="btn btn-success btn-sm float-right col-md-2" onclick="saveSubject();"><i class="fa fa-save"></i> SAVE</button>
	  	</div>
	  </div>
	</div>
  </div>
</div>
 

<div class="modal fade" id="modal-edit-subject">
  <div class="modal-dialog">
  	<div class="modal-content"  style="background-color: #343a40">
	  <div class="modal-header">
	    <div class="col-md-12 text-center">
	    	<label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">SUBJECT DETAILS</font></label>
	    	<button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
	  		<span aria-hidden="true" class="text-white">&times;</span>
	  	</button>
	    </div>
	  </div>
	  <div class="modal-body">
	  	<form id="form-update-subject">
	  	  <div class="row pl-3 pr-3">
	  	    <input type="hidden" id="edit-subject-id" name="">
	  	    <div class="form-group col-md-12">
	  	      <label class="form-text text-white">DEPARTMENT</label>
	  	      <select id="edit-academic-id" class="form-control form-control-sm">
	  	      	<option selected="" disabled="" value="">--Select--</option>
	  	      	@foreach($academic as $a)
	  	      	<option id="op{{$a->id}}" value="{{$a->id}}">{{$a->program_code}}</option>
	  	      	@endforeach
	  	      </select>
	  	    </div>
	  	    <div class="form-group col-md-6">
	  	      <label class="form-text text-white">SUBJECT</label>
	  	      <input type="text" id="edit-subject" class="form-control form-control-sm text-uppercase">
	  		  	</div>
	  		<div class="form-group col-md-6">
	  		  <label class="form-text text-white">CODE</label>
	  		  <input type="text" id="edit-code" class="form-control form-control-sm text-uppercase">
	  		</div>
	  		<div class="form-group col-md-12">
	  		  <label class="form-text text-white">DESCRIPTIVE</label>
	  		  <input type="text" id="edit-descriptive" class="form-control form-control-sm text-uppercase">
	  		</div>
	  		<div class="form-group col-md-4">
	  		  <label class="form-text text-white">LEC</label>
	  		  <input type="text" id="edit-lec" class="form-control form-control-sm text-uppercase" placeholder="-">
	  		</div>
	  		<div class="form-group col-md-4">
	  		  <label class="form-text text-white">LAB</label>
	  		  <input type="text" id="edit-lab" class="form-control form-control-sm text-uppercase" placeholder="-">
	  		</div>
	  		<div class="form-group col-md-4">
	  		  <label class="form-text text-white">UNITS</label>
	  		  <input type="text" id="edit-units" class="form-control form-control-sm text-uppercase">
	  		</div>
	  		<div class="form-group col-md-8">
	  		  <label class="form-text text-white">PRE-REQUISITES</label>
	  		  <input type="text" id="edit-pre_requisites" class="form-control form-control-sm text-uppercase" placeholder="-">
	  		</div>
	  		<div class="form-group col-md-4">
	  		  <label class="form-text text-white">TOTAL HOURS</label>
	  		  <input type="number" id="edit-hours" class="form-control form-control-sm " min="1">
	  		</div>
	  	  </div>
	  	</form>
	  </div>
	  <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d;">
	    <div class="col-md-12 pr-3 pl-3">
	    	<button type="button" class="btn btn-success btn-sm float-right" id="btn-update-subject"><i class="fa fa-"></i> UPDATE</button>
	    </div>
	  </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modal-delete-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center ">
      <input type="hidden" id="r-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to remove <br> <label class="pl-4"  id="sub-name-code" style="text-decoration: underline;"></label>?<br>
        	  This item will be moved to archive.<br>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeSubject()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-restore-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center ">
      <input type="hidden" id="sub-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-trianglfe fa-lg text-warning " ></i> Do you want to restore <br>Subject-Code <label class=""  id="subject-name" style="text-decoration: underline;"></label>?
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="Restore()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<script >
	$.get('quam-all-subject-remove',function(data){
        
        $('#remove-list').text(''+data);
	})




</script>
 <script >

 

 	function Archive(){
 	$('#modal-archive').modal('toggle');
}

function modalConfirm(id)
{    
	$('#sub-id').val(id);
	$.get('quam-get-subject-name/'+id,function(data){
		$('#subject-name').text(''+data.subject+'-'+data.code);
	})
	$('#modal-restore-confirm').modal('toggle');
}


function Restore()
{
    var id =$('#sub-id').val();
	$.get('quam-restore-subject/'+id,function(data){

		var tt=  $("#tbl-list-subject2").DataTable();
            tt.row('#trt'+data.id).remove().draw();

		var t=  $("#tbl-list-subject").DataTable();
				t.row.add([
                data.subject,
                data.code,
                data.descriptive_title,
                data.lec,
                data.lab,
                data.units,
                data.pre_requisites,
                data.hours,
                '<div class="col-sm-12 text-center"><div class="row btn-group">'+
                '<button class="btn btn-outline-info btn-xs" onclick="editSubject('+data.id+')"><i class="fa fa-pen "> </i></button>'+
                '<button class="btn btn-outline-danger btn-xs" onclick="modalRemove('+data.id+')"><i class="fa fa-trash "></i></button>'+
                '</div></div>']).node().id = 'tr'+data.id;
                t.draw();
                
             $('#tr'+data.id).addClass('text-sm p-1 m-1');
		   Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Restored!!</span>',
                  });

		    $('#modal-restore-confirm').modal('hide');

	})
}
</script>

<script>
	function ModalSubject(){

		$('#modal-add-subject').modal('toggle');
	}
</script>

<script>

 const Toast = Swal.mixin({
 	toast: true,
 	position: 'top-end',
 	showConfirmButton: false,
 	timer:3000,
                  
    });

	function saveSubject(){
        var academic_id = $('#academic_id').val();
		var subject = $('#subject').val();
		var code = $('#code').val();
		var descriptive = $('#descriptive').val();
		var lec = $('#lec').val();
		var lab = $('#lab').val();
		var units = $('#units').val();
		var pre_requisites = $('#pre-requisites').val();
		var hours =$('#hours').val();
		
		$.ajax({

			type:"POST",
			url:"quam-save-subject",
			data : {

				"_token"   		: "{{ csrf_token() }}",
				"academic_id"   :academic_id,
				"subject"  		:subject,
				"code" 	   		:code,
				"descriptive"	:descriptive,
				"lab"			:lab,
				"lec"			:lec,
				"units" 		:units,
				"pre_requisites":pre_requisites,
				'hours'         :hours,
			},
			success:function(data){

			     Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Added!!</span>',
                  }); 

				var t=  $("#tbl-list-subject").DataTable();
				t.row.add([
				
                data.subject,
                data.code,
                data.descriptive_title,
                data.lec,
                data.lab,
                data.units,
                data.pre_requisites,
                data.hours,
                '<div class="row btn-group ">'+
                '<button class="btn btn-outline-info btn-xs" onclick="editSubject('+data.id+')"><i class="fa fa-pen "> </i></button>'+
                '<button class="btn btn-outline-danger btn-xs" onclick="modalRemove('+data.id+')"><i class="fa fa-trash " > </i></button>'+
                '</div>']).node().id = 'tr'+data.id;
                t.draw();
                

				$('#form-new-subject')[0].reset();
				//$('#modal-add-subject').modal('hide');
				$('#modal-add-subject').modal('hide');
			},
			error:function(data){

					$.each(data.responseJSON.errors,function(key,value){
						
				Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"></span>'+ value,
                 });	  
					})
			}
		})
	}


//remove subject
	function modalRemove(id){
 		$('#r-id').val(id);

 		$.get('quam-sub-remove-data/'+id,function(data){

 			$('#sub-name-code').text(data.sub.subject+''+data.sub.code);

 		})

 		$('#modal-delete-confirm').modal('toggle');

 	}


function removeSubject(){
	var id = $('#r-id').val();
	  
	    $.get('quam-remove-subject/'+id,function(data){

	    	var tt=  $("#tbl-list-subject").DataTable();
            tt.row('#tr'+data.id).remove().draw();

	    	var t=  $("#tbl-list-subject2").DataTable();
				t.row.add([
			
                data.subject,
                data.code,
                data.descriptive_title,
                data.lec,
                data.lab,
                data.units,
                data.pre_requisites,
                data.hours,
                '<button class="btn btn-outline-info btn-xs" onclick="modalConfirm('+data.id+')"><i class="fa fa-trash-restore "></i></button>'+
                '</div></div>']).node().id = 'trt'+data.id;
                t.draw();
               
             $('#trt'+data.id).addClass('text-sm p-1 m-1');
				
			  Toast.fire({
                    icon: 'success' ,
                    title:'<a><u>'+data.descriptive_title +' </u><br> Successfully Removed </a>',
                 });	

			$('#modal-delete-confirm').modal('hide');

			  
		}); 
		

}


//edit subject
function editSubject(id){
	$.get('quam-edit-subject/'+id,function(data)
	{

		$('#modal-edit-subject').modal('toggle');
		//$('#edit-academic-id').val(data.subject.academic_id);
		$('#edit-subject').val(data.subject.subject);
		$('#edit-subject-id').val(data.subject.id);
		$('#edit-code').val(data.subject.code);
		$('#edit-descriptive').val(data.subject.descriptive_title);
		$('#edit-lec').val(data.subject.lec);
		$('#edit-lab').val(data.subject.lab);
		$('#edit-units').val(data.subject.units);
		$('#edit-pre_requisites').val(data.subject.pre_requisites);
		$('#edit-hours').val(data.subject.hours);
		if (data.subject.academic_id == 0)
		 {
		 	 $('#edit-academic-id').val("");
		 }
		$.each(data.academic,function(key,acad){
			if(data.subject.academic_id == acad.id){
				 $('#edit-academic-id').val(acad.id);
				 $('#op'+acad.id).addClass('bg-primary');
				
			}
			else
			{

				 $('#op'+acad.id).removeClass('bg-primary');
			}
			
		})
	});
	//button update


}//edit funtion
$('#btn-update-subject').click(function()
	{   
		var academic_id = $('#edit-academic-id').val();
		var subject = $('#edit-subject').val();
		var code = $('#edit-code').val();
		var descriptive = $('#edit-descriptive').val();
		var lec = $('#edit-lec').val();
		var lab = $('#edit-lab').val();
		var units = $('#edit-units').val();
		var pre_requisites = $('#edit-pre_requisites').val();
		var hours  = $('#edit-hours').val();
		var id = $('#edit-subject-id').val();
	
		$.ajax(
		{
			type:"put",
			url:"quam-update-subject/"+id,
			data : 
			{

				"_token"   		: "{{ csrf_token() }}",
				"academic_id"   :academic_id,
				"subject"  		:subject,
				"code" 	   		:code,
				"descriptive"	:descriptive,
				"lab"			:lab,
				"lec"			:lec,
				"units" 		:units,
				"pre_requisites":pre_requisites,
				"hours"         :hours,
			},
			success:function(data)
			{  
				  Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Updated!!</span>',
                  });	  

				$('#modal-edit-subject').modal('hide');
				$('#form-update-subject')[0].reset();
				$('#tr' + data.id + ' td:nth-child(1)' ).text(data.subject);
				if(data.code=="" || data.code==null){
                $('#tr' + data.id + ' td:nth-child(2)' ).text('-');
				}
				else{
                 $('#tr' + data.id + ' td:nth-child(2)' ).text(data.code);
				}

				$('#tr' + data.id + ' td:nth-child(3)' ).text(data.descriptive_title);

				if(data.lec=="" || data.lec==null){
                $('#tr' + data.id + ' td:nth-child(4)' ).text('-');
				}
				else{
                 $('#tr' + data.id + ' td:nth-child(4)' ).text(data.lec);
				}

				if(data.lab=="" || data.lab==null){
                $('#tr' + data.id + ' td:nth-child(5)' ).text('-');
				}
				else{
                 $('#tr' + data.id + ' td:nth-child(5)' ).text(data.lab);
				}
	
				$('#tr' + data.id + ' td:nth-child(6)' ).text(data.units);

				if(data.pre_requisites=="" || data.pre_requisites==null){
                $('#tr' + data.id + ' td:nth-child(7)' ).text('-');
				}
				else{
                 $('#tr' + data.id + ' td:nth-child(7)' ).text(data.pre_requisites);
				}

				$('#tr' + data.id + ' td:nth-child(8)' ).text(data.hours);
				
			},
			error:function(data)
			{
					$.each(data.responseJSON.errors,function(key,value){
						
				Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"></span>'+ value,
                 });	  
					})
			}
		})
	})


   setInterval(function(){

     $("#arch-sub").load(window.location.href + " #arch-sub")
  },6000)

</script>

@endsection