@extends('layouts.quam_layout')
@section('content')

<style type="text/css">
  .fc-event-time, .fc-event-title {
text-align: center;
padding: 0 1px;
white-space: normal;
}
 th.fc-timegrid-axis:after{
  content: "Time";
 
}
.fc-col-header{
  background-color:#36414d;
  color:white;
}

.bokeh {
    border: 0.01em solid rgba(150, 150, 150, 0.1);
    border-radius: 50%;
    font-size: 100px;
    height: 1em;
    list-style: outside none none;
    margin: 0 auto;
    position: relative;
    top: 35%;
    width: 1em;
    z-index: 2147483647;
}
.bokeh li {
    border-radius: 50%;
    height: 0.2em;
    position: absolute;
    width: 0.2em;
}
.bokeh li:nth-child(1) {
    animation: 1.13s linear 0s normal none infinite running rota, 3.67s ease-in-out 0s alternate none infinite running opa;
    background: #00c176 none repeat scroll 0 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    top: 0;
    transform-origin: 50% 250% 0;
}
.bokeh li:nth-child(2) {
    animation: 1.86s linear 0s normal none infinite running rota, 4.29s ease-in-out 0s alternate none infinite running opa;
    background: #ff003c none repeat scroll 0 0;
    margin: -0.1em 0 0;
    right: 0;
    top: 50%;
    transform-origin: -150% 50% 0;
}
.bokeh li:nth-child(3) {
    animation: 1.45s linear 0s normal none infinite running rota, 5.12s ease-in-out 0s alternate none infinite running opa;
    background: #fabe28 none repeat scroll 0 0;
    bottom: 0;
    left: 50%;
    margin: 0 0 0 -0.1em;
    transform-origin: 50% -150% 0;
}
.bokeh li:nth-child(4) {
    animation: 1.72s linear 0s normal none infinite running rota, 5.25s ease-in-out 0s alternate none infinite running opa;
    background: #88c100 none repeat scroll 0 0;
    margin: -0.1em 0 0;
    top: 50%;
    transform-origin: 250% 50% 0;
}
@keyframes opa {
12% {
    opacity: 0.8;
}
19.5% {
    opacity: 0.88;
}
37.2% {
    opacity: 0.64;
}
40.5% {
    opacity: 0.52;
}
52.7% {
    opacity: 0.69;
}
60.2% {
    opacity: 0.6;
}
66.6% {
    opacity: 0.52;
}
70% {
    opacity: 0.63;
}
79.9% {
    opacity: 0.6;
}
84.2% {
    opacity: 0.75;
}
91% {
    opacity: 0.87;
}
}

@keyframes rota {
100% {
    transform: rotate(360deg);
}
}

}
</style>
<section class="content-header pb-0">
  <div class="container-fluid">
    <div class="col-md-12">
         <div class="row">
      <div class="col-md-6">
          <label style="font-size: 20px"><i class="nav-icon fas fa-door-open"> Manage Room</i></label>
       
      </div>
      <div class="col-md-6">
         <div id="arch-ro">
             <button class="btn btn-outline-secondary p-1 ml-2 float-sm-right btn-sm " onclick="Archive();"><i class="fas fa-archive "></i><span class="badge badge-warning" style="position: absolute;margin-left: 45px;margin-top:-8px;font-weight: lighter;font-size: 10px" >{{App\tbl_room::where('is_remove',true)->count()}}</span> Archive</button>
         </div>
         <button class="btn btn-outline-primary btn-sm float-right" onclick="Room()"><i class="fa fa-plus"></i> Room</button>
      </div>
    </div>
    </div>
 
  </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="col-sm-12">
      <div class="row ">
        <div class="col-sm-12">
             <div class="card">
   
    <!-- /.card-header -->
    <div class="card-body table-responsive">
      <table id="room-table" class="table table-border table-striped table-hover text-sm">
        <thead class="bg-dark">
        <tr class="text-uppercase">
          <th width="15px">#</th>
          <th  >Room Name</th>
          <th >Building</th>
          <th >Status</th>
          <th width="250px" class=" text-center">Action</th>
        </tr>
        </thead>
        @php
          $i=1;
        @endphp
        <tbody>
          @foreach($rooms as $r)
            <tr id="tr{{$r->id}}">
              <td class="pt-2 pb-2">{{$i++}}</td>
              <td class="pt-2 pb-2 text-uppercase">{{$r->room_description}}</td>
              <td  class="pt-2 pb-2 text-uppercase">{{$r->building}}</td>

              <td class="pt-2 pb-2">@if($r->is_active == 0) <span class="badge badge-secondary "> Not Available </span> @else <span class="badge badge-success ">  Available </span>  @endif</td>
               <td class="text-center pt-2 pb-2">
                
                  <button class=" btn btn-xs btn-outline-success" onclick="schedule({{$r->id}})"><i class="fa fa-clock"> </i> Schedule</button>
                  <a href="#" class="btn btn-outline-info btn-xs" onclick="editRooms({{$r->id}})"><i class="fas fa-edit "> </i> Edit</a>
                  <a href="#" class="btn btn-outline-danger btn-xs" onclick="modalRemove({{$r->id}})"><i class="fa fa-trash "> </i> Remove</a>
             
              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
       
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
          
        </div>
      </div>
    </div>

 
  </div>
 </section>

<div class="modal fade" id="modal-add-room">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header pt-3 pb-3">
         <div class="col-md-12 text-center">
             <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">ROOM</font></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
         </div>
      </div>
      <div class="modal-body">
        <form id="form-add-room" class="text-uppercase">
          @csrf
          <div class="col-md-12 pl-3 pr-3">
             <div class="form-group">
            <label class="form-label text-white">Room Name :</label>
            <input type="text" name="room_name" id="room_name" class="form-control form-control-sm text-uppercase" >
          </div>
          <div class="form-group">
            <label class="form-label text-white">Building :</label>
            <input type="text" name="building" id="building" class="form-control form-control-sm text-uppercase" >
          </div>
          </div>
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d">
        <div class="col-md-12 pl-4 pr-3">
           <button class="btn btn-success btn-sm float-right col-md-2" onclick="addRooms()"><i class="fas fa-save"></i> SAVE</button>
        </div>
      </div>
    </div>
  </div>
</div>
 
<div class="modal fade" id="modal-update-room">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
             <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">ROOM DETAILS</font></label>
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
      </div>
      <div class="modal-body">
        <form id="form-update-room" class="text-uppercase">
          @csrf
          <input type="hidden" id="u-id">
          <div class="col-md-12 pl-3 pr-3">
            <div class="form-group">
            <label class="form-label text-white">Room Name :</label>
            <input type="text"  id="u-room_name" class="form-control form-control-sm text-uppercase" >
          </div>
          <div class="form-group">
            <label class="form-label text-white">Building :</label>
            <input type="text"  id="u-building" class="form-control form-control-sm text-uppercase">
          </div>
          <div class="form-group">
            <label class="form-label text-white">Status :</label>
            <select class="form-control form-control-sm" id="u-is_active">
              <option value="1">Available</option>
              <option value="0"> Not Available</option>
            </select>
          </div>
          </div>
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d">
         <div class="col-md-12 pl-3 pr-3">
            <button class="btn btn-success btn-sm btn-block" onclick="updateRooms()"><i class="fa fa"></i> UPDATE</button>
         </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-remove-room">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="room-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to remove<br> <label  id="room-name" style="text-decoration: underline;"></label>?<br>  This item will be moved to archive.
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeRoom()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-room-archive">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" >
      <div class="modal-header bg-secondary" >
        <div class="col-md-12 text-center">
          <label class="modal-title " style="font-size: 19px">LIST OF REMOVED ROOMS</label>
          <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
          </i>
        </div>
      </div>
      <div class="modal-body">
        <table id="tbl-room-archive" class="table table-border text-sm">
          <thead>
            <tr class="bg-dark">
              <th class="pt-2 pb-2" width="10px">#</th>
              <th class="pt-2 pb-2">ROOM NAME</th>
              <th class="pt-2 pb-2">BUILDING </th>
              <th class="pt-2 pb-2">STATUS</th>
              <th class="pt-2 pb-2 text-center" width="120px">Action</th>
            </tr>
          </thead>
          @php
          $i=1;
          @endphp
          <tbody>
           @foreach($archive as $key => $a)
            <tr id="trt{{$a->id}}">
              <td>{{$i}}</td>
              <td>{{$a->room_description}}</td>
              <td>{{$a->building}}</td>
              <td class="pt-2 pb-2">@if($a->is_active == 0) <span class="badge badge-secondary "> Not Available </span> @else <span class="badge badge-success ">  Available </span>  @endif</td>
              <td align="center"><button class="btn btn-outline-info btn-sm" onclick="RestoreConfirm({{$a->id}})"><i class="fas fa-trash-restore"></i> Restore</button> </td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-restore-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="r-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-trash-restor fa-lg text-warning " ></i> Do you want to restore<br> <label  id="room-description" style="text-decoration: underline;"></label> Room?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="Restore()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

 <div class="modal fade" id="modal-schedule">
        <div class="modal-dialog modal-lg" style="height: 1000px">
          <div class="modal-content">
            <div class="modal-header pt-2 pb-2 bg-secondary">
             <div class="col-sm-12 text-center">
              <label class="modal-title text-white ml-4" id="r-name" style="font-size: 21px;"></label>
              <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
              </i>
              <div class="col-sm-12">
                <label class="modal-title" id="r-building"></label>
              </div>
             </div>
            </div>
            <div class="modal-body col-sm-12">
              <div class="card">
                <div class="card-body">
                  <div class="row">

                    <div class="form-group col-sm-5">
                      <label>School Year</label>

                      <select class="form-control form-control-sm" id="school_year" style="text-align-last: center" onchange="selvalid('school_year')">
                          <option selected="" disabled="" value="">Select School Year</option>
                          @foreach($sy as $s)
                            <option value="{{$s->school_year}}">{{$s->school_year}}</option>

                          @endforeach
                      </select>
                    </div>
                    <div class="form-group col-sm-5">
                      <label>Semester</label>
                      <select class="form-control form-control-sm" id="semester" style="text-align-last: center" onchange="selvalid('semester')">
                           <option selected="" disabled="" value="">Select Semester</option>
                           <option value="1st">First Semester</option>
                            <option value="2nd">Second Semester</option>
                             <option value="summer">Summer</option>
                      </select>
                    </div>
                    <div class="form-group col-sm-2">
                     <label class="" style="width: 100%">Search</label>
                     <button class="btn btn-block btn-outline-primary btn-sm" onclick="Search()"><i class="fa fa-search"> Search</i></button>
                    </div>
                  </div>
                </div>
              </div>
                <div class="col-sm-12 d-flex justify-content-center"  >
        <div class="alert alert-info alert-dismissible col-md-10" id="no-data" style="display: none">
                  
                  <h5><i class="icon fas fa-info"></i> Oops!</h5>
                    <h4>No data to show</h4>
                </div>
      </div>
      <div class="col-sm-12">
        <div id="loading" style="display: none">
                <ul class="bokeh">
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
      </div>
             <div class="card-body" style="display: none" id="cal">
               <div id="calendar" class="col-sm-12" style="width: 100%;">
               </div>
             </div>
                  
                             
              <input type="hidden" id ="room_id">
            </div>
           
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
  </div>


<script src="{{asset('plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/main.js')}}"></script>
<script src="{{asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<script >

 var date = new Date(2021, 2, 22)
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var calendarEl = document.getElementById('calendar');
    var calendar = new Calendar(calendarEl, {
     
      initialView:'timeGridWeek',
      validRange: {
    start: '2021-03-22',
    end: '2021-03-28'
         },
      
      headerToolbar:false,
      themeSystem: 'bootstrap',
      hiddenDays: [0],
      slotMinTime: '07:00:00',
      slotMaxTime: '23:00:00',
      allDaySlot: false,
      dayHeaderFormat :{ weekday: 'short' },
      contentHeight:"auto",
     
   });
    

  function schedule(id)
  {
    $('#room_id').val(id);
    $('#cal').hide();
      $('#school_year').val("")
      $('#semester').val("")
      $('#loading').hide()
      $('#cal').hide()
      $('#no-data').hide()
    $.get('quam-get-room--details/'+id,function(data){
  
       $('#r-name').text(data.room_description);
       $('#r-building').text(data.building);
    })


      $('#modal-schedule').modal('show')
    

    // $("#calendar").width(750).css({'margin-left': 'auto','margin-right': 'auto'});
  }
    function selvalid(val)
  { 
      $('#'+val).css('border-color','blue')
  }

 function Search()
 {
  var id = $('#room_id').val()
   var sy = $('#school_year').val();
  var sem = $('#semester').val();

 if (sy == "0" || sy == null)
       {
        $('#school_year').css('border-color','red')

           Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select school year</span>',
                  });
       
        return false

       }

      if (sem == "0" || sem == null)
       {
        $('#semester').css('border-color','red')

          Toast.fire({
                    icon: 'error' ,
                    title: ' <span class="ml-1"> Please select semester </span>',
                  });

        return false
       }
  calendar.removeAllEvents(); 
   setTimeout(function(){ calendar.render() }, 1000);
    $('#loading').show()
    $('#cal').hide()
    $('#no-data').hide()
    $.get('get-room-schedule/'+id+'/'+sem+'/'+sy,function(data){
      $('#loading').hide()
              if (data.schedule.length > 0)
               {
                $('#no-data').hide()
                $('#cal').show()
                  $.each(data.schedule, function(key,value) {
                 var section="";
                 $.each(data.class,function(k,c){
                  if(c.id == value.class_id)
                  {
                    $.each(data.section,function(k,sec){
                     if(sec.id == c.section_id)
                     {
                      section = sec.section_name
                     }
                  })
                  }
                  
                 
                 })  
            $.each(data.subject, function(key2,value2) {

              if (value.subject_id == value2.id) {

                 var str_hr = value.time_start; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_start_hr=0;
                  var str_min = value.time_start; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_start_min=0;
                  
                  
                      time_start_hr=parseInt(res_hr)
                      time_start_min=parseInt(res_min)
                 
                  //Time End

                  var str_hr = value.time_end; 
                  var res_hr = str_hr.slice(0,2);
                  var search_hr = res_hr.includes('0');
                  var time_end_hr=0;
                  var str_min = value.time_end; 
                  var res_min = str_min.slice(3,5);
                  var search_min = res_min.includes('0');
                  var time_end_min=0;
                  
                   
                  
                      time_end_hr=parseInt(res_hr);
                      time_end_min=parseInt(res_min);
              
                  day = parseInt( value.day);

                  var events={
                           id:value.id,    
                        title          : section,
                        start  : new Date(y, m, d + day , time_start_hr, time_start_min),
                        end            : new Date(y, m, d + day, time_end_hr, time_end_min),
                        allDay         : false,
                          //url            :'https://www.facebook.com',
                        backgroundColor: value.color, //Success (green)
                         

                        };
                         calendar.addEvent(  events );

                        //  $(".fc-event-title").empty().append('<br>'+value.class_id);
                        calendar.render();  
               
                           
              }
                  
            });
              
                });
               }
               else
               {
                  $('#cal').hide()
                 $('#no-data').show()
               }
           

    })
    
    setTimeout(function(){ calendar.render() }, 1000);
 }

 function Archive(){

   $('#modal-room-archive').modal('toggle');

 } 

 function RestoreConfirm(id){
    $('#r-id').val(id);
    $.get('quam-get-room-name/'+id,function(data){
      $('#room-description').text(''+data.room_description);
    })
   $('#modal-restore-confirm').modal('toggle');
 }

function Restore(){
   var status;
  var id=$('#r-id').val();
  $.get('quam-restore-room/'+id,function(data){

       var tt=  $("#tbl-room-archive").DataTable();
        tt.row('#trt'+data.id).remove().draw();
      
       if (data.is_active==0) {
            status='<span class="badge badge-secondary ">Not Available </span>';
          }else{
             status='<span class="badge badge-success ">Available </span>';
          }
    var t=  $("#room-table").DataTable();
        t.row.add([
        '',
         data.room_description,
         data.building,
         status,
         '<div class="text-center" >'+
         '<button onclick="schedule('+data.id+')" class="btn btn-outline-success btn-xs  " > <i class="fa fa-clock"> </i> Schedule</button>'+
         '<button onclick="editRooms('+data.id+')" class="btn btn-outline-info btn-xs  ml-1 mr-1" > <i class="fa fa-edit"> </i> Edit</button>'+
         '<button onclick="modalRemove('+data.id+')" class="btn btn-outline-danger btn-xs  " > <i class="fa fa-trash"></i> Remove</button>'+
                  '</div>']).node().id = 'tr'+data.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#tr'+data.id).addClass('text-sm p-1 m-1');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Restored!!</span>',
                  });

         $('#modal-restore-confirm').modal('hide');

  })
}

 function modalRemove(id){

  $('#room-id').val(id);

  $.get('quam-room-remove-data/'+id,function(data){
     
     $('#room-name').text(data.room.room_description+' '+data.room.building);
  })
   $('#modal-remove-room').modal('toggle');
 }

 function removeRoom(){
  var id =$('#room-id').val();

  $.get('quam-remove-room/'+id,function(data){

    var tt=  $("#room-table").DataTable();
        tt.row('#tr'+data.id).remove().draw();

         if (data.is_active==0) {
            status='<span class="badge badge-secondary ">Not Available </span>';
          }else{
             status='<span class="badge badge-success ">Available </span>';
          }
    var t=  $("#tbl-room-archive").DataTable();
        t.row.add([
        '',
         data.room_description,
         data.building,
         status,
         '<div class="row btn-group text-center" >'+
         '<button onclick="RestoreConfirm('+data.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-trash-restore"></i> Restore</button>'+
                  '</div>']).node().id = 'trt'+data.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#trt'+data.id).addClass('text-sm p-1 m-1');
        //$('#modal-department-archive').modal('hide');
        //$('#modal-restore-department').modal('hide');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Removed!!</span>',
                  });

         $('#modal-remove-room').modal('hide');

  })

 }





  function Room(){
    $('#modal-add-room').modal('toggle');
  }
</script>

<script>
   const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
  });

  function addRooms(){
    var room_name= $('#room_name').val();
    var building= $('#building').val();
    var is_active= 0;
    var is_remove=0;
     
    $.ajax({

      type: 'POST',
      url : 'quam-add-rooms',
      data:{
           "_token"   : "{{ csrf_token() }}",
           "room_name" :room_name,
           "building" :building,
           "is_active" :is_active,
           'is_remove' :is_remove,
      },
      success:function(response){

          Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Added!!</span>',
                    });                       

        var is_active;
        if (response.is_active==0) {
            is_active='<span class="badge badge-secondary ">Not Available </span>';
        }else{
          is_active='<span class="badge badge-success "> Available </span>';
        }
          $('#form-add-room')[0].reset();
          var t=$('#room-table').DataTable();
                t.row.add([
                  '',
                  response.room_description,
                  response.building,
                  is_active,
                
                '<div class="col-sm-12 text-center">'+
                 '<a href="#" class="btn btn-outline-success btn-xs " onclick="schedule(('+response.id+')"><i class="fas fa-clock"> </i> Schedule</a>'+
                 '<a href="#" class="btn btn-outline-info btn-xs ml-1 mr-1" onclick="editRooms('+response.id+')"><i class="fas fa-edit"> </i> Edit</a>'+
                '<a href="#" class="btn btn-outline-danger btn-xs" onclick="modalRemove('+response.id+')"><i class="fa fa-trash "> </i> Remove</a>'+'</div>',


                  ]).node().id = 'tr'+response.id;
                t.draw();
                      t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
         $('#form-add-room')[0].reset();
        $('#modal-add-room').modal('hide');

      },
      error: function (data)
       {
        var errors = data.responseJSON;
        $.each(data.responseJSON.errors, function(key,value) {
          Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
            });
               })
             }
    })
  }
</script>
<script>
  function editRooms(id){

    $.get('quam-edit-rooms/'+id,function(rooms){

      $('#u-room_name').val(rooms.room_description);
      $('#u-building').val(rooms.building);
      $('#u-is_active').val(rooms.is_active);
      $('#u-id').val(id);
      $('#modal-update-room').modal('toggle');

})
  }

  function updateRooms(){


     var room_name =$('#u-room_name').val();
     var building = $('#u-building').val();
     var is_active =$('#u-is_active').val();
      var id = $('#u-id').val();

      $.ajax({

        type:'PUT',
        url: 'quam-update-rooms/'+id,
        data:{
               "_token"   : "{{ csrf_token() }}",
               "room_name" :room_name,
               "building" :building,
               "is_active" :is_active,

        },
        success:function(response){

          Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Updated!!</span>',
                  });

          var is_active;
          if (response.is_active==0) {
            is_active='<span class="badge badge-secondary ">Not Available </span>';
        }else{
          is_active='<span class="badge badge-success "> Available </span>';
        }
           $('#tr' + response.id + ' td:nth-child(2)' ).text(response.room_description);
           $('#tr' + response.id + ' td:nth-child(3)' ).text(response.building);
           $('#tr' + response.id + ' td:nth-child(4)' ).text('');
           $('#tr' + response.id + ' td:nth-child(4)' ).append(is_active);
           $('#modal-update-room').modal('toggle');
           $('#form-update-room')[0].reset();
        },
           error: function (data)
           {
            var errors = data.responseJSON;
            $.each(data.responseJSON.errors, function(key,value) {
              Toast.fire({
                        icon: 'error' ,
                        title: '<span class="mr-1"> </span>'+value,
                });
                   })
                 }
      })
  }


  $('#tbl-room-archive').DataTable({
     columnDefs: [
    {bSortable: false, targets: [1,2,3,4]} 
    ],
     'columnDefs': [{    
      "targets": 4,
      "className": "text-center",
     }],
  });

    setInterval(function(){

     $("#arch-ro").load(window.location.href + " #arch-ro")
  },6000)
</script>
@endsection
