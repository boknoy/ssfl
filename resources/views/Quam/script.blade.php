<script>

  var cy;
  function Curriculum_Year(val){
    cy=val.value;
  }

  $('#btn-curriculum').click(function(){

    $.ajax({
      type: 'POST',
      url : 'quam-save-curriculum',
      data :{
         "_token": "{{ csrf_token() }}",
         'cur_year' :cy,
         'prog_code' : $('input[name=prog_code]').val(),
         'prog_name' : $('input[name=prog_name]').val(),
      },
      success:function(response){
        

        var t=  $("#tbl-lis-curriculum").DataTable().row.add([
        '',
                response.cur_year,
                response.prog_code,
                response.prog_name,
                '<div class="row btn-group" >'+
                    '<a href=""class="btn btn-primary btn-xs  " >Subject <i class="fa fa-tasks"></i></a>'+
                  '</div>']).draw()
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
                $('#curriculum').modal('hide');
      }, 
      error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                  alert(value)
                  if(value == 'cy')
                  {
                    $('#error-cy').text('Curriculum year is required*');
                  }
                }); 
             }
    })
  })

</script>