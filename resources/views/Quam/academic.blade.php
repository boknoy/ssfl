@extends('layouts.quam_layout')
@section('content')

<style type="text/css">
  .centered 
  {
  display: flex;
  align-items: center;
  justify-content: center;
   }

</style>

<section class="content-header pb-1">
  <div class="container-fluid">
    <div class="col-md-12">
        <div class="row">
          <div class="col-md-6">
            <label style="font-size: 20px"><i class="fas fa-graduation-cap"> </i> Manage Department</label>
          </div>
          <div class="col-md-6 ">
            <div  id="arch-dep">
              <button  class="btn btn-outline-secondary p-1 ml-2 float-sm-right btn-sm " onclick="Archive();"><i class="fas fa-archive "></i><span  class="badge badge-warning" style="position: absolute;margin-left: 45px;margin-top:-8px;font-weight: lighter;font-size: 10px" >{{App\tbl_academic::where('is_remove',true)->count()}}</span> Archive
              </button>
            </div>
            <button id="add-academic" class="btn btn-outline-primary btn-sm float-sm-right" onclick="ModalDepartment()"><i class="fas fa-plus"></i> Department 
            </button>
          </div>
        </div>
    </div>
  </div>
</section>
<section class="content" >
  <div class="container-fluid">

      <div class="col-md-12">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-body table-responsive" > 
               @php
                $i=1;
               @endphp
                  <table id="tbl-list-academic" class="table table-hover table-striped table-border text-sm table-sm ">
                  <thead class="bg-dark">
                    <tr class="pt-2 pb-2" style="font-size: 13px">
                      <th class="pt-2 pb-2"  width="5px">#</th>
                      <th class="pt-2 pb-2"  >CODE</th>
                      <th class="pt-2 pb-2" >TITLE</th>
                      <th class="pt-2 pb-2" >COLLEGE</th>
                      <th class="text-center pt-2 pb-2" style="width: 130px">ACTION</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach($academic as $a)
        
                    <tr id="tr{{$a->id}}" style="font-size:13px">
                      <td>{{$i}}</td>
                      <td>{{$a->program_code}}</td>
                      <td>{{$a->program_name}}</td>
                      @if($a->college == null)
                      <td>-</td>
                      @else
                       <td>{{$a->college}}</td>
                      @endif
                      <td align="center">
                        <div class="row btn-group" >
                          <button  onclick="editAcademic({{$a->id}})" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button>
                          <button  onclick="modalRemove({{$a->id}})" class="btn btn-outline-danger btn-xs  " ><i class="fa fa-trash"> </i> Remove</button>
                        </div>
                      </td>
                    </tr>
                    @php
                      $i++
                    @endphp
              
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
        </div>
        </div>
       
      
    </div>
  </div>
</section>

<div class="modal fade" id="modal-add-academic">
  <div class="modal-dialog ">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">DEPARTMENT</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form id="add-academic-form">
          <div class="col-md-12 pr-3 pl-3">
            <div class="form-group">
              <label class="form-text text-white">CODE:</label>
              <input type="text" id="code" name="code" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group">
              <label class="form-text text-white">TITLE:</label>
              <input type="text" id="title" name="title" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="form-text text-white">COLLEGE:</label>
              <input type="text" id="college" name="college" class="form-control form-control-sm text-uppercase" >
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d;">
        <div class="col-md-12 pr-3 pl-3">
          <button class="btn btn-success btn-sm float-right col-md-2" id="btn-academic" ><i class="fas fa-save"> </i> SAVE </button>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modal-academic-update">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">DEPARTMENT DETAILS</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form  id="form-update-academic">
          @csrf
          <div class="col-md-12 pr-3 pl-3">
            <div class=" form-group">
              <label class="form-text text-white">CODE:</label>
              <input type="text" class="form-control form-control-sm text-uppercase"  id="d-code">
            </div>
            <div class=" form-group">
              <label class="form-text text-white">TITLE:</label>
              <input type="text" class="form-control form-control-sm text-uppercase" id="u-title" >
            </div>
             <div class=" form-group">
              <label class="form-text text-white">COLLEGE:</label>
              <input type="text" class="form-control form-control-sm text-uppercase" id="u-college" >
            </div>
          </div>
          <input type="hidden" id="u-id">
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d;">
        <div class="col-md-12 pr-3 pl-3">
          <button type="button" id="btn-academic-update" class="btn btn-success btn-sm float-right col-md-2"><i class="fa fa-"></i> UPDATE</button>
        </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-remove-academic">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="a-id">
     <div class="col-md-12 ">
     
        <p  class="text-muted p-0" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " >&nbsp; </i><u  id="acad-name"> </u> will be remove.<br>
          This item will be moved to archive.<br>
          Do you want to proceed?

      </p>
     </div>

     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeAcademic()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>




<div class="modal fade" id="modal-department-archive">
  <div class="modal-dialog modal-xl">
    <div class="modal-content" >
      <div class="modal-header bg-secondary" >
        <div class="col-md-12 text-center">
          <label class="modal-title " style="font-size: 19px">LIST OF REMOVED DEPARTMENT</label>
          <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
          </i>
        </div>
      </div>
      <div class="modal-body table-responsive">
        <table id="tbl-academic-archive" class="table table-border text-sm">
          <thead>
            <tr class="bg-dark">
              <th class="pt-2 pb-2" width="10px">#</th>
              <th class="pt-2 pb-2">Code</th>
              <th class="pt-2 pb-2">Title</th>
              <th class="pt-2 pb-2">College</th>
              <th class="pt-2 pb-2 text-center">Action</th>
            </tr>
          </thead>
          @php
          $i=1;
          @endphp
          <tbody>
           @foreach($archive as $key => $a)
            <tr id="trt{{$a->id}}">
              <td class="pt-2 pb-2 ">{{$i}}</td>
              <td class="pt-2 pb-2 ">{{$a->program_code}}</td>
              <td class="pt-2 pb-2 ">{{$a->program_name}}</td>
              <td class="pt-2 pb-2 ">{{$a->college}}</td>
              <td class="pt-2 pb-2 " align="center"><button class="btn btn-outline-info btn-xs" onclick="RestoreConfirm({{$a->id}})"><i class="fas fa-trash-restore"></i> Restore</button> </td>
            </tr>
            @php
            $i++;
            @endphp
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-restore-department">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="dep-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-trash-restor fa-lg text-warning " ></i> Do you want to restore<br> <label  id="department-name" style="text-decoration: underline;"></label> Department?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-success btn-xs " onclick="Restore()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<script>

   const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer:3000,
                  
    });

function Archive()
{
   $('#modal-department-archive').modal('toggle');
}



function RestoreConfirm(id)
{  

    $('#dep-id').val(id);
    $.get('quam-get-department-name/'+id,function(data){

      $('#department-name').text(data.acad.program_code);
    })
    $('#modal-restore-department').modal('toggle');
}

function Restore()
{   


  var id=$('#dep-id').val();
   $.get('quam-restore-department/'+id,function(data){
    
    var tt=  $("#tbl-academic-archive").DataTable();
        tt.row('#trt'+data.id).remove().draw();

     var t=  $("#tbl-list-academic").DataTable();
        t.row.add([
        '',
                data.program_code,
                data.program_name,
                data.college,
                '<div class="row btn-group" >'+
                    '<button onclick="editAcademic('+data.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button>'+
                    '<button onclick="modalRemove('+data.id+')" class="btn btn-outline-danger btn-xs  " > <i class="fa fa-trash"></i> Remove</button>'+
                  '</div>']).node().id = 'tr'+data.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#tr'+data.id).addClass('text-sm p-1 m-1');
        //$('#modal-department-archive').modal('hide');
        $('#modal-restore-department').modal('hide');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Restored!!</span>',
                  });

   })
}



  var cy;
  function Academic(val){
    cy=val.value;
  }

  $('#btn-academic').click(function(){

    $.ajax({
      type: 'POST',
      url : 'quam-save-academic',
      data :{
         "_token": "{{ csrf_token() }}",
          'code' : $('input[name=code]').val(),
         'title' : $('input[name=title]').val(),
         'college' : $('input[name=college]').val(),
      },
      success:function(response){

            Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Added!!</span>',
                  });

            var t=  $("#tbl-list-academic").DataTable();
                t.row.add([
                    '',
                  response.program_code,
                  response.program_name,
                  response.college,
                  '<div class="row btn-group" >'+
                      '<button onclick="editAcademic('+response.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button>'+
                      '<button onclick="modalRemove('+response.id+')" class="btn btn-outline-danger btn-xs  " > <i class="fa fa-trash"></i> Remove</button>'+
                    '</div>']).node().id = 'tr'+response.id;

                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();

                $('#add-academic-form')[0].reset();
                  $('#modal-add-academic').modal('hide');
                //$('#modal-add-department').modal('hide');
              
               /* $('#modal-update').append(
                  '<div class="modal fade" id="modal-academic-update'+response.id+'">'+
            '<div class="modal-dialog modal-md">'+
              '<div class="modal-content" >'+
               '<div class="modal-header p-2">'+
                  '<label class="modal-title ">Update ACADEMIC</label>'+
                  '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
                    '<span aria-hidden="true"><i class=" fa fa-times-circle-o" style="color: red"></i></span>'+
                  '</button>'+
                '</div>'+
                '<div class="modal-body">'+
                  '<form method="post" action="#">'+
                  
                  '<div class=" form-group">'+
                    '<label class="">Program Code</label>'+
                    '<input type="text" class="form-control" name="prog_code" value="'+response.prog_code+'" >'+
                  '</div>'+
                    '<div class=" form-group">'+
                    '<label class="">Program Name</label>'+
                    '<input type="text" class="form-control" name="prog_name" value="'+response.prog_name+'" >'+
                  '</div>'+
                 ' <div class="form-group">'+
                    '<button type="button" id="btn-academic-update" class="btn btn-info btn-block">Update</button>'+
                  '</div>'+
                  '</form>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>'

                  )*/
      },

      error: function (data)
       {
                  var errors = data.responseJSON;
               // toastr.error(errors);
              
               $.each(data.responseJSON.errors, function(key,value) {

                   Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
            });
               })
             }
    })
  })

</script>

<script>
  
  function editAcademic(id){
    
    $.get('quam-edit-academic/'+id,function(academic){
        
        $('#d-code').val(academic.program_code);
        $('#u-title').val(academic.program_name);

        $('#u-college').val(academic.college);
        $('#u-id').val(id);
        $('#modal-academic-update').modal('show');

    })

    
  }


$('#btn-academic-update').click(function(){
      

      var code = $('#d-code').val();

      var title = $('#u-title').val();
      var college =$('#u-college').val();
      var id        = $('#u-id').val();
      $.ajax({
        type: 'put',
        url : 'quam-update-academic/'+id,
        data :{
           "_token": "{{ csrf_token() }}",
           'code' : code,
           'title' : title,
           'college' :college,
        },
        success:function(response){

          Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Updated!!</span>',
                  });
          
          $('#tr' + response.id + ' td:nth-child(2)' ).text(response.program_code);
          $('#tr' + response.id + ' td:nth-child(3)' ).text(response.program_name);
          if(response.college=="" || response.college==null){
             $('#tr' + response.id + ' td:nth-child(4)' ).text('-');
          }
           else{
            $('#tr' + response.id + ' td:nth-child(4)' ).text(response.college);
           }

          $('#form-update-academic')[0].reset();
          $('#modal-academic-update').modal('hide');
        },
        error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"></span>'+ value,
                  });
                  
                }); 
             }
      })
    })
</script>



<script >

  function modalRemove(id){
 
    $('#a-id').val(id);
    $.get('quam-academic-remove-data/'+id,function(data){
      $('#acad-name').text(data.program_code);
    
    

    })

    $('#modal-remove-academic').modal('toggle');
  }

  function removeAcademic(){
    var id =$('#a-id').val();
     
    $.get('quam-remove-academic/'+id,function(data){

       var tt=  $("#tbl-list-academic").DataTable();
           tt.row('#tr'+data.id).remove().draw();

          var t=$("#tbl-academic-archive").DataTable();
            t.row.add([
                '',
                data.program_code,
                data.program_name,
                data.college,
                '<div class=" text-center" >'+
                    '<a  onclick="RestoreConfirm('+data.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-trash-restore"> </i> Restore</a>'+
                  '</div>']).node().id = 'trt'+data.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#trt'+data.id).addClass('text-sm p-1 m-1');
        //$('#modal-department-archive').modal('hide');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Removed!!</span>',
                  });


      $('#modal-remove-academic').modal('hide');

    })

  }



  function ModalDepartment(){
    $('#modal-add-academic').modal('toggle');
  }




  $("#tbl-list-academic").DataTable({
       columnDefs: [
    {bSortable: false, targets: [1,2,3,4]} 
    ],
      'columnDefs': [{    
      "targets": 4,
      "className": "text-center",
     }],
 
    });
  $('#tbl-academic-archive').DataTable({
       columnDefs: [
    {bSortable: false, targets: [2,3,4]} 
    ]
     
 
    });


   setInterval(function(){

     $("#arch-dep").load(window.location.href + " #arch-dep")
  },6000)

</script>


@endsection