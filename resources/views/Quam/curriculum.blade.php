@extends('layouts.quam_layout')

@section('content')

 
<style>
  .grow { transition: all .2s ease-in-out; }
.grow:hover { transform: scale(1.1);cursor: pointer }

#div2 {
  display: none;
}
.select2-selection {
  height: 34px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: gray;

}
.no-click {
    pointer-events: none;
}

</style>

<section class="content-header pb-1">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
              <label  style="font-size: 20px"><i class="fas fa-graduation-cap"></i> Manage Curriculum</label>
        </div>
        <div class="col-md-6 ">
      
       
            <button class="btn btn-outline-primary btn-sm float-sm-right " onclick="AddCurriculum()"><i class="fas fa-plus"> </i> Curriculum</button>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="content" id="div1">
  <div class="container-fluid">
    <div class="col-md-12 " >
      <div class="row">
        <div class="col-sm-12">
            <div class="card  card-tabs">
              <div class="card-header border-bottom-0 pb-0 pt-2">
                 <div class="card-tools">
                  <button type="button" class="btn btn-tool btn-xs" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active text-bold" id="folder" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true"><i class="fa fa-folder" style="color: #fccc77"> </i>  Curriculum</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-bold" id="table-tab" data-toggle="pill" href="#table" role="tab" aria-controls="table" aria-selected="false"><i class="fa fa-table"></i>  Tabulated Data</a>
                  </li>
              
                 </ul>

              </div>
      

              <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="folder">
                      <div id="folder-parent" class="row">
                        @foreach($folder as $f)
                          <div class="btn btn-sm text-bold col-sm-2 text-left" onclick="folder('{{$f->curriculum_year}}')"><i class="fa fa-folder fa-2x " style="color: #fccc77"><sup><span class="badge badge-warning navbar-badge" id="sp{{$f->curriculum_year}}">
                            {{App\tbl_curriculum::where('curriculum_year',$f->curriculum_year)->count()}}</span></sup></i> {{$f->curriculum_year}}</div>
                        @endforeach

                     </div>
                     <div id="folder-child">
                      <div class="col-sm-12" >
                         
                        <div ><sup> <span class="btn btn-sm text-primary" onclick="back()"><i class="fa fa-reply"></i></span></sup><i class="fa fa-folder-open fa-2x" style="color: #fccc77"> </i> <label id="folder-label"> </label> </div>
                        <div id="folder-items"></div>
                      </div>
                       
                     </div>
                  </div>
                  <div class="tab-pane fade table-responsive" id="table" role="tabpanel" aria-labelledby="table-tab">
                    <table class="table text-sm  table-hover table-striped table-border " id="tbl-list-curriculum">
                        <thead class="bg-dark">
                          <tr style="font-size: 13px">
                            <th class="pt-2 pb-2" style="width: 10px">#</th>
                            <th class="pt-2 pb-2" >CURRICULUM YEAR</th>
                            <th class="pt-2 pb-2">DEPARTMENT</th>
                            <th class="pt-2 pb-2" width="450px">COURSE</th>
                            <th class="pt-2 pb-2 text-center" style="width: 140px">ACTION</th>
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($curriculum as $key => $c)
                          <tr id="tr{{$c->id}}">
                            <td class="pt-2 pb-2">{{$key+1}}</td>
                            <td class="pt-2 pb-2">{{$c->curriculum_year}}</td>
                            @php
                             $ac= App\tbl_academic::find($c->academic_id);

                            @endphp
                            <td class="pt-2 pb-2">{{$ac->college}}</td>
                            <td class="pt-3 pb-2"> {{$c->course}} </td>
                            <td class="pt-2 pb-2" align="center">
                              <div class="row btn-group">
                                  <button class="btn btn-outline-info btn-xs" onclick="ModalManageCurriculum({{$c->id}})"><i class="fas fa-tasks"></i> Manage</button>
                               {{--<button class="btn btn-outline-info btn-xs" onclick="ModalUpdateCurriculum({{$c->id}})"><i class="fa fa-edit"></i> Manage</button>--}} 
                               {{-- <button class="btn btn-outline-danger btn-xs" onclick="RemoveeCurriculum({{$c->id}})"><i class="fa fa-trash"></i> Remove</button>--}}</div></td>
                              </div>
                          </tr>

                          @endforeach
                        </tbody>
                  </table>
                  </div>
                  
                 
                </div>
              </div>
              <!-- /.card -->
            </div>
          
          </div>
      </div>{{--row--}}
      
      <div class="card card-info card-outline " id="curr-subject">
        <div class="card-header">
          <h6 class="card-title " id="curr-title" ></h6>
          <button id="btn-curr" class="btn btn-outline-success btn-sm float-right ml-1" onclick="saveCurr(this.value)" style="display: none"><i class="fas fa-save"></i> Save</button>
          <button id="btn-new-sem" class="btn btn-outline-primary btn-sm float-right sticky" onclick="AddNewSemester()" style="display: none;"><i class="fas fa-plus"></i> Semester</button>  
           <button id="btn-curr-edit" class="btn btn-outline-info btn-sm float-right ml-1" onclick="editCurr(this.value)" style="display: none"><i class="fas fa-edit"></i> Edit</button>
        </div>
        <div class="card-body p-1 m-1">
          {{--  <form id="form-year-level">
              <div class="row pb-2 justify-content-center">
                <div class="col-sm-2 text-sm-right">
                  <label >Year Level:</label>
                </div>
                <div class="col-md-3 ">
                   <select class="form-control form-control-sm" id="year-level" style="text-align-last: center">
                    <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Year</option>
                    <option value="2nd">Second Year</option>
                    <option value="3rd">Third Year</option>
                    <option value="4th">Fourth Year</option>
                    <option value="5th">Fifth Year</option>
                  </select>
                </div>
                <div class="col-sm-1 ">
                  <label class="m-1">Semester:</label>
                </div>
                <div class="col-md-3 pb-3">
                  <select class="form-control form-control-sm " id="y-semester" style="text-align-last: center" >
                    <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Semester</option>
                    <option value="2nd">Second Semester</option>
                    <option value="summer">Summer</option>
                  </select>
                </div>
                <div class="col-sm-2 text-center">
                  <button type="button" onclick="createYearLevel()" class="btn btn-outline-success btn-sm col-md-10"><i class="fas fa-plus-square"></i> Create</button>
                </div>
              </div>
            </form> 
          <input type="hidden" id="curr-id"/>
          <input type="hidden" id="academic-id"/> --}}
          <div class="col-sm-12 m-0 p-0" id="year-level-items">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-add-new-semester">
  <div class="modal-dialog">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">NEW SEMESTER</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form id="form-year-level">
          <div class="col-sm-12 pr-3 pl-3">
            <div class="">
              <div class="form-group">
                <label class="text-white">Year Level:</label>
                <select class="form-control form-control-sm" id="year-level" style="text-align-last: center">
                  <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Year</option>
                    <option value="2nd">Second Year</option>
                    <option value="3rd">Third Year</option>
                    <option value="4th">Fourth Year</option>
                    <option value="5th">Fifth Year</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="text-white">Semester:</label>
                  <select class="form-control form-control-sm " id="y-semester" style="text-align-last: center" >
                    <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Semester</option>
                    <option value="2nd">Second Semester</option>
                    <option value="summer">Summer</option>
                  </select>
                </div>
                <input type="hidden" id="curr-id"/>
                <input type="hidden" id="academic-id"/>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer  pt-1 pb-1" style="border-top:none;background-color:#75787d;">
          <div class="col-md-12 pr-3 pl-3">
             <button type="button" onclick="createYearLevel()" class="btn btn-success float-right btn-sm col-md-2"><i class="fas fa-save"></i> Save</button>
          </div>
        </div>
          </div>

        </div>
</div>


{{--<div class="modal fade" id="modal-curr-subject">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Subject</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="col-sm-12">
                <form id="form-curr-sub">
                <div class="form-group">
                  <label>Subject :</label>
                      <select id="subject" class="form-control select2" style="width: 100%" onchange="subChange(this)">
                        <option value="" disabled="" selected=""> Select Subject</option>
                        @foreach($subject as $s)
                          <option value="{{$s->id}}">{{$s->subject}} {{$s->code}} - {{$s->descriptive_title}}</option>
                        @endforeach
                      </select>
                  </div>
                <div class="row">
                  <div class="form-group col-sm-4">
                    <label>LEC</label>
                    <input type="text" id="lec" class="form-control " style="border:none;background: none" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                    <label>LAB</label>
                    <input type="text" id="lab" class="form-control " style="border:none;background: none" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                    <label>UNITS</label>
                    <input type="text" id="units" class="form-control " style="border:none;background: none" readonly="">
                  </div>
                  <div class="form-group col-sm-5">
                    <label>PRE-REQUISITES</label>
                    <input type="text" id="pre-req" class="form-control " style="border:none;background: none" readonly="">
                  </div>
                  <input type="hidden" id="year-level-id">
                </div>
              </form>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" onclick="subSave()">Save changes</button>
            </div>
          </div>

        </div>
 
</div>--}}
<div class="modal fade" id="modal-curr-subject">
        <div class="modal-dialog">
          <div class="modal-content" style="background-color: #343a40">
             <div class="modal-header" >
                <div class="col-md-12 text-center">
                <label class="modal-title text-primary" style="font-size: 16px">ADD <font class="text-white">SUBJECT</font></label>
              <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
                <span aria-hidden="true" class="text-white">&times;</span>
              </button>
            </div>
            
            </div>
            <div class="modal-body">
              <div class="col-sm-12">
                <form id="form-curr-sub">
                <div class="form-group mb-4">
                  <label class="form-text text-white">SUBJECT:</label>
                      <select id="subject" class="form-control select2 " style="width: 100%" onchange="subChange(this)">
                        <option value="" disabled="" selected="" class="text-gray"> SELECT</option>
                        @foreach($subject as $s)
                          <option value="{{$s->id}}">{{$s->subject}} {{$s->code}} - {{$s->descriptive_title}}</option>
                        @endforeach
                      </select>
                  </div>
                <div class="row">
                  <div class="form-group col-sm-4 mb-4">
                    <label class="text-white">LEC:</label>
                    <input type="text" id="lec" class="form-control " placeholder="LEC" readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label class="text-white">LAB</label>
                    <input type="text" id="lab" class="form-control " placeholder="LAB" style= readonly="">
                  </div>
                  <div class="form-group col-sm-4">
                     <label class="text-white">PRE-REQUISITES:</label>
                    <input type="text" id="units" class="form-control " placeholder="UNITS" readonly="">
                  </div>
                  <div class="form-group col-sm-12">
                     <label class="text-white">LEC:</label>
                    <input type="text" id="pre-req" class="form-control " placeholder="PRE-REQUISITES"  readonly="">
                  </div>
                  <input type="hidden" id="year-level-id">
                </div>
              </form>
              </div>
                 <div class="col-md-12 mt-1">
                 
                  <button type="button" class="btn btn-outline-success float-right" onclick="subSave()"><i class="fas fa-save"> SAVE</i> </button>
              
              
            </div>
            </div>
         
          </div>

        </div>
 
</div>


<div class="modal fade" id="modal-remove-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="sub-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fas fa-exclamation-triangle fa-lg text-danger " ></i> <label  id="subject-code" style="text-decoration: underline;"></label></u> will be remove.<br>
         Do you want to proceed?</p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeCurrSub()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>


<div class="modal fade" id="modal-remove-curriculum">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="dep-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " > </i> <label  id="department-name" style="text-decoration: underline;"></label> BSIT CURRICULUM 2018<br>  will be remove.<br>   Do you want to proceed?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="Restore()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>


  <div class="modal fade" id="modal-manage-curriculum">
    <div class="modal-dialog modal-md">
      <div class="modal-content" >
        <div class="modal-header bg-secondary">
          <div class="col-md-12  text-center">
            <label class="modal-title text-white" style="font-size: 19px">MANAGE CURRICULUM</label>
            <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
             
          </i>
          </div>
        </div>
        <div class="modal-body">
          <div class="row">
                  <div class="col-md-12">
                    <div class="card" style="background-color: #343a40">
                      <div class="card-body">
                        <input type="hidden" id="cur-id">
                        <div class=" form-group ">
                          @php
                            $years = range('2018', date('Y',strtotime('+20 year')));
                          @endphp
                          <label class="form-text text-white">CURRICULUM YEAR:</label>
                          <select class=" form-control " id="c-curriculum-year">
                            @foreach($years as &$year)
                            <option value="{{$year}}" >{{$year}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="form-text text-white">DEPARTMENT</label>
                          <select id="c-department" class="form-control">
                            @foreach($academic as $a)
                            <option value="{{$a->id}}">{{$a->college}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="form-group">
                          <label class="form-text text-white">COURSE</label>
                         <select class="form-control" style="text-align-last: center;" id="c-course" onchange="FilterCourse(this.value)">
                           
                         </select>
                        </div>
                           <div class="form-group">
                          <label class="form-text text-white">CODE</label>
                          <input type="text" id="c-code" class="form-control text-uppercase">
                        </div>
                        <div class="form-group">
                          <button class="btn btn-success btn-block" onclick="updateCur()">UPDATE</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  {{--<div class="col-md-6">
                    <div class="card" style="background-color: #343a40">
                      <div class="card-body">
                        <ul class="todo-list" data-widget="todo-list" id="to_do_list">
                        </ul>
                      </div>
                    </div>
                  </div>--}}

                </div>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal-update-semester">
  <div class="modal-dialog">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">SEMESTER </font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form id="form-year-level-update">
          <div class="col-sm-12 pr-3 pl-3">
            <div class="">
              <div class="form-group">
                <label class="text-white">YEAR LEVEL:</label>
                <select class="form-control " id="c-year-level" style="text-align-last: center">
                  <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Year</option>
                    <option value="2nd">Second Year</option>
                    <option value="3rd">Third Year</option>
                    <option value="4th">Fourth Year</option>
                    <option value="5th">Fifth Year</option>
                  </select>
                </div>
                <div class="form-group">
                  <label class="text-white">SEMESTER:</label>
                  <select class="form-control " id="c-y-semester" style="text-align-last: center" >
                    <option disabled="" selected="">--Select--</option>
                    <option value="1st">First Semester</option>
                    <option value="2nd">Second Semester</option>
                    <option value="summer">Summer</option>
                  </select>
                </div>
                <input type="hidden" id="cu-id"/>
                <input type="hidden" id="c-academic-id"/>
                <input type="hidden" id="t-id"/>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer  pt-1 pb-1" style="border-top:none;background-color:#75787d;">
          <div class="col-md-12 pr-3 pl-4">
             <button type="button" onclick="saveSem()" class="btn btn-success float-right btn-sm btn-block  "> UPDATE</button>
          </div>
        </div>
      </div>
    </div>
</div>


<div class="modal fade" id="modal-remove-semester-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="temp-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " > </i> <label  id="sem-title" style="text-decoration: underline;"></label> <br> will be remove.  <br> Do you want to proceed?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="removeSem()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>


 @include('Quam.modals')

<script type="text/javascript">
 
</script>
<script >


  function RemoveeCurriculum(id){
   
    $('#modal-remove-curriculum').modal('toggle');
  }


  function AddNewSemester(){

    $('#modal-add-new-semester').modal('toggle');
  }
</script>

<script>
  const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
  });

function removeconfirm(id){
    $('#sub-id').val(id);
    $.get('quam-get-subject-data/'+id,function(data){
        $('#subject-code').text(''+data.subject.subject+'-'+data.subject.code);
        $('#modal-remove-confirm').modal('toggle');

  })

}

  function removeCurrSub(){
    var id = $('#sub-id').val();

   $.get('quam-remove-curr-sub/'+id,function(data){
    $('#tr'+data.curr.id).remove();

     $('#td'+data.curr.year_level_id).text(parseInt($('#td'+data.curr.year_level_id).text())-parseInt(data.sub.units))

       Toast.fire({
                    icon: 'success' ,
                    title: ' Successfully Removed!!',
                  });
      $('#modal-remove-confirm').modal('hide');
   })
  }
  function AddCurriculum(){
    $('#modal-add-curriculum').modal('toggle');
  }
  //add curriculum subject

function addSubject(id)
{
  $('#year-level-id').val(id);
    $('#modal-curr-subject').modal('toggle');
}
  function subSave()
  {

      var subject = $('#subject').val();
      var year_level_id = $('#year-level-id').val();

      $.ajax({

        type:'post',
        url :'quam-add-currsubject',
        data:
        {
          "_token"          : "{{ csrf_token() }}",
          "year_level_id"   : year_level_id,
          "subject"         :subject,
        },
        success:function(data)
        {
         
            Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Added!!</span>',
                  });

          var len = $('#tbody'+data.year_level_id+' tr').length;
          var id = data.subject_id; 
            len=len+1;
          $.get('quam-get-subject/'+id,function(sub){
              $("#tbody"+data.year_level_id).append('<tr id="tr'+data.id+'"><td>'+len+'</td><td>'+sub.subject+ ' '+sub.code+' '+sub.descriptive_title+'</td><td>'+sub.lec+'</td><td>'+sub.lab+'</td><td>'+sub.units+'</td><td>'+sub.pre_requisites+'</td><td class="text-right"><button class="btn  btn-outline-danger btn-xs" onclick="removeconfirm('+data.id+')"><i class="fa fa-trash"></i> Remove </button</td></tr>');
              $('#td'+data.year_level_id).text(parseInt($('#td'+data.year_level_id).text())+parseInt(sub.units))
          })
            

          $('#form-curr-sub')[0].reset();
          $('#subject').val('').select2();
        
        },
        error:function(data)
        {

              $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
                  });
                  
                });   
        }
      })


  }
  function subChange(val)
  {
      $.get('quam-get-subject/'+val.value,function(data){

        $('#lec').val(data.lec);
        $('#lab').val(data.lab);
        $('#units').val(data.units);
        $('#pre-req').val(data.pre_requisites);

      })
  }
</script>




<script>
 
  function ModalManageCurriculum(id){

     $('#cur-id').val(id);
     $.get('quam-manage-curriculum/'+id,function(data){
       $('#c-curriculum-year').val(data.curriculum.curriculum_year);

       $('#c-course').val(data.curriculum.course);
       $('#c-code').val(data.curriculum.course_code);
        $('#c-department').val(data.academic.id)
        $('#c-course').empty()
        $.each(data.course,function(k,c){

          if (data.curriculum.course_code == c.program_code)
           {

             $('#c-course').append('<option value="'+c.id+'" selected>'+c.program_name+'</option>')
           }else
           {
            $('#c-course').append('<option value="'+c.id+'" >'+c.program_name+'</option>')
           }
         
        })
       
       $.each(data.temp,function(key,t){
         if(data.curriculum.id == t.curriculum_id)
         {
            var year;
            var semester;
             switch (t.year_level) {
                            case '1st':
                              year='FIRST';
                              break;
                            case '2nd':
                              year='SECOND';
                              break;
                            case '3rd':
                              year='THIRD';
                              break;
                            case '4th':
                              year='FOURTH';
                              break;
                            case '5th':
                              year='FIFTH';
                              break;
                            
                          }
                      switch (t.semester) {
                            case '1st':
                              semester='FIRST';
                              break;
                            case '2nd':
                              semester='SECOND';
                              break;
                            case 'summer':
                              semester='SUMMER';
                              break;
                            
                            
                          }

            $('#to_do_list').append('<li><span class="handle"> <i class="fas fa-ellipsis-v"></i> <i class="fas fa-ellipsis-v"></i></span> <span class="text">'+year+' YEAR '+semester+' '+ ' SEMESTER '+'</span><div class="tools"><i class="fas fa-edit text-info" onclick="editSem('+t.id+')"></i><i class="fas fa-trash" onclick="removesemConfirm('+t.id+')"></i></div></li>')
         }
       })
    })
    $('#modal-manage-curriculum').modal('toggle');
     $('#to_do_list').empty();
  }

  function updateCur(){
      
       var curriculum_year = $('#c-curriculum-year').val();
       var academic_id = $('#c-department').val();
       var course  =$('#c-course').val();
        var code  =$('#c-code').val();
       var id   = $('#cur-id').val();

       $.ajax({
           type: 'put',
           url: 'quam-update-curriculum/'+id,
           data:{

             "_token"         : " {{ csrf_token() }}",
            'curriculum_year' : curriculum_year,
            'academic_id'     : academic_id,
            'course'          :course,
            'code' :code,
          },
          success:function(data){
          $('#modal-manage-curriculum').modal('hide');

             Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Updated!!</span>',
                  });
          
          $('#tr' + data.curriculum.id + ' td:nth-child(2)' ).text(data.curriculum.curriculum_year);
          $('#tr' + data.curriculum.id + ' td:nth-child(3)' ).text(data.academic.college);
          $('#tr'+  data.curriculum.id + ' td:nth-child(4)' ).text(data.curriculum.course);
        
          },
        error:function(data)
        {

              $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
                  });
                  
                });   
        }

       })
  }


  function editSem(id){

    $('#t-id').val(id)
    $.get('quam-get-year-level-data/'+id,function(data){

      $('#c-year-level').val(data.year_level);
       $('#op1'+data.year_level).addClass('bg-primary');
      $('#c-y-semester').val(data.semester);
      $('#cu-id').val(data.curriculum_id);
      $('#c-academic-id').val(data.academic_id);
    })
 
    $('#modal-update-semester').modal('toggle');
  }
  
  function saveSem(){
   
    var curriculum_id =$('#cu-id').val();
    var academic_id =$('#c-academic-id').val();
    var year_level =$('#c-year-level').val();
    var semester = $('#c-y-semester').val();
    var id = $('#t-id').val();
      $.ajax({
           type: 'put',
           url: 'quam-update-year-level/'+id,
           data:{

             "_token"         : " {{ csrf_token() }}",
            'curriculum_id'   : curriculum_id,
            'academic_id'     : academic_id,
            'year_level'      :year_level,
            'semester'        :semester,
          },
          success:function(data){
          
            Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Updated!!</span>',
                  });

                 var year;
            var semester;
             switch (data.year_level) {
                            case '1st':
                              year='FIRST';
                              break;
                            case '2nd':
                              year='SECOND';
                              break;
                            case '3rd':
                              year='THIRD';
                              break;
                            case '4th':
                              year='FOURTH';
                              break;
                            case '5th':
                              year='FIFTH';
                              break;
                            
                          }
                      switch (data.setmester) {
                            case '1st':
                              semester='FIRST';
                              break;
                            case '2nd':
                              semester='SECOND';
                              break;
                            case 'summer':
                              semester='SUMMER';
                              break;                
                            
                          }

            $('#to_do_list').append('<li><span class="handle"> <i class="fas fa-ellipsis-v"></i> <i class="fas fa-ellipsis-v"></i></span> <span class="text">'+year+' YEAR '+semester+' '+ ' SEMESTER '+'</span><div class="tools"><i class="fas fa-edit text-info" onclick="editSem('+data.id+')"></i><i class="fas fa-trash" onclick="removesemConfirm('+data.id+')"></i></div></li>')


            $('#modal-update-semester').modal('hide');


          },
          error:function(data){

          },
        })

  }

  function removesemConfirm(id){
    $('#temp-id').val(id);
    $.get('quam-get-year-level-info/'+id,function(data){
   
       $('#sem-title').text(data.year_level+' Year '+data.semester+' Semester');

    })
    $('#modal-remove-semester-confirm').modal('toggle');
  }

  function removeSem(){
    var id =$('#temp-id').val();
    $.get('quam-remove-year-level/'+id,function(data){
         
       Toast.fire({
                    icon: 'success' ,
                    title: 'Successfully Removed!!',
                  });

        $('#modal-remove-semester-confirm').modal('hide');
    
    })
   
  }


  function ModalUpdateCurriculum(id){
  
    $.get('quam-edit-curriculum/'+id,function(data){

        $('#u-curriculum-year').val(data.curriculum.curriculum_year);
        $('#c-id').val(data.curriculum.id);
        $('#u-course').val(data.curriculum.course);
         
          $.each(data.academic,function(key,value){
           
            $('#u-academic_id').val(value.id)
              
            })

      $('#modal-update-curriculum').modal('toggle');
    })
   
  }
</script>

<script>
  function SaveUpdateCurriculum(){

       var curriculum_year = $('#u-curriculum-year').val();
       var academic_id = $('#u-academic_id').val();
       var course  =$('#u-course').val();
       var id     = $('#c-id').val();

       $.ajax({
           type: 'put',
           url: 'quam-update-curriculum/'+id,
           data:{

             "_token"         : " {{ csrf_token() }}",
            'curriculum_year' : curriculum_year,
            'academic_id'     : academic_id,
            'course'          :course,
          },
          success:function(data){

             Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Updated!!</span>',
                  });
          
          $('#tr' + data.curriculum.id + ' td:nth-child(2)' ).text(data.curriculum.curriculum_year);
          $('#tr' + data.curriculum.id + ' td:nth-child(3)' ).text(data.academic.college);
          $('#tr'+  data.curriculum.id + ' td:nth-child(4)' ).text(data.curriculum.course);

          },
        error:function(data)
        {

              $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
                  });
                  
                });   
        }
       })
  }
</script>



<script >



$('#folder-child').hide();
$('#curr-subject').hide();
  function folder(year){
    var year = year;
    $('#folder-label').text(' '+ year);
    $('#folder-parent').hide();
    $('#folder-child').show();
     $('#folder-items').empty();

    
     $.get('quam-get-all-curriculum',function(data){
          $.each(data.curr,function(key,value){

              if( year ==  value.curriculum_year)
                  {
                    $.each(data.academic,function(key,a){
                      if(a.id == value.academic_id)
                      {
                        $('#folder-items').append('<div class=" col-sm-12 text-left p-1"><button id="btn'+value.id+'" class="ml-5 btn btn-outline-primary " onclick="currSubject('+value.id+')"><i class="fa fa-graduation-cap text-danger"> </i> '+value.course_code+'</button></div>');
                      }
                      
                    })
                  }
            })
        })
  }
  //folder back
  function back(){
    $('#folder-parent').show();
    $('#folder-child').hide();
    $('#curr-subject').hide();
  }
//create new curriculum
function createCurriculum(){
       var curriculum_year = $('#curriculum-year').val();
       var academic_id     = $('#academic_id').val();
       var course          =$('#course').val();
       var code          = $('#course_code').val();

       $.ajax({
          type: 'POST',
          url : 'quam-add-curriculum',
          data:
          {
            "_token"          : "{{ csrf_token() }}",
            'curriculum_year' : curriculum_year,
            'academic_id'     : academic_id,
            'course'          :course,
            'code'          :code,
          },
          success:function(data)
          {

            $('#form-new-curriculum')[0].reset();
              $('#modal-add-curriculum').modal('hide');

           Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Added!!</span>',
                  });

            if(data.curr !=0)
              {
               
                  $.get('get-count/'+data.year,function(c){

                $('#folder-parent').append('<div class="btn btn-sm text-bold col-md-2 text-left" onclick="folder(\''+data.curr.curriculum_year+'\')"><i class="fa fa-folder fa-2x " style="color: #fccc77"><sup><span class="badge badge-warning navbar-badge" id="sp'+data.year+'">'+c+'</span></sup></i>'+data.curr.curriculum_year+'</div>')
                });
              }else{
                $.get('get-count/'+data.year,function(c){
                      $('#sp'+data.year).text(c);
                  });
              }
          
              
          },
          error:function(data)
          {
            var errors = data.responseJSON;
               // toastr.error(errors);
              
               $.each(data.responseJSON.errors, function(key,value) {

                   Toast.fire({
                    icon: 'error' ,
                    title: '<span class="mr-1"> </span>'+value,
            });
               })
           
          }
       })
}

function saveCurr(id)
{

   $.ajax({

    type :'put',
    url : 'quam-save-curriculum/'+id,
    data: {"_token"          : "{{ csrf_token() }}",},
    success:function(data)
    {
      Toast.fire({
                    icon: 'success' ,
                    title: 'Successfully Saved',
                  });
        currSubject(id);
    }
   })

}
function editCurr(id)
{

   $.ajax({

    type :'put',
    url : 'quam-edit2-curriculum/'+id,
    data: {"_token"          : "{{ csrf_token() }}",},
    success:function(data)
    {
        currSubject(id);
    }
   })

}

function currSubject(id){
  var is_save;
  @foreach($curriculum as $c)

        
              if (id == {{$c->id}}) 
              {
           
               $('#btn'+id).addClass('bg-primary no-click');

               

             
              }else
              {
                $('#btn{{$c->id}}').removeClass('bg-primary no-click');
              }
                 

          
    @endforeach  
  $('#year-level-items').empty();
  $.get('quam-get-curriculum/'+id,function(data){
     $('html, body').animate({
                    scrollTop: $("#curr-subject").offset().top - $('#fixed-header').outerHeight()
                  }, 1000);
            is_save=data.curr.is_save;
         $('#btn-curr').val(id);
         $('#btn-curr-edit').val(id);
         if (is_save)
          {
            $('#btn-new-sem').hide();
            $('#btn-curr-edit').show();
             $('#btn-curr').hide()
          }else
          {
             $('#btn-curr').show()
             $('#btn-new-sem').show();
            $('#btn-curr-edit').hide();
          }
       /* $.get('quam-edit-academic/'+data.academic_id,function(academic){

             
             
              
        })*/
           $('#curr-title').text("("+data.curr.course_code+") "+data.curr.course);
            $('#curr-id').val(id);
            $('#academic-id').val(data.academic.id);
     
            
          $.each(data.year,function(key,yl){
            
            var year;
            var semester;
             switch (yl.year_level) {
                            case '1st':
                              year='FIRST';
                              break;
                            case '2nd':
                              year='SECOND';
                              break;
                            case '3rd':
                              year='THIRD';
                              break;
                            case '4th':
                              year='FOURTH';
                              break;
                            case '5th':
                              year='FIFTH';
                              break;
                            
                          }
                          switch (yl.semester) {
                            case '1st':
                              semester='FIRST';
                              break;
                            case '2nd':
                              semester='SECOND';
                              break;
                            case 'summer':
                              semester='SUMMER';
                              break;
                            
                            
                          }
                          if (is_save == false)
                           {
                            $('#year-level-items').append('<div class="card"><div class="card-header pl-1 pt-0 pb-0 bg-dark" style="background-color:#525252;"><label class="col-form-label ml-1 text-white" >'+year+' YEAR '+semester+' SEMESTER</label><div class="card-tools p-1"><button type="button" class="btn btn-tool mr-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button></div></div><div class="card-body table-responsive">'+
                            '<div class="form-group float-right"></div>'+
                            '<table class="table  table-hover table-sm"><thead><th style="width:10px;padding-bottom:12px;">#</th><th style="padding-bottom:12px;">SUBJECT</th><th style="padding-bottom:12px;">LEC</th><th style="padding-bottom:12px;">LAB</th><th style="padding-bottom:12px;">UNITS</th><th style="padding-bottom:12px;">PRE_REQUISITES</th><th class="text-right" style="width:100px;padding-bottom:12px;"><button onclick="addSubject('+yl.id+')" class="btn btn-xs btn-outline-primary pl-2"><i class="fas fa-plus fa-sm"> </i> Subject</button></th></thead><tbody id="tbody'+yl.id+'"></tbody><tr><td colspan="4" class="text-right text-bold">Total Units:</td><td id="td'+yl.id+'"></td></tr></table></div></div>')
                          }else
                          {
                            $('#year-level-items').append('<div class="card"><div class="card-header pl-1 pt-0 pb-0" style="background-color:#525252;"><label class="col-form-label ml-1 text-white" >'+year+' YEAR '+semester+' SEMESTER</label><div class="card-tools p-1"><button type="button" class="btn btn-tool mr-1" data-card-widget="collapse"><i class="fas fa-minus"></i></button></div></div><div class="card-body table-responsive">'+
                            '<div class="form-group float-right"></div>'+
                            '<table class="table  table-hover table-sm"><thead><th style="width:10px;padding-bottom:12px;">#</th><th style="padding-bottom:12px;">SUBJECT</th><th style="padding-bottom:12px;">LEC</th><th style="padding-bottom:12px;">LAB</th><th style="padding-bottom:12px;">UNITS</th><th style="padding-bottom:12px;">PRE_REQUISITES</th></thead><tbody id="tbody'+yl.id+'"></tbody><tr><td colspan="4" class="text-right text-bold">Total Units:</td><td id="td'+yl.id+'"></td></tr></table></div></div>')
                          }
            
           var t_units=0;
           var i=1;
           var y=1;
            

              $.each(data.currsub,function(key,value){
                if (value.year_level_id == yl.id )
                 {
                   $.each(data.subject,function(key2,value2){
                    if (value.subject_id == value2.id  && is_save == 0)
                     {
                    
                      var len = $("#tbody"+yl.id+' tr').length;
                      $("#tbody"+yl.id).append('<tr id="tr'+value.id+'"><td>'+i+'</td><td>'+value2.subject+ ' '+value2.code+' '+value2.descriptive_title+'</td><td>'+value2.lec+'</td><td>'+value2.lab+'</td><td>'+value2.units+'</td><td>'+value2.pre_requisites+'</td><td class="text-right"><button class="btn  btn-outline-danger btn-xs" onclick="removeconfirm('+value.id+')"><i class="fa fa-trash"></i> Remove </button></td></tr>');

                      i++;

                       t_units=t_units+parseInt(value2.units);
                    }else if(value.subject_id == value2.id  && is_save == 1)
                    {
                         var len = $("#tbody"+yl.id+' tr').length;
                      $("#tbody"+yl.id).append('<tr id="tr'+value.id+'"><td>'+y+'</td><td>'+value2.subject+ ' '+value2.code+' '+value2.descriptive_title+'</td><td>'+value2.lec+'</td><td>'+value2.lab+'</td><td>'+value2.units+'</td><td>'+value2.pre_requisites+'</td></tr>');

                      y++;
                      
                       t_units=t_units+parseInt(value2.units);
                    }
                      
                  })
                 }
                 
                     
              })
                $('#td'+yl.id).text(t_units);
                $('#td'+yl.id).addClass('text-primary text-bold');
          
           
          })
       

  })

  $('#curr-subject').show();
}


function createYearLevel(){
  var id          =  $('#curr-id').val();
  var academic_id =  $('#academic-id').val();
  var year_level  =  $('#year-level').val();
  var semester    =  $('#y-semester').val();

  $.ajax({
    type:'post',
    url :'quam-save-year-level',
    data:
    {
      "_token"          : "{{ csrf_token() }}",
      'id'              : id,
      'academic_id'     : academic_id,
      'year_level'      : year_level,
      'semester'        : semester,
    },
    success:function(data)
    {
        if (data.f1 > 0)
         {
           Toast.fire({
          icon: 'error' ,
          title: ' This semester is already exists',
        });
         }else
         {
            Toast.fire({
          icon: 'success' ,
          title: ' Successfully Added',
        });

      var year;
      var semester;
      switch (data.temp.year_level) {
                            case '1st':
                              year='FIRST';
                              break;
                            case '2nd':
                              year='SECOND';
                              break;
                            case '3rd':
                              year='THIRD';
                              break;
                            case '4th':
                              year='FOURTH';
                              break;
                            case '5th':
                              year='FIFTH';
                              break;
                            
                          }
                          switch (data.temp.semester) {
                            case '1st':
                              semester='FIRST';
                              break;
                            case '2nd':
                              semester='SECOND';
                              break;
                            case 'summer':
                              semester='SUMMER';
                              break;
                            
                            
                          }
      $('#year-level-items').append('<div class="card"><div class="card-header p-1 bg-dark"><label>'+year+' YEAR '+semester+' SEMESTER</label><div class="card-tools p-1"><button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button></div></div><div class="card-body table-responsive">'+
              '<div class="form-group float-right"><button onclick="addSubject('+data.temp.id+')" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"> Subject</i></button></div>'+
              '<table class="table  table-hover table-sm"><thead><th style="width:10px">#</th><th>SUBJECT</th><th>LEC</th><th>LAB</th><th>UNITS</th><th>PRE_REQUISITES</th><th style="width:100px">ACTION</th></thead><tbody id="tbody'+data.temp.id+'"></tbody></table></div></div>')
      $('#form-year-level')[0].reset();
      $('#modal-add-new-semester').modal('hide');
         }
      
      
    }
  })
}

$('#subject').select2();

$('#table-tab').click(function(){
  $('#curr-subject').hide();
})
  $('.todo-list').sortable({
    placeholder: 'sort-highlight',
    handle: '.handle',
    forcePlaceholderSize: true,
    zIndex: 999999
  })
function FilterCourse(val)
{

    @foreach($academic2 as $a)
        if ({{$a->id}} == val)
         {
         
          $('#c-code').val("{{$a->program_code}}")
         }
    @endforeach 
}
$('#folder').click(function(){
  back();
})
</script>


@endsection

