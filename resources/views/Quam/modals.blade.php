
<div class="modal fade" id="modal-add-curriculum">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">CURRICULUM</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form  id="form-new-curriculum">
          @csrf
          <div class="col-md-12 pr-3 pl-3">
            <div class=" form-group ">
              @php
              $years = range('2018', date('Y',strtotime('+20 year')));
              @endphp
              <label class="form-text text-white">CURRICULUM YEAR:</label>
              <select class=" form-control form-control-sm text-bold" id="curriculum-year" style="text-align-last: center;border-color:  #c3b091;color: ">
                <option disabled="" selected="">--SELECT--</option>
                @foreach($years as &$year)
                <option value="{{$year}}" >{{$year}}</option>
                @endforeach
              </select>
            </div>
            <div class=" form-group ">
              <input type="hidden" id="academic_id">
              <label class="form-text text-white">DEPARTMENT:</label>
              <select  class="form-control form-control-sm text-bold "  style="text-align-last: center;border-color: #
              ;color: " onchange="Course(this.value)">
                <option selected disabled>--SELECT--</option>
                @foreach($academic as $a)
                <option value="{{$a->college}}">{{$a->college}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <input type="hidden" id="course">
              <label class="form-text text-white">COURSE</label>
              <select class="form-control text-bold" style="text-align-last: center;" id="course2" disabled="" onchange="Code(this.value)" >
                <option disabled="" selected="" >--COURSE--</option>
              </select>
            </div>
            <div class="form-group">
              <label class="form-text text-white">CODE</label>
              <input type="text" id="course_code"  class="form-control form-control-sm text-uppercase" readonly=""> 
            </div>
            
          </div>
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d;">
        <div class="col-md-12 pr-3 pl-3">
          <button type="button"  onclick="createCurriculum()" class="btn btn-success btn-sm float-right"><i class="fas fa-save"> SAVE</i> </button>
        </div>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="modal-update-curriculum">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
      <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">CURRICULUM</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
      </div>
      <div class="modal-body">
        <form  id="form-update-curriculum">
          @csrf
          <div class="col-md-12 pr-3 pl-3">
            <div class=" form-group ">
              @php
              $years = range('2017', date('Y',strtotime('+20 year')));
              @endphp
              <label class="form-text text-white">CURRICULUM YEAR:</label>
              <select class=" form-control form-control-sm text-bold" id="u-curriculum-year"
              style="text-align-last: center;border-color:  #c3b091;color: " >
                @foreach($years as &$year)
                <option value="{{$year}}" >{{$year}}</option>
                @endforeach
              </select>
            </div>
            <div class=" form-group ">
              <label class="form-text text-white">ACADEMIC:</label>
              <select name="academic_id" class="form-control form-control-sm text-bold " id="u-academic_id" style="text-align-last: center;border-color: #
            ;color: ">
                @foreach($academic as &$a)
                <option value="{{$a->college}}">{{$a->college}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group text-left">
              <label class="form-text text-white">Course</label>
              <input type="text" id="u-course" class="form-control form-control-sm">
            </div>
          </div>
          <input type="hidden" id="c-id">
        </form>
      </div>
         <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d;">
          <div class="col-md-12 pr-3 pl-3">
             <button type="button" id="btn-curriculum" onclick="SaveUpdateCurriculum()" class="btn btn-success btn-sm float-right col-md-2"><i class="fas fa"></i> UPDATE</button>
          </div>
         </div>
        
    </div>
  </div>
</div>






<!--<div class="modal fade" id="addcurriculum">
  <div class="modal-dialog modal-md">
    <div class="modal-content" >
      <div class="modal-header p-2">
        <label class="modal-title ">ADD CURRICULUM</label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class=" fa fa-times-circle-o" style="color: red"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addcur">
        @csrf
          <div class=" form-group">
            <label class="">Academic</label>
            <input type="text" class="form-control" name="academic">
            <input type="hidden" class="form-control" name="academic2">
          </div>
          <div class=" form-group">
            <label class="">School Year</label>
            <input type="text" class="form-control" name="school_year">
          </div>
           <div class=" form-group">
            <label class="">Semester</label>
            <input type="text" class="form-control" name="semester">
          </div>
           <div class=" form-group">
            <label class="">Year Level</label>
            <input type="text" class="form-control" name="year_level">
          </div>
          <div class="form-group">
            <button type="button" id="btn-curriculum-add" class="btn btn-success btn-block">Add</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>-->

<div class="modal fade" id="modal-update-subject">
  <div class="modal-dialog modal-md">
    <div class="modal-content" >
      <div class="modal-header p-2">
        <label class="modal-title "><i class="fa fa-book"></i></label>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class=" fa fa-times-circle-o" style="color: red"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="update-subject">
        @csrf
          <div class="row">
            <div class="col-sm-6 text-center">
              <div class=" form-group">
                <label class="">Subject</label>
                <input type="text" class="form-control text-center" id="u-subject" >
              </div>
            </div>
            <div class="col-sm-6 text-center">
              <div class=" form-group">
                <label class="">Code</label>
                <input type="text" class="form-control text-center" id="u-code" >
              </div>
            </div>
          </div>
           <div class=" form-group text-center">
            <label class="">Descriptive Title</label>
            <input type="text" class="form-control text-center" id="u-descriptive_title" >
          </div>
          <div class="row">
            <div class="col-sm-4 text-center">
              <div class=" form-group">
                <label class="">Lec</label>
                <input type="text" class="form-control text-center" id="u-lec" >
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class=" form-group">
                <label class="">Lab</label>
                <input type="text" class="form-control text-center" id="u-lab" >
              </div>
            </div>
            <div class="col-sm-4 text-center">
              <div class=" form-group">
                <label class="">Units</label>
                <input type="text" class="form-control text-center" id="u-units" >
              </div>
            </div>
          </div>
          
          
        
            <input type="hidden" id="u-id">
           <div class=" form-group text-center">
            <label class="">Pre-requisites</label>
            <input type="text" class="form-control text-center" id="u-pre_requisites" >
          </div>
          <div class="form-group">
            <button type="button" onclick="updateSubject()" class="btn btn-success btn-block">Update</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function Course(val)
  {
    $('#course2').empty().append('<option disabled selected>--COURSE--</option>').attr('disabled',false)
        @foreach($academic2 as $a)
            if (val == "{{$a->college}}")
             {

              $('#course2').append('<option value="{{$a->id}}">{{$a->program_name}}</option>')
             }
      @endforeach

  }
  function Code(val)
  {

    @foreach($academic2 as $a)

      if (val == {{$a->id}})
       {
        
          $('#academic_id').val("{{$a->id}}")
        $('#course_code').val("{{$a->program_code}}")
        $('#course').val("{{$a->program_name}}")
       }

    @endforeach
  }
</script>