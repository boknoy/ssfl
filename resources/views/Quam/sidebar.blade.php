<style>
.sidebar-dark-primary .nav-sidebar>.nav-item>.nav-link.active, .sidebar-light-primary .nav-sidebar>.nav-item>.nav-link.active {
    background-color: maroon;
    color: white;
}
</style>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link text-center">
    
      <span class="brand-text font-weight text-light"><b style="font-size: 12px;">QUALITY ASSURANCE OFFICE</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('img/'.Auth::user()->image)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" data-widget="control-sidebar" data-slide="true" class="d-block">{{Auth::user()->fname}} {{substr(Auth::user()->mname,0,1)}}. {{Auth::user()->lname}}</a>
        </div>
      </div>
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{url('quam-dashboard')}}" class="nav-link @if(Request::is('quam-dashboard')) active  @endif || @if(Request::is('quam_dashboard')) active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              
              </p>
            </a>
          </li>
        
          <li class="nav-item">
            <a href="{{url('quam-academic')}}" class="nav-link @if(Request::is('quam-academic')) active @endif">
              <i class="nav-icon fas fa-user-graduate"></i>
              <p>
                Departments
          
                
              </p>
            </a>
          <!--  -->
          </li>
           <li class="nav-item">
            <a href="{{url('quam-subjects')}}" class="nav-link @if(Request::is('quam-subjects')) active @endif">
              <i class="nav-icon fas fa-book  "></i>
              <p>
                Subjects
          
                
              </p>
            </a>
          <!--  -->
          </li>

           <li class="nav-item @if(Request::is('quam-curriculum') || Request::is('quam-scheduling')) 
            menu-open  @endif  ">
            <a href="#" class="nav-link @if(Request::is('quam-curriculum') || Request::is('quam-scheduling')) 
            active   @endif">
              <i class="nav-icon fas fa-graduation-cap"></i>
              <p>
               Curriculum Management

              </p>
               <i class="fas fa-angle-left right"></i>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('quam-curriculum')}}" class="nav-link @if(Request::is('quam-curriculum')) active @endif">
                  <i class="far fa-circle nav-icon ml-3 @if(Request::is('quam-curriculum')) text-maroon @endif"></i>
                  <p>Curriculum</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('quam-scheduling')}}" class="nav-link @if(Request::is('quam-scheduling')) active @endif">
                  <i class="far fa-circle nav-icon ml-3 @if(Request::is('quam-scheduling')) text-maroon @endif"></i>
                  <p>Subject Scheduling</p>
                </a>
              </li>
            
            </ul>
          </li>
             <li class="nav-item">
            <a href="{{url('quam-faculty-load')}}" class="nav-link @if(Request::is('quam-faculty-load')) active @endif" id="load">
              <i class="nav-icon fas fa-download"></i>
              <p>
                Faculty Load
          
                
              </p>
            </a>
          <!--  -->
          </li>

          <li class="nav-item @if(Request::is('quam-rooms') || Request::is('quam-section')) menu-open @endif">
            <a href="#" class="nav-link @if(Request::is('quam-rooms') || Request::is('quam-section') ) active @endif">
              <i class="nav-icon fas fa-cogs"></i>
              <p>
                Maintenance
              </p>
              <i class="fas fa-angle-left right"></i>
            </a>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('quam-rooms')}}" class="nav-link @if(Request::is('quam-rooms')) active @endif">
               <i class="far fa-circle nav-icon ml-3 @if(Request::is('quam-rooms')) text-maroon @endif"></i>
              <p>
                Rooms
              </p>
            </a>
              </li>
                 <li class="nav-item">
                <a href="{{url('quam-section')}}" class="nav-link @if(Request::is('quam-section')) active @endif">
               <i class="far fa-circle nav-icon ml-3  @if(Request::is('quam-section')) text-maroon @endif "></i>
              <p>
                Sections
              </p>
            </a>
              </li>
             </ul>
          <!--  -->
          </li>
          
      
              <li class="nav-item  " id="report_menu">
              <a href="#" class="nav-link " id="report_href">
                <i class="nav-icon fas fa-file-signature"></i>
                <p>
                  Reports
                </p>
                <i class="fas fa-angle-left right"></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{url('quam-subject-schedule-report')}}" class="nav-link " id="subject_report">
                    <i class="far fa-copy nav-icon"></i>
                    <p>Class Schedule</p>
                  </a>
                </li>
                 <li class="nav-item">
                  <a href="{{url('quam-curriculum-report')}}" class="nav-link " id="curriculum_report">
                    <i class="far fa-copy nav-icon"></i>
                    <p>Curriculum</p>
                  </a>
                </li>
              
              
              </ul>
            <!--  -->
            </li>  
             <li class="nav-item">
            <a href="{{url('quam-manageaccount')}}" class="nav-link @if(Request::is('quam-manageaccount')) active @endif">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Manage Account
              </p>
            </a>
          <!--  -->
          </li>
          
             <li class="nav-item">
            <a href="#" class="nav-link " data-toggle="modal" data-target="#logout">
              <i class="nav-icon fa fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          <!--  -->
          </li>
          

          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


<div class="modal fade" id="logout">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="a-id">
     <div class="col-md-12 ">
     
        <p  class="text-muted p-0" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-circle fa-lg text-warning " > </i><label  id="academic-name" style="text-decoration: underline;"></label> Are you sure you want logout?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
          <a class="btn btn-danger btn-xs" href="{{ route('logout') }}"
   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i>Yes, {{ __('logout') }} </a>
 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
  @csrf
</form>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

  

