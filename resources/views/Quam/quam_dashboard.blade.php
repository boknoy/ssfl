  @extends('layouts/quam_layout')

  @section('content')


      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-12">
              <label style="font-size: 20px"><i class="fas fa-tachometer-alt"> Dashboard</i></label>
        
          <a class="float-right col-form-label" href="{{url('quam-dashboard')}}">Home <font style="color: gray">/ Dashboard</font></a>
          
          </div>
            </div>
        </div><!-- /.container-fluid -->
      </section>
      
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-6 text-dark ">
          <div class="small-box  bg-warning" >
            <div class="inner">
             <h3>{{App\tbl_academic::count()}}</h3>
              <p><b> Departments </b></p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="{{url('quam-academic')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
          <div class="small-box bg-primary">
            <div class="inner text-dark">
              <h3>{{App\tbl_subject::where('is_remove','false')->count()}}</h3>
              <p><b> Subjects </b></p>
            </div>
            <div class="icon">
              <i class="fas fa-book"></i>
            </div>
            <a href="{{url('quam-subjects')}}" class="small-box-footer text-dark">More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-6">
              <div class="small-box bg-success" >
                <div class="inner">
                <h3>{{App\tbl_class::count()}}</h3>
                <p><b> Class </b></p>
                </div>
                <div class="icon">
                  <i class="fas fa-chalkboard-teacher "></i>
                </div>
                <a href="{{url('quam-scheduling')}}" class="small-box-footer text-dark">More info <i class="fa fa-arrow-circle-right"></i>
                </a>
              </div>
        </div>
        
        <div class="col-lg-3 col-6">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>{{App\tbl_curriculum::count()}}</h3>
              <p><b> Curriculum </b></p>
            </div>
            <div class="icon">
              <i class="fas fa-graduation-cap"></i>
            </div>
            <a href="{{url('quam-curriculum')}}" class="small-box-footer text-dark">More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
          <div class="col-lg-3 col-6 text-dark ">
          <div class="small-box  bg-secondary" >
            <div class="inner">
             <h3>{{App\tbl_room::count()}}</h3>
              <p><b> Rooms </b></p>
            </div>
            <div class="icon">
              <i class="fa fa-door-open"></i>
            </div>
            <a href="{{url('quam-rooms')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
         <div class="col-lg-3 col-6 text-dark ">
          <div class="small-box  bg-info" >
            <div class="inner">
             <h3>{{App\tbl_section::count()}}</h3>
              <p><b> Sections </b></p>
            </div>
            <div class="icon">
              <i class="fa fa-th-large"></i>
            </div>
            <a href="{{url('quam-section')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
       
      </div>
  
    </div>
  </div>


      
  <script >
  

    $('#tbl-activity-log').DataTable();
  </script>

  @endsection