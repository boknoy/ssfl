@extends('layouts.quam_layout')
@section('content')
<style >
  .transparent {
  background:#7f7f7f;
  background:rgba(255,255,255,0.-10);
}
</style>
<section class="content-header pb-0">
  <div class="container-fluid">
  	<div class="col-md-12">
  	   <div class="row">
  	   	<div class="col-md-12">
  	   		<label style="font-size: 20px"><i class="fa fa-user-cog"> Manage Account</i></label>
  	   	  <button class="btn btn-outline-primary btn-sm float-right " onclick="ModalUser()"><i class=" fa fa-plus"></i> Account</button>
  	   	</div>
  	   </div>
  	</div>
  </div>
</section>


<div class="content ">
  <div class="container-fluid">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body table-responsive">
              <table id="user-table" class="table tabl-hover table-border table-striped text-sm">
                <thead class="bg-dark">
                  <tr>
                    <th width="10px">#</th>
                    <th>NAME</th>
                    <th>EMAIL</th>
                    <th>ROLE</th>
                    <th>Status</th>
                    <th class="text-center">ACTION</th>
                  </tr>
                </thead>
                @php
                $i=1;
                @endphp
                <tbody>
                  @foreach($user as $u)
                  
                  <tr id="tr{{$u->id}}">
                    <td class="pt-2 pb-2">{{$i++}}</td>
                    <td class="pt-2 pb-2 text-uppercase">{{$u->fname}} {{substr($u->mname, 0, 1)}}. {{$u->lname}}</td>
                    <td class="pt-2 pb-2">{{$u->email}}</td>
                    <td class="pt-2 pb-2">{{$u->role}}</td>
                    <td  class="pt-2 pb-2">@if($u->is_active)  <span class="badge badge-success">Active</span> @else   <span class="badge badge-danger">Inactive</span> @endif</td>
                    <td class="pt-2 pb-2" align="center " >
                      <div class="row btn-group" >
                        <button onclick="editUser({{$u->id}})" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button>
                        @if($u->role == "HR" && $u->is_active)
                        <button" onclick="modalRemove({{$u->id}})" class="btn btn-outline-danger btn-xs  "  ><i class="fa fa-user-lock"> </i> Disable</button>
                        @elseif($u->is_active == false && $u->role == "HR" )
                         <button" onclick="modalActive({{$u->id}})" class="btn btn-outline-success btn-xs  "  ><i class="fa fa-unlock"> </i> Active</button>
                        @endif
                      </div>
                      </td>
                  </tr>
           
                  @endforeach
                </tbody>  
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-add-user">
  <div class="modal-dialog ">
    <div class="modal-content "  style="background-color: #343a40">
    <div class="modal-header">
      <div class="col-md-12 text-center">
           <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">ACCOUNT</font></label>
      <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-white">&times;</span>
      </button>
      </div>
    </div>
      <div class="modal-body" style="margin-bottom: -20px">
        <form  id="form-add-user">
          @csrf
          <div class="col-md-12 pl-3 pr-3 ">
            <div class="form-group ">
              <label class="text-white">FIRST NAME</label>
              <input type="text" id="f_name" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="text-white">MIDDLE NAME</label>
              <input type="text" id="m_name" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="text-white">LAST NAME</label>
              <input type="text" id="l_name" class="form-control form-control-sm text-uppercase" >
            </div>
             <div class="form-group ">
              <label class="text-white">NAME EXT (Jr, I, II)</label>
              <input type="text" id="ext_name" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="text-white">ROLE</label>
              <select id="role_type" class="form-control form-control-sm">
                <option selected="" disabled=""> --SELECT-- </option>
                <option value="HR">HR</option>
                <option disabled="" value="Admin">Admin</option>


              </select>
            </div>
            <div class="form-group ">
              <label class="text-white">EMAIL</label>
              <input type="text" id="email_add" class="form-control form-control-sm " >
            </div>
            <div class="form-group ">
              <label class="text-white">PASSWORD</label>
              <input type="password" id="user_pass" class="form-control form-control-sm " >
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer pt-1 pb-1"  style="border-top:none;background-color:#75787d">
         <div class="col-md-12 pl-4 pr-3">
            <button class="btn btn-success btn-sm btn-block" onclick="addUsers()"><i class="fa fa-save"> SAVE</i> </button>
         </div>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-user-update">
  <div class="modal-dialog ">
      <div class="modal-content"  style="background-color: #343a40">
    <div class="modal-header">
      <div class="col-md-12 text-center">
        <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">ACCOUNT DETAILS</font></label>
      <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-white">&times;</span>
      </button>
      </div>
    </div>
      <div class="modal-body  " style="margin-bottom: -20px">
          <form  id="form-update-user">
          @csrf
          <div class="col-md-12 pl-3 pr-3">
            <div class="form-group ">
              <label class="text-white">FIRST NAME</label>
              <input type="text" id="u-f_name" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="text-white">MIDDLE NAME</label>
              <input type="text" id="u-m_name" class="form-control form-control-sm text-uppercase" >
            </div>
            <div class="form-group ">
              <label class="text-white">LAST NAME</label>
              <input type="text" id="u-l_name" class="form-control form-control-sm text-uppercase" >
            </div>
             <div class="form-group ">
              <label class="text-white">NAME EXT (Jr, I, II)</label>
              <select class="form-control form-control-sm" id="u-ext-name">
                  <option selected="" value="-"> - </option>
                  <option value="JR">JR</option>
                  <option value="II">II</option>
                  <option value="III">III</option>
                  <option value="IV">IV</option>

              </select>
            </div>
           
            <div class="form-group ">
              <label class="text-white">EMAIL</label>
              <input type="text" id="u-email_add" class="form-control form-control-sm form-control-sm " >
            </div>
          
          </div>
          <input type="hidden" id="u-id">
        </form>
          
          </div>
         <div class="modal-footer pt-1 pb-1 mt-0" style="border-top:none;background-color:#75787d">
           <div class="col-md-12 pl-4 pr-3">
             <button class="btn btn-success btn-sm btn-block " onclick="SaveUpdateUser()"><i class="fa fa-"> UPDATE</i></button>
           </div>
         </div>
       
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-block-user">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="user-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to block<br> <label  id="use-account-name" style="text-decoration: underline;"></label>?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="Block()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>
<div class="modal fade" id="modal-active-user">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="user-id2">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-success " ></i> Do you want to active<br> <label  id="use-account-name2" style="text-decoration: underline;"></label>?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-success btn-xs " onclick="Activate()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>
<script>
  
  function modalRemove(id){

    $('#user-id').val(id);

    $.get('quam-user-account-data/'+id,function(data){

      $('#use-account-name').text(data.fname+' '+data.mname.charAt(0)+'. '+data.lname);
    })
   $('#modal-block-user').modal('toggle');
  }
 function modalActive(id){

    $('#user-id').val(id);

    $.get('quam-user-account-data/'+id,function(data){

      $('#use-account-name2').text(data.fname+' '+data.mname.charAt(0)+'. '+data.lname);
    })
   $('#modal-active-user').modal('toggle');
  }

function Block(){
  var id =$('#user-id').val();
  $.get('quam-block-user-account/'+id,function(data){

      Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully disabled!!</span>',
                  });

             $('#tr' + data.id + ' td:nth-child(5)' ).text("").append('<span class="badge badge-danger"> Inactive</span>');
               $('#tr' + data.id + ' td:nth-child(6)' ).text("").append('  <button onclick="editUser('+data.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button><button" onclick="modalActive('+data.id+')" class="btn btn-outline-success btn-xs  "  ><i class="fa fa-unlock"> </i> Active</button>');

      $('#modal-block-user').modal('hide');
  })
}
function Activate(){
  var id =$('#user-id').val();
  $.get('quam-block-user-account2/'+id,function(data){

      Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully activate!!</span>',
                  });

             $('#tr' + data.id + ' td:nth-child(5)' ).text("").append('<span class="badge badge-success"> Active</span>');
              $('#tr' + data.id + ' td:nth-child(6)' ).text("").append('  <button onclick="editUser('+data.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button><button" onclick="modalRemove('+data.id+')" class="btn btn-outline-danger btn-xs  "  ><i class="fa fa-user-lock"> </i> Disable</button>');

      $('#modal-active-user').modal('hide');
  })
}

  function ModalUser(){
    $('#modal-add-user').modal('toggle');
  }
</script>

<script>
   const Toast = Swal.mixin({
                  toast: true,
                  position: 'top-end',
                  showConfirmButton: false,
                  timer:3000,
                  
    });


  function addUsers(){

    var fname = $('#f_name').val();
    var mname =$('#m_name').val();
    var lname =$('#l_name').val();
    var ext_name=$('#ext_name').val();
    var role =$('#role_type').val();
    var email =$('#email_add').val();
    var password=$('#user_pass').val();

    $.ajax({

      type: 'POST',
      url: 'quam-add-user',
      data: {
             "_token"   : "{{ csrf_token()}}",
             "fname"    :fname,
             "mname"    :mname,
             "lname"    :lname,
             "ext_name" :ext_name,
             "role"     :role,
             "email"    :email,
             "password" :password,
      },

      success:function(response){
       
        Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Added!!</span>',
                  });

        $('#form-add-user')[0].reset();
        var t=$('#user-table').DataTable();
        t.row.add([
          '',
          response.fname+' '+response.mname.charAt(0)+'. '+response.lname+' '+response.extension_name,
          response.email,
          response.role,
          '<div class="btn-group">'+
            '<a href="#" class="btn btn-outline-info btn-xs" onclick="editUser('+response.id+')"><i class="fa fa-edit "> </i> Edit</a>'
            {{--'<a href="#" class="btn btn-outline-danger btn-xs"><i class="fa fa-user-lock "> </i> Block</a>'--}},

          ]).node().id = 'tr'+response.id;
                t.draw();
                      t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
       
       $('#form-add-user')[0].reset();
       $('#modal-add-user').modal('hide');
      },
          error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+ value,
                  });
                  
                }); 
             }
      
     
    })
  }
</script>

<script >
   function editUser(id){
    $.get('quam-edit-user/'+id,function(data){
      
      $('#u-id').val(data.user.id);
      $('#u-f_name').val(data.user.fname);
      $('#u-m_name').val(data.user.mname);
      $('#u-l_name').val(data.user.lname);
      $('#u-role_type').val(data.user.role);
      $('#u-email_add').val(data.user.email);
      $('#u-user_pass').val(data.user.password);
     
   

      $('#modal-user-update').modal('toggle');

    })

  }

  function SaveUpdateUser(){
    var fname = $('#u-f_name').val();
    var mname = $('#u-m_name').val();
    var lname = $('#u-l_name').val();
    var role = $('#u-role_type').val();
    var email = $('#u-email_add').val();
    var password = $('#u-user_pass').val();
    var id      = $('#u-id').val();

    $.ajax({
      type: 'put',
      url: 'quam-update-user/'+id,
      data:{
         "_token"   :"{{csrf_token()}}",
         'fname'    :fname,
         'mname'    :mname,
         'lname'    :lname,
         'role'     :role,
         'email'    :email,
         'password' :password
      },
      success:function(response){

        
        Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Updated!!</span>',
                  });

           $('#tr' + response.id + ' td:nth-child(2)' ).text(response.fname+' '+response.mname.charAt(0)+'. '+response.lname);
           $('#tr' + response.id + ' td:nth-child(3)' ).text(response.email);
           $('#tr' + response.id + ' td:nth-child(4)' ).text(response.role);

          $('#modal-user-update').modal('toggle');
      }
    })
  }

  $('#user-table').DataTable({

    columnDefs: [
    {bSortable: false, targets: [2,3,4]},
     { className: "text-center", "targets": [ 4 ] }
    ]
  });
</script>
@endsection