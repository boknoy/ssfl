@extends('layouts.quam_layout')

@section('content')
<style type="text/css">

</style>

<section class="content-header pb-1">
  <div class="container-fluid">
   <div class="col-md-12">
      <div class="row">
      <div class="col-md-6">
          <label style="font-size: 20px"><i class="nav-icon fas fa-chalkboard-teacher"> </i> Manage Section</label>
       
      </div>
      <div class="col-md-6">
        <div id="arch-sec">
          <button class="btn btn-outline-secondary p-1 ml-2 float-sm-right btn-sm " onclick="Archive();"><i class="fas fa-archive "></i><span class="badge badge-warning" style="position: absolute;margin-left: 45px;margin-top:-8px;font-weight: lighter;font-size: 10px" >{{App\tbl_section::where('is_remove',true)->count()}}</span> Archive</button>
        </div>
          <button class="btn btn-outline-primary btn-sm float-sm-right" onclick="AddSection()"><i class="fa fa-plus"> </i> Section</button>
      </div>
    </div>
   </div>
 </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="col-sm-12 justify-content-center">
      <div class="row  justify-content-center">
        <div class="col-md-12">
             <div class="card">
   
  
    <div class="card-body table-responsive">
      <table id="section-table" class="table table-border table-striped table-hover  text-sm">
        <thead class="bg-dark">
        <tr class="text-uppercase">
          <th width="15px">#</th>
          <th>Course </th>
          <th>Section Name</th>
          <th>Year Level</th>
          <th>Status</th>
          <th class="text-center ">Action</th>
        </tr>
        </thead>
        @php
          $i=1;
        @endphp
        <tbody>
          @foreach($section as $s)
            <tr id="tr{{$s->id}}">
              <td>{{$i++}}</td>
              <td class="pt-2 pb-2 text-uppercase" >{{$s->course}}</td>
              <td class="pt-2 pb-2 text-uppercase">{{$s->section_name}}</td>
              <td class="pt-2 pb-2 text-uppercase">{{$s->year_level}}</td>
              <td>@if($s->is_active == 0) <span class="badge badge-secondary"> Not Available </span> @else <span class="badge badge-success ">  Available </span>  @endif</td>
              <td class="text-center pt-2 pb-2">
                <div class="btn-group">
                  <a href="#" class="btn btn-outline-info btn-xs" onclick="editsection({{$s->id}})"><i class="fa fa-edit text-lowercase"></i> Edit</a>
                  <a href="#" class="btn btn-outline-danger btn-xs" onclick="modalRemove({{$s->id}})"><i class="fa fa-trash "> </i> Remove</a>
              </div></td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
       
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
          
        </div>
      </div>
    </div>

 
  </div>
 </section>

<div class="modal fade" id="modal-add-section">
  <div class="modal-dialog modal-md">
    <div class="modal-content"  style="background-color: #343a40">
       <div class="modal-header">
        <div class="col-md-12 text-center">
          <label class="modal-title text-primary" style="font-size: 16px">CREATE <font class="text-white">SECTION</font></label>
          <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
            <span aria-hidden="true" class="text-white">&times;</span>
          </button>
        </div>
       </div>
      <div class="modal-body">
         <form id="form-add-section">
          @csrf
          <div class="col-md-12 pr-3 pl-3">
            <div class="form-group">
            <label class="form-label text-white" >COURSE:</label>
            @php  
              $course = App\tbl_curriculum::all()->unique('course_code');
            @endphp
            <select class="form-control form-control-sm" id="course">
              <option selected="" disabled="">--SELECT--</option>

              @foreach($course as $a)
              <option value="{{$a->course_code}}">{{$a->course_code}}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label class="form-label text-white">YEAR LEVEL :</label>
            <select class="form-control form-control-sm" id="year_level" onchange="academicSelect(this)">
              <option selected="" disabled="">--SELECT--</option>
              <option value="1st">1st</option>
              <option value="2nd">2nd</option>
              <option value="3rd">3rd</option>
              <option value="4th">4th</option>
              <option value="5th">5th</option>
            </select>
          </div>
          <div class="form-group">
            <label class="form-label text-white">SECTION NAME :</label>
            <input type="text" name="section_name" id="section_name" class="form-control form-control-sm text-uppercase" >
          </div>
        </div>
        </form>
          </div>
          <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d">
          <div class="col-md-12 pr-3 pl-3">
            <button type="button" class="btn btn-success btn-sm btn-block" onclick="addsections()">
             <i class="fas fa-save"></i> Save</button>
          </div>
        </div>
    </div>
  </div>
</div>
 
<div class="modal fade" id="modal-update-section">
  <div class="modal-dialog modal-md">
     <div class="modal-content"  style="background-color: #343a40">
       <div class="modal-header">
            <div class="col-md-12 text-center">
                <label class="modal-title text-success" style="font-size: 16px">UPDATE <font class="text-white">SECTION</font></label>
              <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
                <span aria-hidden="true" class="text-white">&times;</span>
              </button>
            </div>
            </div>
      <div class="modal-body">
          <form id="form-update-section" class="text-uppercase">
                @csrf
                <input type="hidden" id="u-id">
                <input type="hidden" id="u-course">
                <div class="col-md-12 pr-3 pl-3">
                <div class="form-group">
              
                <label class="form-label text-white">Course :</label>
                <input type="text" id="u-course-name" class="form-control form-control-sm" disabled="">
                </div>
                <div class="form-group" >
                <label class="form-label text-white">Year Level :</label>
                <input type="text"  id="u-year-level" class="form-control form-control-sm" disabled="">
            </div>
             
            <div class="form-group">
                <label class="form-label text-white">Section Name :</label>
                <input type="text"  id="u-section_name" class="form-control form-control-sm text-uppercase" >
            </div>
          
            <div class="form-group">
                <label class="form-label text-white">Status :</label>
                  <select class="form-control form-control-sm" id="u-is_active">
                    <option value="1">Available</option>
                    <option value="0"> Not Available</option>
                  </select>
            </div>
              </div>
          </form>
          
      </div>
      <div class="modal-footer pt-1 pb-1" style="border-top:none;background-color:#75787d">
        <div class="col-md-12 pl-4 pr-3">
            <button class="btn btn-success btn-sm btn-block" onclick="updatesections()"><i class="fa fa-"></i> UPDATE</button>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-remove-section">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="sec-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to remove<br> <label  id="sec-name" style="text-decoration: underline;"></label>?<br>
            This item will be moved to archive.<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="sectionRemove()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-remove-section">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="sec-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-exclamation-triangle fa-lg text-warning " ></i> Do you want to remove<br> <label  id="sec-name" style="text-decoration: underline;"></label>?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-danger btn-xs " onclick="sectionRemove()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>

<div class="modal fade" id="modal-section-archive">
  <div class="modal-dialog modal-xl">
    <div class="modal-content" >
      <div class="modal-header bg-secondary" >
        <div class="col-md-12 text-center">
          <label class="modal-title " style="font-size: 19px">LIST OF REMOVED SECTION</label>
          <i  class="close fas fa-times" data-dismiss="modal" aria-label="Close" style="font-size: 17px;color: red;cursor: pointer;">
          </i>
        </div>
      </div>
      <div class="modal-body">
        <table id="tbl-section-archive" class="table table-border table-striped table-hover  text-sm">
        <thead class="bg-dark">
        <tr class="text-uppercase">
          <th width="15px">#</th>
          <th>Department </th>
          <th>Section Name</th>
          <th>Year Level</th>
          <th>Status</th>
          <th class="text-center ">Action</th>
        </tr>
        </thead>
        @php
          $i=1;
        @endphp
        <tbody>
           @foreach($archive as $key => $s)
            <tr id="trt{{$s->id}}">
              <td>{{$i++}}</td>
              <td class="pt-2 pb-2 text-uppercase" >{{$s->course}}</td>
              <td class="pt-2 pb-2 text-uppercase">{{$s->section_name}}</td>
              <td class="pt-2 pb-2 text-uppercase">{{$s->year_level}}</td>
              <td class="pt-2 pb-2 ">@if($s->is_active == 0) <span class="badge badge-secondary"> Not Available </span> @else <span class="badge badge-success ">  Available </span>  @endif</td>
              <td class="text-center pt-2 pb-2">
                <button class="btn btn-outline-info btn-xs" onclick="modalConfirm({{$s->id}})"><i class="fa fa-trash-restore"> </i> Restore</a>
              </td>
            </tr>
          @endforeach
        </tbody>
        <tfoot>
       
        </tfoot>
      </table>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modal-restore-confirm">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" >
    <div class="modal-header pt-1 pb-1"  style="border-bottom: none">
        <button  class="close text-white" data-dismiss="modal" aria-label="Close" style="font-size: 19px">
        <span aria-hidden="true" class="text-dark"></span>
      </button>
     
    </div>
    <div class="modal-body text-center">
      <input type="hidden" id="s-id">
     <div class="col-md-12 ">
     
        <p class="text-muted" style="font-size: 16px;font-weight: lighter"><i class="fa fa-trash-restor fa-lg text-warning " ></i> Do you want to restore<br>  Section <label  id="section-name" style="text-decoration: underline;"></label>?<br>
      </p>
     </div>
    
     <div class="col-md-12 text-right">
         <button type="button" class="btn btn-info btn-xs " onclick="Restore()" style="border-color: gray"> Yes, proceed!</button>
             <button type="button" class="btn btn-secondary btn-xs " data-dismiss="modal"><i class="fas"></i> No, cancel!</button>
     </div>
    
  
    
    </div>
  
    </div>
  </div>
</div>
<script type="text/javascript">
  $('#year_level').attr('disabled', 'disabled');
</script>

<script >

  function Archive(){

    $('#modal-section-archive').modal('toggle');
  }

  function modalConfirm(id){
    $('#s-id').val(id);
    $.get('quam-get-section-name/'+id,function(data){
      $('#section-name').text(''+data.section_name);
    })
    $('#modal-restore-confirm').modal('toggle');
  }


  function Restore(){


    var status;
    var department="";
    var id =$('#s-id').val();
    $.get('quam-restore-section/'+id,function(data){
      $.each(data.academic,function(key,ac){

        if(data.sec.academic_id == ac.id){
          department=ac.program_code;
        }
      })
       if (data.sec.is_active==0) {
            status='<span class="badge badge-secondary ">Not Available </span>';
          }else{
             status='<span class="badge badge-success "> Available </span>';
          }

    var tt=  $("#tbl-section-archive").DataTable();
        tt.row('#trt'+data.sec.id).remove().draw();

    var t=$("#section-table").DataTable();
        t.row.add([
        '',
         data.sec.course,
         data.sec.section_name,
         data.sec.year_level,
         status,
         '<div class="row btn-group text-center" >'+
         '<button onclick="editsection('+data.sec.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-edit"> </i> Edit</button>'+
         '<button onclick="modalRemove('+data.sec.id+')" class="btn btn-outline-danger btn-xs  " > <i class="fa fa-trash"></i> Remove</button>'+
                  '</div>']).node().id = 'tr'+data.sec.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#tr'+data.sec.id).addClass('text-sm p-1 m-1');
        //$('#modal-department-archive').modal('hide');
        //$('#modal-restore-department').modal('hide');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Restored!!</span>',
                  });

         $('#modal-restore-confirm').modal('hide');
    })
  }

  function modalRemove(id){
   
    $('#sec-id').val(id);
    $.get('quam-sec-remove-data/'+id,function(data){

      $('#sec-name').text(data.section_name);
    })
    $('#modal-remove-section').modal('toggle');
  }

  function sectionRemove(){
   

   
    var id =$('#sec-id').val();
    var department="";
    $.get('quam-remove-section/'+id,function(data){
         
         $.each(data.academic,function(key,acad){
           if(data.sec.academic_id==acad.id){

               department=acad.program_code;
           }
         })
      
        if (data.sec.is_active==0) {
            status='<span class="badge badge-secondary ">Not Available </span>';
          }else{
             status='<span class="badge badge-success "> Available </span>';
          }

    var tt=  $("#section-table").DataTable();
        tt.row('#tr'+data.sec.id).remove().draw();

    var t=$("#tbl-section-archive").DataTable();
        t.row.add([
        '',
         data.sec.course,
         data.sec.section_name,
         data.sec.year_level,
         status,
         '<div class=" text-center" >'+
         '<button onclick="modalConfirm('+data.sec.id+')" class="btn btn-outline-info btn-xs  " > <i class="fa fa-trash-restore"></i> Restore</button>'+
                  '</div>']).node().id = 'trt'+data.sec.id;
                t.draw();
                t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
              cell.innerHTML = i+1;
            } );
            } ).draw();
             $('#trt'+data.sec.id).addClass('text-sm p-1 m-1');
      
       Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-1"> Successfully Restored!!</span>',
                  });

         $('#modal-remove-section').modal('hide');

    })
  
   
  }


  function AddSection(){
    $('#modal-add-section').modal('toggle');
  }
</script>

<script>
   const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer:3000,
                    
  });


  function addsections(){
    var section_name= $('#section_name').val();
    var year_level= $('#year_level').val();
    var course= $('#course').val();
    var is_active= 0;
     
    $.ajax({

      type: 'POST',
      url : 'quam-add-section',
      data:{
           "_token"   : "{{ csrf_token() }}",
           "section_name" :section_name,
           "year_level" :year_level,
           "course" :course,
           "is_active" :is_active,
      },
      success:function(response){

       
        Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Added!!</span>',
                  });                          

        var is_active;
        if (response.section.is_active==0) {
            is_active='<span class="badge badge-secondary">Not Available </span>';
        }else{
          is_active='<span class="badge badge-success "> Available </span>';
        }

          $('#form-add-section')[0].reset();
          var t=$('#section-table').DataTable();
                t.row.add([
                  '',
                  response.section.course,
                  response.section.section_name,
                  response.section.year_level,
                  is_active,
                
                  '<div class="col-sm-12 text-center"><div class="row btn-group">'+
                  '<button class="btn btn-outline-info btn-xs" onclick="editsection('+response.section.id+')"><i class="fa fa-edit "> </i> Edit</button>'+
                '<button class="btn btn-outline-danger btn-xs"><i class="fa fa-trash "> </i> Remove</button>'+'</div></div>',


                  ]).node().id = 'tr'+response.section.id;
                t.draw();
                      t.on( 'order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

        $('#modal-add-section').modal('toggle');
        $('#form-add-section')[0].reset();

      },
     error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+ value,
                  });
                  
                }); 
             }
    })
  }
</script>
<script>
  function editsection(id){

    $.get('quam-edit-section/'+id,function(data){  

      $('#u-section_name').val(data.section.section_name);
      $('#u-course-name').val(data.section.course);
      $('#u-year-level').val(data.section.year_level);
      $('#u-is_active').val(data.section.is_active);
      $('#u-course').val(data.section.course);

       
      $('#u-id').val(id);
      $.each(data.academic,function(key,acad){
        if(data.section.academic_id == acad.id){
          $('#academic-title').val(acad.program_code);
        }
      })
      $('#modal-update-section').modal('toggle');

})
  }

  function updatesections(){


     var section_name =$('#u-section_name').val();
     var year_level = $('#u-year-level').val();
     var is_active =$('#u-is_active').val();
      var course =$('#u-course').val();
      var id = $('#u-id').val();

      $.ajax({

        type:'PUT',
        url: 'quam-update-section/'+id,
        data:{
               "_token"   : "{{ csrf_token() }}",
               "section_name" :section_name,
               "year_level" :year_level,
               "is_active" :is_active,
               "course" :course,

        },
        success:function(response){
         
         Toast.fire({
                    icon: 'success' ,
                    title: ' <span class="ml-2"> Successfully Updated!!</span>',
                  });

          var is_active;
          if (response.section.is_active==0) {
            is_active='<span class="badge badge-secondary ">Not Available </span>';
        }else{
          is_active='<span class="badge badge-success"> Available </span>';
        }
           $('#tr' + response.section.id + ' td:nth-child(2)' ).text(response.section.course);
           $('#tr' + response.section.id + ' td:nth-child(3)' ).text(response.section.section_name);
           $('#tr' + response.section.id + ' td:nth-child(4)' ).text(response.section.year_level);
           $('#tr' + response.section.id + ' td:nth-child(5)' ).text('');
           $('#tr' + response.section.id + ' td:nth-child(5)' ).append(is_active);
           $('#modal-update-section').modal('toggle');
           $('#form-update-section')[0].reset();
        },
          error: function (data)
       {
                $.each(data.responseJSON.errors, function(key,value) {
                    Toast.fire({
                    icon: 'error' ,
                    title: '<span class="ml-2"></span>'+ value,
                  });
                  
                }); 
             }

      })
  }
</script>
<script>
  $('#course').change(function(){
    $('#year_level').prop('disabled',false);
    var year = $('#year_level').val();
    var course = $('#course').val()

    var str="";
    if (year !=null)
     {
      str = year.slice(0,1);
     }
    
    $('#section_name').val(course+str)
  })
	function academicSelect(val){
		var str =val.value.slice(0,1);

			
			$('#section_name').val($('#course option:selected').text()+str+'-');
				
	}
	function academicSelect_update(val){
		
			
			$('#u-section_name').val($('#u-course option:selected').text()+'-');
				
	}


  $('#tbl-section-archive').DataTable();

    setInterval(function(){
      $("#arch-sec").load(window.location.href + " #arch-sec")
  },6000)
    
</script>
@endsection