<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('logininstructor', function () {
    return view('auth.logininstructor');
});

Route::get('logindean', function () {
    return view('auth.logindean');
});






 /*--- Quam  ----*/

Auth::routes();




Route::group(['middleware' => ['auth','QUAM']], function()
 {
    Route::get('/quam_dashboard', function () {
        return view('Quam.quam_dashboard');
    });
    Route::resource('quam-dashboard','QuamDashboardController');
    Route::get('update-notif-quam/{id}','QuamDashboardController@updateNotif');
    Route::get('quam-proceed/{id}','QuamDashboardController@proceed');
    Route::get('get-notif-quam','QuamDashboardController@getNotifi');
   
   // Route::resource('quam-manage-subject','QuamSubjectController');
    Route::resource('quam-academic','QuamAcademicController');
    Route::post('quam-save-academic','QuamAcademicController@store');
  //  Route::post('quam-filter-academic','QuamAcademicController@getCurriculum');

    //Academic
    Route::get('quam-get-academic/{id}','QuamAcademicController@show');
    Route::get('quam-edit-academic/{id}','QuamAcademicController@edit');
   Route::put('quam-update-academic/{id}','QuamAcademicController@update');
   Route::get('quam-academic-remove-data/{id}','QuamAcademicController@getInfo');
   Route::get('quam-remove-academic/{id}','QuamAcademicController@destroy');

    //YEAR LEVEL 
    Route::post('quam-save-year-level','QuamYearLevelController@store');
    Route::get('quam-get-temp-data/{id}','QuamYearLevelController@show');
    Route::get('quam-get-year/{id}','QuamYearLevelController@index');
    //CREATE CLASS
    
    Route::post('quam-save-class','QuamClassController@store');
    Route::get('quam-edit-class/{id}','QuamClassController@edit');
    Route::get('quam-update-class/{class_id}/{section}/{sy}','QuamClassController@update');
    Route::get('quam-class-get-data/{id}','QuamClassController@show');
    //SUBJECT
    Route::resource('quam-subjects','QuamSubjectController');
    Route::post('quam-save-subject','QuamSubjectController@store');
    Route::get('quam-edit-subject/{id}','QuamSubjectController@edit');
    Route::put('quam-update-subject/{id}','QuamSubjectController@update');
    Route::get('quam-get-all-subject','QuamSubjectController@getAll');
    Route::get('quam-remove-subject/{id}','QuamSubjectController@destroy');
     Route::get('quam-restore-subject/{id}','QuamSubjectController@restore');
     Route::get('quam-get-subject/{id}','QuamSubjectController@show');
     Route::get('quam-get-subject-schedule/{id}/{class}','QuamSubjectController@showSubject');
    Route::get('quam-get-subject-name/{id}','QuamSubjectController@getsubName');
     //lance
     Route::get('quam-get-subject-data/{id}','QuamSubjectController@getSub');
     Route::get('quam-all-subject-remove','QuamSubjectController@allremove');
     Route::get('quam-sub-remove-data/{id}','QuamSubjectController@subRemove');

    //CURRICULUM
    Route::resource('quam-curriculum','QuamCurriculumController');
    Route::post('quam-add-curriculum','QuamCurriculumController@store');
    Route::put('quam-save-curriculum/{id}','QuamCurriculumController@saveCurr');
     Route::put('quam-edit2-curriculum/{id}','QuamCurriculumController@editCurr');
    Route::get('get-count/{id}','QuamCurriculumController@getCount');
    Route::get('quam-edit-curriculum/{id}','QuamCurriculumController@edit');
    Route::put('quam-update-curriculum/{id}','QuamCurriculumController@update');
    Route::get('quam-get-all-curriculum','QuamCurriculumController@getALl');
    Route::get('quam-get-curriculum/{id}','QuamCurriculumController@show');
     Route::get('quam-remove-curr-sub/{id}','QuamCurriculumController@removeCurrSub');
     Route::get('quam-get-curyear/{year}','QuamCurriculumController@getCurYear');
     Route::get('quam-manage-curriculum/{id}','QuamCurriculumController@manageCurriculum');

     //YEAR LEVEL & SEMESTER
     Route::get('quam-get-year-level-data/{id}','QuamYearLevelController@edit');
     Route::put('quam-update-year-level/{id}','QuamYearLevelController@update');
     Route::get('quam-get-year-level-info/{id}','QuamYearLevelController@semInfo');
     Route::get('quam-remove-year-level/{id}','QuamYearLevelController@removeSem');
    //CURRICULUM SUBJECT
    Route::resource('quam-currsubject','QuamCurrSubject');
    Route::post('quam-add-currsubject','QuamCurrSubject@store');
    Route::get('quam-get-currsubject/{id}','QuamCurrSubject@show');

    //ROOMS
     Route::resource('quam-rooms','QuamAddRoomsController');
     Route::post('quam-add-rooms','QuamAddRoomsController@store');
     Route::get('quam-edit-rooms/{id}','QuamAddRoomsController@edit');
     Route::put('quam-update-rooms/{id}','QuamAddRoomsController@update');
     Route::get('quam-get-rooms/{id}','QuamAddRoomsController@show');

     Route::get('quam-room-remove-data/{id}','QuamAddRoomsController@roomData');
     Route::get('quam-remove-room/{id}','QuamAddRoomsController@destroy');
     Route::get('quam-get-room-name/{id}','QuamAddRoomsController@getroomName');
     Route::get('quam-restore-room/{id}','QuamAddRoomsController@restoreRoom');
      Route::get('get-room-schedule/{id}/{sem}/{sy}','QuamAddRoomsController@getSchedule');
      Route::get('quam-get-room--details/{id}','QuamAddRoomsController@getDetails');
    //SECTION

     Route::resource('quam-section','QuamSectionController');
     Route::post('quam-add-section','QuamSectionController@store');
     Route::get('quam-edit-section/{id}','QuamSectionController@edit');
     Route::put('quam-update-section/{id}','QuamSectionController@update');
     //lance
     Route::get('quam-sec-remove-data/{id}','QuamSectionController@secRemove');
     Route::get('quam-remove-section/{id}','QuamSectionController@destroy');
     Route::get('quam-get-section-name/{id}','QuamSectionController@getsecName');
     Route::get('quam-restore-section/{id}','QuamSectionController@restoreSection');

     // Scheduling
      Route::resource('quam-scheduling','QuamSchedulingController');
      Route::get('quam-get-scheduling-data/{id}','QuamSchedulingController@getData');
      Route::post('quam-add-scheduling','QuamSchedulingController@store');
      Route::get('quam-get-schedule-list/{id}','QuamSchedulingController@show');
      Route::get('quam-get-schedule/{id}','QuamSchedulingController@edit');
      Route::put('quam-update-schedule/{id}','QuamSchedulingController@update');
      Route::get('quam-send-schedule/{id}','QuamSchedulingController@send');
      Route::get('quam-get-sched-data/{id}','QuamSchedulingController@getInfo');
      Route::get('get-inc-schedule/{id}/{sem}/{year}','QuamSchedulingController@scheduleInc');
      Route::get('quam-remove-schedule/{id}','QuamSchedulingController@removeSched');
      Route::get('quam-get-course-data/{id}','QuamSchedulingController@getCourse');
      Route::get('get-time/{class}/{subject}/{t_hr}','QuamSchedulingController@getTime');
      Route::get('update-time/{id}/{subject}/{t_hr}','QuamSchedulingController@updateTime');
      //Manage Account
      Route::resource('quam-manageaccount','ManageAccountController');
      Route::post('quam-add-user','ManageAccountController@store');
      Route::get('quam-edit-user/{id}','ManageAccountController@edit');
      Route::put('quam-update-user/{id}','ManageAccountController@update');
      Route::get('quam-block-user-account/{id}','ManageAccountController@Block');
       Route::get('quam-block-user-account2/{id}','ManageAccountController@Activate');
      //lance
      Route::get('quam-user-account-data/{id}','ManageAccountController@userData');
      Route::put('quam-user-update-account/{id}','ManageAccountController@updateInfo');
      //Reports
      
      //Class Report
      Route::resource('quam-subject-schedule-report','QuamScheduleSubjectReportController');
      Route::get('quam-get-section/{sy}/{course}/{sem}/{yl}','QuamScheduleSubjectReportController@getSection');
      Route::get('class-report/{id}','QuamScheduleSubjectReportController@getClass');
       Route::get('print/{id}','QuamScheduleSubjectReportController@show');
        Route::get('print2/{id}','QuamScheduleSubjectReportController@show2');
      //Curriculum Report
      Route::resource('quam-curriculum-report','QuamCurriculumReportController');
       Route::get('quam-curr-report/{year}/{course}','QuamCurriculumReportController@getReport');

      Route::get('quam-report-all-curriculum','QuamCurriculumReportController@getallCurr');

      Route::get('quam-get-department-name/{id}','QuamAcademicController@getDepartment');
      Route::get('quam-restore-department/{id}','QuamAcademicController@dataRestore');

       Route::get('quam-read-notification/{id}','QuamDashboardController@read');
      Route::get('quam-readall-notification/{id}','QuamDashboardController@readall');

      //Faculty Load
     Route::resource('quam-faculty-load','QuamFacultyLoadController');
     Route::get('quam-get-ins-schedule/{id}/{year}/{sem}','QuamFacultyLoadController@getFacultyLoad');
   //  Route::get('quam-faculty-load-view','QuamFacultyLoadController@view');
      
});

    //--Human Resources--//

    Route::group(['middleware' => ['auth','HR']], function() {
    Route::resource('hr-dashboard','HrDashboardController');
    Route::get('get-notif-hr','HrDashboardController@getNotifi');
    Route::get('update-notif-hr/{id}','HrDashboardController@updateNotif');
    //Instructor
    Route::resource('hr-add-instructor','InstructorController');
    Route::post('hr-save-instructor','InstructorController@store2');
    Route::put('hr-update-instructor/{id}','InstructorController@update');
    Route::put('hr-update-instructor-info/{id}','InstructorController@updateinfo');
    Route::get('hr-edit-instructor/{id}','InstructorController@getinfo');
    Route::get('hr-instructor-list','InstructorController@list');
   
    Route::get('get-subject/{id}','QuamSubjectController@show');
    Route::resource('hr-change-pass','HrCHangePassController');
    Route::resource('hr-approved','HrApprovedController');
    Route::get('get-fac-load/{id}/{year}/{sem}','HrApprovedController@getFacLoad');
    Route::get('hr-assign-teacher/{id}','HrApprovedController@show');
    Route::get('hr-get-approval-load/{id}','HrApprovedController@show2');
    Route::post('hr-save-loads','HrApprovedController@store');
     Route::get('hr-get-faculty-details/{id}','HrApprovedController@getFaculty');
    Route::get('hr-get-loads/{id}','HrFacultyLoadController@show');
    Route::get('hr-get-semester/{year}','HrFacultyLoadController@semester');
    Route::get('hr-get-all-loads/{sem}/{id}','HrFacultyLoadController@getLoads');
    Route::get('hr-get-assigned','HrFacultyLoadController@getAssigned');
    Route::get('hr-get-pending','HrFacultyLoadController@getPending');
   //lance//
    Route::resource('approval-request','HrApprovalRequestController');
    Route::get('hr-approval','HrApprovalRequestController@approval');
    Route::get('hr-assign-instructor/{id}','HrApprovalRequestController@show');
    Route::get('hr-get-approval-subject/{id}','HrApprovalRequestController@show2');
    Route::get('approval-request-view/{id}','HrApprovalRequestController@view');
    Route::post('hr-save-approved-subject-loads','HrApprovalRequestController@store');
   //update account
    Route::put('hr-update-account/{id}','HrChangePassController@update');
    //Reports
    Route::resource('hr-faculty-load-report','HrFacultyLoadReportController');
    //Route::get('hr-get-faculty-info/{id}','HrFacultyLoadReportController@getFaculty');
     Route::resource('hr-faculty-department-report','HrFacultyDepartmentReportController');
    Route::resource('hr-approval-report','HrApprovalReportController');
    Route::get('hr-get-subject-info/{id}','HrApprovalRequestController@getSubject');
    Route::resource('hr-schedule-report','HrScheduleReportController');

      Route::get('hr-get-instructor-details/{id}','InstructorController@getDetails');


   
   

});


Route::group(['middleware' => ['auth','Dean']], function() {

   
    Route::resource('dashboard','DeanDashboardController');
    Route::post('save-reched','DeanDashboardController@resched');
     Route::get('dean-ass-ins/{id}/{subject}/{class}','DeanDashboardController@getAss');
     Route::get('dean-ins-name/{id}','DeanDashboardController@getInstructor');
     Route::get('get-notif-dean','DeanDashboardController@getNotifi');
     Route::get('update-notif/{id}','DeanDashboardController@updateNotif');
     Route::get('notif-hour/{date}','DeanDashboardController@ago');
    Route::resource('dean-schedule','DeanScheduleController');
    Route::resource('assign-instructor','DeanAssignInstructorController');
    Route::get('assign-instructor-view/{id}','DeanAssignInstructorController@view');
    Route::post('dean-save-assign','DeanAssignInstructorController@store');
    Route::get('dean-getall-schedule/{id}','DeanAssignInstructorController@show');
    Route::get('dean-get-schedule/{id}','DeanAssignInstructorController@show2');
    Route::resource('dean-change-pass','DeanChangePassController');
    Route::get('get-assigned-teacher/{id}','DeanAssignInstructorController@getAssigned');
    Route::get('dean-get-sy/{year}','DeanAssignInstructorController@getSy');
    //lance//
    Route::resource('dean-other-department','DeanOtherDepartmentController');
    Route::resource('dean-schedule-report','DeanScheduleReportController');
    Route::get('get-class-schedule/{year}/{course}/{sem}/{level}','DeanScheduleReportController@getSchedule');
     Route::get('get-class-schedule/{year}/{course}/{sem}/{level}/{sec}','DeanScheduleReportController@getSchedule2');
    //Get Schedule
    Route::get('dean-get-sched-calendar/{id}','DeanAssignInstructorController@getSched');
    Route::get('dean-get-class-info/{id}','DeanAssignInstructorController@getInfo');

    Route::get('dean-send-schedule/{id}','DeanAssignInstructorController@sendSchedule');

    //Faculty
    Route::resource('dean-faculty','DeanFacultyController');
    Route::get('dean-get-ins-schedule/{id}/{sy}/{sem}','DeanFacultyController@getSchedule');
    Route::get('get-my-schedule/{sem}/{year}','DeanFacultyController@mySchedule');
    //Dash
    Route::post('dean-post-sched-dash','DeanFacultyController@postDash');
    Route::get('remove-dash-sched','DeanFacultyController@removeDash');
    Route::get('get-dash-sched','DeanDashboardController@getSchedule');
    Route::resource('dean-curriculum','DeanCurriculum');
    Route::get('search-curr/{year}/{course}','DeanCurriculum@getCurr');

    Route::resource('dean-my-loads','DeanMyLoadReportController');
     Route::get('get-my-loads/{year}/{sem}','DeanMyLoadReportController@getLoads');

     //Update Asside
     Route::put('dean-update-account/{id}','DeanChangePassController@updateAccount');
     
    Route::get('print-schedule/{class_id}','DeanScheduleReportController@show');

  });


Route::group(['middleware' => ['auth','Instructor']], function() {

    Route::resource('instructor-dashboard','InstructorDashboardController');
    Route::get('get-notif-ins','InstructorDashboardController@getNotifi');
    Route::get('update-notif-instructor/{id}','InstructorDashboardController@updateNotif');
    Route::resource('instructor-myschedule','InstructorMyScheduleController');
    Route::get('instructor-assigned-schedule/{sem}/{year}','InstructorMyScheduleController@mySched');
    Route::post('instructor-post-sched-dash','InstructorMyScheduleController@postDash');
    Route::get('instructor-remove-dash-sched','InstructorMyScheduleController@removeDash');
    Route::get('instructor-dash-sched','InstructorDashboardController@dashSched');
    Route::get('my-subject-assigned','InstructorMyScheduleController@showdata');
   Route::put('instructor-update-account/{id}','InstructorChangePasswordController@update');

   Route::get('print-myschedule/{year}/{sem}','InstructorMyScheduleController@show');

});